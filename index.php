<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Интернет-магазин мебели: каталог с ценами в Санкт-Петербурге | Le Tabouret");
$APPLICATION->SetPageProperty("keywords", "элитная мебель, letabouret, ле табурет, салон элитной мебели, дорогая мебель");
$APPLICATION->SetPageProperty("description", "Интернет-магазин Le Tabouret предлагает мебель и предметы интерьера от ведущих производителей. Разнообразие стилей, качественные материалы и последние коллекции по выгодным ценам. Быстрая доставка по всей России, самовывоз из салона в Санкт-Петербурге");
$APPLICATION->SetTitle("Интернет-магазин мебели и интерьерный салон Le Tabouret");
?>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "name": "Интернет-магазин мебели Le Tabouret",
            "url": " https://letabouret.ru/",
            "sameAs": ["https://vk.com/letabouret"],
            "potentialAction": {
                "@type": "SearchAction",
                "target": {
                    "@type": "EntryPoint",
                    "urlTemplate": "https://letabouret.ru/search/?q={search_term_string}"
                },
                "query-input": "required name=search_term_string"
            }
        }
    </script>
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Organization",
            "url": "https://letabouret.ru/ ",
            "logo": "https://letabouret.ru/local/templates/letabouret/images/logo.webp"
        }
    </script>

<?


    $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "main_banner_new",
    array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => 21,
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "HIDEBUTTON",
            1 => "TEXT_COLOR",
            2 => "BG_COLOR",
            3 => "POSITION",
            4 => "BUTTON_TEXT"
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => "",
        "PAGER_BASE_LINK" => "",
        "PAGER_PARAMS_NAME" => "arrPager",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPONENT_TEMPLATE" => "main_banner",
        "STRICT_SECTION_CHECK" => "N",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO"
    ),
    false
); ?>
<? $APPLICATION->IncludeComponent("realweb:catalog.section.list", "popular",
    array(
        "VIEW_MODE" => "TEXT",
        "SHOW_PARENT_NAME" => "Y",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => IBLOCK_CATALOG_CATALOG,
        "SECTION_ID" => "",
        "SECTION_CODE" => "",
        "SECTION_URL" => "",
        "COUNT_ELEMENTS" => "Y",
        "TOP_DEPTH" => "5",
        "SECTION_FIELDS" => "",
        "SECTION_USER_FIELDS" => array(
            'UF_TOP9',
            'UF_COLLECTION_TEXT'
        ),
        "ADD_SECTIONS_CHAIN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
        "CACHE_GROUPS" => "N",
        "SORT_BY1" => "UF_TOP9SORT",
        "FILTER_NAME" => 'arrFilterSectionPopular'
    )
); ?>
<? $APPLICATION->IncludeComponent("realweb:blank", "catalog_main", array(
    "AJAX_MODE" => "Y",
    "AJAX_OPTION_JUMP" => "N",
    "FOLDER" => "/"
), false, array('HIDE_ICONS' => 'Y')
); ?>
<? $APPLICATION->IncludeComponent("realweb:blank", "catalog_special", array(), false, array('HIDE_ICONS' => 'Y')
); ?>
<? $APPLICATION->IncludeComponent("bitrix:news.list", "contacts", array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => IBLOCK_NEWS_CONTACTS,
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(""),
        "PROPERTY_CODE" => array("COORDINATES", ""),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => "",
        "PAGER_BASE_LINK" => "",
        "PAGER_PARAMS_NAME" => "arrPager",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
    )
); ?>
<? $APPLICATION->IncludeComponent("bitrix:news.line", "slider", array(
        "IBLOCK_TYPE" => "news",
        "IBLOCKS" => array(IBLOCK_NEWS_SALES, IBLOCK_NEWS_NEWS),
        "NEWS_COUNT" => "20",
        "FIELD_CODE" => array("ID", "CODE", "NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT", "PREVIEW_TEXT_TYPE", "DETAIL_TEXT", "DETAIL_PICTURE"),
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "DETAIL_URL" => "",
        "ACTIVE_DATE_FORMAT" => "d F Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_GROUPS" => "N"
    )
); ?>

    <div class="section-bg section_pad_b">
        <div class="container container_md">
            <div id="text_info" class="text_info_footer">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "info.php"
                    )
                ); ?>
            </div>
            <br>
            <!--<p id="red_next_text_info" class="red_next_text_info">Читать полностью</p>-->
        </div>
    </div>
    <script>
        if ($('#text_info').text().length > 850) {
            $('.red_next_text_info').css({visibility: "visible"});
        }
        $('#red_next_text_info').on('click', function () {
            $('#red_next_text_info').css({visibility: 'hidden'});
            $('#text_info').css({"max-height": "none", height: "auto"});
        });
    </script>
    <script>
        $("#open-button").toggle(function () {
            $("#manufacturer-list").css("height", "auto");
        }, function () {
            $("#manufacturer-list").css("height", "126px");
        });
    </script>
    <style>
        .red_next_text_info {
            cursor: pointer;
        }

        .text_info_footer {
            /*max-height: 150px;*/
            overflow: hidden;
            line-height: 1.5;
        }

        .text_info_footer a {
            color: #9c4115;
        }
    </style>

<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>