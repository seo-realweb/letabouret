/*
 * Translated default messages for bootstrap-select.
 * Locale: KO (Korean)
 * Region: KR (South Korea)
 */
(function ($) {
  $.fn.selectpicker.defaults = {
    noneSelectedText: 'н•­лЄ©мќ„ м„ нѓќн•ґмЈјм„ёмљ”',
    noneResultsText: '{0} кІЂмѓ‰ кІ°кіјк°Ђ м—†мЉµл‹€л‹¤',
    countSelectedText: function (numSelected, numTotal) {
      return "{0}к°њлҐј м„ нѓќн•�м�ЂмЉµл‹€л‹¤";
    },
    maxOptionsText: function (numAll, numGroup) {
      return [
        '{n}к°њк№Њм§Ђ м„ нѓќ к°ЂлЉҐн•©л‹€л‹¤',
        'н•ґл‹№ к·ёлЈ№мќЂ {n}к°њк№Њм§Ђ м„ нѓќ к°ЂлЉҐн•©л‹€л‹¤'
      ];
    },
    selectAllText: 'м „мІґм„ нѓќ',
    deselectAllText: 'м „мІґн•ґм њ',
    multipleSeparator: ', '
  };
})(jQuery);
