<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


if(!class_exists('ImpelTemplateTools')){
    class ImpelTemplateTools
    {
        public static function createAMPSRCSetHTML($strFile){

            $returnSetAttrHTML = '';

            $returnSet = array();

            if(stripos($strFile,$_SERVER['DOCUMENT_ROOT']) === false){
                $checkFile = $_SERVER['DOCUMENT_ROOT'].$strFile;
            } else {
                $checkFile = $strFile;
            }

            $correct = static::checkCorrectImage($checkFile);

            if($correct){

                $deviceSizes = array(
                    '320w' => 320,
                    '480w' => 480,
                    '640w' => 640,
                    '960w' => 960,
                    '1036w' => 960,
                    '1334w' => 960,
                    '1920w' => 960);

                foreach($deviceSizes as $currentWidth => $currentSize){

                    $returnSet[$currentWidth] = static::resizeImage($checkFile, $currentSize);

                    if(empty($returnSet[$currentWidth])){
                        unset($returnSet[$currentWidth]);
                    }

                }

            }

            if(!empty($returnSet)){
                foreach ($returnSet as $dimension => $file){
                    $returnSetAttrHTML .= (!empty($returnSetAttrHTML) ? ',' : '') .  $file.' '.$dimension;
                }

                if(!empty($returnSetAttrHTML)){
                    $returnSetAttrHTML = ' srcset="'.$returnSetAttrHTML.'"';
                }

            }

            return $returnSetAttrHTML;
        }

        protected static function checkCorrectImage($image){

            $isCorrect = false;

            if(file_exists($image)
                && filesize($image) > 0
                && is_readable($image)){

                $oSizes = getimagesize($image);

                if(isset($oSizes[0])
                    && !empty($oSizes[0])
                    && isset($oSizes[2])){

                    $rExt = str_ireplace('.','',image_type_to_extension($oSizes[2]));
                    $iExt = strtolower(pathinfo($image,PATHINFO_EXTENSION));
                    $iExt = $iExt == 'jpg' ? 'jpeg' : $iExt;
                    $iExt = trim($iExt);
                    $rExt = trim($rExt);

                    if($rExt == $iExt){
                        $isCorrect = true;
                    }

                }

            }

            return $isCorrect;
        }

        protected static function resizeImage($image, $width = 0, $height = 0){

            $correct = static::checkCorrectImage($image);

            $resizeURI = $resizePath = '';

            if($correct){

                $resizeURI = '';
                $resizePath = static::getImageCachePath($image,$width,$height);

                $sizes = array();

                if(!empty($width)){
                    $sizes['width'] = $width;
                }

                if(!empty($height)){
                    $sizes['height'] = $height;
                } else {
                    $sizes['height'] = $width;
                }

                if(!(file_exists($resizePath)
                    && filesize($resizePath) > 0)){

                    $success = CFile::ResizeImageFile(
                        $image,
                        $resizePath,
                        $sizes,
                        BX_RESIZE_IMAGE_PROPORTIONAL,
                        false,
                        80,
                        false
                    );

                    if(!$success
                        && (file_exists($resizePath)
                            && filesize($resizePath) > 0)){

                        @unlink($resizePath);

                    }

                }

            }

            if(!(file_exists($resizePath)
                && filesize($resizePath) > 0)){
                $resizePath = $image;
            }

            if(file_exists($resizePath)
                && filesize($resizePath) > 0){
                $resizeURI = str_ireplace($_SERVER['DOCUMENT_ROOT'],'',$resizePath);
            }

            return $resizeURI;

        }

        protected static function getImageCachePath($image, $width = 0, $height = 0){

            $cache = $_SERVER['DOCUMENT_ROOT'].'/upload/';

            if(!is_dir($cache.'thumbs/')){
                mkdir($cache.'thumbs/',0775);
            };

            if(!is_writable($cache.'thumbs/')){
                chmod($cache.'thumbs/',0777);
            };

            if(is_dir($cache.'thumbs/')){
                $cache .= 'thumbs/';
            };

            $info = pathinfo($image);

            $filesize = filesize($image);

            $base_name = isset($info['filename'])
                ? $info['filename']
                : str_replace('.'.$info['extension'],'',$info['basename']);

            $base_name .= '_'.$filesize;
            $extension = strtolower($info['extension']);

            if(!empty($width) && !empty($height)){
                $base_name .= '_'.$width.'_'.$height;
            } elseif(!empty($width)){
                $base_name .= '_w'.$width;
            } elseif(!empty($height)){
                $base_name .= '_h'.$height;
            }

            $base_name .= '.'.$extension;
            $cache .= $base_name;

            return $cache;

        }
    }
}