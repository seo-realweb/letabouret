<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

@ini_set('default_charset',LANG_CHARSET);

require_once dirname(__FILE__)."/templateTools.php";
require_once dirname(__FILE__)."/processing/class-amp-content.php";

$lang_id						= strtolower(LANGUAGE_ID);

if($USER->IsAuthorized()){
    $page_class				   .= ' isauthorized';
}


$modifier = LANG_CHARSET == 'UTF-8' ? 'u' : '';

define('IMPEL_PREG_MODIFIER',$modifier);

$page_class .= ' '.strtolower(LANGUAGE_ID);
$impel_amp_path_to_basket = \COption::GetOptionString('impel.amp', "impel_amp_path_to_basket", "", SITE_ID);
$impel_amp_path_to_basket = empty($impel_amp_path_to_basket) ? '/personal/cart/' : $impel_amp_path_to_basket;

$impel_amp_google_analitics_number = \COption::GetOptionString('impel.amp', "impel_amp_google_analitics_number", "", SITE_ID);;

if(CModule::IncludeModule("catalog")
    && file_exists($_SERVER['DOCUMENT_ROOT'].'/'.trim(SITE_DIR,'/')."/amp/sections/")){
    $searchAction = ((stripos($APPLICATION->GetCurPage(), '/news/')  !== false) ? SITE_DIR."amp/news/" : SITE_DIR."amp/sections/");
} else {
    $searchAction = SITE_DIR.'amp/';
}