<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
global $APPLICATION;
require_once(dirname(__FILE__)."/classes/templateConfig.php");
?><!doctype html>
<html amp>
<head>
    <meta charset=utf-8>
    <title>
        <?$APPLICATION->ShowTitle()?>
    </title>
    <?$APPLICATION->ShowViewContent("CANONICAL_PROPERTY");?>
    <?

    $APPLICATION->ShowMeta("description", false, true);
    $APPLICATION->ShowMeta("keywords", false, true);

    ?>
    <meta name="yandex" content="none">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

    <style amp-custom>
        <?=file_get_contents($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH."/css/main_new.css");?>
        <?$APPLICATION->ShowViewContent("AMP_STYLE");?>
        <?=file_get_contents($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH."/css/after.css");?>
    </style>

    <script async custom-element="amp-analytics" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <script async custom-element="amp-sidebar" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-fit-text" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
    <script async custom-element="amp-form" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-template="amp-mustache" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
    <script async custom-element="amp-accordion" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>
    <script async custom-element="amp-carousel" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
    <script async custom-element="amp-list" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-element="amp-social-share" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
    <script async custom-element="amp-iframe" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
    <script async custom-element="amp-youtube" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
    <script async custom-element="amp-vimeo" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-vimeo-0.1.js"></script>
    <script async custom-element="amp-video" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-video-0.1.js"></script>
    <script async custom-element="amp-audio" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-audio-0.1.js"></script>
    <script async custom-element="amp-lightbox" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
    <script async custom-element="amp-bind" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-selector" data-skip-moving="true" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <script async data-skip-moving="true" src="https://cdn.ampproject.org/v0.js"></script>
</head>
<body id="smart_template" data-spy="scroll" data-target="<?php if($second_menu == ""): ?>#bs-example-navbar-collapse-1<?php else: ?>#bs-example-navbar-collapse-3<?php endif; ?>" data-offset="110" class="<?php if(IN_MAIN): ?> in_main<?php else: ?> other-page<?php endif; ?> <?echo $page_class; ?> <?echo $lang_id; ?> <?php echo SITE_ID; ?>">
<header>
    <div class="logotype">
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.detail",
            "amplogo",
            Array(
                "IBLOCK_ID" => "16",
                "IBLOCK_TYPE" => "parameters",
                "ELEMENT_ID" => "",
                "ELEMENT_CODE" => "logotype",
                "CHECK_DATES" => "Y",
                "FIELD_CODE" => array(0=>"PREVIEW_PICTURE",1=>"",2=>"",),
                "PROPERTY_CODE" => array(0=>"LINK",1=>"",),
                "IBLOCK_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "360000",
                "CACHE_GROUPS" => "Y",
                "META_KEYWORDS" => "",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "USE_PERMISSIONS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "",
                "PAGER_TEMPLATE" => "",
                "PAGER_SHOW_ALL" => "Y",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "N",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "USE_SHARE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "LOGO_LINK" => "/amp/"
            )
        );?>
    </div>
    <?

    $APPLICATION->IncludeComponent(
        "bitrix:search.suggest.input",
        "amp",
        array(
            "NAME" => "q",
            "VALUE" => trim($_REQUEST["q"]),
            "INPUT_SIZE" => 40,
            "FORM_ACTION" => $searchAction
        )
    );?>
</header>
<main>
    <?php if(isset($_SESSION['amp_buy_error'])
        && !empty($_SESSION['amp_buy_error'])): ?>
    <div class="has-error">
        <?php
            echo strip_tags($_SESSION['amp_buy_error']);
            $_SESSION['amp_buy_error'] = null;
            unset($_SESSION['amp_buy_error']);
        ?>
    </div>
    <?php endif; ?>
    <?php if (\Realweb\Site\Site::getCurPage() != '/amp/'): ?>
        <h1><?php $APPLICATION->ShowTitle(false); ?></h1>
    <?php endif; ?>
    