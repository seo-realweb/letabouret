<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

$this->setFrameMode(true);

if (!empty($arResult)):?>
    <amp-sidebar id="sidebar" layout="nodisplay" side="left">
        <amp-img class="cross" src="/" width="20" height="20" alt="close sidebar" on="tap:sidebar.close" role="button"
                 tabindex="0">
            <i class="fa fa-times" aria-hidden="true"></i>
        </amp-img>
        <div class="side-menu js-side-menu" role="navigation">
            <ul>
                <li>
                    <a class="js-side-menu-drop" href="/amp/">
                        Каталог
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul>
                        <? foreach ($arResult['CATALOG']['MENU'] as $arItem): ?>
                            <? if (!$arResult['CATALOG']['SECTIONS'][$arItem['ID']]) continue; ?>
                            <li>
                                <a class="js-side-menu-drop" >
                                    <?= $arItem['VALUE'] ?>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul>
                                    <? foreach ($arResult['CATALOG']['SECTIONS'][$arItem['ID']] as $arSubItem): ?>
                                        <li>
                                            <a href="/amp/sections/<?= $arSubItem['ID']; ?>/">
                                                <?= $arSubItem['NAME'] ?>
                                            </a>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </li>
                <?php foreach ($arResult['ITEMS'] as $arItem): ?>
                    <li>
                        <a href="/amp/sections/<?= $arItem['PARAMS']['SECTION']['ID']; ?>/" class="<?php echo $arItem['CHILD'] ? 'js-side-menu-drop' : ''; ?> <?php echo $arItem["SELECTED"] ? 'selected' : ''; ?>">
                            <?= $arItem["TEXT"] ?>
                            <?php echo $arItem['CHILD'] ? '<i class="fa fa-angle-down"></i>' : ''; ?>
                        </a>
                        <?php if ($arItem['CHILD']): ?>
                            <ul>
                                <?php foreach ($arItem['CHILD'] as $arSubItem): ?>
                                    <li>
                                        <a href="/amp/sections/<?= $arSubItem['PARAMS']['SECTION']['ID']; ?>/"
                                           class="<?php echo $arSubItem['CHILD'] ? 'js-side-menu-drop' : ''; ?> <?php echo $arSubItem["SELECTED"] ? 'selected' : ''; ?>">
                                            <?= $arSubItem["TEXT"] ?>
                                            <?php echo $arSubItem['CHILD'] ? '<i class="fa fa-angle-down"></i>' : ''; ?>
                                        </a>
                                        <?php if ($arSubItem['CHILD']): ?>
                                            <ul>
                                                <?php foreach ($arSubItem['CHILD'] as $arSubSubItem): ?>
                                                    <li>
                                                        <a href="/amp/sections/<?= $arSubSubItem['PARAMS']['SECTION']['ID']; ?>/"
                                                           class="<?php echo $arSubSubItem["SELECTED"] ? 'selected' : ''; ?>">
                                                            <?= $arSubSubItem["TEXT"] ?>
                                                        </a>
                                                    </li>
                                                <? endforeach ?>
                                            </ul>
                                        <?php endif; ?>
                                    </li>
                                <? endforeach ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </amp-sidebar>
    <a id="left-menu-button" on="tap:sidebar.toggle">
        <button class="menu-button cross" on="tap:sidebar.toggle" role="button" tabindex="0">
            <i class="fa fa-bars" aria-hidden="true">
            </i>
        </button>
    </a>

<? endif ?>