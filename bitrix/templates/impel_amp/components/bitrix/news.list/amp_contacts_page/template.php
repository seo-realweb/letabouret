<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$jsObject = [];
?>

<div class="section-bg section_contacts">
    <div class="container container">
        <div class="main-contacts__content">
            <h2 class="h2">шоу-румы в петербурге</h2>
            <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
                <? if ($key != 0): ?>
                    <div class="main-delimiter"></div>
                <? endif; ?>
                <div class="main-contacts__item" itemscope itemtype="http://schema.org/Organization">
                    <h4 itemprop="name"><?= $arItem['NAME'] ?></h4>
                    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <p itemprop="streetAddress"><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></p>
                    </div>
                    <p>
                        <?php foreach ($arItem['PROPERTIES']['PHONE']['VALUE'] as $element) {
                            echo '<a itemprop="telephone" href="tel:' . $element . '">' . $element . '</a>';
                        } ?>

                        <a itemprop="email"
                           href="mailto:<?= $arItem['PROPERTIES']['EMAIL']['VALUE'] ?>"><?= $arItem['PROPERTIES']['EMAIL']['VALUE'] ?></a>
                    </p>
                    <p><?= $arItem['PROPERTIES']['TIME']['VALUE'] ?></p>
                </div>
            <? endforeach; ?>
            <br/>
            <? \Realweb\Site\Site::showIncludeText('BANK_PROPS') ?>
        </div>
    </div>
</div>