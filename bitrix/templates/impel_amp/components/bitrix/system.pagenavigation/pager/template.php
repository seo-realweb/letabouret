<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(false);

if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = '?'.($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");

$pageURL = 'PAGEN_'.$arResult["NavNum"].'=';

if($arResult["NavPageCount"] > 1)
{

    ?>
    <div class="blog-page-navigation">
        <ul class="pagination"><?
            if($arResult["bDescPageNumbering"] === true):
                $bFirst = true;
                if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                    if($arResult["bSavePage"]):
                        ?><li>
                        <a class="blog-page-previous" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?><?=($arResult["NavPageNomer"]+1)?>">
                            <?=GetMessage("nav_prev")?>
                        </a>
                        </li><?
                    else:
                        if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):
                            ?><li>
                            <a class="blog-page-previous" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?>">
                                <?=GetMessage("nav_prev")?>
                            </a>
                            </li><?
                        else:
                            ?><li>
                            <a class="blog-page-previous" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?><?=($arResult["NavPageNomer"]+1)?>">
                                <?=GetMessage("nav_prev")?>
                            </a>
                            </li><?
                        endif;
                    endif;
                    ?><?

                    if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
                        $bFirst = false;
                        if($arResult["bSavePage"]):
                            ?><li class="">
                            <a class="blog-page-first" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?><?=$arResult["NavPageCount"]?>">
                                1
                            </a>
                            </li><?
                        else:
                            ?><li class="">
                            <a class="blog-page-first" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?>">
                                1
                            </a>
                            </li><?
                        endif;
                        ?><?
                        if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)):
                            ?><li class="disabled ">
	<span>
		...
	</span>
                        </li><?
                        endif;
                    endif;
                endif;
                do
                {
                    $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;

                    if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
                        ?><li class="disabled ">
                        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?>" class="<?=($bFirst ? "blog-page-first" : "")?>">
				<span>
					<?=$NavRecordGroupPrint?>
				</span>
                        </a>
                        </li><?
                    elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):
                        ?><li class="">
                        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?>" class="<?=($bFirst ? "blog-page-first" : "")?>">
                            <?=$NavRecordGroupPrint?>
                        </a>
                        </li><?
                    else:
                        ?><li class="">
                        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?><?=$arResult["nStartPage"]?>" class="<?=($bFirst ? "blog-page-first" : "")?>">
                            <?=$NavRecordGroupPrint?>
                        </a>
                        </li><?
                    endif;
                    ?><?

                    $arResult["nStartPage"]--;
                    $bFirst 						= false;


                } while($arResult["nStartPage"] >= $arResult["nEndPage"]);

                if ($arResult["NavPageNomer"] > 1):
                    if ($arResult["nEndPage"] > 1):
                        if ($arResult["nEndPage"] > 2):
                            ?><li class="disabled ">
	<span>
		...
	</span>
                        </li><?
                        endif;
                        ?><li class="">
                        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?>1">
                            <?=$arResult["NavPageCount"]?>
                        </a>
                        </li><?
                    endif;

                    ?><li>
                    <a class="blog-page-next"href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?><?=($arResult["NavPageNomer"]-1)?>">
                        <?=GetMessage("nav_next")?>
                    </a>
                    </li><?
                endif;

            else:
                $bFirst = true;

                if ($arResult["NavPageNomer"] > 1):
                    if($arResult["bSavePage"]):
                        ?><li>
                        <a class="blog-page-previous" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?><?=($arResult["NavPageNomer"]-1)?>">
                            <?=GetMessage("nav_prev")?>
                        </a>
                        </li><?
                    else:
                        if ($arResult["NavPageNomer"] > 2):
                            ?><li>
                            <a class="blog-page-previous" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?><?=($arResult["NavPageNomer"]-1)?>">
                                <?=GetMessage("nav_prev")?>
                            </a>
                            </li><?
                        else:
                            ?><li>
                            <a class="blog-page-previous" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?>">
                                <?=GetMessage("nav_prev")?>
                            </a>
                            </li><?
                        endif;

                    endif;
                    ?><?

                    if ($arResult["nStartPage"] > 1):
                        $bFirst = false;
                        if($arResult["bSavePage"]):
                            ?><li class="">
                            <a class="blog-page-first" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?>1">
                                1
                            </a>
                            </li><?
                        else:
                            ?><li class="">
                            <a class="blog-page-first" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?>">
                                1
                            </a>
                            </li><?
                        endif;
                        ?><?
                        if ($arResult["nStartPage"] > 2):
                            ?><li class="disabled ">
	<span>
		...
	</span>
                        </li><?
                        endif;
                    endif;
                endif;

                do
                {
                    if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
                        ?><li class="active ">
                        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?>" class="<?=($bFirst ? "blog-page-first" : "")?>">
                        <span>
                            <?=$arResult["nStartPage"]?>
                        </span>
                        </a>
                        </li><?
                    elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
                        ?><li class="">
                        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?>" class="<?=($bFirst ? "blog-page-first" : "")?>">
                            <?=$arResult["nStartPage"]?>
                        </a>
                        </li><?
                    else:
                        ?><li class="">
                        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?><?=$arResult["nStartPage"]?>" class="<?=($bFirst ? "blog-page-first" : "")?>">
                            <?=$arResult["nStartPage"]?>
                        </a>
                        </li><?
                    endif;
                    ?><?

                    $arResult["nStartPage"]++;
                    $bFirst = false;



                } while($arResult["nStartPage"] <= $arResult["nEndPage"]);

                if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                    if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
                        if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):
                            ?><li class="disabled ">
	<span>
		...
	</span>
                        </li><?
                        endif;
                        ?><li class="">
                        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?><?=$arResult["NavPageCount"]?>">
                            <?=$arResult["NavPageCount"]?>
                        </a>
                        </li><?
                    endif;
                    ?><li>
                    <a class="blog-page-next" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?><?=$pageURL;?><?=($arResult["NavPageNomer"]+1)?>">
                        <?=GetMessage("nav_next")?>
                    </a>
                    </li><?
                endif;
            endif;

            if ($arResult["bShowAll"]):
                if ($arResult["NavShowAll"]):
                    ?><li class="">
                    <a class="blog-page-pagen" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?>&SHOWALL_<?=$arResult["NavNum"]?>=0">
                        <?=GetMessage("nav_paged")?>
                    </a>
                    </li><?
                else:
                    ?><li class="">
                    <a class="blog-page-all" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryString?>&SHOWALL_<?=$arResult["NavNum"]?>=1">
                        <?=GetMessage("nav_all")?>
                    </a>
                    </li><?
                endif;
            endif
            ?></ul>
    </div>
    <?
}
?>