    <?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
    IncludeTemplateLangFile(__FILE__);
    ?>
</main>
<div class="cart-icon">
    <?

    if(CModule::IncludeModule("catalog")
    && CModule::IncludeModule("sale")){

        $APPLICATION->IncludeComponent(
            "bitrix:sale.basket.basket.line",
            "ampbasket",
            array(
                "COMPONENT_TEMPLATE" => "ampbasket",
                "PATH_TO_BASKET" => $impel_amp_path_to_basket,
                "PATH_TO_ORDER" => $impel_amp_path_to_basket,
                "SHOW_DELAY" => "N",
                "SHOW_NOTAVAIL" => "N",
                "SHOW_SUBSCRIBE" => "N",
                "SHOW_NUM_PRODUCTS" => "Y",
                "SHOW_TOTAL_PRICE" => "Y",
                "SHOW_EMPTY_VALUES" => "Y",
                "SHOW_PERSONAL_LINK" => "Y",
                "SHOW_AUTHOR" => "N",
                "SHOW_PRODUCTS" => "N",
                "POSITION_FIXED" => "N",
                "HIDE_ON_BASKET_PAGES" => "N",
                "COMPOSITE_FRAME_MODE" => "A",
            ),
            false
        );

    }

    ?>
</div>
<footer>
    <div>
        <ul class="amp-footer-menu">
            <li><a href="/amp/how-to-buy/">Как купить</a></li>
            <li><a href="/amp/dostavka/">Доставка</a></li>
            <li><a href="/amp/akciya/">Акции</a></li>
            <li><a href="/amp/o-kompanii/">О компании</a></li>
            <li><a href="/amp/salon/">Контакты</a></li>
            <li><a href="/amp/vozvrat-i-obmen/">Обмен и возврат</a></li>
            <li><a href="/amp/quality/">Качество</a></li>
        </ul>
        <p>МЦ «Богатырь» Богатырский пр. д. 18 А</p>
        <div class="about-us-social">
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.detail",
                "ampsocial",
                Array(
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "N",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "USE_SHARE" => "N",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "parameters",
                    "IBLOCK_ID" => "16",
                    "ELEMENT_ID" => "",
                    "ELEMENT_CODE" => "social-icons",
                    "CHECK_DATES" => "Y",
                    "FIELD_CODE" => array(),
                    "PROPERTY_CODE" => array("SOCIAL_ICONS_LINKS", "SOCIAL_ICONS", "SOCIAL_TITLES", "SOCIAL_ICONS_FILES"),
                    "IBLOCK_URL" => "",
                    "SET_TITLE" => "N",
                    "SET_CANONICAL_URL" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "BROWSER_TITLE" => "-",
                    "SET_META_KEYWORDS" => "N",
                    "META_KEYWORDS" => "-",
                    "SET_META_DESCRIPTION" => "N",
                    "META_DESCRIPTION" => "-",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "ADD_ELEMENT_CHAIN" => "N",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "USE_PERMISSIONS" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "PAGER_TEMPLATE" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N"
                ),
                false
            );?>
        </div>
    </div>
</footer>
    <? $APPLICATION->IncludeComponent(
        "bitrix:menu",
        "amptopmenu",
        Array(
            "ALLOW_MULTI_SELECT" => "Y",
            "CHILD_MENU_TYPE" => "",
            "DELAY" => "N",
            "MAX_LEVEL" => "3",
            "MENU_CACHE_GET_VARS" => array(""),
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_USE_GROUPS" => "N",
            "ROOT_MENU_TYPE" => "catalog_top",
            "USE_EXT" => "Y"
        ),
        false
    ); ?>
<?php if(!empty($impel_amp_google_analitics_number)): ?>
    <amp-analytics type="googleanalytics">
        <script type="application/json">
            {
                "vars": {
                    "account": "<?=$impel_amp_google_analitics_number;?>"
                } 
            }
        </script>
    </amp-analytics>
<?php endif; ?>
</body>
</html>