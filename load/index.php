<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Скачать PDF-каталоги");
$APPLICATION->SetPageProperty('CONTENT_PAGE', 'Y');
?><p>
 <strong>Каталог классической мебели</strong>
</p>
<p>
 <a href="/bitrix/images/userfiles/files/classic2014.pdf">Скачать каталог Louis XV, Louis XVI&nbsp;</a>
</p>
<p>
 <a href="/barokko-i-rokoko/louis-xv/"><img src="/bitrix/images/userfiles/images/xv-883l-4.jpg" alt="" style="width: 167px; height: 110px;"></a>&nbsp;<a href="/frantsuzskaya-klassika/louis-xvi/"><img src="/bitrix/images/userfiles/images/PV-551-2_3.jpg" alt="" style="width: 167px; height: 110px;"></a>
</p>
<p>
	 &nbsp;
</p>
<p>
 <strong>Каталог Adalia, Cambridge, Louis</strong>
</p>
<p>
 <a href="/bitrix/images/userfiles/files/adalia_cambridge_louise.pdf">Скачать каталог&nbsp;</a><a href="/userfiles/files/adalia_cambridge_louise.pdf">Adalia,</a><a href="/userfiles/files/adalia_cambridge_louise.pdf">Cambridge, Louis</a>
</p>
<p>
 <a href="/french-provence/adalia/"><img src="/bitrix/images/userfiles/images/info/ad-710-1(1).jpg" alt="" style="width: 167px; height: 110px;"></a>&nbsp;<a href="/english-furniture/cambridge/"><img src="/bitrix/images/userfiles/images/info/111je%20882.JPG" alt="" style="width: 150px; height: 111px;"></a>&nbsp;<a href="/frantsuzskaya-klassika/louis-xvi/"><img src="/bitrix/images/userfiles/images/info/lv-570-1.jpg" alt="" style="width: 167px; height: 110px;"></a>
</p>
<p>
	 &nbsp;
</p>
<p>
 <strong>Каталог Барокко</strong>
</p>
<p>
 <a href="/bitrix/images/userfiles/files/barokko.pdf">Скачать каталог Барокко&nbsp;</a>
</p>
<p>
 <a href="/barokko-i-rokoko/barokko/"><img src="/bitrix/images/userfiles/images/info/0614-1.jpg" alt="" style="width: 167px; height: 110px;"></a>
</p>
<p>
	 &nbsp;
</p>
<p>
 <strong>Каталог Artichoke, La Truffe, Lilac</strong>
</p>
<p>
 <a href="/bitrix/images/userfiles/files/catalog%20artishoke2013.pdf">Скачать каталог Artichoke, La Truffe, Lilac</a>
</p>
<p>
 <a href="/french-provence/"><img src="/bitrix/images/userfiles/images/info/big_images/img_3325_10_201121.jpg" alt="" style="width: 158px; height: 110px;"></a>&nbsp;<img src="/bitrix/images/userfiles/images/info/big_images/img_3135_10_2011.jpg" alt="" style="width: 158px; height: 110px;">&nbsp;<a href="/frantsuzskaya-klassika/"><img src="/bitrix/images/userfiles/images/info/big_images/foto21119.jpg" alt="" style="width: 158px; height: 110px;"></a>
</p>
<p>
	 &nbsp;
</p>
<p>
	 &nbsp;
</p>
<p>
 <strong>Каталог плетеной мебели и аксессуаров Villa Verde</strong>
</p>
<p>
 <a href="/bitrix/images/userfiles/files/villaverde2013.pdf">Скачать каталог Villa Verde</a>
</p>
<p>
 <a href="/pletenaya/pletyonaya-mebel/"><img src="/bitrix/images/userfiles/images/info/big_images/APL_5693%20copy.jpg" alt="" style="width: 158px; height: 110px;"></a>&nbsp;<a href="/pletenaya/pletyonaya-mebel/"><img src="/bitrix/images/userfiles/images/info/big_images/Salima%20set.jpg" alt="" style="width: 158px; height: 110px;"></a>&nbsp;<a href="/pletenaya/pletyonaya-mebel/"><img src="/bitrix/images/userfiles/images/info/big_images/APL_2099%20copy.jpg" alt="" style="width: 158px; height: 110px;"></a>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>