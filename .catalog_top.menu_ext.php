<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Realweb\Site\Site;

global $APPLICATION;
$aMenuLinksExt = Site::getMenu(IBLOCK_CATALOG_CATALOG, 3, 'top');

$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);