<?
//============================================================
/**
 * pr() функция дебага. Выводит строку $o
 *
 * @param $o - Строка для вывода
 * @param bool $show - Если true, то показывать всем пользователям
 * @param bool $die - Если true, то добавляет die() после вывода
 * @param bool $fullBackTrace - Выводит полный backtrace вызова функции pr
 * @return bool Возращает false, если вывод дебажной информации запрещён
 */
function pr($o, $show = false, $die = false, $fullBackTrace = false)
{
    global $USER;
    if ($USER->IsAdmin() || $show) {
        $bt = debug_backtrace();

        $firstBt = $bt[0];
        $dRoot = $_SERVER["DOCUMENT_ROOT"];
        $dRoot = str_replace("/", "\\", $dRoot);
        $firstBt["file"] = str_replace($dRoot, "", $firstBt["file"]);
        $dRoot = str_replace("\\", "/", $dRoot);
        $firstBt["file"] = str_replace($dRoot, "", $firstBt["file"]);
        ?>
        <div style='font-size:9pt; color:#000; background:#fff; border:1px dashed #000;'>
            <div style='padding:3px 5px; background:#99CCFF;'>

                <? if ($fullBackTrace == false): ?>
                    File: <b><?= $firstBt["file"] ?></b> [line: <?= $firstBt["line"] ?>]
                <? else: ?>
                    <? foreach ($bt as $value): ?>
                        <?
                        $dRoot = str_replace("/", "\\", $dRoot);
                        $value["file"] = str_replace($dRoot, "", $value["file"]);
                        $dRoot = str_replace("\\", "/", $dRoot);
                        $value["file"] = str_replace($dRoot, "", $value["file"]);
//                        echo '<pre>';
//                        print_r($value);
//                        echo '</pre>';
                        ?>

                        File: <b><?= $value["file"] ?></b> [line: <?= $value["line"] ?>] <?= $value['class'] . '->'.$value['function'].'()'?><br>
                    <? endforeach ?>
                <?endif; ?>
            </div>
            <pre style='padding:10px;'><? is_array($o) ? print_r($o) :  print_r(htmlspecialcharsbx($o)) ?></pre>
        </div>
        <?if ($die == true) {
            die();
        }?>
        <?
    } else {
        return false;
    }
}

function pr2($o, $show = false, $die = false, $fullBackTrace = false)
{

    global $USER;
    if ($USER -> GetId() == 59 || $_REQUEST['debs'] == 1 ) {

        $bt = debug_backtrace();

        $firstBt = $bt[0];
        $dRoot = $_SERVER["DOCUMENT_ROOT"];
        $dRoot = str_replace("/", "\\", $dRoot);
        $firstBt["file"] = str_replace($dRoot, "", $firstBt["file"]);
        $dRoot = str_replace("\\", "/", $dRoot);
        $firstBt["file"] = str_replace($dRoot, "", $firstBt["file"]);
        ?>
        <div style='font-size:9pt; color:#000; background:#fff; border:1px dashed #000;'>
            <div style='padding:3px 5px; background:#99CCFF;'>

                <? if ($fullBackTrace == false): ?>
                    File: <b><?= $firstBt["file"] ?></b> [line: <?= $firstBt["line"] ?>]
                <? else: ?>
                    <? foreach ($bt as $value): ?>
                        <?
                        $dRoot = str_replace("/", "\\", $dRoot);
                        $value["file"] = str_replace($dRoot, "", $value["file"]);
                        $dRoot = str_replace("\\", "/", $dRoot);
                        $value["file"] = str_replace($dRoot, "", $value["file"]);
                        ?>

                        File: <b><?= $value["file"] ?></b> [line: <?= $value["line"] ?>]<br>
                    <? endforeach ?>
                <?endif; ?>
            </div>
            <pre style='padding:10px;'><? is_array($o) ? print_r($o) :  print_r(htmlspecialcharsbx($o)) ?></pre>
        </div>
        <?if ($die == true) {
            die();
        }?>
        <?
    } else {
        return false;
    }
}

//==================================================================================================
function cutString($string, $maxlen) {
    $len = (mb_strlen($string) > $maxlen)
        ? mb_strripos(mb_substr($string, 0, $maxlen), ' ')
        : $maxlen
    ;
    $cutStr = mb_substr($string, 0, $len);
    return (mb_strlen($string) > $maxlen)
        ? '' . $cutStr . ' ...'
        : '' . $cutStr . ''
        ;
}

function sklonen($n,$s1,$s2,$s3, $b = false, $c=false){
    $m = $n % 10; $j = $n % 100;
    if($m==1) $s = $s1;
    if($m>=2 && $m<=4) $s = $s2;
    if($m==0 || $m>=5 || ($j>=10 && $j<=20)) $s = $s3;
    if($b) $n = '<b>'.$n.'</b>';
    if(!$c)
        return $n.' '.$s;
    else
        return $s;
}

function my_mb_ucfirst($str)
{
    $fc = mb_strtoupper(mb_substr($str, 0, 1));
    return $fc . mb_substr($str, 1);
}

if (!function_exists('GetEntityDataClass')) {
    Cmodule::IncludeModule('highloadblock');

    function GetEntityDataClass($HlBlockId)
    {


        if (empty($HlBlockId) || $HlBlockId < 1) {
            return false;
        }
        $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HlBlockId)->fetch();
        $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        return $entity_data_class;
    }
}

function convertToMp4($arElementId = false){

    if($arElementId == false){
        $filter = array('IBLOCK_ID' => IBLOCK_PARTY_ID, 'ACTIVE' => 'Y', 'PROPERTY_VIDEO_CONVERTED' => 0, 'PROPERTY_IS_FILE_VIDEO' => 1, '!PROPERTY_VIDEO_FILE_VALUE' => false);
    }else{
        $filter = array('IBLOCK_ID' => IBLOCK_PARTY_ID, 'ID' => $arElementId);
    }

    $select = array('ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_VIDEO_FILE');

    $rsElement = CIBlockElement::GetList(array('SORT' => 'ASC', 'ACTIVE_FROM' => 'DESC'), $filter, false, false, $select);
    while ($arElem = $rsElement->GetNext()) {

        $filePath = SITE_DOCUMENT_ROOT.CFile::GetPath($arElem['PROPERTY_VIDEO_FILE_VALUE']);
        $mimeType = mime_content_type($filePath);


        // Если видос и так на mp4, то не конвертим его помечаем, что всё окей
        if($mimeType == 'video/mp4'){
            CIBlockElement::SetPropertyValuesEx($arElem['ID'], false, array('VIDEO_CONVERTED' => true));
        }else if(strpos( $mimeType, 'video' ) === 0){

            CIBlockElement::SetPropertyValuesEx($arElem['ID'], false, array('VIDEO_CONVERTED' => true));
            shell_exec('ffmpeg -y -i '.$filePath.' '.$filePath.'.mp4 -hide_banner >/dev/null 2>/dev/null');

            $arFile =  CFile::MakeFileArray($filePath.'.mp4');
            CIBlockElement::SetPropertyValuesEx($arElem['ID'], false, array('VIDEO_FILE' => array('VALUE' => $arFile, 'DESCRIPTION' => 'converted video to mp4')));
        }
    }

    if($arElementId == false){
        return 'convertToMp4(false);';
    }else{
        return '';
    }
}



function generatePreviewPicture($fileSrc, $arSize = array('width' => 250, 'height' => 210), $second = 20, $returnId = true){
    if(is_file($fileSrc)){
        // Определяем макс продолжительность видоса в секундах
        $fileDuration = floor(shell_exec("ffprobe -i $fileSrc -show_format -v quiet | sed -n 's/duration=//p'"));

        if($fileDuration == 0){
            return false;
        }

        if($fileDuration < $second){
            $second = 1;
        }

        // Получаем превью картинку из видео.
        shell_exec('ffmpeg  -itsoffset -'.$second.' -y  -i '.$fileSrc.' -vcodec mjpeg -vframes 1 -an -f rawvideo -s '.$arSize['width'].'x'.$arSize['height'].' '.$fileSrc.'_preview.jpg');

        if($returnId){
            $arFile =  CFile::MakeFileArray($fileSrc.'_preview.jpg');
            $fid = CFile::SaveFile($arFile, 'genPreview');
            return $fid;
        }else{
            $filePath = str_replace($_SERVER['DOCUMENT_ROOT'], '', $fileSrc . '_preview.jpg');
            return $filePath;
        }

    }else{
        return false;
    }
}

function getSectionById($sectionId, $iblockId, $arSectionSelect = array("ID","NAME", "DEPTH_LEVEL", 'SECTION_PAGE_URL', 'META_TITLE')){
    $arFilter = array(
        'IBLOCK_ID' => $iblockId,
        "=ID" => $sectionId,
        "ACTIVE" => "Y",
        "INCLUDE_SUBSECTIONS" => 'Y'
    );
    return getSectionByFilter($arFilter, $iblockId, $arSectionSelect);
}
function getSectionByCode($sectionCode, $iblockId, $arSectionSelect = array("ID","NAME", "DEPTH_LEVEL", 'SECTION_PAGE_URL', 'META_TITLE'), $depthLevel = false){
    $arFilter = array(
        'IBLOCK_ID' => $iblockId,
        "=CODE" => $sectionCode,
        "ACTIVE" => "Y",
        "INCLUDE_SUBSECTIONS" => 'Y'
    );
    if($depthLevel != false && is_int($depthLevel)){
        $arFilter['DEPTH_LEVEL'] = $depthLevel;
    }

    return getSectionByFilter($arFilter, $iblockId, $arSectionSelect);
}

function getSectionByFilter($arFilter, $iblockId, $arSectionSelect){

    $obCache = new CPHPCache;

    $cache_id = "cmb-catalog-section-".str_replace("/","_", md5(serialize($arFilter))).'ib-id'.$iblockId . md5(serialize($arSectionSelect)) ;
    $cache_path = "/catalog/sectiosn/".SITE_ID."/".$iblockId."/";

    $result["SECTION"] = array();

    if($obCache->InitCache(CUSTOM_CACHE_TIME, $cache_id, $cache_path)) :
        $vars = $obCache->GetVars();
        $result["SECTION_INFO"] = $vars["SECTION"];
    else :
        global $CACHE_MANAGER;
        $CACHE_MANAGER->StartTagCache($cache_path);
        $CACHE_MANAGER->RegisterTag("iblock_id_".$iblockId);

        $rsSect = CIBlockSection::GetList(false,$arFilter,true,$arSectionSelect);

        while ($arSect = $rsSect->GetNext()){

            $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues(
                $arSect["IBLOCK_ID"],
                $arSect["ID"]
            );

            $arSect['IPROPERTY_VALUES'] = $ipropValues->getValues();
            $result["SECTION_INFO"] = $arSect;
        }

        $CACHE_MANAGER->EndTagCache();

        if($obCache->StartDataCache()):
            $obCache->EndDataCache(array(
                "SECTION" => $result["SECTION_INFO"]
            ));

        endif;
    endif;

    return $result["SECTION_INFO"];
}

function getSubSectionById($parentSectionId, $iblockId){
    $obCache = new CPHPCache;
    //    $life_time = 0;

    $cache_id = "cmb-catalog-subsections-".str_replace("/","_", $parentSectionId).'ib-id'.$iblockId;
    $cache_path = "/catalog/sectiosn/".SITE_ID."/".$iblockId."/";

    $result["SECTIONS_LIST"] = array();

    if($obCache->InitCache(CUSTOM_CACHE_TIME, $cache_id, $cache_path)) :
        $vars = $obCache->GetVars();
        $result["SECTIONS"] = $vars["SECTIONS_LIST"];
    else :
        global $CACHE_MANAGER;
        $CACHE_MANAGER->StartTagCache($cache_path);
        $CACHE_MANAGER->RegisterTag("iblock_id_".$iblockId);

        $result['SECTIONS'] = array();

        $arFilter = Array(
            'IBLOCK_ID' => $iblockId,
            'ACTIVE' => 'Y',
            'SECTION_ID' => $parentSectionId
        );

        $arSelect = Array('ID', 'NAME', 'CODE', 'SECTION_PAGE_URL', 'RIGHT_MARGIN', 'LEFT_MARGIN');
        $res = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, false, $arSelect);
        while($obElement = $res->GetNext()){
            if( ($obElement['RIGHT_MARGIN'] - $obElement['LEFT_MARGIN']) > 1){
                $obElement['IS_PARENT'] = 'Y';
                $subSection = getSubSectionById($obElement['ID'], $iblockId);

                $isChildParent = true;

                foreach($subSection as $value){
                    if($value['IS_PARENT'] == 'Y'){
                        $isChildParent = false;
                        break;
                    }
                }

                if($isChildParent){
                    $obElement['IS_CHILD_PARENT'] = 'Y';
                }else{
                    $obElement['IS_CHILD_PARENT'] = 'N';
                }


            }else{
                $obElement['IS_PARENT'] = 'N';
            }

            $obElement['RIGHT_MARGIN-LEFT_MARGIN'] = $obElement['RIGHT_MARGIN'] - $obElement['LEFT_MARGIN'];

            $result['SECTIONS'][] = $obElement;
        }

        $CACHE_MANAGER->EndTagCache();

        if($obCache->StartDataCache()):
            $obCache->EndDataCache(array(
                "SECTIONS_LIST" => $result["SECTIONS"]
            ));

        endif;
    endif;

    return $result["SECTIONS"];
}

// todo wrap into a cache
function getSectionTree($childSectId, $iblockId){
    $result = array();

    if($childSectId > 0 && $iblockId > 0){
        $arTreeSectId = array();

        $nav = CIBlockSection::GetNavChain(false, $childSectId, array('ID', 'IBLOCK_ID', 'NAME'));
        while($arSectionPath = $nav->GetNext()){
            $arTreeSectId[] = $arSectionPath['ID'];
        }

        if(is_array($arTreeSectId) && count($arTreeSectId) > 0){
            $rsElement = CIBlockSection::GetList(
                array('DEPTH_LEVEL' => 'DESC'),
                array('ID' => $arTreeSectId, 'IBLOCK_ID' => $iblockId),
                false,
                array('ID', 'IBLOCK_ID', 'NAME', 'DEPTH_LEVEL')
            );
            while ($arElem = $rsElement->GetNext()) {
                $result[$arElem['ID']] = $arElem;
            }
        }
    }

    return $result;
}


function cacheGetList($filter, $select, $IBLOCK_ID){
    $obCache = new CPHPCache;

    $cache_id = "catalog-element-".md5(serialize($filter)).'_'.md5(serialize($select));
    $cache_path = "/catalog/cacheGetList/".SITE_ID."/".$cache_id."/";

    if($obCache->InitCache(CUSTOM_CACHE_TIME, $cache_id, $cache_path)) :
        $vars = $obCache->GetVars();
        $result["ELEMENTS"] = $vars["ELEMENTS"];
    else :
        global $CACHE_MANAGER;
        $CACHE_MANAGER->StartTagCache($cache_path);
        $CACHE_MANAGER->RegisterTag("iblock_id_".$IBLOCK_ID);

        $rsElement = CIBlockElement::GetList(array('SORT' => 'ASC', 'ACTIVE_FROM' => 'DESC'), $filter, false, false, $select);
        $arElements = array();
        while ($arElem = $rsElement->GetNext()) {
            $arElements[$arElem['ID']] = $arElem;
        }

        $result["ELEMENTS"] = $arElements;
        $CACHE_MANAGER->EndTagCache();

        if($obCache->StartDataCache()):
            $obCache->EndDataCache(array(
                "ELEMENTS" => $result["ELEMENTS"]
            ));

        endif;
    endif;

    return $result["ELEMENTS"];
}


function cacheSectionGetList($filter, $select, $IBLOCK_ID){
    $obCache = new CPHPCache;

    $cache_id = "catalog-section-".md5(serialize($filter)).'_'.md5(serialize($select));
    $cache_path = "/catalog/cacheSectionGetList/".SITE_ID."/".$cache_id."/";

    if($obCache->InitCache(CUSTOM_CACHE_TIME, $cache_id, $cache_path)) :
        $vars = $obCache->GetVars();
        $result["SECTIONS"] = $vars["SECTIONS"];
    else :
        global $CACHE_MANAGER;
        $CACHE_MANAGER->StartTagCache($cache_path);
        $CACHE_MANAGER->RegisterTag("iblock_id_".$IBLOCK_ID);

        $rsElement = CIBlockSection::GetList(array('SORT' => 'ASC', 'ACTIVE_FROM' => 'DESC'), $filter, false, $select);
        $arSections = array();
        while ($arElem = $rsElement->GetNext()) {
            $arSections[$arElem['ID']] = $arElem;
        }

        $result["SECTIONS"] = $arSections;
        $CACHE_MANAGER->EndTagCache();

        if($obCache->StartDataCache()):
            $obCache->EndDataCache(array(
                "SECTIONS" => $result["SECTIONS"]
            ));

        endif;
    endif;

    return $result["SECTIONS"];
}

function formatPrice($price, $currency = 'BYN'){
    $price = floatval($price);

    switch ($currency) {
        case 'BYN':
        default:

            if($price - floor($price) > 0){
                $decPoint = 2;
            }else{
                $decPoint = 0;
            }

            $return = number_format($price, $decPoint, '.', ' '). '&nbsp;р.';
            break;
    }

    return $return;

}

function getHLBlockItems($arFilter, $hlblock){
    $dataClass = GetEntityDataClass($hlblock);
    $arBanks = array();

    $result = $dataClass::getList(array(
        "filter" => $arFilter,
        "limit" => 100,
    )) -> fetchAll();

    return $result;
}

function checkIsProductAvailable($arItem){
    $statusId = $arItem['PROPERTIES']['STATUS_LIST_NALICHIYA']['VALUE'];

    if($arItem["PRICES"]["BASE"] and ($statusId > 0 and $statusId < 8)){
        return true;
    }else{
        return false;
    }
}

// mode 1 доставка, 2 - самовывоз
function showDeliveryDate($status, $mode = 1){
    $status = intval($status);
    $time = time();
    $day = date('D', $time);
    $hour = date('H', $time);

    $resultStr = '';
    $city = getCurrentCity();

    switch($status){
        case 1:
            // Если это Минск или самовывоз
            if($city['ID'] == MINKS_GEO_ID || $mode == 2){
                if( ( $day == 'Sat' and $hour >= 14 ) or $day == 'Sun' ){
                    $deliveryStr = 'в понедельник';
                }else if($hour < 14){
                    $deliveryStr = 'сегодня';
                }else{
                    $deliveryStr = 'завтра';
                }
            }else{
                if( ( $day == 'Sat' and $hour >= 14 ) or $day == 'Sun' ){
                    $deliveryStr = 'от 3-х дней';
                }else if($hour < 14){
                    $deliveryStr = 'от 2-х дней';
                }else{
                    $deliveryStr = 'от 3-х дней';
                }
            }
            break;
        case 2:
            // Если это Минск
            if($city['ID'] == MINKS_GEO_ID || $mode == 2){
                if($day == 'Sat' or $day == 'Sun' or ($day == 'Fri' and $hour >= 14) ){
                    $deliveryStr = 'во вторник';
                }else{
                    $deliveryStr = 'завтра';
                }
            }else{
                $deliveryStr = 'от 3-х дней';
            }
            break;
        case 3:
            $deliveryStr = '';
            break;
        case 4:
            // Если это Минск
            if($city['ID'] == MINKS_GEO_ID || $mode == 2){
                $deliveryStr = 'через 3-5 дней';
            }else{
                $deliveryStr = 'через 5-7 дней';
            }
            break;
        case 5:
            $deliveryStr = ' до 14 дней';
            break;
        case 6:
            $deliveryStr = ' до 30 дней';
            break;
        case 7:
            $deliveryStr = ' до 60 дней';
            break;
        default:
            $deliveryStr = '';
    }

    if(strlen($deliveryStr) > 0){
        if($mode == 1){
            $resultStr .= 'Доставка ';

            if($city['ID'] == 3){
                $resultStr .= "<a class='link product-page-card__delivery-link' >по Минску</a> ";
            }else{
                $resultStr .= "<a class='link product-page-card__delivery-link' >в ".$city['NAME_RU']."</a> ";
            }
            $resultStr .= $deliveryStr.'<br>';

        }else{
            $resultStr .= 'Самовывоз ';
            $resultStr .= $deliveryStr.'<br>';
        }
    }

    return $resultStr;
}



function ResizeImageSlam($picture,$width,$height){
    $isSlamImageModuleExist = false;
    if(CModule::IncludeModule("slam.image"))
    {
        $isSlamImageModuleExist = true;
    }
    if($picture){
        $arMainImg = CFile::ResizeImageGet(
            $picture,
            array("width" => $width, "height" => $height),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );

        if($isSlamImageModuleExist){
            $arMainImg['src'] = GetCompressImage($arMainImg['src'], false);
        }

    }
    return $arMainImg;
}

function getSectionLevelByItemsId( $itemsList, $iblockId, $depthLevel = 1){

    $arSections = array();
    if(!array_diff($itemsList, array('', false, null, array()))){
        return $arSections;
    }
    if(!$iblockId || $iblockId < 0){
        return $arSections;
    }

    $res = \Bitrix\Iblock\ElementTable::getList(array(
        'filter' => array('=ID' => $itemsList, 'IBLOCK_ID' => $iblockId),
        'select' => array( 'IBLOCK_SECTION_ID'),
        'group' => 'IBLOCK_SECTION_ID'

    ));

    $sectionList = array();
    while ($result = $res->fetch()){
        $sectionList[$result['IBLOCK_SECTION_ID']] = $result['IBLOCK_SECTION_ID'];
    }

    $res = \Bitrix\Iblock\SectionTable::getList(array(
        'filter' => array('=ID' => $sectionList),
        'select' => array('ID', 'LEFT_MARGIN', 'RIGHT_MARGIN'),

    ));

    $arSectionFilter = array('LOGIC' => 'OR');
    while($result = $res->fetch()){
        $arSectionFilter[] = array(
            '<LEFT_MARGIN' => $result['LEFT_MARGIN'],
            '>RIGHT_MARGIN' =>  $result['RIGHT_MARGIN']
        );
    }

    $arSectionFilter = array(
        'IBLOCK_ID' => $iblockId,
        'DEPTH_LEVEL' => $depthLevel,
        $arSectionFilter
    );

    $res = \Bitrix\Iblock\SectionTable::getList(array(
        'filter' => $arSectionFilter,
        'select' => array('ID', 'NAME', 'LEFT_MARGIN'),

    ));

    while($result = $res->fetch()){
        $arSections[$result['ID']] = $result;
    }
    return $arSections;
}




function CancelOrder($id, $message = '', $status = '', $cancelPayment = true, $cancelShipment = true , $chekUserOrder = false)
{
    $errors = array();
    \Bitrix\Main\Loader::includeModule('sale');
    $order = \Bitrix\Sale\Order::load($id);
    if(!$order){
        $errors[] = 'Заказ не найден';
        return $errors;
    }

    if($chekUserOrder && $order->getField('USER_ID') != $GLOBALS['USER']->GetId()){
        $errors[] = 'Заказ не найден';
        return $errors;
    }

    if($order->getField('CANCELED') == 'Y'){
        $errors[] = 'Заказ уже отменен';
        return $errors;
    }




    $paymentCollection = $order->getPaymentCollection();
    if($paymentCollection->hasPaidPayment()){

        foreach($paymentCollection as $payment){

            if($payment->isPaid() && $cancelPayment){
                $r = $payment->setReturn("Y");

                if (!$r->isSuccess()) {
                    $errors[] = $r->getErrorMessages();

                }
            }
        }
    }

    $shipmentCollection = $order->getShipmentCollection();

    foreach($shipmentCollection as $shipment){
        if($shipment->getField('DEDUCTED') == 'Y' && $cancelShipment){
            $shipment->setField('DEDUCTED', 'N');
        }
    }

    $order->setField('REASON_CANCELED', $message);

    $r = $order->setField('CANCELED', 'Y');
    if (!$r->isSuccess()) {
        AddMessage2Log(print_r($r->getErrorMessages() ,1));
        $errors = $r->getErrorMessages();
    }
    $r = $order->setField('STATUS_ID', $status);
    if (!$r->isSuccess()) {
        AddMessage2Log(printr_r($r->getErrorMessages() ,1));
        $errors[] = $r->getErrorMessages();
    }
    if(!$errors){
        $r = $order->save();
        if (!$r->isSuccess()) {
            AddMessage2Log(print_r($r->getErrorMessages() ,1));
        }
        return $r->isSuccess();
    }else{
        return $errors;
    }



}


function getCurrentCity(){
    $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
    $cityID = $request->getCookie('AIP_CITY');

    if (!$cityID){
        $cityID = getCityIdByIP($_SERVER['REMOTE_ADDR']);
    }
    if (!$cityID){
        /*Санкт-петербург default*/
        $cityID = 4;
    }

    return getCityById($cityID);
}

function getCityIdByIP($ip){
    $cache = new CPHPCache();
    $cache_time = 360000;
    $cache_id = 'CitySectorIp'.$cacheId;
    $cache_path = '/CitySector/';

    if ( $cache->InitCache($cache_time, $cache_id, $cache_path))
    {
        $res = $cache->GetVars();
        $cityID = $res["CITY_ID"];

    }elseif(!$cityID){

        \Bitrix\Main\Loader::includeModule('ammina.ip');
        $cityID = \Ammina\IP\BlockTable::getCityIdByIP($ip);
        $cache->StartDataCache($cache_time, $cache_id, $cache_path);
        $cache->EndDataCache(array("CITY_ID" => $cityID));
    }

    return $cityID;

}

function getCityById($cityID){
    $arCity = array();
    if(!$cityID){
        return $arCity;
    }

    $cache = new CPHPCache();
    $cache_time = 360000;
    $cache_id = 'CitySectorArray'.$cityID;
    $cache_path = '/CitySector/';

    if ( $cache->InitCache($cache_time, $cache_id, $cache_path))
    {
        $res = $cache->GetVars();
        $arCity = $res["arCity"];

    }elseif(!$arCity){

        \Bitrix\Main\Loader::includeModule('ammina.ip');

        $arCity = \Ammina\IP\CityTable::getList(array(
            "filter" => array(
                "ID" => $cityID,
            ),
            "select" => array('*'),
        ))->fetch();
        $cache->StartDataCache($cache_time, $cache_id, $cache_path);
        $cache->EndDataCache(array("arCity" => $arCity));
    }

    return $arCity;

}
