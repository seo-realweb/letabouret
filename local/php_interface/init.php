<?php

use Bitrix\Main\Loader;
include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");
include_once(__DIR__.'/include/functions.php');
Loader::includeModule("realweb.site");
//
//
//
use Bitrix\Main;
use Bitrix\Main\Entity;
/*Не даем сохранить нулевую цену*/
//$eventManager = Main\EventManager::getInstance();
//$eventManager->addEventHandler("catalog", "\Bitrix\Catalog\Price::OnAfterUpdate", "setAvalibleAfterPriceEdit");
//$eventManager->addEventHandler("catalog", "\Bitrix\Catalog\Price::OnAfterAdd", "setAvalibleAfterPriceEdit");
//$eventManager->addEventHandler("catalog", "OnProductPriceDelete", "PriceOnBeforeDelete");
//
//function PriceOnBeforeDelete($productId, $arPricesId){
//
//    if(!$arPricesId){
//        \Bitrix\Catalog\ProductTable::update($productId, Array('AVAILABLE' => 'N'));
//    }
//
//}
//function setAvalibleAfterPriceEdit ($event){
//    $dataFields = $event->getParameter("fields");
//    $id = $event->getParameter("id");
//    if(is_array($id)  && $id['ID']){
//        $id = $id['ID'];
//    }
//    if(!$id && $dataFields['PRODUCT_ID']){
//        $id = $dataFields['PRODUCT_ID'];
//    }
//    if($id){
//        if ( $dataFields['PRICE'] > 0){
//            \Bitrix\Catalog\ProductTable::update($id, Array('AVAILABLE' => 'Y'));
//        }else{
//            \Bitrix\Catalog\ProductTable::update($id, Array('AVAILABLE' => 'N'));
//        }
//    }
//
//}

function true_wordform($num, $form_for_1, $form_for_2, $form_for_5){
    $stn=$num;

    $num = abs($num) % 100; // берем число по модулю и сбрасываем сотни (делим на 100, а остаток присваиваем переменной $num)
    $num_x = $num % 10; // сбрасываем десятки и записываем в новую переменную
    if ($num > 10 && $num < 20) // если число принадлежит отрезку [11;19]
        return $stn.' '.$form_for_5;
    if ($num_x > 1 && $num_x < 5) // иначе если число оканчивается на 2,3,4
        return $stn.' '.$form_for_2;
    if ($num_x == 1) // иначе если оканчивается на 1
        return $stn.' '.$form_for_1;
    return $stn.' '.$form_for_5;
}
