<?
define('CATALOG_IBLOCK_ID', 1);
if(!$_SERVER['DOCUMENT_ROOT']){
    $_SERVER['DOCUMENT_ROOT'] = '/var/www/letabouret.ru';
}

include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('catalog');
\Bitrix\Main\Loader::includeModule('sale');

$time_start = microtime(true);

$dbResult = \Bitrix\Catalog\ProductTable::getList([
    'select' => ['ID', 'TYPE'],
    'filter' => ['TYPE' => [1,3]],
]);
while ($result = $dbResult->fetch()){

    if(checkProductDiscount($result['ID'])){
        setIsDiscount($result['ID'], 'Y');
    }else{
        setIsDiscount($result['ID'], 'N');
    }

}

function checkProductDiscount($productID){
    $opResult = CCatalogProduct::GetOptimalPrice($productID);

    if($opResult['RESULT_PRICE']['DISCOUNT_PRICE'] != $opResult['PRICE']['PRICE']){

        return true;
    }
    return false;
}

function setIsDiscount($itemId , $value = 'N'){
    if($value != 'Y'){
        $value = 'N';
    }


    $arProps['IS_SALE'] = ['VALUE' => $value];

    CIBlockElement::SetPropertyValuesEx($itemId, CATALOG_IBLOCK_ID, $arProps);
    \Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex(CATALOG_IBLOCK_ID, $itemId);
}



$time_end = microtime(true);
$time = $time_end - $time_start;
