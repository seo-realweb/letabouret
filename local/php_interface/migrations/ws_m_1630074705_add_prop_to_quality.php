<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1630074705_add_prop_to_quality extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "add_prop_to_quality";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "43beaa1c05b2e48a152f78265464043aca52d895";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        $builder = new \WS\ReduceMigrations\Builder\IblockBuilder();
        $iblock = $builder->updateIblock(IBLOCK_NEWS_QUALITY, function (\WS\ReduceMigrations\Builder\Entity\Iblock $rsIblock) {
            $obProp = $rsIblock
                ->addProperty('Скрыть заголовок')
                ->code('HIDE_NAME')
                ->typeCheckbox()
                ->sort(100);
            $obProp->addEnum('Да')->xmlId(1);
        });
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
    }
}