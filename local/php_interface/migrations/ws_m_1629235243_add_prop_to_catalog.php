<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1629235243_add_prop_to_catalog extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "add_prop_to_catalog";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "89bfe44740672e8ec12c01fbc5e880138ea313bf";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        $builder = new \WS\ReduceMigrations\Builder\IblockBuilder();
        $iblock = $builder->updateIblock(IBLOCK_CATALOG_CATALOG, function (\WS\ReduceMigrations\Builder\Entity\Iblock $rsIblock) {
            $obProp = $rsIblock
                ->addProperty('Без цены')
                ->code('WITHOUT_PRICE')
                ->typeCheckbox()
                ->sort(10000);
            $obProp->addEnum('Да')->xmlId(1);
        });
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
    }
}