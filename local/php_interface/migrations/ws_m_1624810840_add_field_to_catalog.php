<?php
use \WS\ReduceMigrations\Builder\IblockBuilder;
use \WS\ReduceMigrations\Builder\Entity\IblockType;
use \WS\ReduceMigrations\Builder\Entity\Iblock;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1624810840_add_field_to_catalog extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "add_field_to_catalog";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "dc8a2cbd991c060cb572d7ff723f298b837a32dd";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        $builder = new \Realweb\Site\Builder\IblockBuilder();
        if ($arIBlock = $builder->GetIblock('catalog', 'catalog')) {
            $iBlockId = $arIBlock['ID'];

            $obField = new \Realweb\Site\Builder\UserField('UF_PICTURE', 'IBLOCK_' . $iBlockId . '_SECTION');
            if (intval($obField->getId()) == 0) {
                $obField->type('file');
                $obField->multiple(true);
                $obField->label(array('ru' => "Фото", 'en' => "Фото"));
                $obField->save();
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
    }
}