<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1624807167_replace_upper_code_in_catalog extends \WS\ReduceMigrations\Scenario\ScriptScenario
{

    /**
     * Name of scenario
     **/
    static public function name()
    {
        return "replace_upper_code_in_catalog";
    }

    /**
     * Priority of scenario
     **/
    static public function priority()
    {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash()
    {
        return "9a59d910890e8eaab265cfb7cd967fdc206d90f6";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime()
    {
        return 10;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit()
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        $arItems = \Bitrix\Iblock\ElementTable::query()
            ->setSelect(array('ID', 'CODE', 'NAME'))
            ->where('IBLOCK_ID', '=', \Realweb\Site\Catalog::getIblockId())
            ->exec()
            ->fetchAll();
        $obElement = new \CIBlockElement();
        foreach ($arItems as $arItem) {
            if (preg_match('/[A-Z]/', $arItem['CODE'])) {
                $obElement->Update($arItem['ID'], array('CODE' => \CUtil::translit($arItem['CODE'], 'ru', array("replace_space" => "-", "replace_other" => "-"))));
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback()
    {
        // my code
    }
}