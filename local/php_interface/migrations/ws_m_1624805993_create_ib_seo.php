<?php
use WS\ReduceMigrations\Builder\Entity\Iblock;
use WS\ReduceMigrations\Builder\Entity\IblockType;
use WS\ReduceMigrations\Builder\IblockBuilder;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1624805993_create_ib_seo extends \WS\ReduceMigrations\Scenario\ScriptScenario
{

    /**
     * Name of scenario
     **/
    static public function name()
    {
        return "create_ib_seo";
    }

    /**
     * Priority of scenario
     **/
    static public function priority()
    {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash()
    {
        return "588922d8534a4925edb54b5f5b0e3385b16eb009";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime()
    {
        return 1;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit()
    {
        $builder = new IblockBuilder();
        $builder->createIblockType('service', function (IblockType $type) {
            $type
                ->inRss(false)
                ->sort(100)
                ->sections('Y')
                ->lang(
                    [
                        'ru' => [
                            'NAME' => 'Сервис',
                            'SECTION_NAME' => 'Разделы',
                            'ELEMENT_NAME' => 'Элементы',
                        ],
                        'en' => [
                            'NAME' => 'Сервис',
                            'SECTION_NAME' => 'Разделы',
                            'ELEMENT_NAME' => 'Элементы',
                        ],
                    ]
                );
        });

        $builder = new \WS\ReduceMigrations\Builder\IblockBuilder();
        $iblock = $builder->createIblock('service', 'СЕО', function (\WS\ReduceMigrations\Builder\Entity\Iblock $rsIblock) {
            $rsIblock
                ->siteId('s1')
                ->sort(100)
                ->version(2)
                ->code('seo')
                ->groupId(['2' => 'R'])
                ->setAttribute('INDEX_SECTION', 'N')
                ->setAttribute('INDEX_ELEMENT', 'N')
                ->setAttribute('FIELDS', [
                    'CODE' => [
                        'IS_REQUIRED' => 'Y',
                        'DEFAULT_VALUE' => [
                            'UNIQUE' => 'Y',
                            'TRANSLITERATION' => 'Y',
                            'TRANS_LEN' => '100',
                            'TRANS_CASE' => 'L',
                            'TRANS_SPACE' => '-',
                            'TRANS_OTHER' => '-',
                            'TRANS_EAT' => 'Y',
                            'USE_GOOGLE' => 'N',
                        ]
                    ],
                ]);
            $rsIblock
                ->addProperty('Каноникал')
                ->code('CANONICAL')
                ->typeString()
                ->sort(100);
            $obProp = $rsIblock
                ->addProperty('Закрыть от индексации')
                ->code('NOINDEX')
                ->typeCheckbox()
                ->sort(200);
            $obProp->addEnum('Да')->xmlId(1);
        });

        $arItems = array(
            array('CODE' => '/collections/chinese-furniture/louvre-home/consolelouvrehome/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/divany-myagkaya/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/divany-myagkaya/dvukhmestnye-mm/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/divany-myagkaya/pryamie-divany/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/divany-myagkaya/trekhmestnye-mm/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/divany-myagkaya/uglovye-mm/', 'NAME' => ''),
            array('CODE' => '/pomeshcheniya/prikhozhaya/gazetnici/', 'NAME' => ''),
            array('CODE' => '/pomeshcheniya/prikhozhaya/podstavki-zontov/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/komplekty-myagkoy-mebeli-mm/', 'NAME' => ''),
            array('CODE' => '/pomeshcheniya/spalnya/spalnye-garnituri/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/kresla-myagkaya/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/kresla-myagkaya/interernye-kresla/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/kresla-myagkaya/kompyuter-kresla/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/kresla-myagkaya/kozhanye-kresla-mm/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/kresla-myagkaya/reklaynery-mm/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/kresla-myagkaya/polukresla-mm/', 'NAME' => ''),
            array('CODE' => '/pomeshcheniya/spalnya/krovati-spalnya/', 'NAME' => ''),
            array('CODE' => '/pomeshcheniya/detskie/krovati-detskie/', 'NAME' => ''),
            array('CODE' => '/myagkaya-mebel/kushetki-i-lezhanki-mm/', 'NAME' => ''),
        );
        $arItemsCanonical = array(
            '/collections/chinese-furniture/louvre-home/',
            '/mebel/divany/',
            '/mebel/divany/dvukhmestnye/',
            '/mebel/divany/pryamye-divany/',
            '/mebel/divany/trekhmestnye/',
            '/mebel/divany/uglovye/',
            '/mebel/khranenie/gazetnitsy/',
            '/mebel/khranenie/podstavki-dlya-zontov/',
            '/mebel/komplekty-i-garnitury/myagkaya-mebel-komplekt/',
            '/mebel/komplekty-i-garnitury/spalnie-garnitury/',
            '/mebel/kresla-meb/',
            '/mebel/kresla-meb/interernye/',
            '/mebel/kresla-meb/kompyuternye-kresla/',
            '/mebel/kresla-meb/kozhanye-kresla/',
            '/mebel/kresla-meb/kresla-reklaynery/',
            '/mebel/kresla-meb/polukresla/',
            '/mebel/krovati-meb/',
            '/mebel/krovati-meb/detskie-krovati/',
            '/mebel/kushetki-i-lezhanki/',
            '/mebel/ottomanki/',
            '/mebel/sekretery-i-byuro/',
            '/mebel/stoly-i-stoliki/servirovochnye-stoliki/',
            '/myagkaya-mebel/divany-myagkaya/modul-divany/',
            '/myagkaya-mebel/divany-myagkaya/raskladnye-mm/',
            '/myagkaya-mebel/pufy-myagkaya/',
            '/pomeshcheniya/stolovaya-i-kukhnya/kukhonnye-tumbi/',
        );

        foreach ($arItems as $key => $item) {
            $arPath = explode("/", trim($item['CODE'], "/"));
            $arSection = \Realweb\Site\Site::getSection(array('CODE' => end($arPath)));
            if ($arSection) {
                $arFields = array(
                    'CODE' => $item['CODE'],
                    'NAME' => $arSection['NAME'],
                    'IBLOCK_ID' => $iblock->getId(),
                    'ACTIVE' => 'Y',
                    'SORT' => 500,
                    'PROPERTY_VALUES' => array(
                        'CANONICAL' => $arItemsCanonical[$key]
                    )
                );
                $this->_addElement($arFields);
            }
        }
    }

    private function _addElement(&$arElement)
    {
        $obElement = new \CIBlockElement();
        $rsElement = \CIBlockElement::GetList(array(), array(
            'IBLOCK_ID' => $arElement['IBLOCK_ID'],
            'CODE' => $arElement['CODE'],
        ), false, false, array('ID', 'IBLOCK_ID', 'CODE'));
        if ($arDbElement = $rsElement->GetNext()) {
            $arElement['ID'] = $arDbElement['ID'];
            $obElement->Update($arDbElement['ID'], $arElement);
        } else {
            $arElement['ID'] = $obElement->Add($arElement);
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback()
    {
        // my code
    }
}