<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1636806902_add_field_to_section_catalog extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "add_field_to_section_catalog";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "3303863991ccf243282e609d3f534971eab7ce97";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        $builder = new \Realweb\Site\Builder\IblockBuilder();
        if ($arIBlock = $builder->GetIblock('catalog', 'catalog')) {
            $iBlockId = $arIBlock['ID'];

            $obField = new \Realweb\Site\Builder\UserField('UF_ELEMENT_TEMPLATE_DESCRIPTION', 'IBLOCK_' . $iBlockId . '_SECTION');
            if (intval($obField->getId()) == 0) {
                $obField->type('sections_html_field');
                $obField->label(array('ru' => "Шаблон текста для карточки товара", 'en' => "Шаблон текста для карточки товара"));
                $obField->save();
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
    }
}