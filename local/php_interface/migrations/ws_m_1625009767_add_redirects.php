<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1625009767_add_redirects extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "add_redirects";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "478aed075b7933897a7774ff12a829461e010192";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 30;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        $arElements = \Realweb\Site\Site::getIBlockElements(array(
            'IBLOCK_ID' => \Realweb\Site\Catalog::getIblockId(),
            'IBLOCK_SECTION_ID' => array(790, 792)
        ));
        foreach ($arElements as $arElement) {
            \Realweb\Site\RedirectTable::add(array(
                'UF_ACTIVE' => 1,
                'UF_FROM' => $arElement['DETAIL_PAGE_URL'],
                'UF_TO' => '/mebel/'
            ));
        }
        $obSection = new \CIBlockSection();
        foreach (array(789, 790, 791, 792) as $iSectionId) {
            $obSection->Update($iSectionId, array('ACTIVE' => 'N'));
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
    }
}