<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arParams['FAVORITE_ID'] = $favId = "bx_fav".$this->randString();
$arParams['templateName'] = $templateName;
$arParams['templateLang'] = strlen($_POST['templateLang'])==2 ? $_POST['templateLang'] : LANGUAGE_ID;


?>

<div class="fixed-profile__fav" id="<?=$favId?>">
    <?require(realpath(dirname(__FILE__)).'/ajax_template.php');?>
</div>

<script type="text/javascript">
    $(document).ready(function () {
       favoriteScriptInit(<?=json_encode($arParams)?>);
    });
</script>
