<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();


$arTemplateParameters = array(
    "BUTTON_CLASS" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("SLAM_BUTTON_CLASS"),
        "TYPE" => "STRING",
        "DEFAULT" => 'cat__favorite',
        "REFRESH" => "N",
    ),
    "ACTIVE_CLASS" => array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("SLAM_ACTIVE_CLASS"),
        "TYPE" => "STRING",
        "DEFAULT" => 'active',
        "REFRESH" => "N",
    ),
    "FLY_ACTIVATE" => array(
        "NAME" => GetMessage("SLAM_FLY_ACTIVATE"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => 'N',
        "REFRESH" => "Y",
    )
);

if($arCurrentValues["FLY_ACTIVATE"] == 'Y'){
    $arTemplateParameters['AREA_CLASS'] = array(
        "NAME" => GetMessage("SLAM_AREA_CLASS"),
        "TYPE" => "STRING",
        "DEFAULT" => "catalog-item",
        "MULTIPLE" => "N",
    );
    $arTemplateParameters['IMAGE_CLASS'] = array(
        "NAME" => GetMessage("SLAM_IMAGE_CLASS"),
        "TYPE" => "STRING",
        "MULTIPLE" => "N",
        "DEFAULT" => "main-image",
    );
    $arTemplateParameters['TARGET_CLASS'] = array(
        "NAME" => GetMessage("SLAM_TARGET_CLASS_COMPARE"),
        "TYPE" => "STRING",
        "MULTIPLE" => "N",
        "DEFAULT" => "fav-target",
    );
}

