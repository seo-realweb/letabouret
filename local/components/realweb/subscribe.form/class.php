<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Data\Cache;

class CRealwebSubscribeComponent extends CBitrixComponent
{
    protected $REQUEST = array();

    public function onPrepareComponentParams($params)
    {
        $this->REQUEST = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        if (!isset($params["CACHE_TIME"]))
            $params["CACHE_TIME"] = 3600;
        return $params;
    }

    protected function doAction()
    {
        switch ($this->REQUEST['action']) {
            case 'saveRubrics':
                $this->addSubscribeUser();
                break;
            case 'removeSubscribeUser':
                $this->removeSubscribeUser();
                break;
        }
    }

    protected function removeSubscribeUser()
    {
        global $USER;
        if (Loader::IncludeModule('subscribe') && check_bitrix_sessid()) {
            $rsSubscribe = \CSubscription::GetList(
                Array(
                    "ID" => "ASC"
                ),
                Array(
                    "CONFIRMED" => "Y",
                    "ACTIVE" => "Y",
                    "USER_ID" => $USER->GetID()
                )
            );
            while (($arSubscribe = $rsSubscribe->Fetch())) {
                \CSubscription::Delete($arSubscribe['ID']);
            }
        }
    }

    protected function addSubscribeUser()
    {
        global $USER;
        if (Loader::IncludeModule('subscribe') && check_bitrix_sessid()) {
            if (!strlen(trim($this->REQUEST['EMAIL']))) {
                $this->arResult['MESSAGE'] = 'Введите email';
            } else {
                $RUBRIC_LIST = $this->REQUEST['RUB_ID'];
                $rsSubscribe = \CSubscription::GetList(
                    Array(
                        "ID" => "ASC"
                    ),
                    Array(
                        "EMAIL" => $this->REQUEST['EMAIL']
                    )
                );
                if ($rsSubscribe->SelectedRowsCount() > 0) {
                    $this->arResult['MESSAGE'] = 'Вы уже подписаны';
                } else {
                    $arFields = Array(
                        'USER_ID' => $USER->GetID(),
                        'SEND_CONFIRM' => 'Y',
                        'EMAIL' => $this->REQUEST['EMAIL'],
                        'ACTIVE' => 'Y',
                        'RUB_ID' => array($RUBRIC_LIST),
                        'CONFIRMED' => 'Y',
                        'FORMAT' => 'html',
                    );
                    $subscr = new \CSubscription;
                    if ($newID = $subscr->Add($arFields)) {
                        $this->arResult['MESSAGE'] = 'Вы успешно подписались';
                    } else {
                        $this->arResult['MESSAGE'] = $subscr->LAST_ERROR;
                    }
                }
            }
        }
    }

    public function executeComponent()
    {
        if ($this->REQUEST['action']) {
            $this->doAction();
        }
        $this->IncludeComponentTemplate();
    }
}

?>