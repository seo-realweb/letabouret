<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<form action="<?= POST_FORM_ACTION_URI ?>" method="post">
	<?= bitrix_sessid_post()?>
    <input type="hidden" name="action" value="saveRubrics">
    <input type="hidden" name="RUB_ID" id="RUB_ID" value="<?= $arParams['RUB_ID'] ?>" />
    <div class="input-group mb-3">
        <input type="email" class="form-control" name="EMAIL" placeholder="Ваш e-mail"
               aria-label="Ваш e-mail" required>
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit">
<!--                <i class="fas fa-envelope"></i>-->
            </button>
        </div>
    </div>
    <div class="form__response"><?= $arResult['MESSAGE'] ?></div>
</form>
