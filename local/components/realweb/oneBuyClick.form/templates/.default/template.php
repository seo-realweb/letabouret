<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<div class="modal-dialog modal-dialog_order" role="document">
    <form action="<?= POST_FORM_ACTION_URI ?>" method="post">
        <?= bitrix_sessid_post() ?>
        <input type="hidden" name="action" value="one_buy_click">
        <input type="hidden" name="PRODUCT_ID" id="PRODUCT_ID" value="<?= $arParams['PRODUCT_ID'] ?>"/>

        <div class="modal-content">
            <div class="modal-body">
                <span class="popup-window-close-icon popup-window-titlebar-close-icon close" data-dismiss="modal"></span>
                <div class="form-group modal-title">
                    <span class="popup-window-titlebar-text">Быстрый заказ</span>
                </div>
                <div class="form-group modal-product">
                    <div class="image">
                        <img class="modalProductImage" src="">
                    </div>
                    <div class="product-name">
                        <span class="modalProductName"></span>
                    </div>
                </div>
                <div class="form__response"><?= $arResult['MESSAGE'] ?></div>
                <div class="form-group">
                    <input type="text"
                           name="NAME"
                           maxlength="50"
                           value=""
                           class="form-control"
                           placeholder="Введите ФИО*"
                           data-bv-message="Пожайлуйста, укажите ФИО"
                           required="">
                </div>
                <div class="form-group">
                    <input type="tel"
                           name="PERSONAL_PHONE"
                           maxlength="50"
                           value=""
                           placeholder="Номер телефона*"
                           pattern="^\+7 ?\(?[0-9]{3}\)? ?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$"
                           class="form-control"
                           data-bv-message="Пожайлуйста, укажите Телефон!"
                           data-bv-regexp-message="Укажите телефон в формате +7 (999) 999-99-99"
                           required="">
                </div>
                <div class="form-group">
                    <input type="email"
                           name="EMAIL"
                           maxlength="50"
                           value=""
                           class="form-control"
                           pattern="^[^@]+@[^\.]+\.[a-zA-Z]{2,6}$"
                           data-bv-regexp-message="Пожалуйста, укажите верный Email"
                           data-bv-message=" "
                           placeholder="Email адрес"
                          >
                </div>

                <div class="form-group form-group_submit">
                    <button type="submit" class="basket__button">Сделать заказ</button>
                </div>
                <small>Поля, отмеченные * обязательны к заполнению</small>
            </div>
        </div>
    </form>
</div>

<script>
$('#oneByClickModal').off('shown.bs.modal').on('shown.bs.modal', function () {
    includeCss('<?=SITE_TEMPLATE_PATH.'/vendor/bootstrapvalidator/css/bootstrapValidator.min.css'?>');
    var validator = includeJs('<?=SITE_TEMPLATE_PATH.'/vendor/bootstrapvalidator/js/bootstrapValidator.min.js'?>');
    var inputMask = includeJs('<?=SITE_TEMPLATE_PATH.'/vendor/jquery.inputmask.min.js'?>');
    var oneClickButton = $('#buyOneClickButton');
    if(oneClickButton.length){
        var productImage = $(this).find('.modalProductImage');
        var productName = $(this).find('.modalProductName');
        var productID = $(this).find('#PRODUCT_ID');
        if(productImage.length){
                productImage.attr('src', oneClickButton.data('productPicture'))
        }
        if(productID.length){
            productID.val(oneClickButton.data('productId'))
        }
        if(productName.length){
            productName.text(oneClickButton.data('productName'))
        }

    }


    //var script = document.createElement("script");
    //script.setAttribute("type", "text/javascript");
    //script.setAttribute("defer", "defer");
    //script.setAttribute("src", '<?//=SITE_TEMPLATE_PATH.'/vendor/bootstrapvalidator/js/bootstrapValidator.min.js'?>//');
    //document.getElementsByTagName("head")[0].appendChild(script);
    validator.onload = function(){
        $('#oneByClickModal form').bootstrapValidator().on('success.form.bv', function(e){
            e.preventDefault();
            var form = $(this);
            ajax_form_send(form);
        })
    };
    inputMask.onload = function(){

        $('#oneByClickModal form input[type="tel"]').inputmask({"mask": "+7 (999) 999-99-99"})
    };

});

function ajax_form_send(form){
    let url = new URL(window.location.origin + form.attr('action'));
    url.searchParams.delete('ajax_by_one_click');
    url.searchParams.append('ajax_by_one_click', 'y');
    $.ajax({
        method: "POST",
        url: url.toString(),
        data: form.serialize(),
        dataType: 'json',
    }).done(function( responce ) {
        if(responce.STATUS == "SUCCESS"){
            form.trigger("reset");
            $('#oneByClickModal form').bootstrapValidator('destroy');
            var succesModal = $('#oneByClickModalSuccess');
            succesModal.find('.succes-text').html(responce.MESSAGE);
            $('#oneByClickModal').modal('hide');
            succesModal.modal('show');

        }else{
            form.find('.form__response').html(responce.MESSAGE);
        }
    });
}



</script>