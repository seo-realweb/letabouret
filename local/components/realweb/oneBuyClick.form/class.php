<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Data\Cache;

class CRealwebOneByClickComponent extends CBitrixComponent
{
    protected $request = array();

    public function onPrepareComponentParams($params)
    {
        $this->request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        if (!isset($params["CACHE_TIME"]))
            $params["CACHE_TIME"] = 3600;
        return $params;
    }

    private function LoadDataShop()
    {
        $arResult = [];
        $arResult['DELIVERY'] = \Bitrix\Sale\Delivery\Services\Manager::getActiveList();
        $res = \Bitrix\Sale\PaySystem\Manager::getList();
        while ($row = $res->fetch()) {
            if ($row['ACTION_FILE'] != 'inner')
                $arResult['PAY_SYSTEM'][] = $row;
        }
        $res = \CSalePersonType::GetList(array("SORT" => "ASC"), array("LID" => SITE_ID));
        while ($row = $res->Fetch()) {
            $arResult['PERSON_TYPE'][] = $row;
        }
        $res = \CSaleStatus::GetList();
        while ($row = $res->Fetch()) {
            $arResult['STATUS'][] = $row;
        }
        return $arResult;
    }

    public static function GetProps($CODE = false)
    {
        $arProps = array();
        if (Loader::includeModule("sale")) {
            $filter = array();
            if ($CODE) {
                $filter = array("CODE" => $CODE);
            }
            $arProps = Sale\Internals\OrderPropsTable::getList(array(
                "select" => array("*"),
                "filter" => $filter,
                "order" => array("SORT"),
            ))->fetchAll();
            if ($arProps) {
                $arProps = array_column($arProps, null, "CODE");
            }
        }
        return $arProps;
    }

    public function GetElementsData($IDS)
    {
        $arResult = [];
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $rsResult = \Bitrix\Iblock\ElementTable::getList(array(
                'select' => ['*', 'IBLOCK.XML_ID'],
                'filter' => ['=ID' => $IDS],
                'runtime' => [
                    new \Bitrix\Main\Entity\ReferenceField(
                        'IBLOCK',
                        '\Bitrix\Iblock\IblockTable',
                        ['=this.IBLOCK_ID' => 'ref.ID']
                    )
                ]
            ));
            while ($row = $rsResult->fetch()) {
                $arResult[$row['ID']] = $row;
            }
        }
        return $arResult;
    }

    private function CreateUser($arFields)
    {
        $dbUser = \Bitrix\Main\UserTable::getList(array(
            'select' => array('ID'),
            'filter' => array('EMAIL' => $arFields['EMAIL'])
        ));
        if ($arUser = $dbUser->fetch()) {
            return $arUser['ID'];
        }
        global $DB;
        $user = new \CUser;
        $arFields['PASSWORD'] = randString(8);
        if (!($arFields['EMAIL']) || ($arFields['EMAIL'] == '')) {
            $phone = $this->phone_digits($arFields['PERSONAL_PHONE']);
            $arFields['EMAIL'] = $phone . '@letabouret.ru';
        }
        $arUsers['VALUES'] = array(
            "NAME" => $arFields['NAME'],
            "EMAIL" => $arFields['EMAIL'],
            "LOGIN" => $arFields['EMAIL'],
            "PERSONAL_PHONE" => $arFields['PERSONAL_PHONE'],
            "ACTIVE" => "Y",
            "PASSWORD" => $arFields['PASSWORD'],
            "CONFIRM_PASSWORD" => $arFields['PASSWORD']
        );
        $arUsers['VALUES']["CHECKWORD"] = md5(\CMain::GetServerUniqID() . uniqid());
        $arUsers['VALUES']["~CHECKWORD_TIME"] = $DB->CurrentTimeFunction();
        $arUsers['VALUES']["CONFIRM_CODE"] = randString(8);
        $arUsers['VALUES']["LID"] = SITE_ID;
        $arUsers['VALUES']["USER_IP"] = $_SERVER["REMOTE_ADDR"];
        $arUsers['VALUES']["USER_HOST"] = @gethostbyaddr($_SERVER["REMOTE_ADDR"]);
        $def_group = \COption::GetOptionString("main", "new_user_registration_def_group", "");
        if ($def_group != "") {
            $arUsers['VALUES']["GROUP_ID"] = explode(",", $def_group);
        }

        if ($user_id = $user->Add($arUsers['VALUES'])) {
//            $arEventFields = $arUsers['VALUES'];
//            \Bitrix\Main\Mail\Event::send(array(
//                "EVENT_NAME" => "NEW_USER",
//                "LID" => \Bitrix\Main\Context::getCurrent()->getSite(),
//                "C_FIELDS" => $arEventFields,
//            ));
            return $user_id;
        }
        print_r($arUsers['VALUES']);
        print_r($user->LAST_ERROR);
        exit;
        //\Bitrix\Main\Diag\Debug::writeToFile($user->LAST_ERROR);
        return false;
    }

    private function phone_digits($phone = '')
    {

        $pattern = '/[^0-9]/';
        return preg_replace($pattern, "", $phone); // 23456


    }

    public function CreateOrder($arFields)
    {
        global $USER;
        if (\Bitrix\Main\Loader::includeModule('sale') && \Bitrix\Main\Loader::includeModule('catalog')) {
            if ($USER->IsAuthorized()) {
                $userId = $USER->GetID();
            } else {
                if (!($arFields['EMAIL']) || ($arFields['EMAIL'] == '')) {
                    $phone = $this->phone_digits($arFields['PERSONAL_PHONE']);
                    $arFields['EMAIL'] = $phone . '@letabouret.ru';
                }
                $userId = $this->CreateUser([
                    'NAME' => $arFields['NAME'],
                    'EMAIL' => $arFields['EMAIL'],
                    'PERSONAL_PHONE' => $arFields['PERSONAL_PHONE']
                ]);
            }
            if (!$userId) {
                return array('STATUS' => 'ERROR', 'MESSAGE' => 'Ошибка регистрации пользователя');
            }
            $arData = $this->LoadDataShop();
            $siteId = \Bitrix\Main\Context::getCurrent()->getSite();
            $currencyCode = \Bitrix\Currency\CurrencyManager::getBaseCurrency();
            $order = \Bitrix\Sale\Order::create($siteId, $userId);
            $order->setPersonTypeId(current($arData['PERSON_TYPE'])['ID']);
            $order->setField('CURRENCY', $currencyCode);
            $basket = \Bitrix\Sale\Basket::create($siteId);
            $PRODUCTS = $this->GetElementsData([$arFields['PRODUCT_ID']]);
            //$arOrderProps = $this->GetProps();
            $properties = [
                'PRODUCT.XML_ID' => [
                    'NAME' => 'Product XML_ID',
                    'CODE' => 'PRODUCT.XML_ID',
                    'VALUE' => $PRODUCTS[$arFields['PRODUCT_ID']]['XML_ID'],
                    'SORT' => 100
                ],
                'CATALOG.XML_ID' => [
                    'NAME' => 'Catalog XML_ID',
                    'CODE' => 'CATALOG.XML_ID',
                    'VALUE' => $PRODUCTS[$arFields['PRODUCT_ID']]['IBLOCK_ELEMENT_IBLOCK_XML_ID'],
                    'SORT' => 100
                ]
            ];
            $item = $basket->createItem('catalog', $arFields['PRODUCT_ID']);
            $item->setFields(array(
                'QUANTITY' => 1,
                'CURRENCY' => $currencyCode,
                'LID' => $siteId,
                'PRODUCT_PROVIDER_CLASS' => '\Bitrix\Catalog\Product\CatalogProvider',
                'PRODUCT_XML_ID' => $PRODUCTS[$arFields['PRODUCT_ID']]['XML_ID'],
                'CATALOG_XML_ID' => $PRODUCTS[$arFields['PRODUCT_ID']]['IBLOCK_ELEMENT_IBLOCK_XML_ID'],
            ));
            $item->save();
            $basketPropertyCollection = $item->getPropertyCollection();
            $basketPropertyCollection->setProperty($properties);
            $basketPropertyCollection->save();
            $order->setBasket($basket);
            $shipmentCollection = $order->getShipmentCollection();
            $shipment = $shipmentCollection->createItem();
            $shipment->setFields(array(
                'DELIVERY_ID' => current($arData['DELIVERY'])['ID'],
                'DELIVERY_NAME' => current($arData['DELIVERY'])['NAME'],
            ));

            $paySystem = current($arData['PAY_SYSTEM']);

            $paymentCollection = $order->getPaymentCollection();
            $payment = $paymentCollection->createItem();
            $payment->setFields(array(
                'PAY_SYSTEM_ID' => $paySystem['ID'],
                'PAY_SYSTEM_NAME' => $paySystem['NAME'],
            ));
            $propertyCollection = $order->getPropertyCollection();
            $phoneProp = $propertyCollection->getPhone();
            $phoneProp->setValue($arFields['PERSONAL_PHONE']);
            $nameProp = $propertyCollection->getPayerName();
            $nameProp->setValue($arFields['NAME']);
            $emailProp = $propertyCollection->getUserEmail();
            $emailProp->setValue($arFields['EMAIL']);

            // Устанавливаем тип покупки
//            if (!empty($arOrderProps['SYS_TYPE_PURCHASE'])) {
//                $sysTypePurchase = $propertyCollection->getItemByOrderPropertyId($arOrderProps['SYS_TYPE_PURCHASE']['ID']);
//                $sysTypePurchase->setValue('oneClickTransaction');
//            }

            $order->doFinalAction(true);
            $result = $order->save();
            //\Bitrix\Main\Diag\Debug::writeToFile($result->isSuccess());
            if ($result->isSuccess()) {
                $_SESSION['SALE_ORDER_ID'][] = $order->getId();
                $orderId = $order->getField('ACCOUNT_NUMBER');
                return array('STATUS' => 'SUCCESS', 'ID' => $orderId);
            } else {
                return array('STATUS' => 'ERROR', 'MESSAGE' => $result->getErrorMessages());
            }
        }
        return false;
    }

    protected function doAction()
    {
        switch ($this->request['action']) {
            case 'one_buy_click':
                $result = $this->CreateOrder($this->request->toArray());
                $this->arResult['STATUS'] = $result['STATUS'];
                if ($result['STATUS'] == 'SUCCESS')
                    $this->arResult['MESSAGE'] = "Ваша заявка успешно принята.<br> Заказ № " . $result['ID'];

                else
                    $this->arResult['MESSAGE'] = $result['MESSAGE'];
                break;
        }
    }

    public function executeComponent()
    {
        if ($this->request['action']) {
            $this->doAction();
        }
        if ($this->request['ajax_by_one_click'] == 'y') {
            $GLOBALS['APPLICATION']->RestartBuffer();
            echo json_encode($this->arResult);
            die;
        } else {
            $this->IncludeComponentTemplate();
        }
    }
}

?>