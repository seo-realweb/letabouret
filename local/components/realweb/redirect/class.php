<?php
use \Realweb\Site\RedirectTable;

class CRealwebRedirectComponent extends CBitrixComponent
{


    public function executeComponent()
    {
        global $APPLICATION;

        $currentPage = $APPLICATION->GetCurPage();
        $arRedirect = RedirectTable::getList(Array(
            "filter" => Array(
                "UF_ACTIVE" => "1",
                0 => array(
                    "LOGIC" => "OR",
                    array("UF_FROM" => $currentPage),
                    array("UF_FROM" => rtrim($currentPage, "/"))
                )
            )
        ))->fetchRaw();
        $linkTo = false;


        if (is_array($arRedirect)) {
            if (strlen($arRedirect["UF_TO"])) {
                $linkTo = $arRedirect["UF_TO"];

            } elseif ($arRedirect["UF_ELEMENT_LINK"]) {
                $arElement = CIBlockElement::GetByID($arRedirect["UF_ELEMENT_LINK"])->GetNext();
                if ($arElement && $arElement["DETAIL_PAGE_URL"]) {
                    $linkTo = $arElement["DETAIL_PAGE_URL"];
                }
            } elseif ($arRedirect["UF_SECTION_LINK"]) {
                $arSection = CIBlockSection::GetByID($arRedirect["UF_SECTION_LINK"])->GetNext();
                if ($arSection && $arSection["SECTION_PAGE_URL"]) {
                    $linkTo = $arSection["SECTION_PAGE_URL"];
                }
            }


        }
        if (strlen($linkTo)) {
            \Realweb\Site\Site::localRedirect301($linkTo);
            exit();
        }
        return false;
    }
}

?>