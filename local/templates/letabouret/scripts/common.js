function lazyReinit(){
    new LazyLoad({
        elements_selector: ".lazy-img"
    });
    return true;
}

$(document).ready(function () {
    $('.js-components-more').bind('click', function () {
        var text = $(this).text();
        if (text==='Подробнее') {
            $(this).text('Свернуть');
        } else {
            $(this).text('Подробнее');
        }
    });
        if (window.innerWidth < 992) {
            $(document).on('click', '.nav-item.dropdown .nav-link', function (e) {
                e.preventDefault();
                if($(this).parent().hasClass('active')) {
                    $(this).parent().parent().find('.active').removeClass('active');
                    $(this).parent().removeClass('active')
                } else {
                    $(this).parent().parent().find('.active').removeClass('active');
                    $(this).parent().addClass('active')
                }
            });

            $('.js-open-mob-search').on('click', function(){
                $('.header__search').addClass('active')
            });
            $('.js-close-mob-search').on('click', function(){
                $('.header__search').removeClass('active')
            });
            $('.navbar-toggler').on('click', function(){
                $('.js-mob-menu').toggleClass('active');
                $('body').toggleClass('mob-menu-opened')
            });
            var $header = $('.js-header');
            var maxScrollTop = 0;
            $(window).on('scroll', function () {
                if ( maxScrollTop < $(this).scrollTop()) {
                    maxScrollTop = $(this).scrollTop();
                    $header.removeClass('header-scroll-top');
                    $('.catalog').removeClass('catalog-scroll-top');
                } else {
                    maxScrollTop = $(this).scrollTop();
                    $header.addClass('header-scroll-top');
                    $('.catalog').addClass('catalog-scroll-top');
                }
                if($(window).width() >= 250){
                    if ( $(window).scrollTop() > 0) {
                        $('.col-header-contacts').slideUp(150);
                        $header.addClass('header-fixed');
                    } else {
                        $('.col-header-contacts').slideDown(200);
                        $header.removeClass('header-fixed');
                    }
                } else{
                    if ( $(window).scrollTop() > 0) {
                        $header.addClass('header-fixed');
                    } else {
                        $header.removeClass('header-fixed');
                    }
                }
            });
        }

    var mobMenu = $('.mob-menu__main-nav .navbar-nav .nav-sub-item');
    mobMenu.each(function() {
        var _this = $(this);
        if(_this.children('ul.nav-child').length >=1){
            _this.addClass('dropdown')
        }
        _this.on('click', function() {
            _this.toggleClass('active')
        });
    });


    /**/
    var phone = $('#bx_basketFKauiI>.nav_service>.head-phone-number').text();
    $('.c-js-set-phone-mon').text(phone);
    $('.c-js-set-phone-mon').attr('href', 'tel:'+phone);

    $('#bx_basketFKauiI>.nav_service>.head-phone-number').append("<p class='c-phone-add-js'>Бесплатно по России</p>");
    /**/


    var blockMenu = false;
    $('#navbarCatalog .nav-tabs a').on('click', function (event) {
        //event.stopPropagation();
        $(this).parents('.dropdown').addClass('keep-show');
    });
    $('#navbarCatalog .dropdown').on('hide.bs.dropdown', function (e) {
        if ($(e.target).hasClass('keep-show')) {
            $(e.target).removeClass('keep-show');
            return false;
        }
    });
    $('[data-toggle="tooltip"]').tooltip();

    lazyReinit();

    window.addEventListener("popstate",function(e){
        setTimeout(lazyReinit, 1000);
    },false);

    window.onresize = function () {
        restructureMenu();
        sliderSwitch();
    };

    $('.js-owl-banner').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: true,
        smartSpeed: 500,
        items: 1,
        navText: ["<i class=\"far fa-angle-left\"></i>", "<i class=\"far fa-angle-right\"></i>"]
    });

    $('.js-owl-reviews').owlCarousel({
        loop: false,
        margin: 0,
        nav: true,
        dots: false,
        smartSpeed: 500,
        items: 1,
        navText: ["<i class=\"far fa-long-arrow-left\"></i>", "<i class=\"far fa-long-arrow-right\"></i>"]
    });

    $('.js-owl-slider').owlCarousel({
        loop: false,
        margin: 20,
        nav: true,
        dots: false,
        smartSpeed: 500,
        items: 4,
        navText: ["<i class=\"far fa-long-arrow-left\"></i>", "<i class=\"far fa-long-arrow-right\"></i>"],
        responsive: {
            0: {
                items: 2
            },
            750: {
                items: 3
            },
            1200: {
                items: 3
            },
            1400: {
                items: 4
            }
        }
    });

    $('.js-owl-country').owlCarousel({
        loop: false,
        margin: 0,
        nav: true,
        dots: false,
        smartSpeed: 500,
        items: 4,
        navText: ["<i class=\"far fa-long-arrow-left\"></i>", "<i class=\"far fa-long-arrow-right\"></i>"],
        responsive: {
            0: {
                items: 2
            },
            750: {
                items: 2
            },
            1200: {
                items: 3
            },
            1400: {
                items: 4
            }
        }

    });


    function sliderSwitch() {
        if (window.innerWidth < 993) {
            if (window.innerWidth < 575) {
                $('.js-owl-mobile').each(function () {
                    var items = $(this).data('mobile-items') ? $(this).data('mobile-items') : 2,
                        dots = $(this).data('mobile-dots') ? true : false;
                    $(this).addClass('owl-carousel owl-theme');
                    $(this).owlCarousel({
                        loop: false,
                        margin: 0,
                        nav: true,
                        dots: dots,
                        smartSpeed: 500,
                        items: items,
                        navText: ["", ""]
                    });
                });
            } else {
                $('.js-owl-tablet').each(function () {
                    var items = $(this).data('tablet-items') ? $(this).data('tablet-items') : 2,
                        dots = $(this).data('tablet-dots') ? true : false;
                    $(this).addClass('owl-carousel owl-theme');
                    $(this).owlCarousel({
                        loop: false,
                        margin: 0,
                        nav: true,
                        dots: dots,
                        smartSpeed: 500,
                        items: items,
                        navText: ["", ""]
                    });
                });
            }
        } else if ($('.js-owl-tablet, .js-owl-mobile').hasClass('owl-carousel')) {
            $('.js-owl-tablet, .js-owl-mobile').removeClass('owl-carousel owl-theme');
            $('.js-owl-tablet, .js-owl-mobile').trigger('destroy.owl.carousel');
        }
    }

    function restructureMenu() {
        if (window.innerWidth > 992) {
            if (!blockMenu) {
                while ($('#navbarCatalog').width() > $('#navbarCatalog').parent().width()) {
                    $('#navbarCatalog .nav-item-more').prev().appendTo($('#navbarCatalog .nav-item-more .dropdown-menu'));
                }
                if (!$('#navbarCatalog .nav-item-more .dropdown-menu').find('li').length) {
                    $('#navbarCatalog .nav-item-more').css('display', 'none');
                } else {
                    $('#navbarCatalog .nav-item-more').css('display', 'list-item');
                }
            }
            $('#navbarCatalog .navbar-nav > .nav-item.dropdown').each(function () {
                var width = $(window).width(), $whatever = $(this).find('.dropdown-menu'), rt = $(this).offset().left + $whatever.outerWidth();
                if (width < rt) {
                    $whatever.removeClass('dropdown-menu-left').css('margin-left', '-' + (rt - width) + 'px');
                }
            });
        } else {
            if (!$('#navbarCatalog .nav-item-more .dropdown-menu').find('li').length) {
                $('#navbarCatalog .nav-item-more').css('display', 'none');
            } else {
                $('#navbarCatalog .nav-item-more').css('display', 'list-item');
            }
            /*$('#navbarCatalog .nav-item-more .dropdown-menu .nav-item').appendTo($('#navbarCatalog .navbar-nav'));
            $('#navbarCatalog .nav-item-more').hide();
            $('.nav_top .nav-item').appendTo($('#navbarCatalog .navbar-nav'));
            blockMenu = true;
            $('.nav-item.dropdown > .nav-link').click(function () {
                if (!$(this).parent().hasClass('nav-item_sale')) {
                    //$(this).siblings('.dropdown-menu').toggleClass('show');
                    return false;
                }
            });*/
        }
    }

    sliderSwitch();
    restructureMenu();

    $(".js-bootstrap-carousel").on('swipeleft', function () {
        alert();
        $(this).carousel('prev');
    });
    $(".js-bootstrap-carousel").on('swiperight', function () {
        $(this).carousel('next');
    });

    $('.dropdown-radio').find('input').change(function () {
        var dropdown = $(this).closest('.dropdown');
        var radioname = $(this).attr('name');
        var checked = 'input[name=' + radioname + ']:checked';

        //update the text
        var checkedtext = $(checked).closest('.dropdown-radio').text();
        dropdown.find('button').text(checkedtext).addClass('selected');

        //retrieve the checked value, if needed in page
        var thisvalue = dropdown.find(checked).val();
    });
    $(document).on('click', '.js-show-filter', function () {
        $('.filter').show();
    });
    $(document).on('click', '.js-hide-filter', function () {
        $('.filter').hide();
    });

    $(document).on('change', '.js-product-delivery', function () {
        $('#product-delivery').html(productDelivery[$(this).val()].text);
    });

    $(document).on('click', '[href="#queryPriceModal"]', function () {
        $('.js-query-price-input').val($(this).data('product'));
    });

    initDetailSlider();
    var jsFooterMenu = $('.js-footer-menu');
    jsFooterMenu.each(function() {
        var _this = $(this);
        _this.on('click', function() {
            if(_this.hasClass('active')){
                _this.removeClass('active');
            } else {
                _this.addClass('active');
            }
        })
    });

    lightGallery(document.querySelector('.js-lightgallery'), {
        plugins: [lgZoom, lgThumbnail, lgFullscreen],
        speed: 500,
        selector: '.js-lightgallery-item'
    });
});

function initDetailSlider() {
    var $thumbGalleryDetail1 = $('.js-owl-gallery-detail'),
        $thumbGalleryThumbs1 = $('.js-owl-gallery-thumbs'),
        flag = false,
        duration = 300;

    $thumbGalleryDetail1
        .owlCarousel({
            items: 1,
            margin: 0,
            nav: false,
            dots: false,
            loop: false,
            autoHeight: true,
            navText: []
        })
        .on('changed.owl.carousel', function (e) {
            if (!flag) {
                flag = true;
                $thumbGalleryThumbs1.trigger('to.owl.carousel', [e.item.index - 1, duration, true]);
                flag = false;
            }
        });

    $thumbGalleryThumbs1
        .owlCarousel({
            margin: 15,
            items: 4,
            nav: false,
            center: false,
            dots: false
        })
        .on('click', '.owl-item', function () {
            $thumbGalleryDetail1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
        })
        .on('changed.owl.carousel', function (e) {
            if (!flag) {
                flag = true;
                $thumbGalleryDetail1.trigger('to.owl.carousel', [e.item.index, duration, true]);
                flag = false;
            }
        });
    // $('.js-owl-gallery-detail').lightGallery({
    //     selector: '[data-entity]'
    // });

    if(typeof(BX) == 'function' && typeof(BX.addCustomEvent) == 'function'){
        BX.addCustomEvent('onAjaxSuccess', function(){


            if(typeof(favoriteScriptInit) == "function"){
                favoriteScriptInit();
            }
        });
    }



}

function includeCss(file){

    var link = document.createElement("link");
    link.setAttribute("rel", "stylesheet");
    link.setAttribute("type", "text/css");
    link.setAttribute("href", file);
    document.getElementsByTagName("head")[0].appendChild(link);
    return link;

}
function includeJs(file) {

    var script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("defer", "defer");
    script.setAttribute("src", file);
    document.getElementsByTagName("head")[0].appendChild(script);
    return script;
}

document.addEventListener("DOMContentLoaded", function () {

    $(document).on('click', '.js-scroll-to', function () {
        let $this = $(this);
        let $this_target = $($this.attr('data-target'));
        let $this_offset = $this.attr('data-offset') || 0;

        $('html, body').animate({
            scrollTop: $this_target.offset().top - $this_offset
        }, 500);
    });

    const $scroll_top = $('.js-scroll-top');
    let _windowHeight = window.innerHeight;

    $(window).on('scroll', function () {
        if ($(this).scrollTop() > _windowHeight) {
            $scroll_top.addClass('active');
        } else {
            $scroll_top.removeClass('active');
        }
    });

});



