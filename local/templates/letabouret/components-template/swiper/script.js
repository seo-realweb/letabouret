"use strict";

window.addEventListener('load', function (event) {
  $('[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var $toggle = $(this);
    var $tab = $($toggle.attr('data-target'));
    var $slider_arr = $tab.find('.js-swiper-slider');
    $slider_arr.each(function (i, el) {
      el.swiper && el.swiper.update && el.swiper.update();
      var $lazy = $(el).find('.swiper-lazy:not(.loaded)');
      $lazy.each(function (i, el) {
        var $img = $(el);

        if (!$img.hasClass('loaded') && !$img.hasClass('loading')) {
          var img_active_lazy_load = new LazyLoad({
            threshold: 0
          }, $(el));
          img_active_lazy_load.load(el, 'force');
        }
      });
    });
  });
});