<?

use \Bitrix\Main\Page\Asset,
    \Realweb\Site\Site,
    \Slam\Base\CSlamAsset;

CModule::IncludeModule("slam.base");

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
<?
    if(!empty($_GET)&&empty($_GET['PAGEN_1']) && strpos($APPLICATION->GetCurPage(),"/filter/")===false) {
        $APPLICATION->SetPageProperty("robots", "noindex, follow");
		$APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER["SERVER_NAME"] . $APPLICATION->GetCurPage(false) . '" />', true);
    }
    
    ?>
    <? $APPLICATION->ShowHead(); CSlamAsset::getInstance()->showHead();?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
<!--<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">-->
	<meta name='wmail-verification' content='9a1fac771df96b59eba4a78f2def0450' />
    <meta name="yandex-verification" content="ae70925c7535478f" />
    <meta name="format-detection" content="telephone=no"/>
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, initial-scale=1.0">

	<link rel="manifest" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/site.webmanifest">
    <link rel="icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/favicon.ico" type="image/x-icon sizes="any">
    <link rel="icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/favicon-32x32.png" type="image/x-icon" sizes="32x32">
    <link rel="icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/favicon-16x16.png" type="image/x-icon" sizes="16x16">
    <link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/favicon-32x32.png" type="image/x-icon">
    <meta name="msapplication-config" content="<?= SITE_TEMPLATE_PATH ?>/images/favicon/browserconfig.xml">
    <link rel="apple-touch-icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon/apple-touch-icon.png">
    <link rel="mask-icon" href="<?= SITE_TEMPLATE_PATH ?>/images/faviconsafari-pinned-tab.svg" color="#00а0ff">
    <link rel="preload" href="/local/templates/letabouret/css/../fonts/ProximaNova-Regular.woff" as="font" type="font/woff2" crossorigin/>
    <link rel="preload" href="/local/templates/letabouret/css/../fonts/ProximaNova-Bold.woff" as="font" type="font/woff2" crossorigin/>
    <link rel="preload" href="/local/templates/letabouret/css/../fonts/ProximaNova-Semibold.woff" as="font" type="font/woff2" crossorigin/>
    <link rel="preload" href="/local/templates/letabouret/css/../fonts/ProximaNova-Light.woff" as="font" type="font/woff2" crossorigin/>
    <link rel="preload" href="/local/templates/letabouret/vendor/fontawesome-pro-5.5.0-web/webfonts/fa-regular-400.woff2" as="font" type="font/woff2" crossorigin/>

    <script data-skip-moving="true">
        var meta = document.querySelector("meta[name='viewport']");
        var windowWidth = (window.innerWidth < window.screen.width) ? window.innerWidth : window.screen.width;
        if (windowWidth < 768) {
            meta.setAttribute("content", "width=540, user-scalable=no");
        }
        window.App = window.App || {};
        App.BX = {TEMPLATE_PATH: '<?= SITE_TEMPLATE_PATH ?>'};
    </script>
    <?
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/vendor/bootstrap/css/bootstrap.min.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/vendor/fontawesome-pro-5.5.0-web/css/all.min.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/vendor/owl.carousel/dist/assets/owl.carousel.min.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/vendor/owl.carousel/dist/assets/owl.theme.default.min.css');

    if (!Site::isCheckGooglePageSpeed()) {
        Asset::getInstance()->addCss('https://cdn.jsdelivr.net/npm/lightgallery@2.0.1/css/lightgallery-bundle.min.css');
    }

    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/main.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/media.css');

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/vendor/jquery/jquery.min.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/vendor/bootstrap/js/bootstrap.bundle.min.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/vendor/owl.carousel/dist/owl.carousel.min.js');

    if (!Site::isCheckGooglePageSpeed()) {
        Asset::getInstance()->addJs('https://cdn.jsdelivr.net/npm/lightgallery@2.0.1/lightgallery.umd.js');
        Asset::getInstance()->addJs('https://cdn.jsdelivr.net/npm/lightgallery@2.0.1/plugins/thumbnail/lg-thumbnail.umd.js');
        Asset::getInstance()->addJs('https://cdn.jsdelivr.net/npm/lightgallery@2.0.1/plugins/fullscreen/lg-fullscreen.umd.js');
        Asset::getInstance()->addJs('https://cdn.jsdelivr.net/npm/lightgallery@2.0.1/plugins/zoom/lg-zoom.umd.js');
    }

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/vendor/lazyload/lazyload.min.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/scripts/common.js');
    ?>
    <?php if(!Site::isCheckGooglePageSpeed()): ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script data-skip-moving="true" async src="https://www.googletagmanager.com/gtag/js?id=UA-90722768-1"></script>
        <script data-skip-moving="true">
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-90722768-1');
        </script>
        <!-- Google Tag Manager -->
        <script data-skip-moving="true">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5CMR9K8');</script>
        <!-- End Google Tag Manager -->
    <?php endif; ?>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "WebPage",
            "name": "<? $APPLICATION->ShowTitle(); ?>",
            "description": "<?$APPLICATION->ShowProperty('description');?>",
            "publisher": {
                "@type": "ProfilePage",
                "name": "Интернет-магазин мебели Le Tabouret"
            }
        }
    </script>

</head>
<body>
<?php if (!Site::isCheckGooglePageSpeed()): ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5CMR9K8"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php endif; ?>
<div id="panel">
    <? $APPLICATION->ShowPanel(); ?>
</div>
<header>
    <div class="header js-header">

        <div class="container">
            <div class="row header-row">
                <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse"
                        data-target="#navbarMobCatalog" aria-controls="navbarMobCatalog" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <div class="navbar-toggler-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>

                <div class="mob-menu js-mob-menu">
                    <div class="container mob-menu-inner">

                        <div class="mob-menu__main-nav">
                            <div class="collapse navbar-collapse" id="navbarMobCatalog">
                                <?php if (!Site::isCheckGooglePageSpeed()): ?>
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:menu",
                                        "catalog_top_mobile",
                                        Array(
                                            "ALLOW_MULTI_SELECT" => "Y",
                                            "CHILD_MENU_TYPE" => "",
                                            "DELAY" => "N",
                                            "MAX_LEVEL" => "3",
                                            "MENU_CACHE_GET_VARS" => array(""),
                                            "MENU_CACHE_TIME" => "3600",
                                            "MENU_CACHE_TYPE" => "A",
                                            "MENU_CACHE_USE_GROUPS" => "N",
                                            "ROOT_MENU_TYPE" => "catalog_top",
                                            "USE_EXT" => "Y"
                                        ),
                                        false
                                    ); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="mob-menu__small-nav">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "top",
                            Array(
                                "ALLOW_MULTI_SELECT" => "Y",
                                "CHILD_MENU_TYPE" => "",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_USE_GROUPS" => "N",
                                "ROOT_MENU_TYPE" => "top_mobile",
                                "USE_EXT" => "Y"
                            ),
                            false
                        ); ?>
                    </div>
                    </div>
                </div>
                <div class="header__logo">
                    <a href="/" class="header__logo-link">
                        <img alt="logo" src="<?= SITE_TEMPLATE_PATH ?>/images/logo.png" width="169px" height="88px">
                    </a>
                </div>

                <div class="col-header-contacts">
                    <?$APPLICATION->IncludeComponent(
                        "ammina:ip.selector",
                        "header",
                        array(
                            "CACHE_TIME" => "86400",
                            "CACHE_TYPE" => "N",
                            "CHANGE_CITY_MANUAL" => "Y",
                            "CITY_VERIFYCATION" => "Y",
                            "COUNT_SHOW_CITY" => "24",
                            "IP" => "",
                            "SEARCH_CITY_TYPE" => "Q",
                            "SHOW_CITY_TYPE" => "F",
                            "USE_GPS" => "Y",
                            "INCLUDE_JQUERY" => "N",
                            "COMPONENT_TEMPLATE" => "header"
                        ),
                        false
                    );?>
                    <div class="header__contacts">
                        <?$arCity = getCurrentCity();?>

                            <div class="col col-auto header__contact">
                                <? Site::showIncludeText('HEADER_CONTACTS_2_SPB') ?>
                            </div>
     <div class="col col-auto header__contact">
                                <? Site::showIncludeText('HEADER_CONTACTS_3_SPB') ?>
                            </div>

                       
                        <? Site::showIncludeText('WORK_TIME') ?>
                    </div>

                </div>
                <div class="d-lg-block">
                    <div class='small-menu hide-on-mob'>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "top",
                            Array(
                                "ALLOW_MULTI_SELECT" => "Y",
                                "CHILD_MENU_TYPE" => "",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_USE_GROUPS" => "N",
                                "ROOT_MENU_TYPE" => "top",
                                "USE_EXT" => "Y"
                            ),
                            false
                        ); ?>
                    </div>

                    <div class="header__search form">
                        <div class="js-open-mob-search search-icon hide-on-desktop"></div>
                        <form action="/search/">
                            <div class="js-close-mob-search search-icon-close hide-on-desktop"></div>
                            <div class="input-group">
                                <input type="text" name="q" class="form-control" placeholder="Поиск по сайту"
                                       aria-label="Введите то, что вы ищите, например 'диван'">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-nav-service">
                    <div class="col-nav-service__item nav-service--basket">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:sale.basket.basket.line",
                            ".default",
                            array(
                                "HIDE_ON_BASKET_PAGES" => "N",
                                "PATH_TO_BASKET" => SITE_DIR . "basket/",
                                "PATH_TO_ORDER" => SITE_DIR . "basket/",
                                "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                                "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                                "PATH_TO_REGISTER" => SITE_DIR . "register/",
                                "POSITION_FIXED" => "N",
                                "SHOW_AUTHOR" => "N",
                                "SHOW_EMPTY_VALUES" => "Y",
                                "SHOW_NUM_PRODUCTS" => "Y",
                                "SHOW_PERSONAL_LINK" => "N",
                                "SHOW_PRODUCTS" => "N",
                                "SHOW_TOTAL_PRICE" => "Y",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH_TO_AUTHORIZE" => SITE_DIR . "auth/",
                                "PATH_TO_FAVORITE" => SITE_DIR . "catalog/favorite/"
                            ),
                            false
                        ); ?>
                    </div>

                    <div class="col-nav-service__item nav-service--favorite">
                        <?$APPLICATION->IncludeComponent(
                            "slam:favorite.items",
                            "header",
                            array(
                                "ACTIVE_CLASS" => "active",
                                "BUTTON_CLASS" => "cat__favorite",
                                "FLY_ACTIVATE" => "Y",
                                "HIDE_ON_FAV_PAGES" => "N",
                                "PATH_TO_FAV_LIST" => SITE_DIR."favorites/",
                                "COMPONENT_TEMPLATE" => "header",
                                "AREA_CLASS" => "product__item",
                                "IMAGE_CLASS" => "product__image",
                                "TARGET_CLASS" => "fav-target"
                            ),
                            false
                        );?>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <nav class="navbar navbar-light navbar_catalog navbar-expand-md hide-on-mob">
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarCatalog">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "catalog_top",
                    Array(
                        "ALLOW_MULTI_SELECT" => "Y",
                        "CHILD_MENU_TYPE" => "",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "3",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "ROOT_MENU_TYPE" => "catalog_top",
                        "USE_EXT" => "Y"
                    ),
                    false
                ); ?>
            </div>
        </div>
    </nav>
</header>
<? if (!Site::isMainPage() && $APPLICATION->GetProperty('HIDE_BREADCRUMBS') != 'Y'): ?>
    <div class="section-bg">
        <div class=" <?= $APPLICATION->GetProperty('PAGE_CLASS_CONTAINER') ?$APPLICATION->GetProperty('PAGE_CLASS_CONTAINER'):'container'?>">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", Array(
                "PATH" => "",
                "START_FROM" => "0",
                "COMPONENT_TEMPLATE" => ".default"
            ), false
            ); ?>
        </div>
    </div>
<? endif; ?>
<? if (!Site::isMainPage() && $APPLICATION->GetProperty('HIDE_TITLE') != 'Y'): ?>
    <? $APPLICATION->AddBufferContent(array('\Realweb\Site\Site', 'showH1')); ?>
<? endif; ?>
<?if(!Site::isMainPage() && $APPLICATION->GetProperty('PAGE_TYPE') != 'empty'): ?>
<div class="<?= $APPLICATION->GetProperty('PAGE_CLASS_SECTION_BG') ?$APPLICATION->GetProperty('PAGE_CLASS_SECTION_BG'):'section-bg'?>">
    <div class="<?= $APPLICATION->GetProperty('PAGE_CLASS_CONTAINER') ?$APPLICATION->GetProperty('PAGE_CLASS_CONTAINER'):'container'?>">

    <? //$APPLICATION->AddBufferContent(array('Realweb\Site\Site', 'showContentPageContainerStart')); ?>
<? endif; ?>




