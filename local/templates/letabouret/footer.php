<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Page\Asset,
    \Realweb\Site\Site;
?>

<?if(!Site::isMainPage() && $APPLICATION->GetProperty('PAGE_TYPE') != 'empty'): ?>
    </div>
</div>

<? endif; ?>
<!--seo_text1-->
<footer class="section-bg footer">
    <div class="container">
        <div class="footer__line"></div>
        <div class="row footer__items">
            <div class="col footer-info pl-xl-5 pr-xl-5">
                <div class="footer__logo">
                    <a href="/">
                        <img alt="logo" src="<?= SITE_TEMPLATE_PATH ?>/images/logo.png">
                    </a>
                    <div class="footer__text">
                        <? Site::showIncludeText('FOOTER_TEXT') ?>
                    </div>
                </div>
            </div>
            <div class="col footer-menu-1 col-menu">
                <div class="footer__menu js-footer-menu">
                    <div class="footer__title">каталог</div>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "bottom",
                        Array(
                            "ALLOW_MULTI_SELECT" => "Y",
                            "CHILD_MENU_TYPE" => "",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(""),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_USE_GROUPS" => "N",
                            "ROOT_MENU_TYPE" => "catalog_bottom",
                            "USE_EXT" => "Y"
                        ),
                        false,
                        Array(
                            'HIDE_ICONS' => 'Y'
                        )
                    ); ?>
                </div>
            </div>
            <div class="col footer-menu-2 col-menu">
                <div class="footer__menu js-footer-menu">
                    <div class="footer__title">Информация</div>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "bottom",
                        Array(
                            "ALLOW_MULTI_SELECT" => "Y",
                            "CHILD_MENU_TYPE" => "",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(""),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_USE_GROUPS" => "N",
                            "ROOT_MENU_TYPE" => "bottom",
                            "USE_EXT" => "Y"
                        ),
                        false,
                        Array(
                            'HIDE_ICONS' => 'Y'
                        )
                    ); ?>
                </div>
            </div>
            <div class="col footer-menu-2 col-menu">
                <div class="footer__menu js-footer-menu">
                    <div class="footer__title">Популярные</div>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "bottom",
                        Array(
                            "ALLOW_MULTI_SELECT" => "Y",
                            "CHILD_MENU_TYPE" => "",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(""),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_USE_GROUPS" => "N",
                            "ROOT_MENU_TYPE" => "bottom_pop",
                            "USE_EXT" => "Y"
                        ),
                        false,
                        Array(
                            'HIDE_ICONS' => 'Y'
                        )
                    ); ?>
                </div>
            </div>
            <div class="col footer-contacts d-none d-md-block">
                <div class="footer__title">Контакты</div>
                <div class="row">
                    <div class="col col-6 col-md-12">
                        <? Site::showIncludeText('FOOTER_CONTACTS_1') ?>
                    </div>
                    <div class="col col-6 col-md-12">
                        <? Site::showIncludeText('FOOTER_CONTACTS_2') ?>
                    </div>
                </div>
            </div>
            <div class="col footer-subscribe col-auto">
                <div class="form">
                    <div class="footer__title">Подписка на новости</div>
                    <? $APPLICATION->IncludeComponent("realweb:subscribe.form", "", array("RUB_ID" => "1", "AJAX_MODE" => 'Y', "AJAX_OPTION_JUMP" => "N")); ?>
                </div>
                <? Site::showIncludeText('SOCIAL') ?>
                <div class="text-center text-md-left">
                    <div class="d-md-none mb-2">Мы принимаем к оплате</div>
                    <img alt="payment" class="lazy-img footer__payment" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" data-src="<?= SITE_TEMPLATE_PATH ?>/images/payment-all.png">
                </div>
            </div>
        </div>
        <div class="copyright mt-4">&copy; <?=date('Y');?>. Все права защищены.</div>
    </div>
	<?php if(!Site::isCheckGooglePageSpeed()): ?>
        <!-- BEGIN JIVOSITE CODE {literal} -->
        <script src="//code-eu1.jivosite.com/widget/aLQUq7fQqn" async></script>
        <!-- {/literal} END JIVOSITE CODE -->
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            document.addEventListener("DOMContentLoaded", function () {

                let ymInitTimeout,
                    ymInitInitFlag = false,
                    width = window.innerWidth
                        || document.documentElement.clientWidth
                        || document.body.clientWidth,
                    ymInit = function () {
                        ymInitInitFlag = true;
                        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
                        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
                        ym(23414974, "init", {
                                id:23414974,
                                clickmap: true,
                                trackLinks: true,
                                accurateTrackBounce: true,
                                webvisor: true}
                        );

                        clearTimeout(ymInitTimeout);
                    },
                    ymInitOnEvent = function () {
                        if (!ymInitInitFlag) {
                            clearTimeout(ymInitTimeout);
                            ymInit();
                        }
                    };


                //if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){}
                //if (width <= 600)
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    let firstView = readCookie('var_cookie');
                    $('body').addClass('no-touch');

                    function writeCookie(name, val, expires) {
                        let date = new Date();
                        date.setDate(date.getDate() + expires);
                        document.cookie = name + "=" + val + "; path=/; expires=" + date.toUTCString();
                    }
                    function readCookie(name) {
                        let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
                        return matches ? decodeURIComponent(matches[1]) : undefined;
                    }

                    if (firstView == undefined) {
                        $('.modal--cookie').addClass('visible');
                        // срок 30 дней
                        writeCookie('var_cookie', 'active', 30);
                    } else {
                        $('body').removeClass('no-touch');
                        ymInitOnEvent();
                    }
                    //ymInitTimeout = setTimeout(ymInit, 10000);


                    $('.modal--cookie .close').on('click touch', function () {
                        $('.modal--cookie').removeClass('visible');
                        $('body').removeClass('no-touch');
                        ymInitOnEvent();
                    });
                    $('.modal--cookie .basket__button').on('click touch', function () {
                        $('.modal--cookie').removeClass('visible');
                        $('body').removeClass('no-touch');
                        ymInitOnEvent();
                    });
                    //document.addEventListener("scroll", ymInitOnEvent);
                    //document.addEventListener("click", ymInitOnEvent);
                    //document.addEventListener("touchstart", ymInitOnEvent);
                } else {
                    ymInit();
                }
            });
        </script>
        <noscript>
            <div><img src="https://mc.yandex.ru/watch/23414974" style="position:absolute; left:-9999px;" alt=""/></div>
        </noscript>
        <!-- /Yandex.Metrika counter -->
    <?php endif; ?>
</footer>
<div class="scroll-top js-scroll-top js-scroll-to" data-target="body">
    <svg class="icon scroll-top__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" id="arrow"><path fill-rule="evenodd" clip-rule="evenodd" d="M9 7.508L13.486 12 9 16.492 10.507 18l5.993-6-5.993-6L9 7.508z"></path></svg>
</div>

<div class="modal" id="queryPriceModal" tabindex="-1" role="dialog">
    <? $APPLICATION->IncludeComponent(
        "bitrix:form.result.new", "modal", Array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "Y",
        "VARIABLE_ALIASES" => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
        "WEB_FORM_ID" => WEB_FORM_QUERY_PRICE,
        'AJAX_MODE' => 'Y',
    ), false
    ); ?>
</div>

<div class="modal modal--cookie" id="cookieModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog_order" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <span class="popup-window-close-icon popup-window-titlebar-close-icon close" data-dismiss="modal"></span>
                <div class="form-group modal-title">
                    Этот сайт использует файлы cookie для хранения данных. Продолжая использовать сайт, Вы даете согласие на работу с этими файлами.
                </div>
                <div class="form-group form-group_submit">
                    <a href="javascript:;" class="basket__button">Ок</a>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>