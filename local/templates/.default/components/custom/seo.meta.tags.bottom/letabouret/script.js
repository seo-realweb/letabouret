$(document).ready(function () {
    let tags = $('.sotbit-seometa-tag-link');
    let newTags = tags.filter(function (i) {
        return $(this).position().top > 50
    })

    if(newTags.length > 0 ){
        newTags.wrapAll("<div class='hidden-clouds'></div>");
        $('.show-clouds').css({'display':'block'});
    }
    $('.show-clouds').click(function () {
        $('.hidden-clouds').slideToggle();
    })
})
