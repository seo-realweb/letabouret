<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div style="position: relative">
<?
if($arResult['ITEMS'])
{
    foreach($arResult['ITEMS'] as $Item)
    {
        if($Item['TITLE'] && $Item['URL'])
        {
            ?>
                <a class="sotbit-seometa-tag-link" href="<?=$Item['URL'] ?>" title="<?=$Item['TITLE'] ?>"><?=$Item['TITLE'] ?></a>
            <?
        }
    }
}
?>
</div>
<div class="show-clouds">Показать еще</div>