<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arPropertyId = array_merge($arResult['PROPERTY_ID_LIST'], $arResult['SKU_PROPERTY_ID_LIST']);

$dbres = \Bitrix\Iblock\PropertyTable::getList([
    'order' => array('SORT' => 'asc'),
    "filter" => array("ACTIVE" => "Y", 'ID' => $arPropertyId),
    'select' => array('ID', 'SORT')
]);
$arResult['ORDERED_PROPERTIES'] = array();

while ($result = $dbres->fetch()) {

    $arResult['ORDERED_PROPERTIES'][] = $result;
};

$ru = explode('?', $_SERVER['REQUEST_URI']);

if ((strpos($ru[0], 'style/') > 0) || (strpos($ru[0], 'collections/') > 0)) {
//Фильтры для стилей коллекций и стран
    $act_p = [104, 8, 80, 51];

    foreach ($arResult['ORDERED_PROPERTIES'] as $key => $prop) {

        if (!in_array($prop['ID'], $act_p)) {

            unset($arResult['ORDERED_PROPERTIES'][$key]);
        }

    }
}

if ($arParams['SECTION_ID'] == 318) {
    //Распродажа
    $act_p = [147, 8, 80, 51];

    foreach ($arResult['ORDERED_PROPERTIES'] as $key => $prop) {

        if (!in_array($prop['ID'], $act_p)) {

            unset($arResult['ORDERED_PROPERTIES'][$key]);
        }

    }
}

$arResult['ORDERED_PROPERTIES'][] = [
    'ID' => 'BASE',
    'SORT' => 399
];


usort($arResult['ORDERED_PROPERTIES'], function ($a, $b) {
    if ($a['SORT'] == $b['SORT']) {
        return 0;
    }
    return ($a['SORT'] < $b['SORT']) ? -1 : 1;
});


$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";

$arFilterItems = array();
foreach ($arResult['ITEMS'] as $arItem) {
    $arCurrentValues = array();
    foreach ($arItem['VALUES'] as $arValue) {
        if (!empty($arValue['CHECKED'])) {
            $arUrl = explode("/", $arResult['SEF_SET_FILTER_URL']);
            $strFilterCode = strtolower($arItem['CODE']) . '-is-';
            foreach ($arUrl as $i => &$strUrlValue) {
                if (stripos($strUrlValue, $strFilterCode) !== false) {
                    $arFilterValues = explode("-or-", str_replace($strFilterCode, '', $strUrlValue));
                    foreach ($arFilterValues as $j => $value) {
                        if ($value == $arValue['URL_ID']) {
                            unset($arFilterValues[$j]);
                        }
                    }
                    if ($arFilterValues) {
                        $strUrlValue = $strFilterCode . implode('-or-', $arFilterValues);
                    } else {
                        unset($arUrl[$i]);
                    }
                }
            }
            $strUrl = implode("/", $arUrl);
            if (mb_strpos($arResult['SEF_DEL_FILTER_URL'], $strUrl) !== false) {
                $strUrl = $arResult['SEF_DEL_FILTER_URL'];
            }
            if ($arValue['VALUE'] == 'Y') {
                $strValue = 'Да';
            } elseif ($arValue['VALUE'] == 'N') {
                $strValue = 'Нет';
            } else {
                $strValue = $arValue['VALUE'];
            }
            $arCurrentValues[] = array(
                'value' => str_replace('.','', $strValue),
                'url' => $strUrl
            );
        }
    }
    if ($arCurrentValues) {
        $arFilterItems[] = array(
            'name' => $arItem['NAME'],
            'values' => $arCurrentValues
        );
    }
}
if ($arFilterItems) {
    $APPLICATION->SetPageProperty('clear_filter_url', $arResult['SEF_DEL_FILTER_URL']);
    $APPLICATION->SetPageProperty('filter_items', $arFilterItems);
    $arResult['FILTER_VALUES'] = $arFilterItems;
}