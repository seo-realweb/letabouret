<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */

$arProps = array($item['PROPERTIES']['ARTICUL']['VALUE'], $item['PROPERTIES']['DIMENSIONS']['VALUE']);
if ($item['PROPERTIES']['COUNT_COLOR']['VALUE']) {
    $arProps[] = $item['PROPERTIES']['COUNT_COLOR']['NAME'] . ': ' . $item['PROPERTIES']['COUNT_COLOR']['VALUE'];
}

if (current($item['MORE_PHOTO'])['SRC'] || $item['PREVIEW_PICTURE']['SRC']) {
    if (current($item['MORE_PHOTO'])['SRC']) {
        $pictureArray = current($item['MORE_PHOTO']);
        $flag = "MORE_PHOTO";
    } else {
        $pictureArray = $item['PREVIEW_PICTURE'];
        $flag = "PREVIEW_PICTURE";
    }

    if (CModule::IncludeModule("slam.image")) {
        $resizedPicture = CSlamImage::ResizeImageGet(
            $pictureArray['ID'],
            array("width" => 360, "height" => 250),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );
    } else {
        $resizedPicture = CFile::ResizeImageGet(
            $pictureArray['ID'],
            array("width" => 360, "height" => 250),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );
    }

    $resizedArray['SRC'] = $resizedPicture['src'];
    $resizedArray['WIDTH'] = $resizedPicture['width'];
    $resizedArray['HEIGHT'] = $resizedPicture['height'];

    if ($flag == "MORE_PHOTO") {
        $item['MORE_PHOTO'][0] = $resizedArray;
    } elseif ($flag == "PREVIEW_PICTURE") {
        $item['PREVIEW_PICTURE'] = $resizedArray;
    }
}

if (!$haveOffers && $item['OFFERS'][$item['OFFERS_SELECTED']]) {


    foreach ($actualItem['PROPERTIES'] as $arProperty) {

        /*Пропускаем привязку к товару*/
        if ($arProperty['CODE'] == 'CML2_LINK' || $arProperty['CODE'] == 'WIDTH' || $arProperty['CODE'] == 'LENGTH') {
            continue;
        }
        /*Пропускаем пустые занчения*/
        if (!$arProperty['VALUE'] && !is_array($arProperty['VALUE'])) {
            continue;
        }

        $arProps[] = $arProperty['VALUE'];

    }
    if ($actualItem['PROPERTIES']['WIDTH']['VALUE'] && $actualItem['PROPERTIES']['LENGTH']['VALUE']) {
        $size = '';
        $size .= $actualItem['PROPERTIES']['WIDTH']['VALUE'] * 10 . '*';
        $size .= $actualItem['PROPERTIES']['LENGTH']['VALUE'] * 10 . '*';
        $size .= str_replace(' см', '', $item['PROPERTIES']['HEIGTH']['VALUE']) * 10;
    }
}

$arProps[] = $size;


?>

<div class="product__picture">
    <div class="product__labels like">
        <div class="product__label ">
            <a class="cat__favorite" href="javascript:void(0);" data-id="<?= $item['ID'] ?>">
                <i class=" far fa-heart"></i>
            </a>
        </div>
        <? if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y'): ?>
            <div class="product__label product__label_sale"
                 style="display: <?= ($price['PERCENT'] > 0 ? '' : 'none') ?>;" id="<?= $itemIds['DSC_PERC'] ?>">
                <?= -$price['PERCENT'] ?>%
            </div>
        <? endif; ?>
        <? if ($item['PROPERTIES']['NEWPRODUCT']['VALUE']): ?>
            <div class="product__label product__label_new">NEW</div>
        <? endif; ?>

    </div>
    <? if ($item['PROPERTIES']['TAG']['VALUE']): ?>
        <?= \Realweb\Site\Site::showTags($item['PROPERTIES']['TAG']['VALUE']) ?>
    <? endif; ?>
    <? if (!empty($item['~DETAIL_PAGE_URL']) && $item['~DETAIL_PAGE_URL'] != "/") {
        $detail_url = $item['~DETAIL_PAGE_URL'];
    } else {
        $detail_url = $item['DETAIL_PAGE_URL'];
    } ?>
    <? if ($_GET['bxajaxid']): ?>
        <a target="_self" href="<?= $detail_url ?>" id="<?= $itemIds['PICT'] ?>"><img class="product__image"
                                                                       src="<?= $item['MORE_PHOTO'] ? current($item['MORE_PHOTO'])['SRC'] : $item['PREVIEW_PICTURE']['SRC'] ?>"
                                                                       alt="<?= $actualItem['NAME'] ?>"></a>
    <? else: ?>
        <a target="_self" href="<?= $detail_url ?>" id="<?= $itemIds['PICT'] ?>"><img class="product__image lazy-img"
                                                                       src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                       data-src="<?= $item['MORE_PHOTO'] ? current($item['MORE_PHOTO'])['SRC'] : $item['PREVIEW_PICTURE']['SRC'] ?>"
                                                                       alt="<?= $actualItem['NAME'] ?>"></a>
    <? endif; ?>

    <span id="<?= $itemIds['PICT_SLIDER'] ?>"></span>
</div>
<div class="product__content">
    <div class="product__about">
        <a target="_self" class="product__title" href="<?= $detail_url ?>">
            <div class="product__brand"><?= $item['SECTION_NAME'] ?></div>
            <div class="product__name"><?= $productTitle ?></div>
        </a>

        <?
        //Прячем артикул
        unset($arProps[0]);
        //Выводим Габариты для кроватей
        $page = $APPLICATION->GetCurPage();

        if (strpos($page, 'mebel/krovati-meb') != false && trim($arProps[1]) != '') {
            $arProps[1] = 'Габариты: ' . $arProps[1];
        }

        ?>
        <div class="product__tags"><?= implode(", ", array_diff($arProps, array(''))) ?></div>
    </div>
    <div class="row row-10 align-items-center product__action" data-entity="price-block">
        <div class="col col-auto">
            <div class="product__query-price"><i class="fal fa-ruble-sign"></i></div>
        </div>
        <div class="col">

            <?
            if ($price['CURRENCY'] != 'RUB') {

                $price['PRINT_RATIO_PRICE'] = CurrencyFormat(round(CCurrencyRates::ConvertCurrency($price['RATIO_PRICE'], "USD", "RUB")),"RUB");
                $price['PRINT_RATIO_BASE_PRICE'] = CurrencyFormat(round(CCurrencyRates::ConvertCurrency($price['RATIO_BASE_PRICE'], "USD", "RUB")),"RUB");

            }

            ?>
            <? if ($price['RATIO_PRICE'] > 0 && !$item['PROPERTIES']['QUERYPRICE']['VALUE']): ?>
                <div class="product__price">
                    <? if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
                        ?>
                        <span class="price-old" id="<?= $itemIds['PRICE_OLD'] ?>"
                            <?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') ?>>
								<?= $price['PRINT_RATIO_BASE_PRICE'] ?>
                        </span>
                        <?
                    } ?>
                    <span class="<?= $price['RATIO_PRICE'] < $price['RATIO_BASE_PRICE'] ? 'price-new' : '' ?>"
                          id="<?= $itemIds['PRICE'] ?>">
                        <? if (!empty($price)) {
                            if ($item['PROPERTIES']['PRICE_OT']['VALUE'] || $haveOffers) {
                                echo Loc::getMessage(
                                    'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                                    array(
                                        '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                                        '#VALUE#' => $measureRatio,
                                        '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                                    )
                                );
                            } else {
                                echo $price['PRINT_RATIO_PRICE'];
                            }
                        } ?>
                    </span>
                </div>
            <? else: ?>
                <a class="product__query" data-toggle="modal"
                   data-product="<?= "[" . $item['ID'] . "] " . $productTitle ?>" href="#queryPriceModal">Запросить
                    цену</a>
            <? endif; ?>
        </div>
    </div>
</div>