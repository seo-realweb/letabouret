<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */

if(CModule::IncludeModule("slam.image")){
    $resizedPicture = CSlamImage::ResizeImageGet(
        $item['PREVIEW_PICTURE']['ID'],
        array("width"=> 360,"height" => 250),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    );
}
else{
    $resizedPicture = CFile::ResizeImageGet(
        $item['PREVIEW_PICTURE']['ID'],
        array("width"=> 360,"height" => 250),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    );
}

$item['PREVIEW_PICTURE']['SRC'] = $resizedPicture['src'];
$item['PREVIEW_PICTURE']['WIDTH'] = $resizedPicture['width'];
$item['PREVIEW_PICTURE']['HEIGHT'] = $resizedPicture['height'];

?>

<div class="special-product__picture">
    <a href="<?= $item['DETAIL_PAGE_URL'] ?>" id="<?= $itemIds['PICT'] ?>">
        <?if($_GET['bxajaxid']):?>
            <img src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>">
        <?else:?>
            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" data-src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>" alt="<?=$item['NAME']?>">
        <?endif;?>
        <!--<div class="special-product__hint">
            <p><?/*= $productTitle */?></p>
            <?/*= $item['PROPERTIES']['DIMENSIONS']['VALUE'] ? ('<p>' . $item['PROPERTIES']['DIMENSIONS']['VALUE'] . '</p>') : '' */?>
            <?/* if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
                */?>
                <span class="price-old" id="<?/*= $itemIds['PRICE_OLD'] */?>"
                    <?/*= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') */?>>
								<?/*= $price['PRINT_RATIO_BASE_PRICE'] */?>
                        </span>
                <?/*
            } */?>
            <span class="<?/*= $price['RATIO_PRICE'] < $price['RATIO_BASE_PRICE'] ? 'price-new' : '' */?>"
                  id="<?/*= $itemIds['PRICE'] */?>">
                    <?/* if (!empty($price)) {
                        if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                            echo Loc::getMessage(
                                'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                                array(
                                    '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                                    '#VALUE#' => $measureRatio,
                                    '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                                )
                            );
                        } else {
                            echo $price['PRINT_RATIO_PRICE'];
                        }
                    } */?>
                </span>
        </div>-->
        <span id="<?= $itemIds['PICT_SLIDER'] ?>"></span>
    </a>
<!--    <a class="special-product__add" href="--><?//= $item['DETAIL_PAGE_URL'] ?><!--"><i class="fal fa-plus"></i></a>-->
</div>
<div class="special-product__content">
    <a class="special-product__title" href="<?= $item['DETAIL_PAGE_URL'] ?>">
        <?= $productTitle ?>
    </a>
    <div class="special-product__price">
        <? if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
            ?>
            <span class="price-old <?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'price-old--hidden' : '') ?>" id="<?= $itemIds['PRICE_OLD'] ?>"
                    <?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="opacity: 0;"' : '') ?>>
								<?= $price['PRINT_RATIO_BASE_PRICE'] ?>
                        </span>
            <?
        } ?>

        <span class="price"><? if (!empty($price)) {
            if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                echo Loc::getMessage(
                    'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                    array(
                        '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                        '#VALUE#' => $measureRatio,
                        '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                    )
                );
            } else {
                echo $price['PRINT_RATIO_PRICE'];
            }
        } ?>
        </span>
    </div>
</div>