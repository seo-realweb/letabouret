<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/**
 * @global CMain $APPLICATION
 */
global $APPLICATION;

//delayed function must return a string
if (empty($arResult))
    return "";

$class = "";

foreach ($arResult as &$value)
{
    if ($value['LINK'] === "/")
        $value['TITLE'] = "Главная";
}

$strReturn = '';

$strReturn .= '<nav aria-label="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList" ><ol class="breadcrumb align-content-end">';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++) {
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
    $title = trim($title);
    $nextRef = ($index < $itemSize - 2 && $arResult[$index + 1]["LINK"] <> "" ? ' itemref="bx_breadcrumb_' . ($index + 1) . '"' : '');
    $child = ($index > 0 ? ' itemprop="child"' : '');
    $arrow = ($index > 0 ? '' : '');

    if ($arResult[$index]["LINK"] <> "" && $index != $itemSize - 1) {
        $strReturn .= '<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem" class="breadcrumb-item" id="bx_breadcrumb_' . $index . '"><a  itemscope itemtype="http://schema.org/Thing"
       itemprop="item"   itemid="'.$index.'" href="' . $arResult[$index]["LINK"] . '">' . '   <span itemprop="name">' . $title . '</span>'. '</a><meta itemprop="position" content="' . $index. '" /></li>';
    } else {
        $strReturn .= '<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem"  class="breadcrumb-item active" aria-current="page">' .  '<span  itemscope itemtype="http://schema.org/Thing"
       itemprop="item"   itemid="'.$index.'"><span itemprop="name">' . $title . '</span></span><meta itemprop="position" content="' . $index. '" />' . '</li>';
    }
}

$strReturn .= '</ol></nav>';

return $strReturn;
?>