<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;

$this->setFrameMode(true);




if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '')
    $arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

$isVerticalFilter = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");
$isSidebar = ($arParams["SIDEBAR_SECTION_SHOW"] == "Y" && isset($arParams["SIDEBAR_PATH"]) && !empty($arParams["SIDEBAR_PATH"]));
$isFilter = ($arParams['USE_FILTER'] == 'Y');

if ($isFilter && !$arParams['SHOW_ALL_WO_SECTION']) {
    $arFilter = array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ACTIVE" => "Y",
        "GLOBAL_ACTIVE" => "Y",
    );
    if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
        $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
    elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
        $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

    $obCache = new CPHPCache();
    if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog")) {
        $arCurSection = $obCache->GetVars();
    } elseif ($obCache->StartDataCache()) {
        $arCurSection = array();
        if (Loader::includeModule("iblock")) {
            $dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", 'IBLOCK_ID', "UF_TOP_DESCRIPTION", 'UF_SECTION_SHOW', 'UF_TITLE_SECTION', 'UF_PICTURE'));

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache("/iblock/catalog");

                if ($arCurSection = $dbRes->Fetch())
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);

                $CACHE_MANAGER->EndTagCache();
            } else {
                if (!$arCurSection = $dbRes->Fetch())
                    $arCurSection = array();
            }

            if ($arCurSection) {
                if ($arCurSection['UF_PICTURE']) {
                    $arCurSection['PICTURES'] = array();
                    foreach ($arCurSection['UF_PICTURE'] as $iFileId) {
                        //$arCurSection['PICTURES'][] = \CFile::GetPath($iFileId);
                        /*
                         *
                         * РЕСАЙЗ МЕДЛЕННЫХ КАРТИНОК*/
                        $full = CFile::ResizeImageGet(
                            $iFileId,
                            array("width" => 1200, "height" => 800),
                            BX_RESIZE_IMAGE_PROPORTIONAL,
                            true
                        );
                        $thumb = CFile::ResizeImageGet(
                            $iFileId,
                            array("width" => 360, "height" => 250),
                            BX_RESIZE_IMAGE_PROPORTIONAL,
                            true
                        );
                        $arCurSection['PICTURES'][] = ['thumb' => $thumb['src'], 'full' => $full['src']];
                    }
                }
                if (Bitrix\Iblock\SectionTable::getCount(array(
                    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                    'IBLOCK_SECTION_ID' => $arCurSection['ID'],
                    '!DETAIL_PICTURE' => false
                ))) {
                    $arCurSection['HAVE_SECTIONS'] = 'Y';
                }
            }
        }
        $obCache->EndDataCache($arCurSection);
    }
}
if (!isset($arCurSection))
    $arCurSection = array();

$sort = $_REQUEST['sort'];
$order = $_REQUEST['order'];

$arSort = \Realweb\Site\Catalog::getSortCatalog($arParams);
$showSections = false;
$sectionName = !empty($arCurSection['UF_TITLE_SECTION']) ? $arCurSection['UF_TITLE_SECTION'] : null;

$arUfSectionShowProducts = 15;
$arUfSectionShowSections = 16;

if ($arCurSection['UF_SECTION_SHOW'] == 15) {
    $showSections = false;
} elseif ($arCurSection['UF_SECTION_SHOW'] == 16 && $arCurSection['HAVE_SECTIONS']) {
    $showSections = true;
} elseif ($arCurSection['HAVE_SECTIONS'] && !$arResult['VARIABLES']['SMART_FILTER_PATH']) {
    $showSections = true;
}
?>

<? if ($showSections): ?>
    <? require("sections.php"); ?>
<? else: ?>
    <div class="section-bg pt-0">
        <div class="container">
            <div class="row row-10 row_catalog catalog">
                <div class="catalog__left">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.smart.filter",
                        "",
                        array(
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "SECTION_ID" => $arCurSection['ID'],
                            "FILTER_NAME" => $arParams["FILTER_NAME"],
                            "PRICE_CODE" => $arParams["PRICE_CODE"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "SAVE_IN_SESSION" => "N",
                            "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                            "XML_EXPORT" => "Y",
                            "SECTION_TITLE" => "NAME",
                            "SECTION_DESCRIPTION" => "DESCRIPTION",
                            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                            "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                            "SEF_MODE" => $arParams["SEF_MODE"],
                            "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
                            "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                            "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
                            "SHOW_ALL_WO_SECTION" => $arParams['SHOW_ALL_WO_SECTION'],
                        ),
                        $component,
                        array('HIDE_ICONS' => 'N')
                    ); ?>
                    <?
                    //sotbit seometa component start
                    $APPLICATION->IncludeComponent(
                        "sotbit:seo.meta",
                        ".default",
                        array(
                            "FILTER_NAME" => $arParams["FILTER_NAME"],
                            "SECTION_ID" => $arCurSection['ID'],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                        )
                    );
                    //sotbit seometa component end
                    ?>
                </div>
                <div class="catalog__right">
                    <h1 class="h1 dsf" id="pagetitle">
                        <? $APPLICATION->ShowTitle(false); ?>
                    </h1>
                    <? if ($arCurSection['UF_TOP_DESCRIPTION'] && !isset($_GET['PAGEN_1']) && (strpos($_SERVER['REQUEST_URI'], 'filter') == false)): ?>
                        <div class="catalog__topDesc"><?= htmlspecialcharsback($arCurSection['UF_TOP_DESCRIPTION']) ?></div>
                    <? endif;
                    global $sotbitSeoMetaTopDesc;//для установки верхнего описания
                    if ($sotbitSeoMetaTopDesc):?>
                        <div class="catalog__topDesc"><?= htmlspecialcharsback($sotbitSeoMetaTopDesc) ?></div>
                    <? endif ?>
                    <div class="product">
                        <div class="product__params">
                            <div class="product__sort">
                                Сортировка:
                                <? foreach ($arSort as $sort): ?>
                                    <a class="<?= $sort['selected'] ?>"
                                       href="<?= $sort['url'] ?>"><?= $sort['name'] ?></a>
                                <? endforeach; ?>
                            </div>
                            <div class="product__changes">
                                <? $APPLICATION->ShowViewContent("ShowFilterProps"); ?>
                            </div>
                            <div class="product__filter-btn js-open-filter ">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                                    <path stroke="#3e5969" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="1.5"
                                          d="M10 6h11-11zM3 6h3-3zm17 6h1-1zm-1 0h2-2zM3 12h12H3zm10 6h8-8zM3 18h6-6zM8 8a2 2 0 100-4 2 2 0 000 4zm9 6a2 2 0 100-4 2 2 0 000 4zm-6 6a2 2 0 100-4 2 2 0 000 4z"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <? $intSectionID = $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "",
                        array(
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                            "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                            "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                            "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                            "PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
                            "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                            "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                            "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                            "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                            "BASKET_URL" => $arParams["BASKET_URL"],
                            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                            "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                            "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                            "FILTER_NAME" => $arParams["FILTER_NAME"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "SET_TITLE" => $arParams["SET_TITLE"],
                            "MESSAGE_404" => $arParams["~MESSAGE_404"],
                            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                            "SHOW_404" => $arParams["SHOW_404"],
                            "FILE_404" => $arParams["FILE_404"],
                            "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                            "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
                            "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                            "PRICE_CODE" => $arParams["PRICE_CODE"],
                            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                            "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                            "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                            "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                            "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

                            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                            "LAZY_LOAD" => $arParams["LAZY_LOAD"],
                            "MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
                            "LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

                            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                            "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                            "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                            "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                            "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                            "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                            "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                            'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

                            'LABEL_PROP' => $arParams['LABEL_PROP'],
                            'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                            'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                            'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
                            'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                            'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
                            'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
                            'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
                            'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                            'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                            'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

                            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                            'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                            'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                            'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
                            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                            'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
                            'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                            'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
                            'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
                            'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                            'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                            'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
                            'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                            'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                            'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

                            'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                            'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                            'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

                            'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                            "ADD_SECTIONS_CHAIN" => "Y",
                            'ADD_TO_BASKET_ACTION' => $basketAction,
                            'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                            'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                            'COMPARE_NAME' => $arParams['COMPARE_NAME'],
                            'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                            'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
                            'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                            "SHOW_ALL_WO_SECTION" => $arParams['SHOW_ALL_WO_SECTION'],
                            'FILTER_ITEMS' => $APPLICATION->GetPageProperty('filter_items'),
                            'CLEAR_FILTER_URL' => $APPLICATION->GetPageProperty('clear_filter_url'),
                            'SHOW_ALL_ITEMS' => 'Y'
                        ),
                        false
                    );
                    ?>
                    <?
                    global $sotbitSeoMetaBottomDesc;
                    if ($sotbitSeoMetaBottomDesc):?>
                        <div class="catalog__topDesc"><?= htmlspecialcharsback($sotbitSeoMetaBottomDesc) ?></div>
                    <? endif ?>
                </div>
            </div>
        </div>
    </div>

    <? if (empty($_GET["PAGEN_1"]) && (strpos($_SERVER['REQUEST_URI'], 'filter') == false)): ?>
        <? $APPLICATION->ShowViewContent("ShowSectionDescription"); ?>
        <?php if (!empty($arCurSection['PICTURES'])): ?>
            <div class="section-bg section_desc">
                <div class="container container_md">
                    <div class="h1">Фото <?php echo $arCurSection['NAME']; ?> в интерьере</div>
                    <div class="owl-carousel js-owl-slider js-lightgallery">
                        <?php foreach ($arCurSection['PICTURES'] as $strPicture): ?>
                            <div data-src="<?php echo $strPicture['full']; ?>" class="js-lightgallery-item"><img
                                        src="<?php echo $strPicture['thumb']; ?>"
                                        alt="<?php echo $arCurSection['NAME']; ?>"></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <? endif; ?>
    <? $APPLICATION->IncludeComponent("realweb:blank", "viewed", array(), false, array('HIDE_ICONS' => 'Y')); ?>
<? endif; ?>
<? //sotbit seometa meta start
global $sotbitSeoMetaTitle;
global $sotbitSeoMetaKeywords;
global $sotbitSeoMetaDescription;
global $sotbitSeoMetaBreadcrumbTitle;

if (!empty($sotbitSeoMetaH1)) {
    $APPLICATION->SetTitle($sotbitSeoMetaH1);
}
if (!empty($sotbitSeoMetaTitle)) {
    $APPLICATION->SetPageProperty("title", $sotbitSeoMetaTitle);
}
if (!empty($sotbitSeoMetaKeywords)) {
    $APPLICATION->SetPageProperty("keywords", $sotbitSeoMetaKeywords);
}
if (!empty($sotbitSeoMetaDescription)) {
    $APPLICATION->SetPageProperty("description", $sotbitSeoMetaDescription);
}
if (!empty($sotbitSeoMetaBreadcrumbTitle)) {
    $APPLICATION->AddChainItem($sotbitSeoMetaBreadcrumbTitle);
}
//sotbit seometa meta end 


$can_set = false;


if (strpos($APPLICATION->GetCurPage(), "/mebel/matrasy/filter/collection-is-matramax/apply/") !== false) {

    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER["SERVER_NAME"] . 'collections/russian-mebel/matramax/matrases/">', true);
    $can_set = true;
}
if (strpos($APPLICATION->GetCurPage(), "/collections/russian-mebel/matramax/filter/catalog-is-matrases/apply/") !== false) {

    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER["SERVER_NAME"] . 'collections/russian-mebel/matramax/matrases/">', true);
    $can_set = true;
}
if (strpos($APPLICATION->GetCurPage(), "/collections/russian-mebel/wiletto/filter/catalog-is-matrases/apply/") !== false) {

    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER["SERVER_NAME"] . 'collections/russian-mebel/wiletto/matrases-wiletto/">', true);
    $can_set = true;
}
if (strpos($APPLICATION->GetCurPage(), "/mebel/matrasy/filter/collection-is-wiletto/apply/") !== false) {

    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER["SERVER_NAME"] . 'collections/russian-mebel/wiletto/matrases-wiletto/">', true);
    $can_set = true;
}

if (strpos($APPLICATION->GetCurPage(), "/filter/") !== false && !isset($can_set)) {
    $APPLICATION->SetPageProperty("robots", "noindex, follow");
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER["SERVER_NAME"] . "/" . $arResult["VARIABLES"]["SECTION_CODE_PATH"] . '/" />', true);
}

?>


<script>
    var openFilter = $('.js-open-filter');
    var closeFilter = $('.js-close-filter');
    var filterParent = openFilter.parents('.row_catalog').find('.catalog__left');

    openFilter.on('click', function () {
        if (!filterParent.hasClass('active')) {
            openFilter.addClass('active');
            filterParent.addClass('active');
            $('body').addClass('mob-filter-open')
        }

    });
    closeFilter.on('click', function () {
        if (filterParent.hasClass('active')) {
            openFilter.removeClass('active');
            filterParent.removeClass('active');
            $('body').removeClass('mob-filter-open')
        }
    })

</script>
