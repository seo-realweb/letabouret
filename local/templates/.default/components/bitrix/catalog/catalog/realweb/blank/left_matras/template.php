<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arResult = $arParams['RESULT'];
$strId = $arParams['ID']
?>
<noindex>
    <ul class="nav nav-tabs element__tabs-nav" id="elementAdditionalTab-<?php echo $strId; ?>"
        role="tablist">


        <li class="nav-item">
            <a class="nav-link" id="delivery-tab" data-toggle="tab"
               href="#sert-<?php echo $strId; ?>" role="tab" aria-controls="sert"
               aria-selected="false">Сертификаты</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="delivery-tab" data-toggle="tab"
               href="#warranty-<?php echo $strId; ?>" role="tab" aria-controls="warranty"
               aria-selected="false">Гарантия</a>
        </li>
    </ul>
    <div class="tab-content element__tabs-content" id="elementAdditionalTabContent">
        <div class="tab-pane fade show active" id="delivery-<?php echo $strId; ?>" role="tabpanel">
            <div class="element__caption" style="display:none">Доставка</div>
            <div class="us-btns">
                <div class="row" data-entity="tab-container" data-value="properties">
                    <? foreach ($arResult['DELIVERY'] as $key => $item): ?>
                        <div class="col-4">
                            <div class="element__prop">
                                <div class="element__name js-product-delivery-btn<? if (empty($firstOne)):$firstOne = true; ?> active<? endif ?>"
                                     data-target="#product-delivery-<?= $strId; ?>"
                                     data-id="<?= $item['ID'] ?>">
                                    <?= $item['NAME'] ?>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
            <? reset($arResult['DELIVERY']); ?>
            <div id="product-delivery-<?= $strId; ?>">
                <?= htmlspecialcharsback(current($arResult['DELIVERY'])['PREVIEW_TEXT']) ?>
            </div>
            <div class="us-btns us-btns-notab">
                <? \Realweb\Site\Site::showIncludeText('PRODUCT_PAYMENT') ?>
            </div>
        </div>
        <div class="tab-pane fade" id="sert-<?php echo $strId; ?>" role="tabpanel">
            <div class="owl-carousel js-owl-slider js-lightgallery">
                <?
                $arCurSection['PICTURES'] = [];
                if ($arResult['PROPERTIES']['BRAND']['VALUE'] == 'MATRAMAX') {


                    $arSelect = array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
                    $arFilter = array("IBLOCK_ID" => 20, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
                    $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);
                    while ($ob = $res->GetNext()) {


                        $iFileId = $ob['PREVIEW_PICTURE'];


                        $full = CFile::ResizeImageGet(
                            $iFileId,
                            array("width" => 1200, "height" => 800),
                            BX_RESIZE_IMAGE_PROPORTIONAL,
                            true
                        );
                        $thumb = CFile::ResizeImageGet(
                            $iFileId,
                            array("width" => 128, "height" => 176),
                            BX_RESIZE_IMAGE_PROPORTIONAL,
                            true
                        );

                        $arCurSection['PICTURES'][] = ['thumb' => $thumb['src'], 'full' => $full['src']];
                    }


                }
                ?>
                <?php foreach ($arCurSection['PICTURES'] as $strPicture): ?>
                    <a href="<?php echo $strPicture['full']; ?>" class="js-lightgallery-item"><img
                                src="<?php echo $strPicture['thumb']; ?>"
                                alt="<?php echo $arCurSection['NAME']; ?>"></a>
                <?php endforeach; ?>
            </div>

        </div>
        <div class="tab-pane fade" id="warranty-<?php echo $strId; ?>" role="tabpanel">
            <? \Realweb\Site\Site::showIncludeText('PRODUCT_WARRANTY') ?>

        </div>
    </div>
    <div class="video-description">
        <? if (is_array($arResult['PROPERTIES']['VIDEO']['VALUE']) && count($arResult['PROPERTIES']['VIDEO']['VALUE']) > 0): ?>
            <div class="element__caption">Видео</div>
            <div class="js-video-result"></div>
        <? endif; ?>
        <div class="video-info">
            <p style="font-size: 14px; color: #9c4115; margin-top: 16px; text-align: justify">
                *Фото- и видеоматериалы на сайте дают общее представление о цвете и фактуре товара. Они могут
                существенно
                отличаться от реальных ввиду различий цветопередачи мониторов.
            </p>
            <p style="font-size: 14px; color: #9c4115; text-align: justify">
                *Информация на сайте носит справочный характер и не является публичной офертой.
            </p>
        </div>
    </div>
</noindex>