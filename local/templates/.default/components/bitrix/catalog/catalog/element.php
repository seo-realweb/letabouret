<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);
$APPLICATION->SetAdditionalCss(SITE_TEMPLATE_PATH . "/components-template/swiper/style.css");
if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y') {
    $basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? array($arParams['COMMON_ADD_TO_BASKET_ACTION']) : array());
} else {
    $basketAction = (isset($arParams['DETAIL_ADD_TO_BASKET_ACTION']) ? $arParams['DETAIL_ADD_TO_BASKET_ACTION'] : array());
}

$isSidebar = ($arParams['SIDEBAR_DETAIL_SHOW'] == 'Y' && !empty($arParams['SIDEBAR_PATH']));

$arElement = Realweb\Site\Site::getIBlockElement(array(
    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
    'ACTIVE' => 'Y',
    'CODE' => $arResult['VARIABLES']['ELEMENT_CODE']
));
if ($arElement && $arElement['DETAIL_PAGE_URL'] != \Realweb\Site\Site::getCurPage() && urldecode($arElement['DETAIL_PAGE_URL']) != \Realweb\Site\Site::getCurPage()) {
    \Realweb\Site\Site::localRedirect301($arElement['DETAIL_PAGE_URL']);
}

if ($arResult['VARIABLES']['SECTION_CODE']) {
    $arSection = current(Realweb\Site\Site::getSections(array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'CODE' => $arResult['VARIABLES']['SECTION_CODE'])));
}


?>
    <div class="section-bg element pt-0">
        <div class="container">
            <div class="row row-10 row_catalog catalog">

                <div class="col-12 catalog__right">
                    <h1 class="h1 m-0"><? $APPLICATION->ShowTitle(false) ?></h1>
                    <? $APPLICATION->ShowViewContent("ShowElementArticle"); ?>
                </div>
            </div>
            <div class="row row-10 row_catalog catalog">

                <div class="col-12 catalog__right">
                    <?
                    $componentElementParams = array(
                        'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                        'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE'],
                        'META_KEYWORDS' => $arParams['DETAIL_META_KEYWORDS'],
                        'META_DESCRIPTION' => $arParams['DETAIL_META_DESCRIPTION'],
                        'BROWSER_TITLE' => $arParams['DETAIL_BROWSER_TITLE'],
                        'SET_CANONICAL_URL' => $arParams['DETAIL_SET_CANONICAL_URL'],
                        'BASKET_URL' => $arParams['BASKET_URL'],
                        'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
                        'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
                        'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
                        'CHECK_SECTION_ID_VARIABLE' => (isset($arParams['DETAIL_CHECK_SECTION_ID_VARIABLE']) ? $arParams['DETAIL_CHECK_SECTION_ID_VARIABLE'] : ''),
                        'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                        'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
                        'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                        'CACHE_TIME' => $arParams['CACHE_TIME'],
                        'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                        'SET_TITLE' => $arParams['SET_TITLE'],
                        'SET_LAST_MODIFIED' => $arParams['SET_LAST_MODIFIED'],
                        'MESSAGE_404' => $arParams['~MESSAGE_404'],
                        'SET_STATUS_404' => $arParams['SET_STATUS_404'],
                        'SHOW_404' => $arParams['SHOW_404'],
                        'FILE_404' => $arParams['FILE_404'],
                        'PRICE_CODE' => $arParams['PRICE_CODE'],
                        'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
                        'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
                        'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
                        'PRICE_VAT_SHOW_VALUE' => $arParams['PRICE_VAT_SHOW_VALUE'],
                        'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                        'PRODUCT_PROPERTIES' => $arParams['PRODUCT_PROPERTIES'],
                        'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['ADD_PROPERTIES_TO_BASKET']) ? $arParams['ADD_PROPERTIES_TO_BASKET'] : ''),
                        'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
                        'LINK_IBLOCK_TYPE' => $arParams['LINK_IBLOCK_TYPE'],
                        'LINK_IBLOCK_ID' => $arParams['LINK_IBLOCK_ID'],
                        'LINK_PROPERTY_SID' => $arParams['LINK_PROPERTY_SID'],
                        'LINK_ELEMENTS_URL' => $arParams['LINK_ELEMENTS_URL'],

                        'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
                        'OFFERS_FIELD_CODE' => $arParams['DETAIL_OFFERS_FIELD_CODE'],
                        'OFFERS_PROPERTY_CODE' => $arParams['DETAIL_OFFERS_PROPERTY_CODE'],
                        'OFFERS_SORT_FIELD' => $arParams['OFFERS_SORT_FIELD'],
                        'OFFERS_SORT_ORDER' => $arParams['OFFERS_SORT_ORDER'],
                        'OFFERS_SORT_FIELD2' => $arParams['OFFERS_SORT_FIELD2'],
                        'OFFERS_SORT_ORDER2' => $arParams['OFFERS_SORT_ORDER2'],

                        'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
                        'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
                        'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
                        'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
                        'SECTION_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['section'],
                        'DETAIL_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['element'],
                        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                        'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
                        'HIDE_NOT_AVAILABLE_OFFERS' => $arParams['HIDE_NOT_AVAILABLE_OFFERS'],
                        'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
                        'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
                        'USE_MAIN_ELEMENT_SECTION' => $arParams['USE_MAIN_ELEMENT_SECTION'],
                        'STRICT_SECTION_CHECK' => (isset($arParams['DETAIL_STRICT_SECTION_CHECK']) ? $arParams['DETAIL_STRICT_SECTION_CHECK'] : ''),
                        'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                        'LABEL_PROP' => $arParams['LABEL_PROP'],
                        'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                        'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                        'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                        'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                        'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                        'DISCOUNT_PERCENT_POSITION' => (isset($arParams['DISCOUNT_PERCENT_POSITION']) ? $arParams['DISCOUNT_PERCENT_POSITION'] : ''),
                        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                        'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                        'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
                        'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                        'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
                        'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
                        'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                        'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                        'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
                        'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                        'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                        'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),
                        'MESS_PRICE_RANGES_TITLE' => (isset($arParams['~MESS_PRICE_RANGES_TITLE']) ? $arParams['~MESS_PRICE_RANGES_TITLE'] : ''),
                        'MESS_DESCRIPTION_TAB' => (isset($arParams['~MESS_DESCRIPTION_TAB']) ? $arParams['~MESS_DESCRIPTION_TAB'] : ''),
                        'MESS_PROPERTIES_TAB' => (isset($arParams['~MESS_PROPERTIES_TAB']) ? $arParams['~MESS_PROPERTIES_TAB'] : ''),
                        'MESS_COMMENTS_TAB' => (isset($arParams['~MESS_COMMENTS_TAB']) ? $arParams['~MESS_COMMENTS_TAB'] : ''),
                        'MAIN_BLOCK_PROPERTY_CODE' => (isset($arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE']) ? $arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE'] : ''),
                        'MAIN_BLOCK_OFFERS_PROPERTY_CODE' => (isset($arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE']) ? $arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE'] : ''),
                        'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
                        'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
                        'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
                        'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
                        'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
                        'BLOG_EMAIL_NOTIFY' => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
                        'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
                        'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
                        'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
                        'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
                        'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
                        'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
                        'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
                        'IMAGE_RESOLUTION' => (isset($arParams['DETAIL_IMAGE_RESOLUTION']) ? $arParams['DETAIL_IMAGE_RESOLUTION'] : ''),
                        'PRODUCT_INFO_BLOCK_ORDER' => (isset($arParams['DETAIL_PRODUCT_INFO_BLOCK_ORDER']) ? $arParams['DETAIL_PRODUCT_INFO_BLOCK_ORDER'] : ''),
                        'PRODUCT_PAY_BLOCK_ORDER' => (isset($arParams['DETAIL_PRODUCT_PAY_BLOCK_ORDER']) ? $arParams['DETAIL_PRODUCT_PAY_BLOCK_ORDER'] : ''),
                        'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
                        'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                        'ADD_SECTIONS_CHAIN' => (isset($arParams['ADD_SECTIONS_CHAIN']) ? $arParams['ADD_SECTIONS_CHAIN'] : ''),
                        'ADD_ELEMENT_CHAIN' => (isset($arParams['ADD_ELEMENT_CHAIN']) ? $arParams['ADD_ELEMENT_CHAIN'] : ''),
                        'DISPLAY_PREVIEW_TEXT_MODE' => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
                        'DETAIL_PICTURE_MODE' => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : array()),
                        'ADD_TO_BASKET_ACTION' => $basketAction,
                        'ADD_TO_BASKET_ACTION_PRIMARY' => (isset($arParams['DETAIL_ADD_TO_BASKET_ACTION_PRIMARY']) ? $arParams['DETAIL_ADD_TO_BASKET_ACTION_PRIMARY'] : null),
                        'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                        'DISPLAY_COMPARE' => (isset($arParams['USE_COMPARE']) ? $arParams['USE_COMPARE'] : ''),
                        'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                        'BACKGROUND_IMAGE' => (isset($arParams['DETAIL_BACKGROUND_IMAGE']) ? $arParams['DETAIL_BACKGROUND_IMAGE'] : ''),
                        'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
                        'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                        'SET_VIEWED_IN_COMPONENT' => (isset($arParams['DETAIL_SET_VIEWED_IN_COMPONENT']) ? $arParams['DETAIL_SET_VIEWED_IN_COMPONENT'] : ''),
                        'SHOW_SLIDER' => (isset($arParams['DETAIL_SHOW_SLIDER']) ? $arParams['DETAIL_SHOW_SLIDER'] : ''),
                        'SLIDER_INTERVAL' => (isset($arParams['DETAIL_SLIDER_INTERVAL']) ? $arParams['DETAIL_SLIDER_INTERVAL'] : ''),
                        'SLIDER_PROGRESS' => (isset($arParams['DETAIL_SLIDER_PROGRESS']) ? $arParams['DETAIL_SLIDER_PROGRESS'] : ''),
                        'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                        'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                        'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

                        'USE_GIFTS_DETAIL' => $arParams['USE_GIFTS_DETAIL'] ?: 'Y',
                        'USE_GIFTS_MAIN_PR_SECTION_LIST' => $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST'] ?: 'Y',
                        'GIFTS_SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
                        'GIFTS_SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
                        'GIFTS_DETAIL_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
                        'GIFTS_DETAIL_HIDE_BLOCK_TITLE' => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
                        'GIFTS_DETAIL_TEXT_LABEL_GIFT' => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
                        'GIFTS_DETAIL_BLOCK_TITLE' => $arParams['GIFTS_DETAIL_BLOCK_TITLE'],
                        'GIFTS_SHOW_NAME' => $arParams['GIFTS_SHOW_NAME'],
                        'GIFTS_SHOW_IMAGE' => $arParams['GIFTS_SHOW_IMAGE'],
                        'GIFTS_MESS_BTN_BUY' => $arParams['~GIFTS_MESS_BTN_BUY'],
                        'GIFTS_PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                        'GIFTS_SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                        'GIFTS_SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                        'GIFTS_SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

                        'GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
                        'GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],
                        'GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE'],
                        'OFFER_ID' => \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->get('oid') ?: \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->get('offerId')
                    );

                    if (isset($arParams['USER_CONSENT'])) {
                        $componentElementParams['USER_CONSENT'] = $arParams['USER_CONSENT'];
                    }

                    if (isset($arParams['USER_CONSENT_ID'])) {
                        $componentElementParams['USER_CONSENT_ID'] = $arParams['USER_CONSENT_ID'];
                    }

                    if (isset($arParams['USER_CONSENT_IS_CHECKED'])) {
                        $componentElementParams['USER_CONSENT_IS_CHECKED'] = $arParams['USER_CONSENT_IS_CHECKED'];
                    }

                    if (isset($arParams['USER_CONSENT_IS_LOADED'])) {
                        $componentElementParams['USER_CONSENT_IS_LOADED'] = $arParams['USER_CONSENT_IS_LOADED'];
                    }

                    $elementId = $APPLICATION->IncludeComponent(
                        'bitrix:catalog.element',
                        '',
                        $componentElementParams,
                        $component
                    );

                    $GLOBALS['CATALOG_CURRENT_ELEMENT_ID'] = $elementId;
                    $GLOBALS['arrFilterRecommended'] = $APPLICATION->GetPageProperty('RECOMMENDED_FILTER', false);

                    $SEC_FOR_FILT = "";
                    if (!CSite::InDir("/collections/")) {
                        if (!empty($arResult["VARIABLES"]["SECTION_CODE"])) {
                            $arSectionFilter = \Realweb\Site\Site::getSection(array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'CODE' => $arResult["VARIABLES"]["SECTION_CODE"]));
                            if ($arSectionFilter) {
                                $SEC_FOR_FILT = $arSectionFilter["ID"];
                            } else if (!empty($GLOBALS["SEC_ID_DET"])) {
                                $SEC_FOR_FILT = $GLOBALS["SEC_ID_DET"];
                            }
                            $GLOBALS['arrFilterCollection'] = ['IBLOCK_SECTION_ID' => $SEC_FOR_FILT, '!CODE' => $arResult['VARIABLES']['ELEMENT_CODE']];
                        }
                    } else {
                        if (!empty($GLOBALS["SEC_ID_DET"])) {
                            $SEC_FOR_FILT = $GLOBALS["SEC_ID_DET"];
                        }
                        $GLOBALS['arrFilterCollection'] = ['IBLOCK_SECTION_ID' => $SEC_FOR_FILT, '!CODE' => $arResult['VARIABLES']['ELEMENT_CODE']];
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

<? //if($_GET["dev"]=="Y"){pr($arResult);}?>
<? if ($_GET["dev"] == "Y") {
    if (!empty($SEC_FOR_FILT)):
        //if($_GET["dev"] =="Y"){pr($GLOBALS['arrFilterCollection']);}
        ?>
        <div class="section-bg product-list-bg">
            <div class="container container_md">
                <div class="row justify-content-between align-items-center">
                    <div class="col col-12 col-md-auto"><span
                                class="h2 h2_p">Посмотрите другие товары <span>этой коллекции</span></span></div>
                </div>
            </div>
            <div class="container ">
                <? if ($_REQUEST['dbAjaxNav'] == "Y"):
                    $GLOBALS['APPLICATION']->RestartBuffer();
                endif; ?>
                <? $APPLICATION->IncludeComponent("bitrix:catalog.section", "same_collection",
                    Realweb\Site\Catalog::getCatalogSectionParams(array(
                        'FILTER_NAME' => 'arrFilterCollection',
                        'PAGE_ELEMENT_COUNT' => "24",
                        'CONTAINER_CLASS' => 'product-list',
                        'ROW_CLASS' => 'product-list__inner',
                        'COL_CLASS' => 'product__card product__item',
                        'ITEM_CLASS' => '',
                        //"HIDE_NOT_AVAILABLE" => "N",
                        'PAGER_TEMPLATE' => 'pag_ajax',
                        'DISPLAY_BOTTOM_PAGER' => 'Y',
                    )),
                    false, array('HIDE_ICONS' => 'Y')); ?>
                <? if ($_REQUEST['dbAjaxNav'] == "Y"):
                    die();
                endif; ?>
            </div>
        </div>
    <?endif;
} ?>

    <div class="section-bg section-bg_gray">
        <div class="container mt-2 mb-2">
            <div class="row justify-content-between align-items-center">
                <div class="col col-12 col-md-auto"><span class="h2 h2_p">c этим товаром <span>покупают</span></span>
                </div>
            </div>
        </div>
        <div class="container">
            <? $APPLICATION->IncludeComponent("bitrix:catalog.section", "same_collection",
                Realweb\Site\Catalog::getCatalogSectionParams(array(
                    'FILTER_NAME' => 'arrFilterRecommended',
                    'PAGE_ELEMENT_COUNT' => "20",
                    'CONTAINER_CLASS' => 'product product__items_simple',
                    'ROW_CLASS' => 'product__items owl-carousel owl-product-items js-owl-slider',
                    'COL_CLASS' => 'product__item',
                    'ITEM_CLASS' => '',
                )),
                false, array('HIDE_ICONS' => 'Y')); ?>
        </div>
    </div>


<?php

$tov = $APPLICATION->GetPageProperty('TOV_PROPS');
$cats = [];
$res = CIBlockElement::GetElementGroups($tov['ID'], false, ['ID', 'NAME', 'CODE']);
$isk = false;
$ism = false;
$ist = false;
$kr = [589, 590, 591, 592, 593, 594, 1077, 1078, 1079];
$mat = [620];
$div = [559, 560, 561, 563, 564, 887, 986];
$top = [1108];
while ($ar_res = $res->Fetch()) {


    if (in_array($ar_res['ID'], $kr)) {
        $cats[] = $ar_res;
        $isk = true;
    }
    if (in_array($ar_res['ID'], $mat)) {

        $ism = true;
    }
    if (in_array($ar_res['ID'], $top)) {

        $ist = true;
    }
    if (in_array($ar_res['ID'], $div)) {
        $cats[] = $ar_res;
        $isd = true;
    }
}
if ($ism && $ist) {
    unset($ism);
}
if ($isk) {
    ?>

    <div class="section-bg section-bg_gray">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col col-12 col-md-auto"><span class="h2 h2_p">Вместе с этой моделью ищут кровати:</span>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="tagcloud">
                <? foreach ($cats as $c) { ?>
                    <? if ($c['ID'] >= 590) { ?>

                        <? $tit = mb_strtolower($c['NAME'], 'UTF-8') ?>


                        <a href="/mebel/krovati-meb/<?= $c['CODE'] ?>/">
                            <?= $tit ?>
                        </a>
                    <? } ?>
                <? } ?>

                <? $props = $tov['PROPS']; ?>


                <? if ($props['IS_SALE']['VALUE'] == 'Y') { ?>
                    <a href="/mebel/krovati-meb/filter/is_sale-is-y/">со скидками</a>
                <? } ?>

                <? if ($props['KROVAT_SIZE']['VALUE'] != '') { ?>
                    <a href="/mebel/krovati-meb/filter/krovat_size-is-<?= $props['KROVAT_SIZE']['VALUE'] ?>/"><?= $props['KROVAT_SIZE']['VALUE'] ?></a>
                <? } ?>




                <?
                //Цвет кровати
                if ($props['KROVAT_COLOR']['VALUE'] != '') {
                    $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById(24)->fetch();

                    $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                    $entity_data_class = $entity->getDataClass();

                    $rsData = $entity_data_class::getList(array(
                        "select" => array("*"),
                        "order" => array("ID" => "ASC"),
                        "filter" => array()  // Задаем параметры фильтра выборки
                    ));
                    $vals = [];

                    while ($arData = $rsData->Fetch()) {
                        $vals[$arData['UF_XML_ID']] = $arData['UF_NAME'];

                    }


                    $tname = str_replace('ый', 'ые', $vals[$props['KROVAT_COLOR']['VALUE']]);
                    $tname = str_replace('ой', 'ые', $tname);
                    $tname = mb_strtolower($tname, 'UTF-8');
                    ?>
                    <a href="/mebel/krovati-meb/filter/krovat_color-is-<?= $props['KROVAT_COLOR']['VALUE'] ?>/"><?= $tname; ?></a>
                <? } ?>
                <?

                if (!empty($props['STYLE']['VALUE'])) {
                    //Стиль кроватей


                    ?>
                    <? foreach ($props['STYLE']['VALUE'] as $style) {

                        $res2 = CIBlockSection::GetByID($style);
                        ?>
                        <? if ($ar_res2 = $res2->GetNext()) { ?>
                            <a href="/mebel/krovati-meb/filter/style-is-<?= $ar_res2['CODE']; ?>/"><?= mb_strtolower($ar_res2['NAME'], 'UTF-8'); ?></a>

                            <?
                        }
                    } ?>
                    <?
                }
                ?>
                <?
                if (!empty($props['material_krovat']['VALUE'])) {
                    //Материал кроватей
                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "material_krovat"));
                    $vals = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        if ($enum_fields['XML_ID'] != 'derevo') {
                            //Исключаем деревянные чтобы не было дубля с категорией
                            $vals[$enum_fields["VALUE"]] = $enum_fields["XML_ID"];


                        }

                    }

                    ?>
                    <? foreach ($props['material_krovat']['VALUE'] as $mat) { ?>
                        <? if (isset($vals[$mat])) { ?>
                            <a href="/mebel/krovati-meb/filter/material_krovat-is-<?= $vals[$mat]; ?>/"><?= mb_strtolower($mat, 'UTF-8'); ?></a>

                            <?
                        }
                    } ?>
                    <?
                }
                ?>
                <?
                if ($props['PORODA_DEREVA']['VALUE'] != '') {
                    //Порода дерева кроватей
                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "PORODA_DEREVA"));
                    $vals = [];
                    while ($enum_fields = $property_enums->GetNext()) {

                        $vals[$enum_fields["VALUE"]] = $enum_fields["XML_ID"];


                    }

                    ?>
                    <? $mat = $props['PORODA_DEREVA']['VALUE']; ?>
                    <? if (isset($vals[$mat])) { ?>
                        <a href="/mebel/krovati-meb/derevyannye-krovati/filter/poroda_dereva-is-<?= $vals[$mat]; ?>/"><?= mb_strtolower($mat, 'UTF-8'); ?></a>

                        <?

                    } ?>
                    <?
                }
                ?>
                <?
                //Особенности кровати
                if (!empty($props['OSOBENNOST']['VALUE'])) {

                    $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById(22)->fetch();

                    $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                    $entity_data_class = $entity->getDataClass();

                    $rsData = $entity_data_class::getList(array(
                        "select" => array("*"),
                        "order" => array("ID" => "ASC"),
                        "filter" => array()  // Задаем параметры фильтра выборки
                    ));
                    $vals = [];
                    while ($arData = $rsData->Fetch()) {
                        $vals[$arData['UF_XML_ID']] = $arData['UF_NAME'];
                    }
                    ?>
                    <? foreach ($props['OSOBENNOST']['VALUE'] as $osob) {
                        if ($osob != 's-myagkim-izgolovem' && $osob != 's-podemnym-mehanizmom') {
                            ?>
                            <a href="/mebel/krovati-meb/filter/osobennost-is-<?= $osob ?>/"><?= mb_strtolower($vals[$osob], 'UTF-8'); ?></a>
                            <?
                        }
                    } ?>
                <? } ?>


            </div>


        </div>
    </div>
<? } ?>

<?php
//Матрасы перелинковка
if ($ism) {
    //Для матрасов
    ?>
    <div class="section-bg section-bg_gray">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col col-12 col-md-auto"><span class="h2 h2_p">Вместе с этой моделью ищут матрасы:</span>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="tagcloud">


                <? $props = $tov['PROPS']; ?>


                <? if ($props['IS_SALE']['VALUE'] == 'Y') { ?>
                    <a href="/mebel/matrasy/filter/is_sale-is-y/">со скидками</a>
                <? } ?>
                <?

                if ($props['matras_size']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "matras_size"));
                    $mat_size = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $mat_size[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }

                    foreach ($props['matras_size']['VALUE'] as $msize) {
                        ?>
                        <a href="/mebel/matrasy/filter/matras_size-is-<?= $mat_size[$msize] ?>/"><?= mb_strtolower($msize, 'UTF-8') ?></a>
                        <?
                    }
                }
                ?>


                <?

                if ($props['TYPE_COMPOSITION']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "TYPE_COMPOSITION"));
                    $mtype = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $mtype[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    foreach ($props['TYPE_COMPOSITION']['VALUE'] as $msize) {
                        ?>
                        <a href="/mebel/matrasy/filter/type_composition-is-<?= $mtype[$msize] ?>/"><?= mb_strtolower($msize, 'UTF-8') ?></a>
                        <?
                    }
                }
                ?>
                <?

                if ($props['material_matrasov']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "material_matrasov"));
                    $mats = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $mats[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    ?>
                    <a href="/mebel/matrasy/filter/material_matrasov-is-<?= $mats[$props['material_matrasov']['VALUE']] ?>/"><?= mb_strtolower($props['material_matrasov']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>
                <?

                if ($props['spalnyh_mest']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "spalnyh_mest"));
                    $spm = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $spm[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    ?>
                    <a href="/mebel/matrasy/filter/spalnyh_mest-is-<?= $spm[$props['spalnyh_mest']['VALUE']] ?>/"><?= mb_strtolower($props['spalnyh_mest']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>
                <?

                if ($props['zhestkost']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "zhestkost"));
                    $zs = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $zs[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    ?>
                    <a href="/mebel/matrasy/filter/zhestkost-is-<?= $zs[$props['zhestkost']['VALUE']] ?>/"><?= mb_strtolower($props['zhestkost']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>

                <?

                if ($props['osobennosti_matrasov']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "osobennosti_matrasov"));
                    $oso = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $oso[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    foreach ($props['osobennosti_matrasov']['VALUE'] as $osob) {
                        ?>
                        <a href="/mebel/matrasy/filter/osobennosti_matrasov-is-<?= $oso[$osob] ?>/"><?= mb_strtolower($osob, 'UTF-8') ?></a>
                    <?
                    }

                }
                ?>


            </div>


        </div>
    </div>
    <?
}
?>

<?php
//Топперы перелинковка
if ($ist) {
    //Для топперов
    ?>
    <div class="section-bg section-bg_gray">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col col-12 col-md-auto"><span class="h2 h2_p">Вместе с этой моделью ищут топперы:</span>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="tagcloud">


                <? $props = $tov['PROPS']; ?>


                <? if ($props['IS_SALE']['VALUE'] == 'Y') { ?>
                    <a href="/mebel/topperi/filter/is_sale-is-y/">со скидками</a>
                <? } ?>
                <?

                if ($props['razmery_topperov']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "razmery_topperov"));
                    $mat_size = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $mat_size[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }

                    foreach ($props['razmery_topperov']['VALUE'] as $msize) {
                        ?>
                        <a href="/mebel/topperi/filter/razmery_topperov-is-<?= $mat_size[$msize] ?>/"><?= mb_strtolower($msize, 'UTF-8') ?></a>
                        <?
                    }
                }
                ?>



                <?

                if ($props['material_topperov']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "material_topperov"));
                    $mats = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $mats[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    foreach ($props['material_topperov']['VALUE'] as $osob) {
                        ?>
                        <a href="/mebel/topperi/filter/material_topperov-is-<?= $mats[$osob] ?>/"><?= mb_strtolower($osob, 'UTF-8') ?></a>
                        <?
                    }
                }
                ?>
                <?

                if ($props['CHISLO_SPALNYH_MEST']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "CHISLO_SPALNYH_MEST"));
                    $spm = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $spm[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    ?>
                    <a href="/mebel/topperi/filter/chislo_spalnyh_mest-is-<?= $spm[$props['CHISLO_SPALNYH_MEST']['VALUE']] ?>/"><?= mb_strtolower($props['CHISLO_SPALNYH_MEST']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>
                <?

                if ($props['zhestkost_topperov']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "zhestkost_topperov"));
                    $zs = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $zs[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    ?>
                    <a href="/mebel/topperi/filter/zhestkost_topperov-is-<?= $zs[$props['zhestkost_topperov']['VALUE']] ?>/"><?= mb_strtolower($props['zhestkost_topperov']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>

                <?

                if ($props['osobennosti_topperov']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "osobennosti_topperov"));
                    $oso = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $oso[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }

                    ?>
                    <a href="/mebel/topperi/filter/osobennosti_topperov-is-<?= $oso[$props['osobennosti_topperov']['VALUE']] ?>/"><?= mb_strtolower($props['osobennosti_topperov']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>


            </div>


        </div>
    </div>
    <?
}
?>
<?php
//Диваны перелинковка
if ($isd) {
    ?>
    <div class="section-bg section-bg_gray">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col col-12 col-md-auto"><span class="h2 h2_p">Вместе с этой моделью ищут диваны:</span>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="tagcloud">

                <? foreach ($cats as $c) { ?>
                    <? if ($c['ID'] == 562) { ?>

                        <? $tit = mb_strtolower($c['NAME'], 'UTF-8') ?>


                        <a href="/mebel/divany/<?= $c['CODE'] ?>/">
                            <?= $tit ?>
                        </a>
                    <? } ?>
                <? } ?>
                <? $props = $tov['PROPS']; ?>


                <? if ($props['IS_SALE']['VALUE'] == 'Y') { ?>
                    <a href="/mebel/divany/filter/is_sale-is-y/">со скидками</a>
                <? } ?>
                <?

                if ($props['forma_divanov']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "forma_divanov"));
                    $formad = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $formad[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    $link = $formad[$props['forma_divanov']['VALUE']];
                    if ($link == 'pryamie') {

                        $link = 'pryamye-divany';
                    }
                    if ($link == 'uglovie') {

                        $link = 'uglovye';
                    }
                    if ($link == 'modulnie') {

                        $link = 'modulnye-divany';
                    }
                    ?>
                    <a href="/mebel/divany/<?= $link; ?>/"><?= mb_strtolower($props['forma_divanov']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>
                <?

                if ($props['razmery_divanov']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "razmery_divanov"));
                    $formad = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $formad[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    $link = $formad[$props['razmery_divanov']['VALUE']];

                    $link_ext='filter/razmery_divanov-is-'.$link;

                    if ($link == 'dvuhmestnie') {

                        $link_ext = 'dvukhmestnye';
                    }
                    if ($link == 'trehmestnie') {

                        $link_ext = 'trekhmestnye';
                    }

                    ?>
                    <a href="/mebel/divany/<?= $link_ext; ?>/"><?= mb_strtolower($props['razmery_divanov']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>

                <?

                if (!empty($props['STYLE']['VALUE'])) {
                    //Стиль диванов


                    ?>
                    <? foreach ($props['STYLE']['VALUE'] as $style) {

                        $res2 = CIBlockSection::GetByID($style);
                        ?>
                        <? if ($ar_res2 = $res2->GetNext()) { ?>
                            <a href="/mebel/divany/filter/style-is-<?= $ar_res2['CODE']; ?>/"><?= mb_strtolower($ar_res2['NAME'], 'UTF-8'); ?></a>

                            <?
                        }
                    } ?>
                    <?
                }
                ?>
                <?

                if ($props['spalnie_mesta']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "spalnie_mesta"));
                    $spm = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $spm[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    ?>
                    <a href="/mebel/divany/filter/spalnie_mesta-is-<?= $spm[$props['spalnie_mesta']['VALUE']] ?>/"><?= mb_strtolower($props['spalnie_mesta']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>
                <?

                if ($props['mechanism_transformacii']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "mechanism_transformacii"));
                    $mt = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $mt[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    ?>
                    <a href="/mebel/divany/filter/mechanism_transformacii-is-<?= $mt[$props['mechanism_transformacii']['VALUE']] ?>/"><?= mb_strtolower($props['mechanism_transformacii']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>
                <?

                if ($props['karkas']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "karkas"));
                    $karkas = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $karkas[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    ?>
                    <a href="/mebel/divany/filter/karkas-is-<?= $karkas[$props['karkas']['VALUE']] ?>/"><?= mb_strtolower($props['karkas']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>

                <?

                if ($props['obivka_divanov']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "obivka_divanov"));
                    $formad = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $formad[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    $link = $formad[$props['obivka_divanov']['VALUE']];

                    $link_ext='filter/obivka_divanov-is-'.$link;

                    if ($link == 'kozha') {

                        $link_ext = 'kozhanye';
                    }


                    ?>
                    <a href="/mebel/divany/<?= $link_ext; ?>/"><?= mb_strtolower($props['obivka_divanov']['VALUE'], 'UTF-8') ?></a>
                    <?

                }
                ?>
                <?

                if ($props['pomeshenie']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "pomeshenie"));
                    $pomds = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $pomds[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }

                    foreach ($props['pomeshenie']['VALUE'] as $pomd){
                    $link = $pomds[$pomd];

                    $link_ext='mebel/divany/filter/pomeshenie-is-'.$link;

                    if ($link == 'gostinnaya') {

                        $link_ext = 'pomeshcheniya/gostinaya-pom/divany-gostinaya';
                    }


                    ?>
                    <a href="/<?= $link_ext; ?>/"><?= mb_strtolower($pomd, 'UTF-8') ?></a>
                    <?
                    }
                }
                ?>
                <?

                if ($props['osobennosti_divanov']['VALUE'] != '') {

                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "osobennosti_divanov"));
                    $oso = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $oso[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }
                    foreach ($props['osobennosti_divanov']['VALUE'] as $osob) {
                        ?>
                        <a href="/mebel/divany/filter/osobennosti_divanov-is-<?= $oso[$osob] ?>/"><?= mb_strtolower($osob, 'UTF-8') ?></a>
                    <?
                    }

                }
                ?>
                <?
                //Цвет диванов
                if ($props['cvet_divanov']['VALUE'] != '') {
                    $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 1, "CODE" => "cvet_divanov"));
                    $cd = [];
                    while ($enum_fields = $property_enums->GetNext()) {
                        $cd[$enum_fields["VALUE"]] = $enum_fields['XML_ID'];
                    }

                    foreach ($props['cvet_divanov']['VALUE'] as $cdb) {
                    ?>
                        <a href="/mebel/divany/filter/cvet_divanov-is-<?= $cd[$cdb] ?>/"><?= mb_strtolower($cdb, 'UTF-8') ?></a>
<?}?>
                  <? } ?>

            </div>


        </div>
    </div>
    <?
}
?>


    <div class="modal" id="oneByClickModal" tabindex="-1" role="dialog">
        <? $APPLICATION->IncludeComponent("realweb:oneBuyClick.form", "", array('PRODUCT_ID' => $elementId, 'AJAX_MODE' => 'N'), false, array('HIDE_ICONS' => 'Y')); ?>
    </div>
    <div class="modal" id="oneByClickModalSuccess" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog_order" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-title succes-text text-center"></div>

                    <div class="form-group form-group_submit">
                        <button class="basket__button" data-dismiss="modal">ОК</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<? $APPLICATION->IncludeComponent("realweb:blank", "viewed", array('SECTION_CLASS' => 'section-bg'), false, array('HIDE_ICONS' => 'Y')); ?>