<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;
\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/components-template/filter/style.css", true);
$this->setFrameMode(true);
$APPLICATION->SetPageProperty('HIDE_H1', 'Y');

$arFilter = array(
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "ACTIVE" => "Y",
    "GLOBAL_ACTIVE" => "Y",
);
if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
    $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
    $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog")) {
    $arCurSection = $obCache->GetVars();
} elseif ($obCache->StartDataCache()) {
    $arCurSection = array();
    if (Loader::includeModule("iblock")) {
        $dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "IBLOCK_ID", "UF_PROPERTY_FILTER", "UF_TITLE_SECTION", "UF_TOP_DESCRIPTION", "DESCRIPTION", "SECTION_PAGE_URL"));

        if (defined("BX_COMP_MANAGED_CACHE")) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache("/iblock/catalog");

            if ($arCurSection = $dbRes->GetNext())
                $CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);

            $CACHE_MANAGER->EndTagCache();
        } else {
            if (!$arCurSection = $dbRes->GetNext())
                $arCurSection = array();
        }

        if ($arCurSection) {
            $arCurSection['ELEMENT_ID'] = [];
            $rsElement = \CIBlockElement::GetList(
                array("SORT" => "ASC"),
                array("IBLOCK_ID" => IBLOCK_CATALOG_CATALOG, "ACTIVE" => "Y", "PROPERTY_STYLE" => $arCurSection['ID']),
                array("ID")
            );
            while ($arElement = $rsElement->GetNext()) {
                $arCurSection['ELEMENT_ID'][] = $arElement['ID'];
            }
            $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues(
                $arCurSection["IBLOCK_ID"],
                $arCurSection["ID"]
            );
            $arCurSection["IPROPERTY_VALUES"] = $ipropValues->getValues();
        }
    }
    $obCache->EndDataCache($arCurSection);
}
if (!isset($arCurSection) || !$arCurSection) {
    $arCurSection = array();
    \Realweb\Site\Site::setPageStatus404();
}
$arSort = \Realweb\Site\Catalog::getSortCatalog($arParams);

$customElement = new \Realweb\Site\ElementList();
$customElement->setParams(Realweb\Site\Catalog::getCatalogSectionParams());
global $arrCustomFilter;
$arrCustomFilter = $customElement->parseCondition($arCurSection["~UF_PROPERTY_FILTER"]);
$GLOBALS[$arParams["FILTER_NAME"]] = $arrCustomFilter;
if (!empty($arCurSection['ELEMENT_ID'])) {
//    $GLOBALS[$arParams["FILTER_NAME"]]['ID'] = $arCurSection['ELEMENT_ID'];
}
if (empty($GLOBALS[$arParams["FILTER_NAME"]])) {
//    $GLOBALS[$arParams["FILTER_NAME"]]['ID'] = 0;
}
unset($GLOBALS[$arParams["FILTER_NAME"]]['ID']);
$GLOBALS[$arParams["FILTER_NAME"]]['=PROPERTY_STYLE'] = $arCurSection['ID'];


?>
    <div class="section-bg pt-0">
        <div class="container">
            <div class="row row-10 row_catalog catalog">
                <div class="catalog__left">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.smart.filter",
                        "",
                        array(
                            "IBLOCK_TYPE" => 'catalog',
                            "IBLOCK_ID" => IBLOCK_CATALOG_CATALOG,
                            "SECTION_ID" => array(),
                            "SEC_ID" => $arCurSection['ID'],
                            "PREFILTER_NAME" => $arParams["FILTER_NAME"],
                            "FILTER_NAME" => $arParams["FILTER_NAME"],
                            "PRICE_CODE" => $arParams["PRICE_CODE"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "SAVE_IN_SESSION" => "N",
                            "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                            "XML_EXPORT" => "Y",
                            "SECTION_TITLE" => "NAME",
                            "SECTION_DESCRIPTION" => "DESCRIPTION",
                            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                            "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                            "SEF_MODE" => $arParams["SEF_MODE"],
                            "SEF_RULE" => $arCurSection['SECTION_PAGE_URL'] . $arResult["URL_TEMPLATES"]["smart_filter"],
                            "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                            "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
                            "SHOW_ALL_WO_SECTION" => "Y",
                            'HIDE_PROPERTY_CODE' => ['STYLE']
                        ),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                    ); ?>
                    <?// unset ($_SESSION[$arParams["FILTER_NAME"]][0]["arrFilter_80_".$keyCrc]);
                    //sotbit seometa component start
                    $APPLICATION->IncludeComponent(
                        "sotbit:seo.meta",
                        ".default",
                        Array(
                            "FILTER_NAME" => $arParams["FILTER_NAME"],
                            "SECTION_ID" => $arCurSection['ID'],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                        )
                    );
                    //sotbit seometa component end
                    ?>
                </div>
                <div class="catalog__right">
                    <h1 class="h1">
                        <?global $sotbitSeoMetaH1;?>
                        <?if(!empty($sotbitSeoMetaH1)):?>
                            <?= $sotbitSeoMetaH1?>
                        <?elseif ($arCurSection["UF_TITLE_SECTION"]):?>
                            <?= $arCurSection["UF_TITLE_SECTION"]?>
                        <?else:?>
                            <? $APPLICATION->ShowTitle(false); ?>
                        <?endif?>
                    </h1>
                    <? if ($arCurSection['UF_TOP_DESCRIPTION']): ?>
                        <div class="catalog__topDesc"><?= htmlspecialcharsback($arCurSection['UF_TOP_DESCRIPTION']) ?></div>
                    <? endif;
                    global $sotbitSeoMetaTopDesc;//для установки верхнего описания
                    if($sotbitSeoMetaTopDesc):?>
                        <div class="catalog__topDesc"><?= htmlspecialcharsback($sotbitSeoMetaTopDesc) ?></div>
                    <?endif?>
                    <div class="product">
                        <div class="product__params">
                            <div class="product__sort">
                                Сортировка:
                                <? foreach ($arSort as $sort): ?>
                                    <a href="<?= $sort['url'] ?>"><?= $sort['name'] ?></a>
                                <? endforeach; ?>
                            </div>
                            <div class="product__changes">
                                <? $APPLICATION->ShowViewContent("ShowFilterProps"); ?>
                            </div>
                            <div class="product__filter-btn js-open-filter ">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                                    <path stroke="#638EA7" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 6h11-11zM3 6h3-3zm17 6h1-1zm-1 0h2-2zM3 12h12H3zm10 6h8-8zM3 18h6-6zM8 8a2 2 0 100-4 2 2 0 000 4zm9 6a2 2 0 100-4 2 2 0 000 4zm-6 6a2 2 0 100-4 2 2 0 000 4z"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <? $arResult["FOLDER"] = '/'; ?>

                    <? $intSectionID = $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "",
                        array(
                            "IBLOCK_TYPE" => 'catalog',
                            "IBLOCK_ID" => IBLOCK_CATALOG_CATALOG,
                            "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                            "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                            "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                            "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                            "PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
                            "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                            "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                            "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                            "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                            "BASKET_URL" => $arParams["BASKET_URL"],
                            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                            "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                            "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                            "FILTER_NAME" => $arParams["FILTER_NAME"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "SET_TITLE" => $arParams["SET_TITLE"],
                            "MESSAGE_404" => $arParams["~MESSAGE_404"],
                            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                            "SHOW_404" => $arParams["SHOW_404"],
                            "FILE_404" => $arParams["FILE_404"],
                            "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                            "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
                            "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                            "PRICE_CODE" => $arParams["PRICE_CODE"],
                            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                            "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                            "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                            "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                            "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

                            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                            "LAZY_LOAD" => $arParams["LAZY_LOAD"],
                            "MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
                            "LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

                            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                            "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                            "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                            "SECTION_ID" => '',
                            "SECTION_CODE" => '',
                            "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                            "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                            "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                            'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

                            'LABEL_PROP' => $arParams['LABEL_PROP'],
                            'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                            'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                            'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
                            'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                            'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
                            'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
                            'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
                            'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                            'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                            'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

                            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                            'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                            'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                            'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
                            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                            'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
                            'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                            'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
                            'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
                            'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                            'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                            'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
                            'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                            'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                            'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

                            'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                            'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                            'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

                            'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                            "ADD_SECTIONS_CHAIN" => "Y",
                            'ADD_TO_BASKET_ACTION' => $basketAction,
                            'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                            'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                            'COMPARE_NAME' => $arParams['COMPARE_NAME'],
                            'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                            'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
                            'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                            "SHOW_ALL_WO_SECTION" => "Y",
                            'FILTER_ITEMS' => $APPLICATION->GetPageProperty('filter_items'),
                            'CLEAR_FILTER_URL' => $APPLICATION->GetPageProperty('clear_filter_url')
                        ),
                        false
                    );
                    ?>
                    <?
                    global $sotbitSeoMetaBottomDesc;
                    if($sotbitSeoMetaBottomDesc):?>
                        <div class="catalog__topDesc"><?= htmlspecialcharsback($sotbitSeoMetaBottomDesc) ?></div>
                    <?endif?>
                </div>
            </div>
        </div>
    </div>
<? $APPLICATION->IncludeComponent("realweb:blank", "countries", array(), false, array('HIDE_ICONS' => 'Y')); ?>
<?if(empty($_GET["PAGEN_1"])){?>
	<? $APPLICATION->ShowViewContent("ShowSectionDescription"); ?>
	<? if ($arCurSection['DESCRIPTION']): ?>
    <div class="section-bg section_desc">
        <div class="container container_md">
            <div class="text">
                <?= htmlspecialcharsback($arCurSection['DESCRIPTION']) ?>
            </div>
        </div>
    </div>
<? endif; ?>
	<?}?>

<? $APPLICATION->IncludeComponent("realweb:blank", "viewed", array(), false, array('HIDE_ICONS' => 'Y')); ?>

<?
if ($arCurSection["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) {
    $APPLICATION->SetTitle($arCurSection["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]);
    $APPLICATION->AddChainItem($arCurSection["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]);
}
if ($arCurSection["IPROPERTY_VALUES"]["SECTION_META_TITLE"]) {
    $APPLICATION->SetPageProperty('title', $arCurSection["IPROPERTY_VALUES"]["SECTION_META_TITLE"]);
}
if ($arCurSection["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]) {
    $APPLICATION->SetPageProperty('description', $arCurSection["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]);
}
if ($arCurSection["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"]) {
    $APPLICATION->SetPageProperty('keywords', $arCurSection["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"]);
}
?>
<?//sotbit seometa meta start
global $sotbitSeoMetaTitle;
global $sotbitSeoMetaKeywords;
global $sotbitSeoMetaDescription;
global $sotbitSeoMetaBreadcrumbTitle;
if(!empty($sotbitSeoMetaH1))
{
    $APPLICATION->SetTitle($sotbitSeoMetaH1);
}
if(!empty($sotbitSeoMetaTitle))
{
    $APPLICATION->SetPageProperty("title", $sotbitSeoMetaTitle);
}
if(!empty($sotbitSeoMetaKeywords))
{
    $APPLICATION->SetPageProperty("keywords", $sotbitSeoMetaKeywords);
}
if(!empty($sotbitSeoMetaDescription))
{
    $APPLICATION->SetPageProperty("description", $sotbitSeoMetaDescription);
}
if(!empty($sotbitSeoMetaBreadcrumbTitle) ) {
    $APPLICATION->AddChainItem($sotbitSeoMetaBreadcrumbTitle  );
}
//sotbit seometa meta end ?>


<script>
    var openFilter = $('.js-open-filter');
    var closeFilter = $('.js-close-filter');
    var filterParent = openFilter.parents('.row_catalog').find('.catalog__left');

    openFilter.on('click', function() {
        if (!filterParent.hasClass('active')) {
            openFilter.addClass('active');
            filterParent.addClass('active');
            $('body').addClass('mob-filter-open')
        }

    });
    closeFilter.on('click', function() {
        if (filterParent.hasClass('active')) {
            openFilter.removeClass('active');
            filterParent.removeClass('active');
            $('body').removeClass('mob-filter-open')
        }
    })

</script>

