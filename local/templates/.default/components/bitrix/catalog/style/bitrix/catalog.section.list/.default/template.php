<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

if (0 < $arResult["SECTIONS_COUNT"]) {
    ?>
    <div class="row row-10 collection-list">
        <?
        foreach ($arResult['SECTIONS'] as &$arSection) {
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            if (!$arSection['PICTURE']) {
                $arSection['PICTURE'] = array(
                    'SRC' => SITE_TEMPLATE_PATH . '/images/no-photo.png',
                    'ALT' => (
                    '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                        ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                        : $arSection["NAME"]
                    ),
                    'TITLE' => (
                    '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                        ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                        : $arSection["NAME"]
                    )
                );
            }
            ?>
            <div class="col col-6 col-md-6 col-xl-4 collection-list__item"
                 id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                <div class="collection-list__card">
                    <a target="_self" href="<? echo $arSection['SECTION_PAGE_URL']; ?>">
                        <? if ($arSection['DISCOUNT']): ?>
                            <div class="product__label product__label_sale"><?= $arSection['DISCOUNT'] ?></div>
                        <? endif; ?>
                        <img src="<?= $arSection['PICTURE']['SRC'] ?>" alt="<?= $arSection['PICTURE']['ALT'] ?>">
                        <div class="collection-list__content">
                            <span class="collection-list__title"><? echo $arSection['NAME']; ?></span>
                        </div>
                    </a>
                </div>
            </div>
            <?
        }
        ?>
    </div>
    <?
}
?>