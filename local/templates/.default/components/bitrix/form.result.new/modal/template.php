<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="modal-dialog modal-dialog_order" role="document">
    <div class="modal-content">
        <div class="modal-body">
            <?= $arResult["FORM_HEADER"] ?>
            <?
            foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
                if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
                    echo $arQuestion["HTML_CODE"];
                } else {
                    ?>
                    <div class="form-group">
                        <?= $arQuestion["HTML_CODE"] ?>
                    </div>
                    <?
                }
            }
            ?>
            <?
            if ($arResult["isUseCaptcha"] == "Y") {
                ?>
                <div class="form-group">
                    <input type="hidden" name="captcha_sid"
                           value="<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/><img
                            src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"
                            width="180" height="40"/>
                    <input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext"/>
                </div>
                <?
            }
            ?>
            <div class="form__response">
                <? if ($arResult["isFormErrors"] == "Y"): ?>
                    <?= $arResult["FORM_ERRORS_TEXT"]; ?>
                <? endif; ?>
                <? if ($arResult["isFormNote"] == "Y"): ?>
                    <?= $arResult["FORM_NOTE"] ?>
                <? endif; ?>
            </div>
            <div class="form-group form-group_submit">
                <button type="submit" class="basket__button"><?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?></button>
            </div>

            <input <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="hidden"
                                                                                              name="web_form_submit"
                                                                                              value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>"/>
            <?= $arResult["FORM_FOOTER"] ?>
            <small class="mt-2" style="width:100%;text-align:center;display: block;">Поля, отмеченные * обязательны к заполнению</small>
        </div>
    </div>
</div>
<script>
    var inputMask = includeJs('<?=SITE_TEMPLATE_PATH.'/vendor/jquery.inputmask.min.js'?>');
    inputMask.onload = function(){

        $('input[name="form_text_2"]').inputmask({"mask": "+7 (999) 999-99-99"})
    };

</script>