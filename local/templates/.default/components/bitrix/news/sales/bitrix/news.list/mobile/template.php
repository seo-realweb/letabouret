<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list" style="margin-top:-35px; margin-left:-15px">
<table cellpadding="15" style="text-align:center; margin-left:20px">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?$item=0; foreach($arResult["ITEMS"] as $arItem):?>
<?if ($item % 3 == 0):?>
<tr style="vertical-align:top;">
<?endif?>
<td style="padding-right:13px">
<div class="cat-item">
	<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"><?echo $arItem["NAME"]?></a>
	<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" width=225 height=150px class="normalSizeSale"></a>		
								
    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="kat_zag" title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"><?echo $arItem["NAME"]?></a>
    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"><span class="cat-item-overlay"><span class="cat-item-overlay-ico cat-ico-sale"></span></span></a>

</div>
</td>
<?if ($item++ % 3 == 2):?>
</tr>
<?endif?>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
<?if ($item % 0 != 2):?>
</tr>
<?endif?>
</table>
</div>
<?
	if (!$arResult["SECTION"]["ID"]) {
		echo CIBlock::GetArrayByID(7, "DESCRIPTION");
	}
?>
<script>
jQuery(function() {
	//console.log('13434');
	jQuery(".cat-item").mouseenter(function(){
		jQuery(this).find('img').removeClass('normalSizeSale').animate({
    "height": "100%",
	"margin-left": "-27px"
  }, 250, function() {
    // Animation complete.
	//if (jQuery(this).find('img').hasClass('normalSize'))
	//	jQuery(this).find('img').css({"height":"160px", "width":"225px"});
  })
	})
	jQuery(".cat-item").mouseleave(function(){
		console.log("3");
		jQuery(this).find('img').addClass("normalSizeSale").css({"height":"150px", "margin-left":"0px"});
	})
})
</script>
