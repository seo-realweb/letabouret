<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="product">
    <div class="row row-10">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <div class="col col-6 col-xl-4 product__item">
                <div class="product__card">
                    <div class="product__picture">
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                            <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"  alt="<?=$arItem["NAME"]?>">
                        </a>
                    </div>
                    <div class="product__content">
                        <a class="product__title" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                            <div class="product__name"><?= $arItem["NAME"] ?></div>
                        </a>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>