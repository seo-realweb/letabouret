<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->SetPageProperty('SIMILAR', $arResult['PROPERTIES']['SIMILAR']['VALUE']);
$APPLICATION->SetPageProperty('THEME', $arResult['PROPERTIES']['THEME']['VALUE']);