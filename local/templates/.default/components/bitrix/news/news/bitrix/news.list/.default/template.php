<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?>
<? endif; ?>
    <div class="news">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="news__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="row row-10">
                    <div class="col col-12 col-lg-6">
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img class="news__picture"
                                                                         src="<?= is_array($arItem["PREVIEW_PICTURE"]) ? $arItem["PREVIEW_PICTURE"]["SRC"] : (SITE_TEMPLATE_PATH . '/images/no-photo.png') ?>"></a>
                    </div>
                    <div class="col col-12 col-lg-6">
                        <div class="news__content">
                            <div class="news__title"><? echo $arItem["NAME"] ?></div>
                            <div class="news__text">
                                <? if ($arItem['PREVIEW_TEXT']): ?>
                                    <? if ($arItem['PREVIEW_TEXT_TYPE'] == 'html'): ?>
                                        <?= $arItem['PREVIEW_TEXT'] ?>
                                    <? else: ?>
                                        <p><?= $arItem['PREVIEW_TEXT'] ?></p>
                                    <? endif; ?>
                                <? elseif ($arItem['DETAIL_TEXT']): ?>
                                    <p><?= substr(strip_tags($arItem['DETAIL_TEXT']), 0, 300) ?>...</p>
                                <? endif; ?>
                            </div>
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="news__detail">Читать далее —</a>
                            <div class="news__footer d-none">
                                <div class="news__counter">
                                    <i class="fal fa-heart"></i><?= intval($arItem['PROPERTIES']['LIKE']['VALUE']) ?>
                                </div>
                                <div class="news__counter">
                                    <i class="fal fa-comments-alt"></i><?= intval($arItem['PROPERTIES']['COMMENT']['VALUE']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?>
<? endif; ?>