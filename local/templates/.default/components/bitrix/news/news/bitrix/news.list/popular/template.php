<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="list js-owl-mobile js-owl-tablet" data-tablet-dots="Y" data-mobile-dots="Y">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="list__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="list__picture">
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img
                            src="<?= is_array($arItem["PREVIEW_PICTURE"]) ? $arItem["PREVIEW_PICTURE"]["SRC"] : (SITE_TEMPLATE_PATH . '/images/no-photo.png') ?>"></a>
            </div>
            <div class="list__content">
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="list__title"><? echo $arItem["NAME"] ?></a>
                <div class="list__text">
                    <? if ($arItem['PREVIEW_TEXT']): ?>
                        <? if ($arItem['PREVIEW_TEXT_TYPE'] == 'html'): ?>
                            <?= $arItem['PREVIEW_TEXT'] ?>
                        <? else: ?>
                            <p><?= $arItem['PREVIEW_TEXT'] ?></p>
                        <? endif; ?>
                    <? elseif ($arItem['DETAIL_TEXT']): ?>
                        <p><?= substr(strip_tags($arItem['DETAIL_TEXT']), 0, 100) ?>...</p>
                    <? endif; ?>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>