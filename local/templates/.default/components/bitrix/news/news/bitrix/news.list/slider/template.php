<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($arResult["ITEMS"]): ?>
    <div class="section-bg section_pad_b">
        <div class="container container_md">
            <div class="row justify-content-between align-items-center">
                <div class="col col-12 col-md-auto"><h2 class="h2 h2_p"><?= $arParams['~TITLE'] ?></h2></div>
            </div>
        </div>
        <div class="container container_md">
            <div class="special-product">
                <div class="row row-10 js-owl-mobile js-owl-tablet" data-tablet-dots="Y" data-tablet-items="2"
                     data-mobile-dots="Y" data-mobile-items="2">
                    <? foreach ($arResult["ITEMS"] as $arItem): ?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <div class="col" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                            <div class="special-product__item">
                                <div class="special-product__picture">
                                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img
                                                src="<?= is_array($arItem["PREVIEW_PICTURE"]) ? $arItem["PREVIEW_PICTURE"]["SRC"] : (SITE_TEMPLATE_PATH . '/images/no-photo.png') ?>"></a>
                                </div>
                                <div class="special-product__content">
                                    <a class="special-product__caption" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                        <? echo $arItem["NAME"] ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>