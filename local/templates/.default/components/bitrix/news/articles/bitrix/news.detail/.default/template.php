<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-detail" style="float:left; width:100%; margin-top: 0px; margin-bottom:15px" itemscope itemtype="https://schema.org/Article">
	
<link itemprop="mainEntityOfPage" href="https://letabouret.ru/<?=$arResult["DETAIL_PAGE_URL"]?>" />
    
<meta itemprop="headline name" content="<?=$arResult["NAME"]?>">

<meta itemprop="datePublished" datetime="<?=date('Y-m-d',strtotime($arResult["DATE_CREATE"]))?>" content="<?=date('Y-m-d',strtotime($arResult["DATE_CREATE"]))?>">
<meta itemprop="dateModified" datetime="<?=date('Y-m-d',strtotime($arResult["TIMESTAMP_X"]))?>" content="<?=date('Y-m-d',strtotime($arResult["TIMESTAMP_X"]))?>">
<?if (is_array($arResult["DETAIL_PICTURE"])){?>    
<link itemprop="image" href="https://letabouret.ru<?=$arResult["DETAIL_PICTURE"]["SRC"]?>">
<?}?>

<div itemprop="author" itemscope="" itemtype="http://schema.org/Person" style="display:none;">
<link itemprop="sameAs" href="https://letabouret.ru/" />
<img itemprop="image" src="https://letabouret.ru/local/templates/letabouret/images/logo.png" alt="LeTabouret" />
<a href="https://letabouret.ru/o-kompanii/"><span itemprop="name">LeTabouret</span></a>
</div>
<div itemprop="publisher" itemscope itemtype="https://schema.org/Organization" style="display:none;">
        <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
            <img itemprop="url image" src="https://letabouret.ru/local/templates/letabouret/images/logo.png" alt="LeTabouret" title="LeTabouret" />
        </div>
<img itemprop="image" src="https://letabouret.ru/local/templates/letabouret/images/logo.png" alt="LeTabouret" />
        <meta itemprop="name" content="letabouret.ru">
        <meta itemprop="telephone" content="+7(812) 635-05-65">
        <meta itemprop="address" content="Россия">
    </div>
<div itemprop="description" style="display:none;">
<?echo $arResult["PREVIEW_TEXT"];?>
   </div>
<div style="width:100%">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>

	<?endif?>
	</div>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<!--<h3><?=$arResult["NAME"]?></h3>-->
		<h3 style="text-align: center;">
	<span style="color: rgb(102, 51, 153);"><span style="font-size: 16px;"><?=$arResult["NAME"]?></span></span></h3>

	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
	<br />
	<?foreach($arResult["FIELDS"] as $code=>$value):
		if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
			if (!empty($value) && is_array($value))
			{
				?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
			}
		}
		else
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
		}
		?><br />
	<?endforeach;
	foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
		<!--<?=$arProperty["NAME"]?>:&nbsp;-->
		<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
			<?foreach ($arProperty["VALUE"] as $val):?>

			<a href="<?=CFile::GetPath($val)?>" rel="ex_group" title=""><img alt="" src="<?=CFile::GetPath($val)?>" style="height: 257px; margin-left:10px; margin-right: 6px;margin-bottom: 20px;"  title="" /></a>
			<?endforeach?>
		<?else:?>
		<div class="nw_date2" style="text-align:right;"><?=$arProperty["DISPLAY_VALUE"]?></div>
			
		<?endif?>
	<?endforeach;
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
	?>
</div>