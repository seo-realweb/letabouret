<?php

$resElement = \CIBlockElement::GetList(
    [],
    [
        'IBLOCK_ID' => 11,
        'ID' => $arResult['ID'],
    ],
    false,
    false,
    [
        'ID',
        'PROPERTY_INTEREST',
    ]
);

if (!($element = $resElement->getNext())) {

    return;
} else {
    $int = $element['PROPERTY_INTEREST_VALUE'];


    if ($int > 0) {


        $res = CIBlockSection::GetByID($int);
        if ($ar_res = $res->GetNext()) {

            $sect = ['NAME' => $ar_res['NAME'], 'URL' => $ar_res['SECTION_PAGE_URL']];
            $GLOBALS['arrFilterS'] = ['IBLOCK_SECTION_ID' => $ar_res['ID']];

            ?>

            <p class="h3">Вам может быть интересно <a style="color: #7c4900;text-decoration:underline" href="<?=$sect['URL']?>"><?=$sect['NAME']?></a></p>
            <? $APPLICATION->IncludeComponent("bitrix:catalog.section", "interest",
                Realweb\Site\Catalog::getCatalogSectionParams(array(
                    'FILTER_NAME' => 'arrFilterS',
                    'PAGE_ELEMENT_COUNT' => "20",
                    'CONTAINER_CLASS' => 'product product__items_simple',
                    'ROW_CLASS' => 'product__items owl-carousel owl-product-items js-owl-slider',
                    'COL_CLASS' => 'product__item',
                    'ITEM_CLASS' => '',
                    //"HIDE_NOT_AVAILABLE" => "N",

                )),
                false, array('HIDE_ICONS' => 'Y')); ?>


            <?
        }


    }


}


?>

