<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if (!$arParams["NEWSID"]) { ?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="nw_link"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></a></div>
	<?if ($arItem["PREVIEW_PICTURE"]) { ?>
		<div class="photo page_borr"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem["NAME"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" width="125"></a></div>	
	<? } ?>		
	<div class="nw_date2"><?=$arItem["DISPLAY_PROPERTIES"]["UF_INTERIERDATE"]["VALUE"]?></div>							
	<p class="nw_txt"><?=$arItem["PREVIEW_TEXT"]?></p>	
	<div class="clear"></div>
	<div class="nw_line"></div>
<?endforeach;?>
<? } else { ?>
<div class="news-list" style="margin-top:-20px; margin-bottom:-20px">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<h3 style="text-align: center;">
		<span style="font-size: 16px;"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>">Коллекция в интерьерах . . .</a></span>
	</h3>
	<div style="text-align: center;">
		<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
		<?			
			//$el = CIBlockElement::GetList(Array(), Array("ID"=>$arParams["NEWSID"], 'IBLOCK_ID' => "11"), false, false, array("ID", "UF_INTERIERSPHOTO","UF_INTERIERSPHOTOA"));
			$el = CIBlockElement::GetProperty(11, $arParams["NEWSID"], Array(), Array("CODE"=>"UF_INTERIERSPHOTOA"));
			while ($ob = $el->GetNext()):?>
				<img alt="" src="<?=CFile::GetPath($ob['VALUE'])?>" style="width: 240px; height: 167px;" />&nbsp; &nbsp;
			<?endwhile?>
		</a>
	</div>
<?endforeach;?>
<? } ?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
