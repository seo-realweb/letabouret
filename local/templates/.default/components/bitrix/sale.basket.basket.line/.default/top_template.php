<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
$arResult['NUM_FAVORITE'] = 0;

?>
<ul class="nav nav_service" style="justify-content: flex-end ;">

    <li class="nav-item" >
        <a class="nav-link" href="<?= $arParams['PATH_TO_BASKET'] ?>">
            <span class="basket-icon">
                <span class="label-counter"><?= $arResult['NUM_PRODUCTS'] ?></span>
            </span>
        </a>
    </li>
    <li class="nav-item" style="display: none;">
        <a class="nav-link" href="<?= $arParams['PATH_TO_PROFILE'] ?>">
            <i class="far fa-user"></i>
            <span class="nav-link-text">Аккаунт</span>
        </a>
    </li>
</ul>
<style>
    .head-phone-number {
        margin-right: 52px;
        margin-top: 10px;
        font-size: 16px;
        color: #9c4115;
        font-weight: 700;
    }

    @media screen and (max-width: 766px) {
        .head-phone-number {
           display: none;
        }
    }
</style>