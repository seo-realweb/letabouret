<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */


$globalsFilter = $GLOBALS[$arParams['FILTER_NAME']];
$offersFilter = array();
foreach ($globalsFilter['OFFERS'] as $propCode => $value){
    $property = explode('PROPERTY_', $propCode);
    if (is_numeric($property[1])){
        $offersFilter[$property[1]] = array(
            'ID' =>(int) $property[1],
            'SING' => $property[0],
            'VALUE' => $value
        );

    }
}

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
foreach ($arResult['ITEMS'] as &$item) {

    $ids[] = $item["IBLOCK_SECTION_ID"];
}
if (!empty($ids)) {
    $arGroups = [];
    $rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], 'ID' => $ids));
    while ($arSection = $rsSection->GetNext()) {
        $arGroups[$arSection['ID']] = $arSection;
    }
    //$arGroups = array_column(\Bitrix\Iblock\SectionTable::getList(array(
    //    'select' => array("ID", "NAME"),
    //    'filter' => array("IBLOCK_ID" => $arParams['IBLOCK_ID'], 'ID' => $ids)
    //))->fetchAll(), 'NAME', 'ID');
    foreach ($arResult['ITEMS'] as &$item) {
        if ($arGroups[$item["IBLOCK_SECTION_ID"]]) {
            $item['DETAIL_PAGE_URL'] = $arGroups[$item["IBLOCK_SECTION_ID"]]['SECTION_PAGE_URL']  . $item['CODE'] . '/';
            if ($name = $arGroups[$item["IBLOCK_SECTION_ID"]]['NAME']) {
                $item['SECTION_NAME'] = $name;
            }
        }

        $arFilteredOffers = array();
        if($item['OFFERS'] && $offersFilter){
            foreach ($item['OFFERS'] as $offer) {
                $offerFilterd = false;
                $breakForeach = false;
                foreach ($offer['PROPERTIES'] as $arProperty){
                    if($breakForeach){
                        break;
                    }
                    if($offersFilter[$arProperty['ID']]){

                        switch ($offersFilter[$arProperty['ID']]['SING']) {
                            case '>=':
                                if($arProperty['VALUE'] >= $offersFilter[$arProperty['ID']]['VALUE'][0]){
                                    $offerFilterd = true;
                                }else{
                                    $offerFilterd = false;
                                    $breakForeach = true;
                                }
                                break;
                            case '<=':
                                if($arProperty['VALUE'] <= $offersFilter[$arProperty['ID']]['VALUE'][0]){
                                    $offerFilterd = true;
                                }else{
                                    $offerFilterd = false;

                                    $breakForeach = true;
                                }
                                break;
                            case '><':
                                if($arProperty['VALUE'] >= $offersFilter[$arProperty['ID']]['VALUE'][0] && $arProperty['VALUE'] <= $offersFilter[$arProperty['ID']]['VALUE'][1]){
                                    $offerFilterd = true;
                                }else{
                                    $offerFilterd = false;

                                    $breakForeach = true;
                                }
                                break;
                            default:

                                if($arProperty['VALUE'] == $offersFilter[$arProperty['ID']]['VALUE'][0] || $arProperty['VALUE_ENUM_ID'] == $offersFilter[$arProperty['ID']]['VALUE'][0]){

                                    $offerFilterd = true;
                                }else{
                                    $offerFilterd = false;

                                    $breakForeach = true;
                                }

                                break;
                        }
                    }
                }

                if($offerFilterd){
                    $arFilteredOffers[] = $offer;
                }
            }
        }

        if(count($arFilteredOffers) == 1){
            $uri = new Bitrix\Main\Web\Uri($item['DETAIL_PAGE_URL']);
            $uri->addParams(array("offerId" => $arFilteredOffers[0]['ID']));
            $item['DETAIL_PAGE_URL'] = $uri->getUri();
        }
    }
}

/*
if(CModule::IncludeModule("slam.image")){
    $moduleReize = 'CSlamImage';
}
else{
    $moduleReize = 'CFile';
}


foreach ($arResult['SECTIONS'] as $key => &$arSection){

    $photoArr = $moduleReize::ResizeImageGet(
        $arSection['DETAIL_PICTURE'],
        array("width"=> 600,"height" => 600),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    );

    $arSection['PICTURE_RESIZED']['SRC'] = $photoArr['src'];
    $arSection['PICTURE_RESIZED']['WIDTH'] = $photoArr['width'];
    $arSection['PICTURE_RESIZED']['HEIGHT'] = $photoArr['height'];
}*/