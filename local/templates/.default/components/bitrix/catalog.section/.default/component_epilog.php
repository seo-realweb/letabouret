<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */

global $APPLICATION;

if ($arResult['PICTURE']['SRC'] && !$APPLICATION->GetProperty('og:image')) {
    $APPLICATION->SetPageProperty('og:image', $arResult['PICTURE']['SRC']);
}

if (isset($templateData['TEMPLATE_THEME'])) {
    $APPLICATION->SetAdditionalCSS($templateFolder . '/themes/' . $templateData['TEMPLATE_THEME'] . '/style.css');
    $APPLICATION->SetAdditionalCSS('/bitrix/css/main/themes/' . $templateData['TEMPLATE_THEME'] . '/style.css', true);
}

if (!empty($templateData['TEMPLATE_LIBRARY'])) {
    $loadCurrency = false;
    if (!empty($templateData['CURRENCIES'])) {
        $loadCurrency = \Bitrix\Main\Loader::includeModule('currency');
    }

    CJSCore::Init($templateData['TEMPLATE_LIBRARY']);

    if ($loadCurrency) {
        ?>
        <script>
            BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
        </script>
        <?
    }
}

//	lazy load and big data json answers
$request = \Bitrix\Main\Context::getCurrent()->getRequest();

if ($request->isAjaxRequest() && ($request->get('action') === 'showMore' || $request->get('action') === 'deferredLoad')) {
    $content = ob_get_contents();
    ob_end_clean();

    list(, $itemsContainer) = explode('<!-- items-container -->', $content);
    list(, $paginationContainer) = explode('<!-- pagination-container -->', $content);

    if ($arParams['AJAX_MODE'] === 'Y') {
        $component->prepareLinks($paginationContainer);
    }

    $component::sendJsonAnswer(array(
        'items' => $itemsContainer,
        'pagination' => $paginationContainer
    ));
}

$GLOBALS['SHOW_PAGE_DESCRIPTION'] = $arResult['SHOW_PAGE_DESCRIPTION'];


global $APPLICATION;
if ($APPLICATION->GetCurPage(false) !== '/'):
    $x = substr($arResult["ID"], -2, 2);
    if ($x > 50) {
        $x = $x / 2 + 3;
    }
    if ($x < 10) {
        $x = 10;
    }
    $cnt = round($x);


    $APPLICATION->AddHeadString('<script type="application/ld+json">{"@context":"https://schema.org/","@type":"Product","name":"' . $arResult['NAME'] . '","description":"' . $arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"] . '","aggregateRating":{"@type":"AggregateRating","ratingValue":"4.8","reviewCount":"' . $cnt . '","bestRating": "5"}}</script>', true);
 endif;
?>

