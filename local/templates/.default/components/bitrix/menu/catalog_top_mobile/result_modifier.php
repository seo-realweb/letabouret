<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/menu/catalog")) {
    $arSections = $obCache->GetVars();
} elseif ($obCache->StartDataCache()) {
    $arSections = [];
    if (CModule::IncludeModule("iblock")) {
        $arMenu = array_column(Realweb\Site\Site::getUserFieldEnumValues(
            array(
                'XML_ID' => 'UF_TYPE_MENU',
                'ENTITY_ID' => 'IBLOCK_' . IBLOCK_CATALOG_CATALOG . '_SECTION'
            )
        ), null, 'XML_ID');
        unset($arMenu['top']);
        unset($arMenu['bottom']);
        $arSections['MENU'] = $arMenu;
        $rsSection = CIBlockSection::GetList(
            array('SORT' => 'ASC'),
            array('IBLOCK_ID' => IBLOCK_CATALOG_CATALOG, 'ACTIVE' => 'Y', 'UF_TYPE_MENU' => array_column($arMenu, 'ID', null))
            , false, array('*', 'UF_*')
        );
        while ($arSection = $rsSection->GetNext()) {
            if ($arSection['UF_PICTURE_MENU']) {
                $arSection['UF_PICTURE_MENU'] = CFile::GetFileArray($arSection['UF_PICTURE_MENU']);
            }
            foreach ($arSection['UF_TYPE_MENU'] as $typeId) {
                $arSections['SECTIONS'][$typeId][$arSection['ID']] = $arSection;
            }
        }

		$arSections['SECTIONS'][$arMenu['style']['ID']] = array();
        $rsSection = CIBlockSection::GetList(
            array('SORT' => 'ASC'),
            array('IBLOCK_ID' => IBLOCK_CATALOG_STYLE, 'ACTIVE' => 'Y')
            , false, array('*', 'UF_*')
        );
        while ($arSection = $rsSection->GetNext()) {
            if ($arSection['UF_PICTURE_MENU']) {
                $arSection['UF_PICTURE_MENU'] = CFile::GetFileArray($arSection['UF_PICTURE_MENU']);
            }
            $arSections['SECTIONS'][$arMenu['style']['ID']][$arSection['ID']] = $arSection;
		}

        if (defined("BX_COMP_MANAGED_CACHE")) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache("/iblock/menu/aks");
            $CACHE_MANAGER->RegisterTag("iblock_id_" . IBLOCK_CATALOG_CATALOG);
            $CACHE_MANAGER->EndTagCache();
        }
    }
    $obCache->EndDataCache($arSections);
}

$arItems = [];
$currentItem = false;
$i = 0;
$prev = 0;
$parent = false;
foreach ($arResult as $arItem) {
    if ($prev > $arItem['DEPTH_LEVEL']) {
        $currentItem = &$parent;
    }
    if ($arItem['DEPTH_LEVEL'] == 1) {
        $arItems[$i] = $arItem;
        $currentItem = &$arItems[$i];
    } else {
        $currentItem['CHILD'][$i] = $arItem;
        if ($arItem['IS_PARENT']) {
            $parent = &$currentItem;
            $currentItem = &$currentItem['CHILD'][$i];
        }
    }
    $prev = $arItem['DEPTH_LEVEL'];
    $i++;
}
$arResult = array('ITEMS' => $arItems, 'CATALOG' => $arSections);