<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$previousLevel = 0;
?>
<? if (!empty($arResult)): ?>
    <ul class="navbar-nav m-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" href="/pomeshcheniya/">
                Каталог<i class="far fa-angle-down"></i>
            </a>
            <?php if (!\Realweb\Site\Site::isCheckGooglePageSpeed()): ?>
                <div class="dropdown-menu dropdown-menu-left">
                    <ul class="nav-sub">
                        <? foreach ($arResult['CATALOG']['MENU'] as $arItem): ?>
                            <? if (!$arResult['CATALOG']['SECTIONS'][$arItem['ID']]) continue; ?>
                            <li class="nav-sub-item">
                                <span class="nav-sub-link"><?= $arItem['VALUE'] ?></span>
                                <ul class="nav-child">
                                    <? foreach ($arResult['CATALOG']['SECTIONS'][$arItem['ID']] as $arSubItem): ?>
                                        <li class="nav-child-item">
                                            <a class="nav-child-link"
                                               href="<?= $arSubItem['UF_MENU_LINK'] ? $arSubItem['UF_MENU_LINK'] : $arSubItem['SECTION_PAGE_URL'] ?>">
                                                <? if ($arSubItem['UF_PICTURE_MENU']): ?>
                                                    <img src="<?= $arSubItem['UF_PICTURE_MENU']['SRC'] ?>">
                                                <? endif; ?>
                                                <span><?= $arSubItem['NAME'] ?></span>
                                            </a>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </li>
                            <? $i++; ?>
                        <? endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </li>

        <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
            <? if (!$arItem['LINK']) continue; ?>
            <li class="nav-item<?= $arItem["CHILD"] ? ' dropdown' : '' ?><?= $arItem["SELECTED"] ? ' active' : '' ?><?= $arItem['PARAMS']['SECTION']['UF_MENU_CLASS'] ? (" " . $arItem['PARAMS']['SECTION']['UF_MENU_CLASS']) : '' ?>">
                <a class="nav-link"
                   href="<?= $arItem['PARAMS']['SECTION']['UF_MENU_LINK'] ? $arItem['PARAMS']['SECTION']['UF_MENU_LINK'] : $arItem["LINK"] ?>">
                    <?= $arItem["TEXT"] ?>
                    <? if ($arItem['CHILD']): ?>
                        <i class="far fa-angle-down"></i>
                    <? endif; ?>
                </a>
                <?php if (!\Realweb\Site\Site::isCheckGooglePageSpeed()): ?>
                    <? if ($arItem['CHILD']): ?>
                        <? $i = 0; ?>
                        <div class="dropdown-menu dropdown-menu-left">
                            <ul class="nav-sub">
                                <? foreach ($arItem['CHILD'] as $arSubItem): ?>
                                    <?= $i % 4 == 0 && $i != 0 ? '</ul><ul class="nav-sub">' : '' ?>
                                    <li class="nav-sub-item">
                                        <a href="<?= $arSubItem['PARAMS']['SECTION']['UF_MENU_LINK'] ? $arSubItem['PARAMS']['SECTION']['UF_MENU_LINK'] : $arSubItem["LINK"] ?>"
                                           class="nav-sub-link"><?= $arSubItem['TEXT'] ?></a>
                                        <? if ($arSubItem['CHILD']): ?>
                                            <ul class="nav-child">
                                                <? foreach ($arSubItem['CHILD'] as $arChildItem): ?>
                                                    <li class="nav-child-item">
                                                        <a class="nav-child-link"
                                                           href="<?= $arChildItem['PARAMS']['SECTION']['UF_MENU_LINK'] ? $arChildItem['PARAMS']['SECTION']['UF_MENU_LINK'] : $arChildItem["LINK"] ?>">
                                                            <? if ($arChildItem['PARAMS']['SECTION']['UF_PICTURE_MENU']): ?>
                                                                <img src="<?= $arChildItem['PARAMS']['SECTION']['UF_PICTURE_MENU']['SRC'] ?>">
                                                            <? endif; ?>
                                                            <span><?= $arChildItem['TEXT'] ?></span>
                                                        </a>
                                                    </li>
                                                <? endforeach; ?>
                                            </ul>
                                        <? endif; ?>
                                    </li>
                                    <? $i++; ?>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    <? endif; ?>
                <?php endif; ?>
            </li>
        <? endforeach ?>

        <li class="nav-item nav-item-more dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMore" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                Еще<i class="far fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right"></ul>
        </li>

    </ul>
<? endif ?>