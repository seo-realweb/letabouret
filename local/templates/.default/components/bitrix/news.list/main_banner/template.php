<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="owl-carousel js-owl-banner">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="banner"
             id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
<!--            style="background-image: url(--><?//= $arItem["PREVIEW_PICTURE"]["SRC"] ?><!--)"-->
            <div class="banner-picture">
                <picture>
                    <source media="(max-width: 575px)" srcset="<?= $arItem["MOBILE_PICTURE"]["SRC"] ?>">
                    <source media="(min-width: 576px)" srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                    <img alt="<?php echo $arItem['NAME']; ?>" class="banner-picture__img" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $arItem["NAME"] ?>">
                </picture>
            </div>
            <a href="<?= ($arItem['PROPERTIES']['LINK']['VALUE'])? $arItem['PROPERTIES']['LINK']['VALUE']: "/"?>" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; cursor: pointer" ></a>
            <a class="banner__link" href="https://www.letabouret.ru/berkenwood-home/laguna/">Перейти в коллекцию<i class="far fa-long-arrow-right"></i></a>
            <? foreach ($arItem['PROPERTIES']['POINTS']['VALUE'] as $value): ?>
                <?php if ($value[2] && $value[3]): ?>
                    <a style="top: <?= $value[1] ?>;left:<?= $value[0] ?>;" href="<?= $value[2] ?>"
                       class="banner__tooltip" data-container=".banner"
                       data-toggle="tooltip"
                       data-placement="right" data-html="true"
                       title='<?= $value[3] ?>'>
                        <i class="fal fa-plus"></i>
                    </a>
                <?php endif; ?>
            <? endforeach; ?>
	</div>

    <? endforeach; ?>
</div>