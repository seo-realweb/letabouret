<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$jsObject = [];
?>

<div class="section-bg section_contacts">
    <div class="container container_md">
<div class="row">
        <div class="main-contacts__content col-md-4">
            <span class="h2 like_h2">шоу-румы в петербурге</span>
            <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <? if ($key != 0): ?>
            <div class="main-delimiter"></div>
            <? endif; ?>
            <div class="main-contacts__item">
                <span class="like_h4"><?= $arItem['NAME'] ?></span>
                <p><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></p>
                <p>
                    <?php foreach ($arItem['PROPERTIES']['PHONE']['VALUE'] as $element) {
                        echo '<a href="tel:' . $element . '">' . $element . '</a>';
                    } ?>
                    <a href="mailto:<?= $arItem['PROPERTIES']['EMAIL']['VALUE'] ?>"><?= $arItem['PROPERTIES']['EMAIL']['VALUE'] ?></a>
                </p>
                <p><?= $arItem['PROPERTIES']['TIME']['VALUE'] ?></p>
            </div>
            <?
                $coordinates = explode(",", $arItem['PROPERTIES']['COORDINATES']['VALUE']);
                $jsObject[$arItem['ID']] = array(
                    'name' => $arItem['NAME'],
                    'lat' => $coordinates[0],
                    'lng' => $coordinates[1],
                );
            ?>
            <? endforeach; ?>
        </div>


    <div class="col-md-8 main-contacts">
        <div id="map_main" class="main-contacts__map">
            <button class="btn btn-primary map-load">Загрузить карту</button>
            <picture class="map_preload-wrap">
                <source media="(max-width: 768px)" srcset="<?= $arResult['MAP_PLACEHOLDER']['MOBILE']['src'] ?>">
                <source media="(max-width: 1023px)" srcset="<?= $arResult['MAP_PLACEHOLDER']['TABLET']['src'] ?>">
                <source media="(min-width: 1024px)" srcset="<?= $arResult['MAP_PLACEHOLDER']['DESKTOP']['src'] ?>">
                <img class="map_preload" src="<?= $arResult['MAP_PLACEHOLDER']['DESKTOP']['src'] ?>" alt="contacts map"/>
            </picture>
        </div>
    </div>
  </div>
    </div>
<script>
    function map_init(){
        if ($('#map_main').length) {
            var myPlacemark;
            var myMap;
                // Создание карты.
            $('.map-load').remove();
            $('.map_preload-wrap').remove();
            myMap = new ymaps.Map("map_main", {
                // Координаты центра карты.
                // Порядок по умолчанию: «широта, долгота».
                // Чтобы не определять координаты центра карты вручную,
                // воспользуйтесь инструментом Определение координат.
                center: [59.999293181154876,30.258947504824416],
                // Уровень масштабирования. Допустимые значения:
                // от 0 (весь мир) до 19.
                zoom: 14
            }), myPlacemark, i = 0, myCollection = new ymaps.GeoObjectCollection();

            if (App.BX.maps) {
                for (var j in App.BX.maps) {
                    myPlacemark = new ymaps.Placemark([App.BX.maps[j].lat, App.BX.maps[j].lng], {
                        hintContent: App.BX.maps[j].name,
                        balloonContent: App.BX.maps[j].name
                    }, {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: 'default#image',
                        // Своё изображение иконки метки.
                        iconImageHref: App.BX.TEMPLATE_PATH + '/images/map.png',
                        // Размеры метки.
                        iconImageSize: [51, 51],
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [-26, -51]
                    });
                    myCollection.add(myPlacemark);
                }
                myMap.geoObjects.add(myCollection);
               // myMap.behaviors.disable('drag');
            }
            if($('.main-contacts__map').hasClass('preloader')){
                $('.main-contacts__map').removeClass('preloader');

            }
        }
    }


    var loadFlag = false;
    $('.map-load').on( "click", function() {
        if(loadFlag == false){
            $('#map_main').addClass('preloader');
            let script = document.createElement('script');
            //скрипт после ините карты вызовет функцию, описанную выше (onload=)
            script.src = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=map_init';
            if(document.body.appendChild(script)){
                loadFlag = true;
            }
        }
    });
	
	// $(document).ready(function(){
		// if(loadFlag == false){
            // $('#map_main').addClass('preloader');
            // let script = document.createElement('script');
          ////  скрипт после ините карты вызовет функцию, описанную выше (onload=)
            // script.src = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=map_init';
            // if(document.body.appendChild(script)){
                // loadFlag = true;
            // }
        // }
	// })
    App.BX.maps = <?= CUtil::PhpToJSObject($jsObject, false, true) ?>;
</script>

<style>
.main-contacts{
		position:relative;
		left:auto;
		right:auto;
		display:block;
}
.main-contacts__content {

    max-width:100%;
    padding-right: 15px;
    padding-left: 15px;

}
	.main-contacts__map {width:100%;}
@media (max-width: 767px){
.main-contacts__content {
    max-width: fit-content;
    padding: 1rem;
}}
</style>