<?php

if(CModule::IncludeModule("slam.image")){
    $moduleReize = 'CSlamImage';
}
else{
    $moduleReize = 'CFile';
}

$arResult['MAP_PLACEHOLDER']['DESKTOP'] = $moduleReize::ResizeImageGet(
    $arResult['ITEMS'][0]['PREVIEW_PICTURE']['ID'],
    array('width'=>782, 'height'=>275),
    BX_RESIZE_IMAGE_EXACT,
    true
);
$arResult['MAP_PLACEHOLDER']['TABLET'] = $moduleReize::ResizeImageGet(
    $arResult['ITEMS'][0]['PREVIEW_PICTURE']['ID'],
    array('width'=>540, 'height'=>275),
    BX_RESIZE_IMAGE_EXACT,
    true
);
$arResult['MAP_PLACEHOLDER']['MOBILE'] = $moduleReize::ResizeImageGet(
    $arResult['ITEMS'][0]['PREVIEW_PICTURE']['ID'],
    array('width'=>740, 'height'=>275),
    BX_RESIZE_IMAGE_EXACT,
    true
);

