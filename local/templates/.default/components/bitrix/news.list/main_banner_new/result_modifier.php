<?php

if(CModule::IncludeModule("slam.image")){
    $moduleReize = 'CSlamImage';
}
else{
    $moduleReize = 'CFile';
}

foreach ($arResult["ITEMS"] as &$arItem){
    $fullPhotoArr = $moduleReize::ResizeImageGet(
        $arItem['PREVIEW_PICTURE']['ID'],
        array("width"=> 1903,"height" => 630),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    );
    $mobilePhotoArr = $moduleReize::ResizeImageGet(
        $arItem['PREVIEW_PICTURE']['ID'],
        array("width"=> 540,"height" => 400),
        BX_RESIZE_IMAGE_EXACT,
        true
    );
    $arItem['PREVIEW_PICTURE']['SRC'] = $fullPhotoArr['src'];
    $arItem['PREVIEW_PICTURE']['WIDTH'] = $fullPhotoArr['width'];
    $arItem['PREVIEW_PICTURE']['HEIGHT'] = $fullPhotoArr['height'];

    $arItem['MOBILE_PICTURE']['SRC'] = $mobilePhotoArr['src'];
    $arItem['MOBILE_PICTURE']['WIDTH'] = $mobilePhotoArr['width'];
    $arItem['MOBILE_PICTURE']['HEIGHT'] = $mobilePhotoArr['height'];


    /*if($moduleReize == "CSlamImage")
        $photo['SRC'] = createWebp($photo['SRC'], 95);*/
}