<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="owl-carousel js-owl-banner">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <div class="banner"
             id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <!--            style="background-image: url(--><? //= $arItem["PREVIEW_PICTURE"]["SRC"] ?><!--)"-->
            <a href="<?=$arItem['CODE'];?>">
            <div class="banner-picture">




    <picture>
                    <source media="(max-width: 575px)" srcset="<?= $arItem["MOBILE_PICTURE"]["SRC"] ?>">
                    <source media="(min-width: 576px)" srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                    <img alt="<?php echo $arItem['NAME']; ?>" class="banner-picture__img"
                         src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $arItem["NAME"] ?>">
                </picture>
                <?
                $addclass='';
                $hide='';
                if ($arItem['PROPERTIES']['POSITION']['VALUE']=='Слева'){
                    $addclass='fl_left';
                }
                if ($arItem['PROPERTIES']['POSITION']['VALUE']=='Справа'){
                    $addclass='fl_right';
                }
                if ($arItem['PROPERTIES']['HIDEBLOCK']['VALUE']=='Да'){
                    $hide='d-none';
                }
                ?>
                <div class="main_slider_info <?=$addclass;?><?=$hide;?>">
                    <div class="in_text" style="color:<?= $arItem['PROPERTIES']['TEXT_COLOR']['VALUE']; ?>;">
                        <div class="collections__name"><?= $arItem['NAME']; ?></div>
                        <div class="main_slider_text"><?= $arItem['PREVIEW_TEXT']; ?></div>
                        <? if ($arItem['PROPERTIES']['SHOW_BUTTON']['VALUE'] == 'Да') { ?>
                                <br>
                            <span class="slider_more" ><?= $arItem['PROPERTIES']['BUTTON_TEXT']['VALUE']; ?></span>
                        <? } ?>
                    </div>


                </div>



            </div>

            </a>
        </div>
    <? endforeach; ?>
</div>
