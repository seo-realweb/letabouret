<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$jsObject = [];
?>

<div class="section-bg section_contacts">
    <div class="container container">
<div class="row">


        <div class="col-md-4 main-contacts__content">
            <h2 class="h2">шоу-румы в петербурге</h2>
            <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <? if ($key != 0): ?>
                <div class="main-delimiter"></div>
            <? endif; ?>
            <div class="main-contacts__item" itemscope itemtype="http://schema.org/Organization">
                <div itemscope itemtype="https://schema.org/LocalBusiness">
                    <div itemscope itemtype="https://schema.org/Store">
                        <div itemscope itemtype="http://schema.org/FurnitureStore">

                            <h4 itemprop="name"><?= $arItem['NAME'] ?></h4>
                            <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                <p itemprop="streetAddress"><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></p>
                            </div>
                            <p>
                                <?php foreach ($arItem['PROPERTIES']['PHONE']['VALUE'] as $element) {
                                    $el = explode(',', $element);
                                    $element = $el[0];
                                    echo '<a href="tel:' . $element . '">' . $element . '</a>';
                                } ?>
                                <span itemprop="telephone" style="display:none"><?=$element;?></span>
                                <a itemprop="email"
                                   href="mailto:<?= $arItem['PROPERTIES']['EMAIL']['VALUE'] ?>"><?= $arItem['PROPERTIES']['EMAIL']['VALUE'] ?></a>
                            </p>
                            <p><?= $arItem['PROPERTIES']['TIME']['VALUE'] ?></p>
                            <div style="display:none">

                      <span itemprop="paymentAccepted" content="Cash, Credit Card">

  </span>
                                <span itemprop="priceRange">$$</span>
                            </div>
                        </div>
                        <?
                        $coordinates = explode(",", $arItem['PROPERTIES']['COORDINATES']['VALUE']);
                        $jsObject[] = array(
                            'name' => $arItem['NAME'],
                            'lat' => $coordinates[0],
                            'lng' => $coordinates[1],
                        );
                        ?>

                        <div class="js-contacts-mobile-map"><br></div>
                             <? if ($key != 0): ?>
                        <br/>
                        <? \Realweb\Site\Site::showIncludeText('BANK_PROPS') ?>
                            <? endif; ?>
                    </div>
                </div>
            </div>
                                    <? endforeach; ?>
        </div>
          <div class="col-md-8">
           <div id="map" class="main-contacts__map" style="width: 100%;"></div>
          </div>
        </div>
   
   </div>
   
</div>
<div class="section-bg">
</div>
<script>
    function map_init_cont() {
        if ($('#map').length) {
            $('.map-load').remove();
            $('.map_preload-wrap').remove();
            // Создание карты.
            var myMap = new ymaps.Map("map", {
                // Координаты центра карты.
                // Порядок по умолчанию: «широта, долгота».
                // Чтобы не определять координаты центра карты вручную,
                // воспользуйтесь инструментом Определение координат.
                center: [59.999293181154876,30.258947504824416],
                // Уровень масштабирования. Допустимые значения:
                // от 0 (весь мир) до 19.
                zoom: 14
            }), myPlacemark, i = 0, myCollection = new ymaps.GeoObjectCollection();
            if (App.BX.maps) {
                console.log(App.BX.maps);
                for (var j in App.BX.maps) {
                    myPlacemark = new ymaps.Placemark([App.BX.maps[j].lat, App.BX.maps[j].lng], {
                        hintContent: App.BX.maps[j].name,
                        balloonContent: App.BX.maps[j].name
                    }, {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: 'default#image',
                        // Своё изображение иконки метки.
                        iconImageHref: App.BX.TEMPLATE_PATH + '/images/map.png',
                        // Размеры метки.
                        iconImageSize: [51, 51],
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [-26, -51]
                    });
                    myCollection.add(myPlacemark);
                }
                myMap.geoObjects.add(myCollection);
                // myMap.behaviors.disable('drag');
                //myMap.setBounds(myCollection.getBounds(), {checkZoomRange:true,zoomMargin: 100});
            }
            if ($('.main-contacts__map').hasClass('preloader')) {
                $('.main-contacts__map').removeClass('preloader');

            }

        }
    }

    var loadFlag = false;
    $(document).ready(function () {
        if (loadFlag == false) {
            if (window.innerWidth < 768) {
                $('.main-contacts').appendTo($('.js-contacts-mobile-map'));
            }
            $('#map').addClass('preloader');
            let script = document.createElement('script');
            //скрипт после ините карты вызовет функцию, описанную выше (onload=)
            script.src = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=map_init_cont';
            if (document.body.appendChild(script)) {
                loadFlag = true;
            }
        }
    });

    App.BX.maps = <?= CUtil::PhpToJSObject($jsObject, false, true) ?>;
</script>
<style>
.main-contacts__content {

    max-width:100%;
    padding-right: 15px;
    padding-left: 15px;

}
	
@media (max-width: 767px){
.main-contacts__content {
    max-width: fit-content;
    padding: 1rem;
}}
</style>