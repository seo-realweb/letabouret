<?php

if(CModule::IncludeModule("slam.image")){
    $moduleReize = 'CSlamImage';
}
else{
    $moduleReize = 'CFile';
}

$arResult['MAP_PLACEHOLDER']['DESKTOP'] = $moduleReize::ResizeImageGet(
    $arResult['ITEMS'][0]['DETAIL_PICTURE']['ID'],
    array('width'=>1050, 'height'=>783),
    BX_RESIZE_IMAGE_EXACT,
    true
);
$arResult['MAP_PLACEHOLDER']['TABLET'] = $moduleReize::ResizeImageGet(
    $arResult['ITEMS'][0]['DETAIL_PICTURE']['ID'],
    array('width'=>540, 'height'=>467),
    BX_RESIZE_IMAGE_EXACT,
    true
);
$arResult['MAP_PLACEHOLDER']['MOBILE'] = $moduleReize::ResizeImageGet(
    $arResult['ITEMS'][0]['DETAIL_PICTURE']['ID'],
    array('width'=>740, 'height'=>300),
    BX_RESIZE_IMAGE_EXACT,
    true
);

