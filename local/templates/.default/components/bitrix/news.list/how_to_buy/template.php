<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
<div id="bx_incl_area_4"><ul id="how-to-buy-list" style="padding-left:0;">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?$i=0;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$i++;
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<li class="how-to-buy-item-<?=$i?>">
		<h2><?=$i?>. <?echo $arItem["NAME"]?></h2>
		<?=$arItem["PREVIEW_TEXT"]?>
		<?if (is_array($arItem["PROPERTIES"]["UF_HOWTOBUYTARIFF"]["VALUE"])>0):
			//print_r($arItem["PROPERTIES"]["UF_HOWTOBUYTARIFF"]);
			$j=0;?>
			<div class="tarify-block">
				<h3 class="tarifchyk"> Тарифы:</h3>
				
			<?foreach ($arItem["PROPERTIES"]["UF_HOWTOBUYTARIFF"]["VALUE"] as $row):?>
				<?$j++;?>
				<div class="blochek<?=$j?> class12">
					<span class="htb-step step-9"><?=$arItem["PROPERTIES"]["UF_HOWTOBUYTARIFF"]["DESCRIPTION"][$j-1]?></span>
				</div>
				<div class="super-block" style="display: none;">
					<?=str_replace(array("&lt;", "&gt;", "&quot;"), array("<",">","\""), $row["TEXT"])?>
				</div>
				
			<?endforeach?>
			</div>
		<?endif?>
	</li>
	<!--<p class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img
						class="preview_picture"
						border="0"
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
						height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						style="float:left"
						/></a>
			<?else:?>
				<img
					class="preview_picture"
					border="0"
					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
					width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
					height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
					style="float:left"
					/>
			<?endif;?>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
			<span class="news-date-time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
		<?endif?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><b><?echo $arItem["NAME"]?></b></a><br />
			<?else:?>
				<b><?echo $arItem["NAME"]?></b><br />
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<?echo $arItem["PREVIEW_TEXT"];?>
		<?endif;?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<div style="clear:both"></div>
		<?endif?>
		<?foreach($arItem["FIELDS"] as $code=>$value):?>
			<small>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
			</small><br />
		<?endforeach;?>
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<small>
			<?=$arProperty["NAME"]?>:&nbsp;
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
				<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
			<?else:?>
				<?=$arProperty["DISPLAY_VALUE"];?>
			<?endif?>
			</small><br />
		<?endforeach;?>
	</p>-->
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</ul></div>
</div>
<div class="tarify-block" style=" margin-left: 25px;">
	<div class="cirk1 cirk">
		 &nbsp;
	</div>
	<div class="cirk2 cirk">
		 &nbsp;
	</div>
	<div class="cirk3 cirk">
		 &nbsp;
	</div>
	<div class="cirk4 cirk">
		 &nbsp;
	</div>
	<div class="cirk5 cirk">
		 &nbsp;
	</div>
	<div class="cirk6 cirk">
		 &nbsp;
	</div>
	<div class="cirk7 cirk">
		 &nbsp;
	</div>
 <img src="/bitrix/images/img/how-to-buy/circle23.PNG" alt="">
</div>
<p>
	 <style type="text/css">
.htb-step {
display: block !important;
}	</style>
</p>
 <br>
<script>
$(document).ready(function(){
$('.super-block').hide();
$('.class12').click(function(){
    $(this).next().toggle()});
});
</script>