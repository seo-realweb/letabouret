<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>
    <div style="margin-top:20px"></div>
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        echo '<pre>';
        $arPreg = preg_match('a:2:{s:4:"TEXT";s:\d*:"(.*)";s:4:"TYPE";s:4:"HTML";}', $arItem['PROPERTIES']["UF_REVIEWREVIEW"]["VALUE"]['TEXT']);
        print_r($arPreg);
        echo '</pre>';
        $answer = $arItem["DISPLAY_PROPERTIES"]["UF_REVIEWANSWER"]["VALUE"]["TEXT"];
        $answer = str_replace(array("&lt;", "&gt;", "&amp;"), array("<", ">", "&"), $answer);
        ?>
        <div class="faq_tab" itemscope itemtype="https://schema.org/Review">
            <span class="d-none">
                <meta itemprop="datePublished" content="<?php echo date_format(date_create($arItem["PROPERTIES"]["UF_REVIEWDATE"]["VALUE"]), 'Y-m-d')?>"/>
                <span itemprop="name">
                    <link itemprop="url" href="<?php echo \Realweb\Site\Site::getCurrentPage(); ?>?id=<?php echo $arItem['ID']; ?>">
                </span>
                <span itemprop="author" itemscope itemtype="https://schema.org/Person">
                    <span itemprop="name"><?= $arItem["DISPLAY_PROPERTIES"]["UF_REVIEWNAME"]["VALUE"] ?></span>
                </span>
                <span itemprop="itemReviewed" itemscope itemtype="https://schema.org/Organization">
                    <meta itemprop="name" content="Le tabouret">
                    <p itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
                        <meta itemprop="addressLocality" content="Санкт-Петербург">
                        <meta itemprop="streetAddress" content="Богатырский пр. д. 18 А, к. 2, 2-й этаж, секции 300, 302">
                    </p>
                </span>
                <span itemprop="reviewRating" itemscope itemtype="https://schema.org/Rating">
                    <meta itemprop="worstRating" content="5">
                    <meta itemprop="ratingValue" content="5">
                    <meta itemprop="bestRating" content="5"/>
                </span>
            </span>
            <table style="width:100%;">
                <tr>
                    <td colspan="2" class="faq_kto"
                        style="padding-top:10px"><?= $arItem["DISPLAY_PROPERTIES"]["UF_REVIEWNAME"]["VALUE"] ?>
                        / <?= $arItem["DISPLAY_PROPERTIES"]["UF_REVIEWDATE"]["VALUE"] ?></td>
                </tr>
                <tr style="vertical-align:top;">
                    <td style="width:8%;" class="faq_v"><span>Отзыв:</span></td>
                    <td style="width:92%;" class="faq_v" itemprop="reviewBody"><?= $arItem["DISPLAY_PROPERTIES"]["UF_REVIEWREVIEW"]["VALUE"]["TEXT"] ?></td>
                </tr>
                <tr style="vertical-align:top;">
                    <td style="width:8%;" class="faq_v"><span>Ответ:</span></td>
                    <td style="width:92%;" class="faq_o"><?= $answer ?>
                    </td>
                </tr>
            </table>
        </div>

    <? endforeach; ?>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>
