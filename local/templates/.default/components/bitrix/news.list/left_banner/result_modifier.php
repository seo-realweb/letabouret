<?

if (CModule::IncludeModule("slam.image")) {
    $moduleReize = 'CSlamImage';
} else {
    $moduleReize = 'CFile';
}


foreach ($arResult["ITEMS"] as &$arItem) {
    $photoArr = $moduleReize::ResizeImageGet(
        $arItem["PREVIEW_PICTURE"],
        array("width" => 238, "height" => 342),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    );
    $arItem["PREVIEW_PICTURE"]['SRC'] = $photoArr['src'];
}
