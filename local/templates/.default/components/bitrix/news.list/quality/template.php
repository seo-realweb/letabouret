<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="quality">
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>
    <? $i = 0; ?>
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $i++;
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="accordeon-item">
            <div class="accordeon-item-top">
                <? if ($arItem["PREVIEW_PICTURE"]["SRC"] != '') { ?>
                    <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $arItem["NAME"] ?>">
                <? } ?>
                <div class="item-wrap-acc">
                    <?php if (!$arItem['PROPERTIES']['HIDE_NAME']['VALUE']): ?>
                        <h2><? echo $arItem["NAME"] ?></h2>
                    <?php endif; ?>
                    <p>
                        <? echo $arItem["PREVIEW_TEXT"]; ?>
                    </p>
                </div>
            </div>
            <div class="accordeon-item-content">
                <? echo $arItem["DETAIL_TEXT"]; ?>
            </div>
        </div>
    <? endforeach; ?>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>