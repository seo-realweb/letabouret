<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
    'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList,
    'ITEM' => array(
        'ID' => $arResult['ID'],
        'IBLOCK_ID' => $arResult['IBLOCK_ID'],
        'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
        'JS_OFFERS' => $arResult['JS_OFFERS']
    )
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
    'ID' => $mainId,
    'DISCOUNT_PERCENT_ID' => $mainId . '_dsc_pict',
    'STICKER_ID' => $mainId . '_sticker',
    'BIG_SLIDER_ID' => $mainId . '_big_slider',
    'BIG_IMG_CONT_ID' => $mainId . '_bigimg_cont',
    'SLIDER_CONT_ID' => $mainId . '_slider_cont',
    'OLD_PRICE_ID' => $mainId . '_old_price',
    'PRICE_ID' => $mainId . '_price',
    'DISCOUNT_PRICE_ID' => $mainId . '_price_discount',
    'PRICE_TOTAL' => $mainId . '_price_total',
    'SLIDER_CONT_OF_ID' => $mainId . '_slider_cont_',
    'QUANTITY_ID' => $mainId . '_quantity',
    'QUANTITY_DOWN_ID' => $mainId . '_quant_down',
    'QUANTITY_UP_ID' => $mainId . '_quant_up',
    'QUANTITY_MEASURE' => $mainId . '_quant_measure',
    'QUANTITY_LIMIT' => $mainId . '_quant_limit',
    'BUY_LINK' => $mainId . '_buy_link',
    'ADD_BASKET_LINK' => $mainId . '_add_basket_link',
    'BASKET_ACTIONS_ID' => $mainId . '_basket_actions',
    'NOT_AVAILABLE_MESS' => $mainId . '_not_avail',
    'COMPARE_LINK' => $mainId . '_compare_link',
    'TREE_ID' => $mainId . '_skudiv',
    'DISPLAY_PROP_DIV' => $mainId . '_sku_prop',
    'DISPLAY_MAIN_PROP_DIV' => $mainId . '_main_sku_prop',
    'OFFER_GROUP' => $mainId . '_set_group_',
    'BASKET_PROP_DIV' => $mainId . '_basket_prop',
    'SUBSCRIBE_LINK' => $mainId . '_subscribe',
    'TABS_ID' => $mainId . '_tabs',
    'TAB_CONTAINERS_ID' => $mainId . '_tab_containers',
    'SMALL_CARD_PANEL_ID' => $mainId . '_small_card_panel',
    'TABS_PANEL_ID' => $mainId . '_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob' . preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
    : $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
    : $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
    : $arResult['NAME'];


$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers) {
    $actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
        ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
        : reset($arResult['OFFERS']);
    $showSliderControls = false;

    foreach ($arResult['OFFERS'] as $offer) {
        if ($offer['MORE_PHOTO_COUNT'] > 1) {
            $showSliderControls = true;
            break;
        }
    }
} else {
    $actualItem = $arResult;
    $showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
    'left' => 'product-item-label-left',
    'center' => 'product-item-label-center',
    'right' => 'product-item-label-right',
    'bottom' => 'product-item-label-bottom',
    'middle' => 'product-item-label-middle',
    'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION'])) {
    foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos) {
        $discountPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
    }
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION'])) {
    foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos) {
        $labelPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
    }
}
if (!empty($arResult['SECTION']['UF_ELEMENT_TEMPLATE_DESCRIPTION'])) {
    $arResult['DETAIL_TEXT'] .= str_replace(
        array('#NAME#', '#PRICE#'),
        array($name, $price['PRINT_RATIO_PRICE']),
        $arResult['SECTION']['UF_ELEMENT_TEMPLATE_DESCRIPTION']
    );
}
?>
<? $this->SetViewTarget('ShowElementArticle'); ?>
    <div class="element__article">Артикул: <?= $arResult['PROPERTIES']['ARTICUL']['VALUE'] ?></div>
<? $this->EndViewTarget(); ?>


    <div class="element__data" id="<?= $itemIds['ID'] ?>"
         itemscope itemtype="http://schema.org/Product">
        <div class="row row-10 mb-4">
            <div class="col col-12 col-md-12 col-lg-6" data-entity="images-slider-block">
                <div class="product-tag product-tag_top_left">
                    <a class="cat__favorite" href="javascript:void(0);" data-id="<?= $arResult['ID'] ?>">
                        <i class=" far fa-heart"></i>
                    </a>
                </div>
                <? if ($arResult['PROPERTIES']['TAG']['VALUE']): ?>
                    <?= \Realweb\Site\Site::showTags($arResult['PROPERTIES']['TAG']['VALUE']) ?>
                <? endif; ?>
                <div style="display: none">
                    <div id="<?= $itemIds['STICKER_ID'] ?>" <?= (!$arResult['LABEL'] ? 'style="display: none;"' : '') ?>>
                        <?
                        if ($arResult['LABEL'] && !empty($arResult['LABEL_ARRAY_VALUE'])) {
                            foreach ($arResult['LABEL_ARRAY_VALUE'] as $code => $value) {
                                ?>
                                <div>
                                    <span title="<?= $value ?>"><?= $value ?></span>
                                </div>
                                <?
                            }
                        }
                        ?>
                    </div>
                    <? if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y') {
                        if ($haveOffers) {
                            ?>
                            <div id="<?= $itemIds['DISCOUNT_PERCENT_ID'] ?>"
                                 style="display: none;">
                            </div>
                            <?
                        } else {
                            if ($price['DISCOUNT'] > 0) {
                                ?>
                                <div id="<?= $itemIds['DISCOUNT_PERCENT_ID'] ?>"
                                     title="<?= -$price['PERCENT'] ?>%">
                                    <span><?= -$price['PERCENT'] ?>%</span>
                                </div>
                                <?
                            }
                        }
                    } ?>
                </div>
                <span class="product-item-detail-slider-close" data-entity="close-popup"></span>
                <div class="swiper-with-nav catalog-product-slider js-catalog-product-slider"
                     id="<?= $itemIds['BIG_SLIDER_ID'] ?>" data-entity="images-container">

                    <div class="swiper swiper-main js-swiper-main" id="lightgallery">
                        <div class="swiper-container js-swiper-slider">
                            <div class="swiper-wrapper">
                                <? foreach ($actualItem['MORE_PHOTO'] as $key => $photo): ?>
                                    <span class="lazy-img-wrap catalog-product-slide swiper-slide" data-entity="image"
                                          data-lg-size="<?= $photo['WIDTH'] ?>-<?= $photo['HEIGHT'] ?>"
                                          data-src="<?= $photo['SRC'] ?>">
                                        <img class="swiper-lazy catalog-product-slide-img"
                                             src="<?= $actualItem['MORE_PHOTO_RESIZED'][$key]['SRC'] ?>"
                                             data-src="<?= $actualItem['MORE_PHOTO_RESIZED'][$key]['SRC'] ?>"
                                             alt="<?= $alt ?>">
                                    </span>
                                <? endforeach; ?>
                            </div>
                        </div>
                    </div>

                    <div class="swiper swiper-nav js-swiper-nav">
                        <div class="swiper-container js-swiper-slider">
                            <div class="swiper-wrapper">
                                <? if ($showSliderControls): ?>
                                    <? foreach ($actualItem['MORE_PHOTO_RESIZED'] as $key => $photo): ?>
                                        <span class="lazy-img-wrap swiper-thumbnail swiper-slide">
                                            <img
                                                class="swiper-lazy swiper-thumbnail-img"
                                                src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                data-src="<?= $photo['SRC'] ?>" alt="img">
                                        </span>
                                    <? endforeach; ?>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-none d-md-block mt-4">
                    <?php

                    if ($arResult['PROPERTIES']['BRAND']['VALUE'] == 'MATRAMAX') {
                        $tmp = 'matras';
                    } else {
                        $tmp = 'description';
                    }

                    $APPLICATION->IncludeComponent('realweb:blank', 'left_' . $tmp, array('RESULT' => $arResult, 'ID' => 'desktop'), $component->getParent()); ?>
                </div>
            </div>
            <div class="col col-12 col-md-12 col-lg-6">
                <div class="row" data-entity="tab-container"
                     data-value="properties">

                    <? $codes = array("MATERIAL_KORPUSA", "MATERIAL_OBIVKI");
                    if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']) {
                        if (!empty($arResult['DISPLAY_PROPERTIES'])) {
                            $i = 0;
                            foreach ($arResult['DISPLAY_PROPERTIES'] as $property) {
                                if ($i % 2 == 0 && $i) echo '<div class="col-12"></div>';
                                if (!in_array($property['CODE'], $codes)) {
                                    ?>
                                    <div class="col col-12 col-sm-6 col-md-6 col-lg-12 col-xl-6">
                                        <div class="element__prop">
                                            <div class="element__name"><?= $property['NAME'] ?></div>
                                            <div class="element__value">
                                                <? if ($property['CODE'] == 'COLLECTION'): ?>
                                                    <? if ($arResult['SECTION_COLLECTION']): ?>
                                                        <a href="<?= $arResult['SECTION_COLLECTION'] ?>"><?= (
                                                            is_array($property['DISPLAY_VALUE'])
                                                                ? implode(' / ', $property['DISPLAY_VALUE'])
                                                                : $property['DISPLAY_VALUE']
                                                            ) ?></a>
                                                    <? else: ?>
                                                        <? foreach ($property['VALUE'] as $key => $value): ?>
                                                            <a href="<?= $property['FULL_VALUE'][$key]['UF_LINK'] ?>"><?= $property['FULL_VALUE'][$key]['UF_NAME'] ?></a>
                                                        <? endforeach; ?>
                                                    <? endif; ?>
                                                <? else: ?>
                                                    <?= (
                                                    is_array($property['DISPLAY_VALUE'])
                                                        ? implode(' / ', $property['DISPLAY_VALUE'])
                                                        : $property['DISPLAY_VALUE']
                                                    ) ?>
                                                <? endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                    $i++;
                                }
                            }
                            unset($property);
                        }
                        if ($arResult['SHOW_OFFERS_PROPS']) {
                            ?>
                            <div class="col-6 product-item-detail-properties"
                                 id="<?= $itemIds['DISPLAY_PROP_DIV'] ?>"></div>
                            <?
                        }
                    } ?>
                </div>
                <div class="element__tags"></div>
                <div class="row row-10 align-items-center">
                    <? if ($price['RATIO_PRICE'] > 0 && !$arResult['PROPERTIES']['QUERYPRICE']['VALUE']): ?>
                        <div class="col col-auto">
                            <div class="product__query-price"><i class="fal fa-ruble-sign"></i></div>
                        </div>
                        <div class="col col-auto">
                            <?
                            if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
                                ?>
                                <div class="price-old"
                                     id="<?= $itemIds['OLD_PRICE_ID'] ?>"
                                     style="display: <?= ($showDiscount ? '' : 'none') ?>;">
                                    <?= ($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '') ?>
                                </div>
                                <div class="d-none" id="<?= $itemIds['DISCOUNT_PRICE_ID'] ?>"
                                     style="display: <?= ($showDiscount ? '' : 'none') ?>;">
                                    <? if ($showDiscount) {
                                        echo Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO2', array('#ECONOMY#' => $price['PRINT_RATIO_DISCOUNT']));
                                    } ?>
                                </div>
                                <?
                            }
                            ?>
                            <div class="product__price-price <?= $price['RATIO_PRICE'] < $price['RATIO_BASE_PRICE'] ? 'price-new' : '' ?>"
                                 id="<?= $itemIds['PRICE_ID'] ?>">
                                <?
                                if ($arResult['PROPERTIES']['PRICE_OT']['VALUE']) {
                                    echo Loc::getMessage(
                                        'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                                        array(
                                            '#PRICE#' => $price['PRINT_RATIO_PRICE']
                                        )
                                    );
                                } else {
                                    echo $price['PRINT_RATIO_PRICE'];
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col col-auto" data-entity="main-button-container">
                            <div class="element__action" id="<?= $itemIds['BASKET_ACTIONS_ID'] ?>"
                                 style="display: <?= ($actualItem['CAN_BUY'] ? '' : 'none') ?>;">
                                <?
                                if ($showAddBtn) {
                                    ?>
                                    <a class="btn <?= $showButtonClassName ?> product-item-detail-buy-button"
                                       id="<?= $itemIds['ADD_BASKET_LINK'] ?>"
                                       href="javascript:void(0);">
                                        <i class="far fa-shopping-cart"></i>
                                        <span><?= $arParams['MESS_BTN_ADD_TO_BASKET'] ?></span>
                                    </a>
                                    <?
                                }

                                if ($showBuyBtn) {
                                    ?>
                                    <a class="btn <?= $buyButtonClassName ?> product-item-detail-buy-button"
                                       id="<?= $itemIds['BUY_LINK'] ?>"
                                       href="javascript:void(0);">
                                        <i class="far fa-shopping-cart"></i>
                                        <span><?= $arParams['MESS_BTN_BUY'] ?></span>
                                    </a>
                                    <?
                                }
                                ?>
                            </div>
                            <div class="element__action"
                                 style="display: <?= ($actualItem['CAN_BUY'] ? '' : 'none') ?>;">
                                <a id="buyOneClickButton"
                                   class="btn product-item-detail-buy-button"
                                   data-product-id="<?= $actualItem['ID'] ?>"
                                   data-product-name="<?= $arResult['NAME'] ?>"
                                   data-product-picture="<?= reset($actualItem['MORE_PHOTO'])['SRC'] ?>"
                                   href="#oneByClickModal" data-toggle="modal">
                                    <i class="fal fa-shopping-bag"></i>
                                    <span>Купить в 1 клик</span>
                                </a>
                            </div>
                        </div>
                    <? else: ?>
                        <div class="col col-auto">
                            <div class="element__action">
                                <div class="product__query-price"><i class="fal fa-ruble-sign"></i></div>
                                <a class="product__query"
                                   data-product="<?= "[" . $arResult['ID'] . "]" . htmlspecialchars($arResult['NAME']) ?>"
                                   data-toggle="modal" href="#queryPriceModal">Запросить цену</a>
                            </div>
                        </div>
                        <div class="col col-auto">
                            <a class="element__link-price" href="#"
                               data-toggle="tooltip" data-placement="right" data-html="true"
                               title="<? \Realweb\Site\Site::showIncludeText('PRODUCT_PRICE_HINT') ?>">Почему цена
                                доступна<br/>
                                только по запросу?</a>
                        </div>
                    <? endif; ?>
                </div>
                <? if ($haveOffers && !empty($arResult['OFFERS_PROP'])) {
                    ?>
                    <div class="row mt-5 mb-5" id="<?= $itemIds['TREE_ID'] ?>">
                        <?php if (!empty($arResult['OFFERS_PROP']['WIDTH']) || !empty($arResult['OFFERS_PROP']['LENGTH'])): ?>
                            <?php if ($arResult['PROPERTIES']['CATALOG']['FULL_VALUE']): ?>
                                <?php if (mb_stripos($arResult['PROPERTIES']['CATALOG']['FULL_VALUE']['UF_NAME'], 'кровати') !== false): ?>
                                    <div class="col-12">
                                        <div class="element__caption mt-0">Выберите размер кровати (в см.) и категорию
                                            ткани
                                        </div>
                                    </div>
                                <?php elseif (mb_stripos($arResult['PROPERTIES']['CATALOG']['FULL_VALUE']['UF_NAME'], 'матрасы') !== false): ?>
                                    <div class="col-12">
                                        <div class="element__caption mt-0">Выберите размер матраса (в см.)</div>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?
                        foreach ($arResult['SKU_PROPS'] as $skuProperty) {
                            if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
                                continue;

                            $propertyId = $skuProperty['ID'];
                            $skuProps[] = array(
                                'ID' => $propertyId,
                                'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                                'VALUES' => $skuProperty['VALUES'],
                                'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                            );
                            $skuProperty['SHOW_MODE'] = 'TEXT';
                            ?>
                            <div class="product-item-detail-info-container col-6" data-entity="sku-line-block">
                                <?= htmlspecialcharsEx($skuProperty['NAME']) ?>
                                <div class="dropdown filter__dropdown">
                                    <button class="product-item-detail-info-container-title btn-select"
                                            data-toggle="dropdown"><? ?></button>
                                    <div class="product-item-scu-container">
                                        <div class="product-item-scu-block">
                                            <div class="product-item-scu-list">
                                                <ul class="product-item-scu-item-list dropdown-menu dropdown-menu-select">
                                                    <?
                                                    foreach ($skuProperty['VALUES'] as &$value) {
                                                        $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                                        if ($skuProperty['SHOW_MODE'] === 'PICT' && $value['PICT']['ID']) {
                                                            ?>
                                                            <li class="product-item-scu-item-color-container dropdown-text"
                                                                title="<?= $value['NAME'] ?>"
                                                                data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                                                data-onevalue="<?= $value['ID'] ?>">
                                                                <div class="product-item-scu-item-color-block">
                                                                    <div class="product-item-scu-item-color"
                                                                         title="<?= $value['NAME'] ?>"
                                                                         style="background-image: url(<?= $value['PICT']['SRC'] ?>);">
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <?
                                                        } else {
                                                            ?>
                                                            <li class="product-item-scu-item-text-container dropdown-text"
                                                                title="<?= $value['NAME'] ?>"
                                                                data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                                                data-onevalue="<?= $value['ID'] ?>">
                                                                <div class="product-item-scu-item-text-block">
                                                                    <div class="product-item-scu-item-text"><?= $value['NAME'] ?></div>
                                                                </div>
                                                            </li>
                                                            <?
                                                        }
                                                    }
                                                    ?>
                                                </ul>
                                                <div style="clear: both;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                    <?
                } ?>
                <?
                //если detail или preview text не заполнены - откроем по умолчанию вкладку характеристики

                if (empty($arResult['PREVIEW_TEXT']) && empty($arResult['DETAIL_TEXT'])) {
                    $showCharsTab = true;
                }
                ?>
                <? if (!empty($arResult['ICONS'])) { ?>
                    <ul class="product_icons">
                        <?
                        foreach ($arResult['ICONS'] as $ICON) {
                            ?>

                            <li><img src="<?= CFile::GetPath($ICON['PREVIEW_PICTURE']) ?>"
                                     alt="<?= $ICON['NAME']; ?> - преимущество матрасов Le Tabouret"
                                     title="<?= $ICON['NAME']; ?>"></li>
                        <? } ?>

                    </ul>

                <? } ?>
                <ul class="nav nav-tabs element__tabs-nav mt-4" id="elementTab" role="tablist">

                    <li class="nav-item">
                        <a class="nav-link <? if ($showCharsTab): ?>active show<? endif; ?>" id="props-tab"
                           data-toggle="tab" href="#props"
                           role="tab" aria-controls="props"
                           aria-selected="<? if ($showCharsTab): ?>true<? else: ?>false<? endif; ?>">характеристики</a>
                    </li>
                    <? if ($arResult['DETAIL_TEXT'] != '' || $arResult['PREVIEW_TEXT'] != '') { ?>
                        <li class="nav-item">
                            <a class="nav-link <? if (!$showCharsTab): ?>active<? endif; ?>" id="about-tab"
                               data-toggle="tab" href="#about"
                               role="tab" aria-controls="about" aria-selected="false">описание</a>
                        </li>
                    <? } ?>
                    <? if (is_array($arResult['PROPERTIES']['VIDEO']['VALUE']) && count($arResult['PROPERTIES']['VIDEO']['VALUE']) > 0): ?>
                        <li class="nav-item js-video-scroll">
                            <a class="nav-link" id="video-descr">видео</a>
                        </li>
                    <? endif; ?>
                    <? if (!empty($arResult['PROPERTIES']['TECH_FEATURES']['VALUE'])): ?>
                        <li class="nav-item">
                            <a target="_blank"
                               href="<?= CFile::GetPath($arResult['PROPERTIES']['TECH_FEATURES']['VALUE']); ?>"
                               class="nav-link" id="video-descr">Техническая информация</a>
                        </li>
                    <? endif; ?>
                </ul>

                <div class="tab-content element__tabs-content" id="elementTabContent">
                    <div class="tab-pane fade show <? if (!$showCharsTab): ?>active<? endif; ?> tab-pane-descr"
                         id="about" role="tabpanel">
                        <div class="element__detail-text m-0">
                            <? if (
                                $arResult['PREVIEW_TEXT'] != ''
                                && (
                                    $arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'S'
                                    || ($arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'E' && $arResult['DETAIL_TEXT'] == '')
                                )
                            ) {
                                echo $arResult['PREVIEW_TEXT_TYPE'] === 'html' ? $arResult['PREVIEW_TEXT'] : '<p>' . $arResult['PREVIEW_TEXT'] . '</p>';
                            }

                            if ($arResult['DETAIL_TEXT'] != '') {
                                echo $arResult['DETAIL_TEXT_TYPE'] === 'html' ? $arResult['DETAIL_TEXT'] : '<p>' . $arResult['DETAIL_TEXT'] . '</p>';
                            } ?>
                        </div>
                    </div>
                    <div class="tab-pane fade show <? if ($showCharsTab): ?>active<? endif; ?> tab-pane-descr"
                         id="props" role="tabpanel">
                        <? if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']) {
                            ?>
                            <div class="product-item-detail-tab-content" data-entity="tab-container"
                                 data-value="properties">
                                <?
                                if (!empty($arResult['DISPLAY_PROPERTIES']) || !empty($arResult['PROPERTIES']['ADDITIONAL']['VALUE'])) {
                                    ?>
                                    <dl class="product-item-detail-properties">
                                        <?
                                        foreach ($arResult['DISPLAY_PROPERTIES'] as $property) {
                                            ?>
                                            <dt><?= $property['NAME'] ?></dt>
                                            <dd>
                                                <? if ($property['CODE'] == 'COLLECTION'): ?>
                                                    <a href="<?= $arResult['SECTION']['SECTION_PAGE_URL'] ?>filter/collection-is-<?= str_replace(" ", "%20", mb_strtolower(implode("-or-", $property['VALUE']), "Windows-1251")) ?>/apply/"><?= (
                                                        is_array($property['DISPLAY_VALUE'])
                                                            ? implode(' / ', $property['DISPLAY_VALUE'])
                                                            : $property['DISPLAY_VALUE']
                                                        ) ?></a>
                                                <? else: ?>
                                                    <?= (
                                                    is_array($property['DISPLAY_VALUE'])
                                                        ? implode(' / ', $property['DISPLAY_VALUE'])
                                                        : $property['DISPLAY_VALUE']
                                                    ) ?>
                                                <? endif; ?>
                                            </dd>
                                            <?
                                        }
                                        unset($property);
                                        ?>
                                    </dl>
                                    <?
                                }

                                if ($arResult['SHOW_OFFERS_PROPS']) {
                                    ?>
                                    <dl class="product-item-detail-properties"
                                        id="<?= $itemIds['DISPLAY_PROP_DIV'] ?>"></dl>
                                    <?
                                }
                                ?>
                                <? if (!empty($arResult['PROPERTIES']['ADDITIONAL']['VALUE'])): ?>
                                    <dl class="product-item-detail-properties">
                                        <? foreach ($arResult['PROPERTIES']['ADDITIONAL']['VALUE'] as $key => $value): ?>
                                            <? if (stripos($value, "|") !== false): ?>
                                                <? $value = explode("|", $value); ?>
                                                <? foreach ($value as $val): ?>
                                                    <? $valText = explode("%", $val); ?>
                                                    <dt><?= $valText[0] ?></dt>
                                                    <dd><?= $valText[1] ?></dd>
                                                <? endforeach; ?>
                                            <? else: ?>
                                                <dt><?= $value ?></dt>
                                                <dd><?= $arResult['PROPERTIES']['ADDITIONAL']['DESCRIPTION'][$key] ?></dd>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    </dl>
                                <? endif; ?>
                            </div>
                            <?
                        } ?>
                    </div>
                </div>
            </div>

            <? if (!empty($arResult["COMPS"])) { ?>
                <div class="matras_comps">
                    <div class="element__caption">Основные компоненты</div>
                    <? foreach ($arResult["COMPS"] as $comp) {

                        ?>

                        <div class="row row-10 mb-4">
                            <div class="col col-12 col-md-2">
                                <img class="components-item-img img-fluid"
                                     src="<?= CFile::GetPath($comp['PREVIEW_PICTURE']) ?>">
                            </div>
                            <div class="col col-12 col-md-10">
                                <div class="components-preview">
                                    <div class="components-title"><?= $comp['NAME'] ?>   </div>
                                    <?= strip_tags($comp['PREVIEW_TEXT']) ?>

                                    <div class="collapse" id="collapse<?= $comp['ID'] ?>">

                                        <?= $comp['DETAIL_TEXT'] ?>

                                    </div>

                                    <br>
                                    <a href="javascript:;" class="nav-link js-components-more extremum-click"
                                       data-toggle="collapse"
                                       data-target="#collapse<?= $comp['ID'] ?>" aria-expanded="false"
                                       aria-controls="collapseExample">Подробнее</a>
                                </div>
                            </div>
                        </div>


                    <? } ?>
                </div>
            <? } ?>

        </div>


        <div class="row row-10 justify-content-between">
            <div class="col col-12 col-lg-6 d-md-none">

                <?php
                if (mb_stripos($arResult['PROPERTIES']['CATALOG']['FULL_VALUE']['UF_NAME'], 'матрасы') !== false) {
                    $tmp = 'matras';
                } else {
                    $tmp = 'description';
                }

                $APPLICATION->IncludeComponent('realweb:blank', 'left_' . $tmp, array('RESULT' => $arResult, 'ID' => 'mobile'), $component->getParent()); ?>
            </div>
            <div class="col col-12 col-lg-6 col-xl-5">
                <? if ($arResult['REVIEWS']): ?>
                    <div class="review">
                        <div class="review__title">Отзывы</div>
                        <div class="review__items owl-carousel js-owl-reviews">
                            <? foreach ($arResult['REVIEWS'] as $review): ?>
                                <div class="review__item">
                                    <div class="review__name"><?= $review['PROPS']['UF_REVIEWNAME'] ?></div>
                                    <div class="review__text">
                                        <?= $review['PROPS']['UF_REVIEWREVIEW']['TEXT'] ?>
                                    </div>
                                    <div class="review__stars">
                                        <? for ($i = 1; $i < 6; $i++): ?>
                                            <i class="fas fa-star <?= $i <= $review['PROPS']['RATING'] ? 'active' : '' ?>"></i>
                                        <? endfor; ?>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>

        <meta itemprop="name" content="<?= $name ?>"/>
        <meta itemprop="category" content="<?= $arResult['CATEGORY_PATH'] ?>"/>
        <?
        if ($haveOffers) {
            foreach ($arResult['JS_OFFERS'] as $offer) {
                $currentOffersList = array();

                if (!empty($offer['TREE']) && is_array($offer['TREE'])) {
                    foreach ($offer['TREE'] as $propName => $skuId) {
                        $propId = (int)substr($propName, 5);

                        foreach ($skuProps as $prop) {
                            if ($prop['ID'] == $propId) {
                                foreach ($prop['VALUES'] as $propId => $propValue) {
                                    if ($propId == $skuId) {
                                        $currentOffersList[] = $propValue['NAME'];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                $offerPrice = $offer['ITEM_PRICES'][$offer['ITEM_PRICE_SELECTED']];
                ?>
                <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <link itemprop="url"
                          href="<?php echo \Realweb\Site\Site::getDomain() . $arResult['DETAIL_PAGE_URL']; ?>"/>
                    <meta itemprop="sku" content="<?= htmlspecialcharsbx(implode('/', $currentOffersList)) ?>"/>
                    <meta itemprop="price" content="<?= doubleval($offerPrice['RATIO_PRICE']) ?>"/>
                    <meta itemprop="priceCurrency" content="<?= $offerPrice['CURRENCY'] ?: 'RUB' ?>"/>
                    <meta itemprop="priceValidUntil" content="<?= date('Y-m-d') ?>"/>
                    <link itemprop="availability"
                          href="http://schema.org/<?= ($offer['CAN_BUY'] ? 'InStock' : 'OutOfStock') ?>"/>
                </span>
                <?
            }

            unset($offerPrice, $currentOffersList);
        } else {
            ?>
            <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                 <link itemprop="url"
                       href="<?php echo \Realweb\Site\Site::getDomain() . $arResult['DETAIL_PAGE_URL']; ?>"/>
                <meta itemprop="price" content="<?= doubleval($price['RATIO_PRICE']) ?>"/>
                <meta itemprop="priceCurrency" content="<?= $price['CURRENCY'] ?: 'RUB' ?>"/>
                <meta itemprop="priceValidUntil" content="<?= date('Y-m-d') ?>"/>
                <link itemprop="availability"
                      href="http://schema.org/<?= ($actualItem['CAN_BUY'] ? 'InStock' : 'OutOfStock') ?>"/>
            </span>
            <?
        }
        ?>
        <span itemprop="aggregateRating" itemtype="http://schema.org/AggregateRating" itemscope>
            <meta itemprop="reviewCount" content="1"/>
            <meta itemprop="ratingValue" content="5"/>
        </span>
        <span itemprop="brand" itemtype="http://schema.org/Brand" itemscope>
            <meta itemprop="name" content="<?php echo $arResult['PROPERTIES']['BRAND']['VALUE'] ?: 'Le tabouret'; ?>"/>
        </span>
        <meta itemprop="description"
              content="<?php echo htmlspecialchars($arResult['PREVIEW_TEXT'] ?: $arResult['NAME']); ?>"/>
        <?php if ($arResult['PREVIEW_PICTURE']): ?>
            <link itemprop="image"
                  href="<?php echo \Realweb\Site\Site::getDomain() . $arResult['PREVIEW_PICTURE']['SRC']; ?>"/>
        <?php endif; ?>
        <?php if ($arResult['DETAIL_PICTURE']): ?>
            <link itemprop="image"
                  href="<?php echo \Realweb\Site\Site::getDomain() . $arResult['DETAIL_PICTURE']['SRC']; ?>"/>
        <?php endif; ?>
        <?php foreach ($actualItem['MORE_PHOTO'] as $key => $photo): ?>
            <link itemprop="image" href="<?php echo \Realweb\Site\Site::getDomain() . $photo['SRC']; ?>"/>
        <?php endforeach; ?>
        <span itemprop="review" itemtype="http://schema.org/Review" itemscope>
            <span itemprop="author" itemtype="http://schema.org/Person" itemscope>
                <meta itemprop="name" content="Le tabouret"/>
            </span>
            <span itemprop="reviewRating" itemtype="http://schema.org/Rating" itemscope>
                <meta itemprop="ratingValue" content="5"/>
                <meta itemprop="bestRating" content="5"/>
            </span>
        </span>
        <meta itemprop="sku" content="<?php echo $arResult['PROPERTIES']['ARTICUL']['VALUE'] ?: $arResult['ID']; ?>"/>
        <meta itemprop="mpn" content="<?php echo $arResult['PROPERTIES']['ARTICUL']['VALUE'] ?: $arResult['ID']; ?>"/>
    </div>
<?
if ($haveOffers) {
    $offerIds = array();
    $offerCodes = array();

    $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

    foreach ($arResult['JS_OFFERS'] as $ind => &$jsOffer) {
        $offerIds[] = (int)$jsOffer['ID'];
        $offerCodes[] = $jsOffer['CODE'];

        $fullOffer = $arResult['OFFERS'][$ind];
        $measureName = $fullOffer['ITEM_MEASURE']['TITLE'];

        $strAllProps = '';
        $strMainProps = '';
        $strPriceRangesRatio = '';
        $strPriceRanges = '';

        if ($arResult['SHOW_OFFERS_PROPS']) {
            if (!empty($jsOffer['DISPLAY_PROPERTIES'])) {
                foreach ($jsOffer['DISPLAY_PROPERTIES'] as $property) {
                    $current = '<div class="element__prop"> <div class="element__name">' . $property['NAME'] . '</div><div class="element__value">' . (
                        is_array($property['VALUE'])
                            ? implode(' / ', $property['VALUE'])
                            : $property['VALUE']
                        ) . '</div></div>';
                    $strAllProps .= $current;

                    if (isset($arParams['MAIN_BLOCK_OFFERS_PROPERTY_CODE'][$property['CODE']])) {
                        $strMainProps .= $current;
                    }
                }

                unset($current);
            }
        }

        if ($arParams['USE_PRICE_COUNT'] && count($jsOffer['ITEM_QUANTITY_RANGES']) > 1) {
            $strPriceRangesRatio = '(' . Loc::getMessage(
                    'CT_BCE_CATALOG_RATIO_PRICE',
                    array('#RATIO#' => ($useRatio
                            ? $fullOffer['ITEM_MEASURE_RATIOS'][$fullOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']
                            : '1'
                        ) . ' ' . $measureName)
                ) . ')';

            foreach ($jsOffer['ITEM_QUANTITY_RANGES'] as $range) {
                if ($range['HASH'] !== 'ZERO-INF') {
                    $itemPrice = false;

                    foreach ($jsOffer['ITEM_PRICES'] as $itemPrice) {
                        if ($itemPrice['QUANTITY_HASH'] === $range['HASH']) {
                            break;
                        }
                    }

                    if ($itemPrice) {
                        $strPriceRanges .= '<dt>' . Loc::getMessage(
                                'CT_BCE_CATALOG_RANGE_FROM',
                                array('#FROM#' => $range['SORT_FROM'] . ' ' . $measureName)
                            ) . ' ';

                        if (is_infinite($range['SORT_TO'])) {
                            $strPriceRanges .= Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
                        } else {
                            $strPriceRanges .= Loc::getMessage(
                                'CT_BCE_CATALOG_RANGE_TO',
                                array('#TO#' => $range['SORT_TO'] . ' ' . $measureName)
                            );
                        }

                        $strPriceRanges .= '</dt><dd>' . ($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']) . '</dd>';
                    }
                }
            }

            unset($range, $itemPrice);
        }

        $jsOffer['DISPLAY_PROPERTIES'] = $strAllProps;
        $jsOffer['DISPLAY_PROPERTIES_MAIN_BLOCK'] = $strMainProps;
        $jsOffer['PRICE_RANGES_RATIO_HTML'] = $strPriceRangesRatio;
        $jsOffer['PRICE_RANGES_HTML'] = $strPriceRanges;
    }

    $templateData['OFFER_IDS'] = $offerIds;
    $templateData['OFFER_CODES'] = $offerCodes;
    unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);

    $jsParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => true,
            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
            'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
            'OFFER_GROUP' => $arResult['OFFER_GROUP'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
            'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
            'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
            'USE_STICKERS' => true,
            'USE_SUBSCRIBE' => $showSubscribe,
            'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
            'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
            'ALT' => $alt,
            'TITLE' => $title,
            'MAGNIFIER_ZOOM_PERCENT' => 200,
            'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
            'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
            'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
                ? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
                : null
        ),
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'VISUAL' => $itemIds,
        'DEFAULT_PICTURE' => array(
            'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
            'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
        ),
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'ACTIVE' => $arResult['ACTIVE'],
            'NAME' => $arResult['~NAME'],
            'CATEGORY' => $arResult['CATEGORY_PATH']
        ),
        'BASKET' => array(
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'BASKET_URL' => $arParams['BASKET_URL'],
            'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        ),
        'OFFERS' => $arResult['JS_OFFERS'],
        'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
        'TREE_PROPS' => $skuProps
    );
} else {
    $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
    if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !$emptyProductProperties) {
        ?>
        <div id="<?= $itemIds['BASKET_PROP_DIV'] ?>" style="display: none;">
            <?
            if (!empty($arResult['PRODUCT_PROPERTIES_FILL'])) {
                foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo) {
                    ?>
                    <input type="hidden" name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propId ?>]"
                           value="<?= htmlspecialcharsbx($propInfo['ID']) ?>">
                    <?
                    unset($arResult['PRODUCT_PROPERTIES'][$propId]);
                }
            }

            $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
            if (!$emptyProductProperties) {
                ?>
                <table>
                    <?
                    foreach ($arResult['PRODUCT_PROPERTIES'] as $propId => $propInfo) {
                        ?>
                        <tr>
                            <td><?= $arResult['PROPERTIES'][$propId]['NAME'] ?></td>
                            <td>
                                <?
                                if (
                                    $arResult['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L'
                                    && $arResult['PROPERTIES'][$propId]['LIST_TYPE'] === 'C'
                                ) {
                                    foreach ($propInfo['VALUES'] as $valueId => $value) {
                                        ?>
                                        <label>
                                            <input type="radio"
                                                   name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propId ?>]"
                                                   value="<?= $valueId ?>" <?= ($valueId == $propInfo['SELECTED'] ? '"checked"' : '') ?>>
                                            <?= $value ?>
                                        </label>
                                        <br>
                                        <?
                                    }
                                } else {
                                    ?>
                                    <select name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propId ?>]">
                                        <?
                                        foreach ($propInfo['VALUES'] as $valueId => $value) {
                                            ?>
                                            <option value="<?= $valueId ?>" <?= ($valueId == $propInfo['SELECTED'] ? '"selected"' : '') ?>>
                                                <?= $value ?>
                                            </option>
                                            <?
                                        }
                                        ?>
                                    </select>
                                    <?
                                }
                                ?>
                            </td>
                        </tr>
                        <?
                    }
                    ?>
                </table>
                <?
            }
            ?>
        </div>
        <?
    }

    $jsParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
            'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
            'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
            'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
            'USE_STICKERS' => true,
            'USE_SUBSCRIBE' => $showSubscribe,
            'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
            'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
            'ALT' => $alt,
            'TITLE' => $title,
            'MAGNIFIER_ZOOM_PERCENT' => 200,
            'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
            'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
            'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
                ? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
                : null
        ),
        'VISUAL' => $itemIds,
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'ACTIVE' => $arResult['ACTIVE'],
            'PICT' => reset($arResult['MORE_PHOTO']),
            'NAME' => $arResult['~NAME'],
            'SUBSCRIPTION' => true,
            'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
            'ITEM_PRICES' => $arResult['ITEM_PRICES'],
            'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
            'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
            'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
            'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
            'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
            'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
            'SLIDER' => $arResult['MORE_PHOTO'],
            'CAN_BUY' => $arResult['CAN_BUY'],
            'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
            'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
            'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
            'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
            'CATEGORY' => $arResult['CATEGORY_PATH']
        ),
        'BASKET' => array(
            'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
            'EMPTY_PROPS' => $emptyProductProperties,
            'BASKET_URL' => $arParams['BASKET_URL'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        )
    );
    unset($emptyProductProperties);
}

if ($arParams['DISPLAY_COMPARE']) {
    $jsParams['COMPARE'] = array(
        'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
        'COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
        'COMPARE_PATH' => $arParams['COMPARE_PATH']
    );
}
?>
    <script>
        BX.message({
            ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
            TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
            TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
            BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
            BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
            BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
            BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
            BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
            TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
            COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
            COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
            COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
            BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
            PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
            PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
            RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
            RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
            SITE_ID: '<?=SITE_ID?>'
        });

        var <?=$obName?> =
            new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
        var productDelivery = <?=CUtil::PhpToJSObject($arResult['JS_DELIVERY'], false, true)?>;
    </script>
<?
unset($actualItem, $itemIds, $jsParams);
?>
<?php if (is_array($arResult['PROPERTIES']['VIDEO']['VALUE']) && count($arResult['PROPERTIES']['VIDEO']['VALUE']) > 0): ?>
    <script>
        window.addEventListener('load', function (event) {
            if (!window.youtubeLoader) {
                window.youtubeLoader = function (args = {}) {
                    if (!args.name) {
                        console.warn('vendorLoader: You must pass the name!');
                        return;
                    }
                    if (!args.path) {
                        console.warn('vendorLoader: You must pass the path!');
                        return;
                    }

                    !window.vendor && (window.vendor = {});

                    !window.SITE_TEMPLATE_PATH && (window.SITE_TEMPLATE_PATH = '/local/templates/html/');

                    window.vendor[args.name] = {};
                    window.vendor[args.name].load = {};
                    window.vendor[args.name].load.timeout;
                    window.vendor[args.name].load.status = false;
                    window.vendor[args.name].load.loading = function () {

                        if (!window.vendor[args.name].load.status) {
                            window.vendor[args.name].load.status = true;
                            clearTimeout(window.vendor[args.name].load.timeout);
                            $(document).off('scroll.vendor-' + args.name);
                            $(document).off('click.vendor-' + args.name);
                            $(document).off('mouseover.vendor-' + args.name);
                            if (args.path === 'none') {
                                window.vendor[args.name].load.status = true;
                                args.callback && args.callback();
                                $(document).trigger('load.' + args.name)
                            } else {
                                $.getScript(
                                    (args.http ? '' : window.SITE_TEMPLATE_PATH) + args.path,
                                    args.callback || function () {
                                    }
                                ).fail(
                                    function (jqxhr, settings, exception) {
                                        console.log(jqxhr, settings, exception);
                                    }
                                );
                            }
                        }
                    };


                    if (args.event.scroll) {
                        $(document).on('scroll.vendor-' + args.name, function () {
                            window.vendor[args.name].load.loading();
                        });

                        var doc = document.documentElement;
                        var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

                        top > 100 && window.vendor[args.name].load.loading();

                    }

                    if (args.event.click) {
                        $(document).on('click.vendor-' + args.name, function () {
                            window.vendor[args.name].load.loading();
                        });
                    }

                    if (args.event.mouseover) {
                        $(document).on('mouseover.vendor-' + args.name, args.event.mouseover.trigger, function () {
                            window.vendor[args.name].load.loading();
                        });
                    }

                    if (args.event.timeout) {
                        window.vendor[args.name].load.timeout = setTimeout(function () {
                            window.vendor[args.name].load.loading();
                        }, args.event.timeout || 3000)
                    }
                }
            }
            window.youtubeLoader && window.youtubeLoader({
                name: 'metrics',
                path: 'none',
                event: {
                    scroll: true,
                    click: true,
                    timeout: 6000,
                    mouseover: 'body',
                },
                callback: function () {
                    $.ajax({
                        url: '<?=$templateFolder?>/get_youtube_video.php',
                        type: "POST",
                        data: {
                            'youtube_link': <?=json_encode($arResult['PROPERTIES']['VIDEO']['VALUE'])?>,
                        },
                        success: function (data) {
                            $('.js-video-result').html(data);
                        },

                    });
                }
            });
        });

    </script>
<?php endif; ?>