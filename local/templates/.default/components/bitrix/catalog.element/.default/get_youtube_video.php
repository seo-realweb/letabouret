<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>

<?
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$arLink = $request->get('youtube_link');

if(is_array($arLink) && count($arLink) > 0):?>
    <?foreach ($arLink as $link):?>
        <iframe width="100%" height="315" src="<?=$link?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <?endforeach;?>
<?endif;?>


