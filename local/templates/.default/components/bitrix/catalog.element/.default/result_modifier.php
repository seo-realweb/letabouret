<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$GLOBALS['elementCollectionsProp'] = $arResult['DISPLAY_PROPERTIES']['COLLECTION']['VALUE'];


$arResult['COMPS']=[];
if (is_array($arResult['PROPERTIES']['MATRAS_COMPS']['VALUE'])&&!empty($arResult['PROPERTIES']['MATRAS_COMPS']['VALUE'])){

    $vals=$arResult['PROPERTIES']['MATRAS_COMPS']['VALUE'];

    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","PREVIEW_TEXT","DETAIL_TEXT");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("IBLOCK_ID"=>18, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","ID"=>$vals);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
    while($ob = $res->GetNext()){
        $arResult['COMPS'][]=$ob;
    }
}
$arResult['ICONS']=[];
if ($arResult['PROPERTIES']['BRAND']['VALUE']== 'MATRAMAX'){


    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    $arFilter = Array("IBLOCK_ID"=>19, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
    while($ob = $res->GetNext()){
        $arResult['ICONS'][]=$ob;
    }
}

$arResult['REVIEWS'] = \Realweb\Site\Site::getIBlockElements(array(
    'IBLOCK_ID' => IBLOCK_OTZIVY_OTZIVY,
    'PROPERTY_ELEMENT_ID' => $arResult['ID']
));

$arResult['DELIVERY'] = \Realweb\Site\Site::getIBlockElements(array(
    'IBLOCK_ID' => IBLOCK_NEWS_DOSTAVKA
));

foreach ($arResult['DELIVERY'] as $item) {
    $arResult['JS_DELIVERY'][$item['ID']] = array('text' => $item['PREVIEW_TEXT']);
}

$db_old_groups = CIBlockElement::GetElementGroups($arResult['ID'], true);
while ($ar_group = $db_old_groups->GetNext()) {
    $nav = CIBlockSection::GetNavChain(false, $ar_group['ID']);
    while ($arSectionPath = $nav->GetNext()){
        if ($arSectionPath['CODE'] == 'collections') {
            $arResult['SECTION_COLLECTION'] = $ar_group['SECTION_PAGE_URL'];
            break;
        }
    }
    if ($arResult['SECTION_COLLECTION']) {
        break;
    }
}
$offerID = $arParams['OFFER_ID'];
if(  $offerID > 0){

    foreach ($arResult['OFFERS'] as $key => $offer){

        if($offer['ID'] == $offerID){
            $arResult['OFFERS_SELECTED'] = $key;
        }
    }
}


if($USER->IsAdmin()){

    foreach ($arResult['DISPLAY_PROPERTIES'] as &$arProperty){
        if($arProperty['CODE'] == 'COLLECTION' && $arProperty['USER_TYPE_SETTINGS']['TABLE_NAME']){

            bitrix\Main\Loader::includeModule("highloadblock");


            $hlbl = 1; // Указываем ID нашего highloadblock блока к которому будет делать запросы.
            $hlblock = HL\HighloadBlockTable::getList(['filter' => ['TABLE_NAME' =>  $arProperty['USER_TYPE_SETTINGS']['TABLE_NAME']]])->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $dbres = $entity_data_class::getList(array(
                'filter' => array('=UF_XML_ID' => $arProperty['VALUE'])
            ));
            $arValues = [];
            while($res = $dbres->fetch()){
                $arValues[$res['UF_XML_ID']] = $res;
            }
            if(is_array($arProperty['VALUE'])){
                foreach ($arProperty['VALUE'] as $key => $value) {
                    $arProperty['FULL_VALUE'][$key] = $arValues[$value];
                }
            }
            
        }

    }
}


if(CModule::IncludeModule("slam.image")){
    $moduleReize = 'CSlamImage';
}
else{
    $moduleReize = 'CFile';
}


foreach($arResult['MORE_PHOTO'] as $key => &$photo){
    $photoArr = $moduleReize::ResizeImageGet(
        $photo['ID'],
        array("width"=> 770,"height" => 675),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    );
    $arResult['MORE_PHOTO_RESIZED'][$key]['SRC'] = $photoArr['src'];
    $arResult['MORE_PHOTO_RESIZED'][$key]['WIDTH'] = $photoArr['width'];
    $arResult['MORE_PHOTO_RESIZED'][$key]['HEIGHT'] = $photoArr['height'];
    /*if($moduleReize == "CSlamImage")
        $photo['SRC'] = createWebp($photo['SRC'], 95);*/
}

foreach($arResult['OFFERS'] as $keyOffer => &$offer){
    foreach($offer['MORE_PHOTO'] as $key => &$photo){
        $photoArr = $moduleReize::ResizeImageGet(
            $photo['ID'],
            array("width"=> 570,"height" => 500),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );
        $arResult['OFFERS'][$keyOffer]['MORE_PHOTO_RESIZED'][$key]['SRC'] = $photoArr['src'];
        $arResult['OFFERS'][$keyOffer]['MORE_PHOTO_RESIZED'][$key]['WIDTH'] = $photoArr['width'];
        $arResult['OFFERS'][$keyOffer]['MORE_PHOTO_RESIZED'][$key]['HEIGHT'] = $photoArr['height'];
        /*if($moduleReize == "CSlamImage")
            $photo['SRC'] = createWebp($photo['SRC'], 95);*/
    }
}

if ($arResult['PROPERTIES']['CATALOG']['VALUE']) {
    $hbCatalog = new \Realweb\Site\Hload('Collectionfilter');
    $arRow = $hbCatalog->GetRow(array("order" => array('ID'), "filter" => array("UF_XML_ID" => $arResult['PROPERTIES']['CATALOG']['VALUE'])));
    if ($arRow) {
        $arResult['PROPERTIES']['CATALOG']['FULL_VALUE'] = $arRow;
    }
}
if ($arResult['SECTION']['ID']) {
    $arSection = (new \Bitrix\Iblock\ORM\Query(\Bitrix\Iblock\Model\Section::compileEntityByIblock($arResult['IBLOCK_ID'])))
        ->setSelect(array('UF_ELEMENT_TEMPLATE_DESCRIPTION'))
        ->where('ID', '=', $arResult['SECTION']['ID'])
        ->exec()
        ->fetch();
    $arResult['SECTION'] = \Realweb\Site\ArrayHelper::merge($arResult['SECTION'], $arSection);
}

$this->getComponent()->setResultCacheKeys(array('PROPERTIES', 'MORE_PHOTO', 'DETAIL_PICTURE'));