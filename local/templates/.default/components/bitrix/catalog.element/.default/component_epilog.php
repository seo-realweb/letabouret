<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;
//Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/scripts/common.js");
/**
 * @var array $templateData
 * @var array $arParams
 * @var string $templateFolder
 * @global CMain $APPLICATION
 */

global $APPLICATION;
if(!empty($arResult["DETAIL_PICTURE"]['SRC'])){
	
        $APPLICATION->SetPageProperty('og:image', $arResult["DETAIL_PICTURE"]['SRC']);
    
}else{
foreach ($arResult['MORE_PHOTO'] as $photo) {
    if($photo['SRC']){
        $APPLICATION->SetPageProperty('og:image', $photo['SRC']);
    }
}
}
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/components-template/swiper/style.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/components-template/swiper/script.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/vendor/lazy/lazyload.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/vendor/swiper/swiper.min.js");

$viewed = unserialize(\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getCookie("VIEWED"));
//if($_GET["dev"]=="Y"){pr($arResult["PROPERTIES"]["SAME_PROD_SEC"]["VALUE"]);}
if(!empty($arResult["PROPERTIES"]["SAME_PROD_SEC"]["VALUE"])){
	$GLOBALS["SEC_ID_DET"]=$arResult["PROPERTIES"]["SAME_PROD_SEC"]["VALUE"];
}else{
	$GLOBALS["SEC_ID_DET"]=$arResult["IBLOCK_SECTION_ID"];
}



if (!$viewed[$arResult['ID']]) {

    $cookie = new Cookie("VIEWED", $viewed);
    Application::getInstance()->getContext()->getResponse()->addCookie($cookie);
//    \Realweb\Site\Site::setCookie('VIEWED', serialize($viewed));
}

$GLOBALS['elementCollectionsProp'] = $arResult['PROPERTIES']['COLLECTION']['VALUE'];
if ($arResult['PROPERTIES']['RECOMMENDED']['VALUE']) {
    $APPLICATION->SetPageProperty('RECOMMENDED_FILTER', array('ID' => $arResult['PROPERTIES']['RECOMMENDED']['VALUE']));
} elseif ($arResult['PROPERTIES']['COLLECTION']['VALUE']) {
    $APPLICATION->SetPageProperty('RECOMMENDED_FILTER', array('PROPERTY_COLLECTION' => $arResult['PROPERTIES']['COLLECTION']['VALUE'], '!ID' => $arResult['ID']));
}
$APPLICATION->SetPageProperty('TOV_PROPS', ['ID'=>$arResult["ID"],'PROPS'=>$arResult['PROPERTIES']]);
if (!empty($templateData['TEMPLATE_LIBRARY']))
{
    $loadCurrency = false;

    if (!empty($templateData['CURRENCIES']))
    {
        $loadCurrency = Loader::includeModule('currency');
    }

    CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
    if ($loadCurrency)
    {
        ?>
        <script>
            BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
        </script>
        <?
    }
}

if (isset($templateData['JS_OBJ']))
{
    ?>
    <script>
        BX.ready(BX.defer(function(){
            if (!!window.<?=$templateData['JS_OBJ']?>)
            {
                window.<?=$templateData['JS_OBJ']?>.allowViewedCount(true);
            }
        }));
    </script>

    <?

    // check compared state
    if ($arParams['DISPLAY_COMPARE'])
    {
        $compared = false;
        $comparedIds = array();
        $item = $templateData['ITEM'];

        if (!empty($_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]))
        {
            if (!empty($item['JS_OFFERS']))
            {
                foreach ($item['JS_OFFERS'] as $key => $offer)
                {
                    if (array_key_exists($offer['ID'], $_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']))
                    {
                        if ($key == $item['OFFERS_SELECTED'])
                        {
                            $compared = true;
                        }

                        $comparedIds[] = $offer['ID'];
                    }
                }
            }
            elseif (array_key_exists($item['ID'], $_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']))
            {
                $compared = true;
            }
        }

        if ($templateData['JS_OBJ'])
        {
            ?>
            <script>
                BX.ready(BX.defer(function(){
                    if (!!window.<?=$templateData['JS_OBJ']?>)
                    {
                        window.<?=$templateData['JS_OBJ']?>.setCompared('<?=$compared?>');

                        <? if (!empty($comparedIds)): ?>
                        window.<?=$templateData['JS_OBJ']?>.setCompareInfo(<?=CUtil::PhpToJSObject($comparedIds, false, true)?>);
                        <? endif ?>
                    }
                }));
            </script>
            <?
        }
    }

    // select target offer
    $request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
    $offerNum = false;
    $offerId = (int)$this->request->get('OFFER_ID');
    $offerCode = $this->request->get('OFFER_CODE');

    if ($offerId > 0 && !empty($templateData['OFFER_IDS']) && is_array($templateData['OFFER_IDS']))
    {
        $offerNum = array_search($offerId, $templateData['OFFER_IDS']);
    }
    elseif (!empty($offerCode) && !empty($templateData['OFFER_CODES']) && is_array($templateData['OFFER_CODES']))
    {
        $offerNum = array_search($offerCode, $templateData['OFFER_CODES']);
    }

    if (!empty($offerNum))
    {
        ?>
        <script>
            BX.ready(function(){
                if (!!window.<?=$templateData['JS_OBJ']?>)
                {
                    window.<?=$templateData['JS_OBJ']?>.setOffer(<?=$offerNum?>);
                }
            });
        </script>
        <?
    }
}

