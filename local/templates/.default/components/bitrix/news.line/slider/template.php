<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$i = 0;
?>
<div class="section-bg section_pad">
    <div class="container container_md">
        <div class="main-news">
            <div id="carouselNews" class="carousel js-bootstrap-carousel slide carousel-fade" data-interval="false"
                 data-ride="carousel">
                <div class="carousel-inner">
                    <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        if (!$arItem['PICTURE_RESIZED']) continue;
                        ?>
                        <div class="carousel-item <?= $i == 0 ? 'active' : '' ?>">
                            <div class="row">
                                <div class="col col-12 col-md-6 col-xl-8">
                                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img class="d-block w-100 lazy-img"
                                                                                     src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" data-src="<?= $arItem['PICTURE_RESIZED']['SRC'] ?>"
                                                                                     alt="First slide"></a>
                                </div>
                                <div class="col col-12 col-md-6 col-xl-4">
                                    <div class="h2 mt-4 mt-md-0">новости <span>и акции</span></div>
                                    <div class="main-news__date"><?= $arItem['DISPLAY_ACTIVE_FROM'] ?></div>
                                    <div class="main-delimiter"></div>
                                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="main-news__name">
                                        <?= $arItem['NAME'] ?>
                                    </a>
                                    <div class="main-news__text">
                                        <? if ($arItem['PREVIEW_TEXT']): ?>
                                            <? if ($arItem['PREVIEW_TEXT_TYPE'] == 'html'): ?>
                                                <?= $arItem['PREVIEW_TEXT'] ?>
                                            <? else: ?>
                                                <p><?= $arItem['PREVIEW_TEXT'] ?></p>
                                            <? endif; ?>
                                        <? elseif ($arItem['DETAIL_TEXT']): ?>
                                            <p><?= substr(strip_tags($arItem['DETAIL_TEXT']), 0, 300) ?>...</p>
                                        <? endif; ?>
                                    </div>
                                    <div class="row align-items-center main-news__nav">
                                        <div class="col col-auto">
                                            <a href="#carouselNews" role="button" data-slide="prev">
                                                <i class="far fa-long-arrow-left"></i>
                                            </a>
                                        </div>
                                        <div class="col col-auto"><i class="fas fa-th"></i></div>
                                        <div class="col col-auto">
                                            <a href="#carouselNews" role="button" data-slide="next">
                                                <i class="far fa-long-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? $i++; ?>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
