<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
$countForLoad = $arResult["NavPageSize"];

if($countForLoad > ($arResult["NavRecordCount"] - $arResult["NavLastRecordShow"])){
    $countForLoad = $arResult["NavRecordCount"] - $arResult["NavLastRecordShow"];
}

if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

if($arResult['NavPageNomer'] == 1):?>
    <script>
        var in_process = false;
        var page = <?=$arResult['NavPageNomer']?>;
        var nEndPage = <?=$arResult['NavPageCount']?>;
        var LastPage = <?=$arResult['NavPageNomer']?>;

        function get_next_items() {
            $(".dbAjaxnavItem").addClass("preloader-block");
            if (in_process) {
                return false;
            }
            if(LastPage<page)
                return false;

            page = page + 1;
            LastPage = page;

            if(page>nEndPage)
                return false;

            url = window.location.toString();
            url = url.replace('#','');

            in_process = true;

            var pagenData = "dbAjaxNav=Y&PAGEN_1="+page;

            if(window.filterUrlDataGlobal && window.filterUrlDataGlobal.length){
                pagenData = window.filterUrlDataGlobal + '&PAGEN_1=' + page
            }

            $.ajax({
                type: "GET",
                dataType: "html",
                data: pagenData,
                url: url + (window.location.search != '' ? "&" : "?") + "type=html",
                beforeSend: function(){
                    $('.pagin_button').hide();
                   // $('.pagin_preloader').removeClass("hidden");
                },
                success: function( HTML ){
                    if(HTML)
                    {
                        $(".dbAjaxnavItem").removeClass("preloader-block");
                        $(HTML).insertAfter('div.dbAjaxnavItem:last');
                        $('div.dbAjaxnavItem:first').remove();
                        lazyReinit();
                        favoriteScriptInit();
                    }
                },
                complete: function(){
                    in_process = false;
                }
            });
        }
        /*
        $(window).scroll(function() {
            if  ($(window).scrollTop()+200 >= $(document).height() - $(window).height())
                get_next_items();
        });
        //*/
    </script>
<?endif;?>

<?if($arResult['NavPageNomer'] != $arResult['NavPageCount']):?>
    <div class="dbAjaxnavItem">
        <div class="product-list__button">
            <!--noindex--><span rel="nofollow" class="btn btn-show-more" onclick="get_next_items();return false;">
                Показать еще <?=$countForLoad?>
            </span><!--/noindex-->
        </div>
    </div>
<?endif;?>
