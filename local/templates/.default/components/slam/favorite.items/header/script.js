//'use strict';
function favoriteScriptInit(params) {

    var app = {
        // Анимация полета
        fly_animate: function (item, x_stop, y_stop) {
            item.animate({
                    top: y_stop + 20,
                    left: x_stop + 30,
                    height: 0,
                    width: 0,
                }, 700,
                function () {
                    // Удаляем клонированную картинку после анимации
                    item.remove();
                });
        },


        // Функция Летающей корзины
        fly: function (button) {

            // Определяем настройки
            var btn = button;// Кнопка, которая вызвала анимацию
            var trg = $('.' + params.TARGET_CLASS);// класс, в какую точку летит анимация
            var productItem = btn.closest('.' + params.AREA_CLASS);
            if(!productItem.length){
                productItem = $('.element__data.product__card')
            }
            var img = productItem.find('.' + params.IMAGE_CLASS);// картинка с блока, которая будет анимироваться

            if (btn.length && trg.length && img.length) {

                //Определяем координаты
                var x_start = img.offset().left;
                var y_start = img.offset().top;
                var x_stop = trg.offset().left;
                var y_stop = trg.offset().top;

                // Клонируем картинку
                var item = img.clone().addClass('fly').appendTo('body').css({
                    top: y_start,
                    left: x_start,
                    position: 'absolute',
                    height: img.height(),
                    width: img.width(),
                    zIndex: 9999
                });
                console.log( y_stop + 20);
                console.log(x_stop + 30);

                // Вызываем анимацию
               this.fly_animate(item, x_stop, y_stop);

            }
        },

        // Установить активность блока
        setDisabled: function (_self) {

            // Добавить класс disabled
            _self.addClass(params.ACTIVE_CLASS);

        },


        // Снять активность блока
        setNone: function (_self) {
            // Снять класс disabled
            _self.removeClass(params.ACTIVE_CLASS);
            if(params.IS_FAV_PAGE){
                var productItem = _self.closest('.' + params.AREA_CLASS);
                productItem.remove();
            }

        },


        // Клик по кнопке добавить в корзину
        addBtn: function () {

            var favButtons = $('.' + params.BUTTON_CLASS);
            var time = false;
            var favoriteBlock = $('#' + params.FAVORITE_ID);


            favButtons.on('click', function (e) {
                e.preventDefault();

                var _self = $(this);
                // Если кнопка не disabled
                if (!_self.hasClass(params.ACTIVE_CLASS)) {

                    // Вызываем функцию анимации
                    app.fly(_self);

                    // ID блока
                    var goodsID = _self.data('id');

                    // Вызываем аякс
                    $.ajax({
                        method: 'POST',
                        url: params.TEMPLATE_FOLDER,
                        //dataType: 'json',
                        data: {
                            MODULE: 'favorite',
                            FAVORITE_ADD: goodsID,
                            arParams: params
                        }
                    }).done(function (result) {

                        favoriteBlock.html(result);
                        app.setDisabled(_self);

                        window.global_fav_params.FAVORITE_ITEMS_ID.push(goodsID);
                        console.log('add ' + window.global_fav_params.FAVORITE_ITEMS_ID);
                    });
                } else {

                    // ID блока
                    var goodsID = _self.data('id');

                    // Вызываем аякс
                    $.ajax({
                        method: 'POST',
                        url: params.TEMPLATE_FOLDER,
                        //dataType: 'json',
                        data: {
                            MODULE: 'favorite',
                            FAVORITE_DEL: goodsID,
                            arParams: params
                        }
                    }).done(function (result) {
                        favoriteBlock.html(result);
                        app.setNone(_self);

                        window.global_fav_params.FAVORITE_ITEMS_ID = $.grep(window.global_fav_params.FAVORITE_ITEMS_ID, function(value) {
                            return value != goodsID;
                        });
                        console.log(window.global_fav_params.FAVORITE_ITEMS_ID);
                        if(!window.global_fav_params.FAVORITE_ITEMS_ID.length){
                            var emptyBlock = $('.emptyFavorits');
                            if(emptyBlock.length){
                                emptyBlock.css({display:'block'});
                            }
                        }

                    });
                }
            });
        },

        // Пройти по активным пунктам и проставить активность
        setActive: function (items) {
            if (typeof items !== 'undefined' && $('.' + params.BUTTON_CLASS).length) {

                $.each(items, function (index, item) {

                    var self = $('.' + params.BUTTON_CLASS).parent().find('[data-id="' + item + '"]');

                    if (self.length)
                        app.setDisabled(self);
                });


            }
        },

        // Инициализация приложения
        init: function () {

            if(params){
                window.global_fav_params = params;
            }else if(window.global_fav_params){
                params = window.global_fav_params;
            }

            this.addBtn();
            this.setActive(params.FAVORITE_ITEMS_ID);

            // Запрещаем переход по ссылке
            return false;
        }
    };

    // вызов приложения
    app.init();

}