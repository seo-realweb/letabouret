<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$i = 0;
function mb_substr_replace($str, $repl, $start, $length = null)
{
    preg_match_all('/./us', $str, $ar);
    preg_match_all('/./us', $repl, $rar);
    $length = is_int($length) ? $length : utf8_strlen($str);
    array_splice($ar[0], $start, $length, $rar[0]);
    return implode($ar[0]);
}
?>
<? if ($arSections = Realweb\Site\Site::getSections(array('IBLOCK_ID' => IBLOCK_CATALOG_CATALOG, '!UF_SHOW_MAIN_PAGE' => false))): ?>
    <? foreach ($arSections as $arSection): ?>
        <div class="<?= $i % 2 == 0 ? 'section-bg' : 'section-bg_gray' ?>">
            <div class="container container_md">
                <div class="row justify-content-between align-items-center">
                    <div class="col col-12 col-md-auto"><span
                                class="h2 h2_p like_h2 ">
						<?=$arSection['NAME']/* count(explode(" ", $arSection['NAME'])) > 1 ?
                                mb_substr_replace(
                                        $arSection['NAME'],
                                        ' <span>',
                                        stripos($arSection['NAME'], " "),
									1) : $arSection['NAME']*/ ?><!--/span-->
					</span>
                    </div>
                    <div class="col col-12 col-md-auto">
                        <a class="link-more" href="<?= $arSection['SECTION_PAGE_URL'] ?>">Посмотреть все —</a>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="product product_offset">
                    <? if ($arSubSections = Realweb\Site\Site::getSections(array(
                        'IBLOCK_ID' => $arSection['IBLOCK_ID'],
                        '>LEFT_MARGIN' => $arSection['LEFT_MARGIN'],
                        '<RIGHT_MARGIN' => $arSection['RIGHT_MARGIN'],
                        '=DEPTH_LEVEL' => $arSection['DEPTH_LEVEL'] + 1
                    ), true, true)): ?>
                        <? $curSection = current($arSubSections)['ID'] ?>
                        <ul class="nav-left">
                            <? foreach ($arSubSections as $section): ?>
                                <li class="nav-left__item">
                                    <a class="nav-left__link"
                                       href="<?= $arParams['FOLDER'] ?>?section_<?= $arSection['ID'] ?>=<?= $section['ID'] ?>"><?= $section['NAME'] ?></a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    <? else: ?>
                        <? $curSection = $arSection['ID'] ?>
                    <? endif; ?>
                    <? $APPLICATION->IncludeComponent("bitrix:catalog.section", "",
                        Realweb\Site\Catalog::getCatalogSectionParams(array(
                            'FILTER_NAME' => 'arrFilterMainProducts',
                            'SECTION_ID' => $_REQUEST['section_' . $arSection['ID']] ? $_REQUEST['section_' . $arSection['ID']] : $curSection,
                            'PAGE_ELEMENT_COUNT' => "3",
                            'CONTAINER_CLASS' => 'product__items product__items_simple',
                            'COL_CLASS' => 'col col-md-4 product__item',
                            'ITEM_CLASS' => '',
                            'SLIDER' => 'Y'
                        )),
                        false, array('HIDE_ICONS' => 'Y')); ?>
                </div>
            </div>
        </div>
        <? $i++; ?>
    <? endforeach; ?>
<? endif; ?>
