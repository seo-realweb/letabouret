<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$GLOBALS['arrFilterSectionCountries'] = array();
if ($row = current(\Realweb\Site\Site::getUserFieldEnumValues(array('XML_ID' => 'UF_TYPE_MENU', 'ENTITY_ID' => 'IBLOCK_' . IBLOCK_CATALOG_CATALOG . '_SECTION'), array('XML_ID' => 'country')))) {
    $GLOBALS['arrFilterSectionCountries']['UF_TYPE_MENU'] = $row['ID'];
}
?>
<? $APPLICATION->IncludeComponent("realweb:catalog.section.list", "countries",
    Array(
        "VIEW_MODE" => "TEXT",
        "SHOW_PARENT_NAME" => "Y",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => IBLOCK_CATALOG_CATALOG,
        "SECTION_ID" => "",
        "SECTION_CODE" => "",
        "SECTION_URL" => "",
        "COUNT_ELEMENTS" => "Y",
        "TOP_DEPTH" => "3",
        "SECTION_FIELDS" => "",
        "SECTION_USER_FIELDS" => array(
            'UF_PICTURE_MENU'
        ),
        "ADD_SECTIONS_CHAIN" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
        "CACHE_GROUPS" => "N",
        "SORT_BY1" => "SORT",
        "FILTER_NAME" => 'arrFilterSectionCountries'
    )
); ?>