<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($GLOBALS['arrFilterViewed']['ID'] = Realweb\Site\Catalog::getViewedProducts()): ?>
    <div class="<?= $arParams['SECTION_CLASS'] ? $arParams['SECTION_CLASS'] : 'section-bg section-bg_gray' ?>">
        <div class="container container_md">
            <div class="row justify-content-between align-items-center">
                <div class="col col-12 col-md-auto"><span class="h2 h2_p like_h2">Вы недавно <span>смотрели</span></span></div>
            </div>
        </div>
        <div class="container">
            <? $APPLICATION->IncludeComponent("bitrix:catalog.section", "",
                Realweb\Site\Catalog::getCatalogSectionParams(array(
                    'FILTER_NAME' => 'arrFilterViewed',
                    'PAGE_ELEMENT_COUNT' => "20",
                    'CONTAINER_CLASS' => 'product product__items_simple',
                    'ROW_CLASS' => 'product__items owl-carousel owl-product-items js-owl-slider',
                    'COL_CLASS' => 'product__item',
                    'ITEM_CLASS' => '',
                )),
                false, array('HIDE_ICONS' => 'Y')); ?>
        </div>
    </div>
<? endif; ?>