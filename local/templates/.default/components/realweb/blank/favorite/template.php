<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arIds = array();
if (\Bitrix\Main\Loader::IncludeModule("slam.favorite")) {
    $obFavoriteUser = (new slam\Favorite\UserFavorite());
    $arIds = \Realweb\Site\ArrayHelper::getColumn($obFavoriteUser
        ->getFavoriteObj()
        ->query()
        ->setSelect(array("ITEM_ID"))
        ->setFilter(array(
                "=FAVORITE_USER_ID_LINK.HASH" => $obFavoriteUser->getCurrentUserHash(),
                "!FAVORITE_USER_ID_LINK.HASH" => false
            )
        )
        ->exec()
        ->fetchAll(), 'ITEM_ID');
}
?>
<div class="section-bg">
    <div class="container">
        <?php if ($GLOBALS['arrFilterFavorite']['ID'] = $arIds): ?>
            <? $APPLICATION->IncludeComponent("bitrix:catalog.section", "",
                Realweb\Site\Catalog::getCatalogSectionParams(array(
                    'FILTER_NAME' => 'arrFilterFavorite',
                    'PAGE_ELEMENT_COUNT' => "200",
                    'CONTAINER_CLASS' => 'product product__items_simple',
                    'ROW_CLASS' => 'row row-10',
                    'COL_CLASS' => 'col col-6 col-xl-3 product__item',
                    'ITEM_CLASS' => 'product__card',
                )),
                false, array('HIDE_ICONS' => 'Y')
            ); ?>
        <?php else: ?>
            <div class="alert alert-danger">Список избранного пуст</div>
        <?php endif; ?>
    </div>
</div>
