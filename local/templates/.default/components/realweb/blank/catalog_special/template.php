<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$GLOBALS['arrFilterCatalogSpecial'] = array("PROPERTY_ONMAIN_VALUE" => "да");
?>
<div class="section-bg section_pad_b">
    <div class="container container_md">
        <div class="row justify-content-between align-items-center">
            <div class="col col-12 col-md-auto"><span class="h2 h2_p like_h2">Специальные предложения</span></div>
            <div class="col col-12 col-md-auto"><a class="link-more" href="/sale/">Посмотреть все —</a></div>
        </div>
    </div>
    <div class="container container_md">
        <? $APPLICATION->IncludeComponent("bitrix:catalog.section", "",
            Realweb\Site\Catalog::getCatalogSectionParams(array(
                'FILTER_NAME' => 'arrFilterCatalogSpecial',
                'PAGE_ELEMENT_COUNT' => "5",
                'CONTAINER_CLASS' => 'special-product',
                'ROW_CLASS' => '',
                'ITEM_CLASS' => 'special-product__item',
                'COL_CLASS' => 'col',
                'SLIDER' => 'Y',
                'TYPE_ITEM_VIEW' => 'SPECIAL'
            )),
            false, array('HIDE_ICONS' => 'Y')); ?>
    </div>
</div>