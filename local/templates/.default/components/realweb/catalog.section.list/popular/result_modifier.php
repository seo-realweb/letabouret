<?
if(CModule::IncludeModule("slam.image")){
    $moduleReize = 'CSlamImage';
}
else{
    $moduleReize = 'CFile';
}


foreach ($arResult['SECTIONS'] as $key => &$arSection){

    $photoArr = $moduleReize::ResizeImageGet(
        $arSection['DETAIL_PICTURE'],
        array("width"=> 600,"height" => 600),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    );

    $arSection['PICTURE_RESIZED']['SRC'] = $photoArr['src'];
    $arSection['PICTURE_RESIZED']['WIDTH'] = $photoArr['width'];
    $arSection['PICTURE_RESIZED']['HEIGHT'] = $photoArr['height'];
}