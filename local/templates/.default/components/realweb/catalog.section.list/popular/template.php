<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
$arPosition = array(
    0 => array('COL_START' => "Y", "COL_END" => "N", "CLASS" => "collections__card_r", "ITEM_CLASS" => ""),
    1 => array('COL_START' => "N", "COL_END" => "Y", "CLASS" => "collections__card_l", "ITEM_CLASS" => ""),
    2 => array('COL_START' => "Y", "COL_END" => "Y", "CLASS" => "collections__card_bg", "ITEM_CLASS" => 'collections__item_large'),
    3 => array('COL_START' => "Y", "COL_END" => "Y", "CLASS" => "collections__card_r", "ITEM_CLASS" => ""),
    4 => array('COL_START' => "Y", "COL_END" => "Y", "CLASS" => "collections__card_r", "ITEM_CLASS" => ""),
);
$i = 0;
?>
<div class="section-bg collections p-0">
    <div class="container container_md">
        <div class="row justify-content-between align-items-center">
            <div class="col col-12 col-md-auto"><span class="h2 h2_p like_h2">популярные коллекции</span></div>
            <!-- <div class="col col-12 col-md-auto"><a class="link-more" href="/catalog/collections/">Посмотреть все —</a> -->
        </div>
    </div>
    <div class="container">
        <div class="row row-10 collections__items">
            <? foreach ($arResult['SECTIONS'] as $key => $arSection): ?>
                <?
                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                if (!$arSection['UF_TOP9']) continue;
                if (!$arPosition[$i]) break;
                if (false === $arSection['PICTURE'])
                    $arSection['PICTURE'] = array(
                        'SRC' => $arCurView['EMPTY_IMG'],
                        'ALT' => (
                        '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                            ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                            : $arSection["NAME"]
                        ),
                        'TITLE' => (
                        '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                            ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                            : $arSection["NAME"]
                        )
                    );
                ?>
                <? if ($arPosition[$i]['COL_START'] == 'Y'): ?><div class="col col-12 col-lg-6"><? endif; ?>
                    <div class="collections__item <?= $arPosition[$i]['ITEM_CLASS'] ?>"
                         id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                        <a href="<?= $arSection['SECTION_PAGE_URL'] ?>"
                           class="collections__card <?= $arPosition[$i]['CLASS'] ?>">
                            <div class="collections__description">
                                <div class="collections__content">
                                    <div class="collections__name"><?= $arSection['NAME'] ?></div>
                                    <div class="collections__text">
                                        <?= $arSection['UF_COLLECTION_TEXT'] ?>
                                    </div>
                                    <span class="collections__detail">Подробнее —</span>
                                </div>
                            </div>
                            <div class="collections__picture">
                                <img alt="<?php echo $arSection['NAME']; ?>" class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" data-src="<?= $arSection['PICTURE_RESIZED']['SRC'] ?>">
                            </div>
                        </a>
                    </div>
                <? if ($arPosition[$i]['COL_END'] == 'Y'): ?></div><? endif; ?>
                <? $i++; ?>
            <? endforeach; ?>
        </div>
    </div>
</div>
