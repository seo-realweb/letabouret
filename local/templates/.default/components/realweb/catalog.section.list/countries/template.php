<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>
<div class="section-bg">
    <div class="section_border mt-2 mb-5">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col col-12 col-md-3"><span class="h2 h2_p like_h2">другие <span>страны</span></span></div>
                <div class="col col-12 col-md-9">
                    <div class="owl-carousel country owl-country-items js-owl-country">
                        <? foreach ($arResult['SECTIONS'] as $key => $arSection): ?>
                            <?
                            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                            if (false === $arSection['PICTURE'])
                                $arSection['PICTURE'] = array(
                                    'SRC' => $arCurView['EMPTY_IMG'],
                                    'ALT' => (
                                    '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                                        ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                                        : $arSection["NAME"]
                                    ),
                                    'TITLE' => (
                                    '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                                        ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                                        : $arSection["NAME"]
                                    )
                                );
                            ?>
                            <a href="<?= $arSection['SECTION_PAGE_URL'] ?>" class="country__item" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                                <img src="<?= CFile::GetPath($arSection['UF_PICTURE_MENU']) ?>"><span><?= $arSection['NAME'] ?></span>
                            </a>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
