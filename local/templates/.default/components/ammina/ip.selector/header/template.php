<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

?>
<div class="bam-ip">
	<a href="javascript:void(0)" title="<?= $arResult['FULL_NAME'] ?>" class="bam-ip-link"><?= $arResult['CITY_INFO']['CITY']['NAME'] ?></a>
</div>

<div class="bam-ip-confirm">
	<?
	if ($arResult['CONFIRM_REQUEST_SHOW']) {
		?>
		<div class="bam-ip-confirm-content">
			<div class="bam-ip-confirm-content-arrow"></div>
			<div class="bam-ip-confirm-title">
				<span><?= Loc::getMessage("AMCIP_CITY_TITLE") ?></span>
				<p><strong><?= $arResult['CITY_INFO']['CITY']['NAME'] ?></strong> (<?= $arResult['FULL_NAME_NO_CITY'] ?>
					)?</p>
			</div>
			<div class="bam-ip-confirm-buttons">
				<a href="javascript:void(0)" class="bam-ip-confirm-button bam-ip-confirm-button-no"><?= Loc::getMessage("AMCIP_NO") ?></a><a href="javascript:void(0)" class="bam-ip-confirm-button bam-ip-confirm-button-yes"><?= Loc::getMessage("AMCIP_YES") ?></a>
			</div>
		</div>
		<?
	}
	?>
</div>

<div class="bam-ip-popup"></div>

<div class="bam-ip-popupbg"></div>

<script type="text/javascript">
	$(document).ready(function () {
		$(".bam-ip").amminaIp(<?=CUtil::PhpToJSObject($arResult)?>, <?=CUtil::PhpToJSObject($arParams)?>);
	});
</script>