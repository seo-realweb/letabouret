<?php

use Realweb\Site\Site;
use Bitrix\Main\EventManager;

EventManager::getInstance()->addEventHandler("main", "OnPageStart", "onPageStart");
EventManager::getInstance()->addEventHandler('iblock', 'OnIBlockPropertyBuildList', array('Realweb\Site\Property\MultiString', 'GetUserTypeDescription'));
EventManager::getInstance()->addEventHandler('iblock', 'OnIBlockPropertyBuildList', array('Realweb\Site\Property\YoutubeVideo', 'GetUserTypeDescription'));
EventManager::getInstance()->addEventHandler('iblock', 'OnIBlockPropertyBuildList', array('Realweb\Site\Property\Hload', 'GetUserTypeDescription'));
EventManager::getInstance()->addEventHandler("main", "OnUserTypeBuildList", array("Realweb\Site\Property\UfProperty", "GetUserTypeDescription"));
EventManager::getInstance()->addEventHandler("main", "OnUserTypeBuildList", array("Realweb\Site\Property\Html", "GetUserTypeDescription"));
EventManager::getInstance()->addEventHandler("main", "OnUserTypeBuildList", array("Realweb\Site\Property\StringDesc", "GetUserTypeDescription"));
EventManager::getInstance()->addEventHandler('main', 'OnBeforeEventAdd', array('Realweb\Site\Handler', "OnBeforeEventAddHandler"));
EventManager::getInstance()->addEventHandler("search", "BeforeIndex", array("Realweb\Site\Handler", "reindex"));
//EventManager::getInstance()->addEventHandler("sale", "Bitrix\Sale\Internals\DiscountTable::OnAfterUpdate", array('\Realweb\Site\Handler', 'OnAfterUpdateDiscount'));
//EventManager::getInstance()->addEventHandler("sale", "Bitrix\Sale\Internals\DiscountTable::OnAfterAdd", array('\Realweb\Site\Handler', 'OnAfterUpdateDiscount'));
//EventManager::getInstance()->addEventHandler("sale", "Bitrix\Sale\Internals\DiscountTable::OnAfterDelete", array('\Realweb\Site\Handler', 'OnAfterUpdateDiscount'));
EventManager::getInstance()->addEventHandler("catalog", "OnDiscountAdd", array('\Realweb\Site\Handler', 'OnDiscountAdd'));
EventManager::getInstance()->addEventHandler("catalog", "OnDiscountUpdate", array('\Realweb\Site\Handler', 'OnDiscountUpdate'));
EventManager::getInstance()->addEventHandler("catalog", "OnDiscountDelete", array('\Realweb\Site\Handler', 'OnDiscountDelete'));
EventManager::getInstance()->addEventHandler('main', 'OnEpilog', array('\Realweb\Site\Site', "OnEpilog"));
EventManager::getInstance()->addEventHandler("main", "OnEndBufferContent", array('Realweb\Site\Site', 'OnEndBufferContent'));
EventManager::getInstance()->addEventHandler("main", "OnEndBufferContent", array('\Realweb\Site\Webp', 'convertAllToWebp'));
EventManager::getInstance()->addEventHandler("iblock", "OnTemplateGetFunctionClass", array('\Realweb\Site\Seo', "myOnTemplateGetFunctionClass"));
EventManager::getInstance()->addEventHandler("main", 'OnEpilog', array('\Realweb\Site\Seo', 'OnEpilog'), false, 100000);
EventManager::getInstance()->addEventHandler('catalog', '\Bitrix\Catalog\Price::OnAfterUpdate', array('Realweb\Site\Catalog', 'OnAfterPriceSave'));
EventManager::getInstance()->addEventHandler('catalog', '\Bitrix\Catalog\Price::OnAfterAdd', array('Realweb\Site\Catalog', 'OnAfterPriceSave'));
EventManager::getInstance()->addEventHandler('catalog', 'OnProductPriceDelete', array('Realweb\Site\Catalog', 'OnAfterPriceDelete'));

function onPageStart()
{
    global $APPLICATION;
    Site::definders();
    Site::getFilterPage();
    Site::checkUrl();
    $APPLICATION->IncludeComponent("realweb:redirect", "", Array());

}