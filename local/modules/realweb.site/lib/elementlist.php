<?php

namespace Realweb\Site;

use Bitrix\Main\DB\MysqlSqlHelper;
use Bitrix\Main\Loader;

class ElementList extends \Bitrix\Iblock\Component\ElementList
{
    protected $params;


    public function __construct($component = null)
    {
        parent::__construct($component);

        Loader::includeModule('catalog');
        Loader::includeModule('sale');
    }

    public function changeParam($paramName, $paramValue)
    {
        $this->params[$paramName] = $paramValue;
    }

    public function setParams($params)
    {
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }


    public function parseCondition($condition, $params = false)
    {
        if ($params == false) {
            $params = $this->params;
        }
        if (is_string($condition)) {
            $condition = json_decode($condition, true);
        }
        return parent::parseCondition($condition, $params);
    }


    public function getStages($elementId = false, $sectionId = false)
    {
        $arStage = Array();
        if ($elementId || $sectionId) {


            $arFilter = $this->getFilter($elementId, $sectionId);

            $rsResult = StageCacheTable::getList(Array('filter' => $arFilter));
            while ($arItem = $rsResult->fetch()) {
                $arStage[$arItem["STAGE_ID"]][] = $arItem["PRODUCT_ID"];
            }
            if (empty($arStage)) {
                $arStage = $this->__getStages_noCache($elementId, $sectionId);

                foreach ($arStage as $stageId => $arProductId) {
                    foreach ($arProductId as $productId) {

                        StageCacheTable::add(Array("PRODUCT_ID" => $productId, "STAGE_ID" => $stageId));
                    }
                }

            }
        }
        return $arStage;
    }

    public function getCountElement($section = false)
    {
        $count = 0;
        $arSection = [];
        $arSectionFilter = Array("IBLOCK_ID" => IBLOCK_CATALOG_TECHNOLOGY, "DEPTH_LEVEL" => 2, "ACTIVE" => "Y", "!UF_PROPERTY" => false);
        $rsStage = \CIBlockSection::GetList(Array(), $arSectionFilter, false, Array("ID", "UF_*"));
        while ($arStage = $rsStage->fetch()) {
            $arCondition = $this->parseCondition($arStage["UF_PROPERTY"]);
            if (!empty($arCondition)) {
                $arFilter = [];
                $arFilter[] = $arCondition;
                $count_section = \CIBlockElement::GetList(Array(), $arFilter, array(), false, Array("ID"));
                $count += $count_section;
                $arSection[$arStage['ID']] = $count_section;
            }
        }
        if ($section)
            return $arSection;
        else
            return $count;
    }

    public function clearAllCache()
    {
        $connection = \Bitrix\Main\Application::getConnection();

        $connection->truncateTable(StageCacheTable::getTableName());

    }


    public function clearCache($elementId = false, $sectionId = false)
    {


        $connection = \Bitrix\Main\Application::getConnection();
        $sqlHelper = new MysqlSqlHelper($connection);

        foreach (Array("PRODUCT_ID" => $elementId, "STAGE_ID" => $sectionId) as $columnName => $arValue)
            if (is_array($arValue)) {
                foreach ($arValue as &$value) {
                    $value = intval($value);
                }
                unset($value);
                $connection->query('DELETE FROM ' . StageCacheTable::getTableName() . " WHERE {$sqlHelper->quote($columnName)}  IN (" . implode(",", $arValue) . ")");

            }


        return true;

    }

    protected function getFilter($elementId = false, $sectionId = false)
    {
        $arFilter = Array();
        if ($elementId !== false || $sectionId !== false) {


            if ($elementId !== false) {
                $arFilter[] = array_merge(Array("PRODUCT_ID" => $elementId, "LOGIC" => "AND"));
            }
            if ($sectionId !== false) {
                $arFilter[] = array_merge(Array("STAGE_ID" => $sectionId, "LOGIC" => "AND"));
            }
            if (!empty($arFilter)) {
                $arFilter["LOGIC"] = "OR";
            }
        }

        return $arFilter;
    }


    public function __getStages_noCache($elementId = false, $sectionId = false)
    {

        $__arStages = Array();
        $arSectionFilter = Array("IBLOCK_ID" => IBLOCK_CATALOG_TECHNOLOGY, "DEPTH_LEVEL" => 2, "ACTIVE" => "Y", "!UF_PROPERTY" => false);
        if ($sectionId !== false) {
            $arSectionFilter["ID"] = $sectionId;
        }
        $rsStage = \CIBlockSection::GetList(Array(), $arSectionFilter, false, Array("ID", "UF_*"));

        while ($arStage = $rsStage->fetch()) {

            $arCondition = $this->parseCondition($arStage["UF_PROPERTY"]);
            if (!empty($arCondition)) {
                $arFilter = Array();
                if ($elementId) {

                    $arFilter["ID"] = $elementId;

                }
                $arFilter[] = $arCondition;
                $rsProduct = \CIBlockElement::GetList(Array(), $arFilter, false, false, Array("ID"));
                while ($arProduct = $rsProduct->fetch()) {
                    $__arStages[$arStage["ID"]][] = $arProduct["ID"];
                }
            }
        }
        return $__arStages;

    }


}