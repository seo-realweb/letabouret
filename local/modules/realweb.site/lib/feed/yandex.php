<?php


namespace Realweb\Site\Feed;


use Realweb\Site\Feed;

class Yandex extends Feed
{
    /**
     * @var \DomDocument
     */
    private $_dom = null;

    public function generate()
    {
        $catalog = $this->_getDom()->appendChild($this->_getDom()->createElement('yml_catalog'));
        $catalog->setAttribute('date', date('Y-m-d H:i:s'));
        /** @var  $shop */
        $shop = $this->_addShop($catalog);
        $this->_addCategories($shop);
        $this->_addOffers($shop);
        $this->_getDom()->formatOutput = true;
        $this->_getDom()->save(\Bitrix\Main\Application::getDocumentRoot() . "/gitignore/yandex_catalog.xml");
    }

    private function _getDom()
    {
        if ($this->_dom === null) {
            $this->_dom = new \DomDocument('1.0', 'UTF-8');
        }

        return $this->_dom;
    }

    private function _addShop(\DOMElement $node)
    {
        $shop = $node->appendChild($this->_getDom()->createElement('shop'));
        $shop->appendChild($this->_getDom()->createElement('name'))->nodeValue = 'letabouret.ru';
        $shop->appendChild($this->_getDom()->createElement('company'))->nodeValue = 'Le Tabouret';
        $shop->appendChild($this->_getDom()->createElement('url'))->nodeValue = $this->_getDomain();
        $currencies = $shop->appendChild($this->_getDom()->createElement('currencies'));
        $currency = $currencies->appendChild($this->_getDom()->createElement('currency'));
        $currency->setAttribute('id', 'RUR');
        $currency->setAttribute('rate', '1');

        return $shop;
    }

    private function _addCategories(\DOMElement $node)
    {
        $categories = $node->appendChild($this->_getDom()->createElement('categories'));
        $arSections = $this->_getSections();
        foreach ($arSections as $arSection) {
            if ($arSection['IBLOCK_SECTION_ID'] && empty($arSections[$arSection['IBLOCK_SECTION_ID']])) {
                continue;
            }
            $category = $categories->appendChild($this->_getDom()->createElement('category'));
            $category->setAttribute('id', $arSection['ID']);
            if ($arSection['IBLOCK_SECTION_ID']) {
                $category->setAttribute('parentId', $arSection['IBLOCK_SECTION_ID']);
            }
            $category->nodeValue = htmlspecialchars($arSection['NAME']);
        }

        return $categories;
    }

    private function _addOffers(\DOMElement $node)
    {
        $offers = $node->appendChild($this->_getDom()->createElement('offers'));
        $arElements = $this->_getElements();
        $arOffers = $this->_getOffers();
        $arSections = $this->_getSections();
        foreach ($arElements as $arElement) {
            if (empty($arSections[$arElement['IBLOCK_SECTION_ID']])) {
                continue;
            }
            if (!empty($arOffers[$arElement['ID']])) {
                foreach ($arOffers[$arElement['ID']] as $arOffer) {
                    $this->_addOffer($offers, $arElement, $arOffer);
                }
            } else {
                $this->_addOffer($offers, $arElement);
            }
        }
    }

    private function _addOffer(\DOMElement $node, $arElement, $arOffer = null)
    {
        $arItem = $arOffer ?: $arElement;
        if ($arItem['PRICE']['RESULT_PRICE']['DISCOUNT_PRICE'] /*&& $arItem['CATALOG_QUANTITY'] > 0*/) {
            $offer = $node->appendChild($this->_getDom()->createElement('offer'));
            $offer->setAttribute('id', $arItem['ID']);
            $offer->setAttribute('type', 'vendor.model');
            $offer->setAttribute('available', 'true');
            $offer->appendChild($this->_getDom()->createElement('name'))->nodeValue = htmlspecialchars($arItem['NAME']);
            $offer->appendChild($this->_getDom()->createElement('url'))->nodeValue = $this->_getDomain() . $arElement['DETAIL_PAGE_URL'] . ($arOffer ? ('?oid=' . $arItem['ID']) : '');
            $offer->appendChild($this->_getDom()->createElement('price'))->nodeValue = number_format($arItem['PRICE']['RESULT_PRICE']['DISCOUNT_PRICE'], 0, '.', '');
            $offer->appendChild($this->_getDom()->createElement('currencyId'))->nodeValue = 'RUR';
            $offer->appendChild($this->_getDom()->createElement('categoryId'))->nodeValue = $arElement['IBLOCK_SECTION_ID'];

		

            if ($arItem['PREVIEW_PICTURE_SRC']) {
                $offer->appendChild($this->_getDom()->createElement('picture'))->nodeValue = $this->_getDomain() . $arItem['PREVIEW_PICTURE_SRC'];
            } elseif ($arElement['PREVIEW_PICTURE_SRC']) {
                $offer->appendChild($this->_getDom()->createElement('picture'))->nodeValue = $this->_getDomain() . $arElement['PREVIEW_PICTURE_SRC'];
            } elseif ($arItem['DETAIL_PICTURE_SRC']) {
                $offer->appendChild($this->_getDom()->createElement('picture'))->nodeValue = $this->_getDomain() . $arItem['DETAIL_PICTURE_SRC'];
            } elseif ($arElement['DETAIL_PICTURE_SRC']) {
                $offer->appendChild($this->_getDom()->createElement('picture'))->nodeValue = $this->_getDomain() . $arElement['DETAIL_PICTURE_SRC'];
            }

            foreach (array('TYPE', 'RIGIDITY', 'HEIGTH', 'TYPE_COMPOSITION', 'COUNTRY', 'MATERIAL_FACADE', 'COLOR_FACADE', 'MATERIAL_KORPUSA', 'OBEM', 'MATERIAL_OBIVKI', 'RAZMER', 'VYSOTA_POTOLKA', 'POKRYTIE_KORPUSA', 'DIMENSIONS', 'STYLE', 'COLOR', 'MATERIAL', 'COLLECTION') as $strCode) {
                if ($arElement['PROPERTIES'][$strCode]['VALUE']) {
                    $param = $offer->appendChild($this->_getDom()->createElement('param'));
                    $param->nodeValue = htmlspecialchars(is_array($arElement['PROPERTIES'][$strCode]['VALUE']) ? implode(' / ', $arElement['PROPERTIES'][$strCode]['VALUE']) : $arElement['PROPERTIES'][$strCode]['VALUE']);
                    $param->setAttribute('name', htmlspecialchars($arElement['PROPERTIES'][$strCode]['NAME']));
                }
            }

            foreach (array('WIDTH', 'LENGTH', 'TISSUE', 'trim') as $strCode) {
                if (!empty($arItem['PROPERTIES'][$strCode]['VALUE'])) {
                    $param = $offer->appendChild($this->_getDom()->createElement('param'));
                    $param->nodeValue = htmlspecialchars(is_array($arItem['PROPERTIES'][$strCode]['VALUE']) ? implode(' / ', $arItem['PROPERTIES'][$strCode]['VALUE']) : $arItem['PROPERTIES'][$strCode]['VALUE']);
                    $param->setAttribute('name', htmlspecialchars($arItem['PROPERTIES'][$strCode]['NAME']));
                }
            }

            $offer->appendChild($this->_getDom()->createElement('vendor'))->nodeValue = $arElement['PROPERTIES']['BRAND']['VALUE'] ?: 'Le Tabouret';
            $offer->appendChild($this->_getDom()->createElement('model'))->nodeValue = $arElement['PROPERTIES']['ARTICUL']['VALUE'] ?: $arElement['ID'];
            if ($arItem['PREVIEW_TEXT']) {
                $offer->appendChild($this->_getDom()->createElement('description'))->nodeValue = htmlspecialchars($arItem['PREVIEW_TEXT']);
            } elseif ($arElement['PREVIEW_TEXT']) {
                $offer->appendChild($this->_getDom()->createElement('description'))->nodeValue = htmlspecialchars($arElement['PREVIEW_TEXT']);
            }
            $offer->appendChild($this->_getDom()->createElement('pickup'))->nodeValue = 'true';
            $offer->appendChild($this->_getDom()->createElement('delivery'))->nodeValue = 'true';

            return $offer;
        }
    }

    public static function agentGenerateYml()
    {
        $obFeed = new self();
        $obFeed->generate();

        return "\Realweb\Site\Feed\Yandex::agentGenerateYml();";
    }
}