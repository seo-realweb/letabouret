<?php


namespace Realweb\Site;


use Bitrix\Main\Loader;

abstract class Feed
{
    private $_sections = null;

    abstract public function generate();

    public function __construct()
    {
        Loader::includeModule('iblock');
        Loader::includeModule('catalog');
    }

    protected function _getElements()
    {
        $arResult = array();
        $rsElement = \CIBlockElement::GetList(
            array('SORT' => 'ASC'),
            array('IBLOCK_ID' => Catalog::getIblockId(), 'ACTIVE' => 'Y'),
            false,
            false,
            array("ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "PREVIEW_TEXT", "CATALOG_GROUP_1")
        );
        while ($obElement = $rsElement->GetNextElement()) {
            $arElement = $obElement->GetFields();
            if ($arElement['PREVIEW_PICTURE']) {
                $arElement['PREVIEW_PICTURE_SRC'] = \CFile::GetPath($arElement['PREVIEW_PICTURE']);
            }
            if ($arElement['DETAIL_PICTURE']) {
                $arElement['DETAIL_PICTURE_SRC'] = \CFile::GetPath($arElement['DETAIL_PICTURE']);
            }
            $arElement['PROPERTIES'] = $obElement->GetProperties();
            $arElement['PRICE'] = \CCatalogProduct::GetOptimalPrice($arElement['ID'], 1, array(2));
            $arResult[$arElement['ID']] = $arElement;
        }

        return $arResult;
    }

    protected function _getOffers()
    {
        $arResult = array();
        $rsElement = \CIBlockElement::GetList(
            array('SORT' => 'ASC'),
            array('IBLOCK_ID' => Catalog::getIblockOffersId(), 'ACTIVE' => 'Y'),
            false,
            false,
            array("ID", "NAME", "IBLOCK_ID", "PREVIEW_PICTURE", "PREVIEW_TEXT", "CATALOG_GROUP_1")
        );
        while ($obElement = $rsElement->GetNextElement()) {
            $arElement = $obElement->GetFields();
            if ($arElement['PREVIEW_PICTURE']) {
                $arElement['PREVIEW_PICTURE_SRC'] = \CFile::GetPath($arElement['PREVIEW_PICTURE']);
            }
            if ($arElement['DETAIL_PICTURE']) {
                $arElement['DETAIL_PICTURE_SRC'] = \CFile::GetPath($arElement['DETAIL_PICTURE']);
            }
            $arElement['PROPERTIES'] = $obElement->GetProperties();
            $arElement['PRICE'] = \CCatalogProduct::GetOptimalPrice($arElement['ID'], 1, array(2));
            $arResult[$arElement['PROPERTIES']['CML2_LINK']['VALUE']][$arElement['ID']] = $arElement;
        }

        return $arResult;
    }

    protected function _getSections()
    {
        if ($this->_sections === null) {
            $this->_sections = array();
            $rsElement = \CIBlockSection::GetList(
                array('LEFT_MARGIN' => 'ASC', 'SORT' => 'ASC'),
                array('IBLOCK_ID' => Catalog::getIblockId(), 'ACTIVE' => 'Y'),
                false,
                array("ID", "NAME", "IBLOCK_SECTION_ID", "IBLOCK_ID"),
                false
            );
            while ($arSection = $rsElement->GetNext()) {
                $this->_sections[$arSection['ID']] = $arSection;
            }
        }

        return $this->_sections;
    }

    protected function _getDomain()
    {
        return 'https://letabouret.ru';
    }
}