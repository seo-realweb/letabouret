<?php

namespace Realweb\Site;

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\UserFieldTable;

class Site
{

    public static function definders()
    {
        Loader::includeModule('iblock');
        Loader::includeModule('form');
        $rsResult = \Bitrix\Iblock\IblockTable::getList(array(
            'select' => array('ID', 'IBLOCK_TYPE_ID', 'CODE'),
        ));
        while ($row = $rsResult->fetch()) {
            $CONSTANT = ToUpper(implode('_', array('IBLOCK', $row['IBLOCK_TYPE_ID'], $row['CODE'])));
            if (!defined($CONSTANT)) {
                define($CONSTANT, $row['ID']);
            }
        }
        $rsForms = \CForm::GetList($by = "s_id", $order = "desc", array(), $is_filtered);
        while ($arForm = $rsForms->Fetch()) {
            $CONSTANT = ToUpper(implode('_', array('WEB_FORM', $arForm['SID'])));
            if (!defined($CONSTANT)) {
                define($CONSTANT, $arForm['ID']);
            }
        }
    }

    public static function localRedirect301($url)
    {
        LocalRedirect($url, false, "301 Moved Permanently");
        exit();
    }

    public static function isMainPage()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPage(false) == SITE_DIR;
    }

    public static function getCurPage()
    {
        global $APPLICATION;

        return $APPLICATION->GetCurPage(false);
    }

    public static function is404Page()
    {
        return (defined("ERROR_404") && ERROR_404 == "Y");
    }

    public static function showIncludeText($code, $isHideChange = false)
    {
        global $APPLICATION;
        $APPLICATION->IncludeComponent(
            "realweb:base.include",
            ".default",
            array(
                "CODE" => $code,
                "COMPONENT_TEMPLATE" => ".default",
                "EDIT_TEMPLATE" => ""
            ),
            false,
            array(
                "SHOW_ICON" => $isHideChange ? "Y" : 'N',
            )
        );
    }

    public function showH1()
    {
        global $APPLICATION;
        $html = '';
        $H1 = $APPLICATION->GetTitle();
        $HIDE_H1 = $APPLICATION->GetPageProperty('HIDE_H1', "N");
        $strClass = $APPLICATION->GetProperty('PAGE_CLASS_CONTAINER', 'container');
        if ($HIDE_H1 != "Y") {
            $html = '<div class="section-bg h1-block"><div class="'.$strClass.'"><h1 id="pagetitle">' . $H1 . '</h1></div></div>';
        }
        return $html;
    }

    public function showContentPageContainerStart()
    {
        global $APPLICATION;
        if ($APPLICATION->GetPageProperty('CONTENT_PAGE', 'N') == 'Y') {
            return '<div class="section-bg"><div class="container">';
        }
    }

    public function showContentPageContainerEnd()
    {
        global $APPLICATION;
        if ($APPLICATION->GetPageProperty('CONTENT_PAGE', 'N') == 'Y') {
            return '</div></div>';
        }
    }

    public static function setCookie($cookName, $value, $expire = false)
    {
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        if ($cookName && $value) {
            $cookie = new \Bitrix\Main\Web\Cookie($cookName, $value, $expire ? $expire : (time() + 60 * 60 * 24));
            $cookie->setDomain($context->getServer()->getHttpHost());
            $cookie->setHttpOnly(false);
            $context->getResponse()->addCookie($cookie);
            $context->getResponse()->flush("");
        }
    }

    public static function getMenu($iblockId, $level, $type = '')
    {
        global $APPLICATION;
        $aMenuLinksExt = array();
        if (Loader::IncludeModule('iblock')) {
            $arFilter = array(
                "SITE_ID" => SITE_ID,
                "ID" => $iblockId
            );

            $dbIBlock = \CIBlock::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter);
            $dbIBlock = new \CIBlockResult($dbIBlock);

            if ($arIBlock = $dbIBlock->GetNext()) {
                if (defined("BX_COMP_MANAGED_CACHE"))
                    $GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_" . $arIBlock["ID"]);

                if ($arIBlock["ACTIVE"] == "Y") {
                    $filterName = 'arrFilterSection' . $type;
                    $GLOBALS[$filterName] = array();
                    if ($type) {
                        if ($row = current(self::getUserFieldEnumValues(array('XML_ID' => 'UF_TYPE_MENU', 'ENTITY_ID' => 'IBLOCK_' . $iblockId . '_SECTION'), array('XML_ID' => $type)))) {
                            $GLOBALS[$filterName]['UF_TYPE_MENU'] = $row['ID'];
                        }
                    }
                    $aMenuLinksExt = $APPLICATION->IncludeComponent("realweb:menu.sections", "", array(
                        "IS_SEF" => "Y",
                        "SEF_BASE_URL" => "",
                        "SECTION_PAGE_URL" => $arIBlock['SECTION_PAGE_URL'],
                        "DETAIL_PAGE_URL" => $arIBlock['DETAIL_PAGE_URL'],
                        "IBLOCK_TYPE" => $arIBlock['IBLOCK_TYPE_ID'],
                        "IBLOCK_ID" => $arIBlock['ID'],
                        "DEPTH_LEVEL" => $level,
                        "CACHE_TYPE" => "N",
                        "FILTER_SECTION" => $filterName
                    ), false, Array('HIDE_ICONS' => 'Y'));
                }
            }

            if (defined("BX_COMP_MANAGED_CACHE"))
                $GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_new");
        }
        return $aMenuLinksExt;
    }

    public static function getUserFieldEnumValues($arFilter, $arFilterValues = array())
    {
        $arResult = array();
        $cacheTime = 3600;
        $cacheDir = __FUNCTION__;
        $cacheId = md5(__FUNCTION__ . "|" . serialize(array($arFilter, $arFilterValues)));
        $cache = Cache::createInstance();
        if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
            $arResult = $cache->GetVars();
        } elseif ($cache->StartDataCache()) {
            $rows = UserFieldTable::getList(array(
                'select' => array('ID'),
                'filter' => $arFilter
            ))->fetch();
            $FIELD_STATUS_ID = $rows['ID'];
            $cUserFieldsEnum = new \CUserFieldEnum();
            $rows = $cUserFieldsEnum->GetList(array("ID" => "ASC"), array_merge(array('USER_FIELD_ID' => $FIELD_STATUS_ID), $arFilterValues));
            while ($row = $rows->Fetch()) {
                $arResult[$row['ID']] = $row;
            }
            $cache->endDataCache($arResult);
        }
        return $arResult;
    }

    public static function getPropEnumValues($CODE, $IBLOCK_ID)
    {
        $arValues = [];
        $rsPropertyEnums = \CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $IBLOCK_ID, "CODE" => $CODE));
        while ($enumFields = $rsPropertyEnums->GetNext()) {
            $arValues[$enumFields["ID"]] = $enumFields;
        }
        return $arValues;
    }

    public static function getPropValue($CODE, $IBLOCK_ID, $ELEMENT_ID)
    {
        $arResult = array();
        if (Loader::includeModule('iblock')) {
            $dbProperty = \CIBlockElement::getProperty(
                $IBLOCK_ID,
                $ELEMENT_ID,
                "sort",
                "asc",
                array("CODE" => $CODE)
            );
            while ($arProperty = $dbProperty->Fetch()) {
                $arResult[] = $arProperty;
            }
        }
        return $arResult;
    }

    public static function setPropValue($CODE, $VALUE, $IBLOCK_ID, $ELEMENT_ID)
    {
        if (Loader::includeModule('iblock')) {
            \CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array($CODE => $VALUE ? $VALUE : false));
        }
    }

    public static function normalizePhone($phone)
    {
        return preg_replace('/[^0-9]/', "", $phone);
    }

    public static function getDomain()
    {
        return (\CMain::IsHTTPS() ? 'https' : 'http') . "://" . $_SERVER['SERVER_NAME'];
    }

    public static function getIblockElement($filter, $bMeta = false, int $cacheTime = 3600)
    {
        $arItems = self::getIBlockElements($filter, $bMeta, $cacheTime);
        if ($arItems) {
            return current($arItems);
        } else {
            return null;
        }
    }

    public static function getIBlockElements($filter, $bMeta = false, int $cacheTime = 3600)
    {
        $arResult = array();
        if (Loader::includeModule("iblock")) {
            global $CACHE_MANAGER;
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . serialize($filter));
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arResult = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                $rsElement = \CIBlockElement::GetList(
                    array("SORT" => "ASC"),
                    $filter,
                    false,
                    false,
                    array("*", "PROPERTY_*")
                );
                while ($obElement = $rsElement->GetNextElement()) {
                    $arFields = $obElement->GetFields();
                    $arFields['FIELDS'] = $arFields;
                    if ($arFields['PREVIEW_PICTURE']) $arFields['PREVIEW_PICTURE'] = \CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
                    if ($arFields['DETAIL_PICTURE']) $arFields['DETAIL_PICTURE'] = \CFile::GetFileArray($arFields['DETAIL_PICTURE']);
                    $arFields['PROPERTIES'] = $obElement->GetProperties();
                    foreach ($arFields['PROPERTIES'] as &$prop) {
                        if ($prop['PROPERTY_TYPE'] == 'F') {
                            if (is_array($prop['VALUE'])) {
                                foreach ($prop['VALUE'] as $val) {
                                    $prop['DISPLAY_VALUE'][] = \CFile::GetFileArray($val);
                                }
                            } else {
                                $prop['DISPLAY_VALUE'] = \CFile::GetFileArray($prop['VALUE']);
                            }
                        }
                        $arFields['PROPS'][$prop['CODE']] = $prop['VALUE'];
                    }
                    if ($bMeta) {
                        $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arFields['IBLOCK_ID'], $arFields['ID']);
                        $arFields['META'] = $ipropValues->getValues();
                    }
                    $arResult[$arFields['ID']] = $arFields;
                }
                $CACHE_MANAGER->RegisterTag("iblock_id_" . $filter['IBLOCK_ID']);
                $CACHE_MANAGER->EndTagCache();
                $cache->endDataCache($arResult);
            }
        }
        return $arResult;
    }

    public static function getSection($filter, $cntElement = false, $withElements = false)
    {
        $arSections = self::getSections($filter, $cntElement = false, $withElements = false);
        if ($arSections) {
            return current($arSections);
        }
    }

    public static function getSections($filter, $cntElement = false, $withElements = false)
    {
        $arResult = array();
        if (Loader::includeModule("iblock")) {
            global $CACHE_MANAGER;
            $cacheTime = 3600;
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . serialize($filter) . "|" . $cntElement . "|" . $withElements);
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arResult = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                $rsElement = \CIBlockSection::GetList(
                    array("SORT" => "ASC"),
                    $filter,
                    $cntElement,
                    array("*", "UF_*")
                );
                while ($obElement = $rsElement->GetNext()) {
                    if ($withElements && !$obElement['ELEMENT_CNT']) {
                        continue;
                    }
                    $arResult[$obElement['ID']] = $obElement;
                }
                $CACHE_MANAGER->RegisterTag("iblock_id_" . $filter['IBLOCK_ID']);
                $CACHE_MANAGER->EndTagCache();
                $cache->endDataCache($arResult);
            }
        }
        return $arResult;
    }

    public static function getFilterPage()
    {
        global $APPLICATION;
        if ($APPLICATION->GetCurPage(false) == '/') {
            $GLOBALS['arrFilterSectionPopular'] = array('!UF_TOP9' => false);
        }
    }

    public static function getCityName($code)
    {
        $item = \Bitrix\Sale\Location\LocationTable::getByCode($code, array(
            'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID),
            'select' => array('*', 'NAME_RU' => 'NAME.NAME')
        ))->fetch();
        if ($item) {
            return $item['NAME_RU'];
        }
    }

    public static function getTags()
    {
        $result = [];
        $tag = new \Realweb\Site\Hload('Tag');
        $position = self::getUserFieldEnumValues(array('XML_ID' => 'UF_POSITION', 'ENTITY_ID' => 'HLBLOCK_' . $tag->hlblock_id), array());
        foreach ($tag->GetList() as $item) {
            foreach ($item['UF_STYLE'] as &$val) {
                $val = unserialize(base64_decode($val));
                if ($val[0] == 'width') {
                    $item['WIDTH'] = $val[1];
                }
                $val = implode(":", $val);
            }
            if ($item['UF_PICTURE']) {
                $item['UF_PICTURE'] = \CFile::GetPath($item['UF_PICTURE']);
            }
            $item['UF_POSITION'] = $position[$item['UF_POSITION']];
            $result[$item['ID']] = $item;
        }
        return $result;
    }

    public static function showTags($ids)
    {
        $html = '';
        $tags = \Realweb\Site\Site::getTags();
        $prev = [];
        foreach ($ids as $tagId) {
            if ($tag = $tags[$tagId]) {
                $style = '';
                if ($prev) {
                    if ($prev['UF_POSITION']['XML_ID'] == $tag['UF_POSITION']['XML_ID']) {
                        switch ($tag['UF_POSITION']['XML_ID']) {
                            case 'top_left':
                                $style = 'margin-left: calc(10px + ' . $prev['WIDTH'] . ')';
                                break;
                            case 'top_right':
                                $style = 'margin-right: calc(10px + ' . $prev['WIDTH'] . ')';
                                break;
                        }
                    }
                }
                $html .= '<img class="product-tag product-tag_' . $tag['UF_POSITION']['XML_ID'] . '" src="' . $tag['UF_PICTURE'] . '" alt="' . $tag['UF_NAME'] . '" style="' . implode(";", $tag['UF_STYLE']) . ';' . $style . '">';
                $prev = $tag;
            }
        }
        return $html;
    }

    public static function OnEpilog()
    {
        global $lastModified;
        if (empty($lastModified)) {
            $lastModified = time();
        }
        if ($lastModified) {
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $lastModified) . ' GMT');
            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
                header('HTTP/1.1 304 Not Modified');
                exit();
            }
        }
    }

    public static function isCheckGooglePageSpeed()
    {
        return strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') !== false;
    }

    public function OnEndBufferContent(&$content)
    {
        if (!defined('ADMIN_SECTION')) {
            if (Site::isCheckGooglePageSpeed()) {
                self::deleteKernelCss($content);
                self::deleteKernelJs($content);
            }
        }
    }

    public static function deleteKernelJs(&$content)
    {
        $arPatternsToRemove = array(
            '/<script.+?src=".+?kernel_main\/kernel_main.+?\.js\?\d+"><\/script\>/',
            '/<script.+?src=".+?bitrix\/js\/main\/core\/core[^"]+"><\/script\>/',
            '/<script.+?src=".+?bitrix\/js\/[^"]+"><\/script\>/',
            '/<script.+?>BX\.(setCSSList|setJSList)\(\[.+?\]\).*?<\/script>/',
            '/<script.+?>if\(\!window\.BX\)window\.BX.+?<\/script>/',
            '/<script[^>]+?>\(window\.BX\|\|top\.BX\)\.message[^<]+<\/script>/',
        );

        $content = preg_replace($arPatternsToRemove, "", $content);
        $content = preg_replace("/\n{2,}/", "\n\n", $content);
    }

    public static function deleteKernelCss(&$content)
    {
        $arPatternsToRemove = array(
            '/<link.+?href=".+?kernel_main\/kernel_main\.css\?\d+"[^>]+>/',
            '/<link.+?href=".+?bitrix\/js\/[^"]+"[^>]+>/',
        );

        $content = preg_replace($arPatternsToRemove, "", $content);
        $content = preg_replace("/\n{2,}/", "\n\n", $content);

//        if (Site::isCheckGooglePageSpeed()) {
//            $content = preg_replace('/rel="stylesheet"/', 'rel="preload" as="style" onload="this.rel=\'stylesheet\'"', $content);
//        }
    }

    public static function getCurrentPage()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPage(false);
    }

    public static function checkUrl()
    {
        $strUrl = $_SERVER['REQUEST_URI'];
        if ($strUrl) {
            if (strlen($strUrl) > 1) {
                if (mb_substr($strUrl, -2) === '/?') {
                    Site::localRedirect301(rtrim($strUrl, '?'));
                }
            }
            if (strpos($strUrl, 'filter/clear')>0){

                $newurl=str_replace('filter/clear','',$strUrl);
                $newurl=str_replace('//','/',$newurl);

                Site::localRedirect301($newurl);
            }
            if (preg_match('/^(.*?)\/{2,}(.*?)$/', $strUrl)) {
                $strRedirectUrl = preg_replace('/([\/]+)/', '/', $strUrl);
                if ($strUrl != $strRedirectUrl) {
                    Site::localRedirect301($strRedirectUrl);
                }
            }
        }
    }

    public static function getMenuTree($arItems)
    {
        $arResult = [];
        $currentItem = false;
        $i = 0;
        $prev = 0;
        $parent = false;
        foreach ($arItems as $arItem) {
            if ($prev > $arItem['DEPTH_LEVEL']) {
                $currentItem = &$parent;
            }
            if ($arItem['DEPTH_LEVEL'] == 1) {
                $arResult[$i] = $arItem;
                $currentItem = &$arResult[$i];
            } else {
                $currentItem['CHILD'][$i] = $arItem;
                if ($arItem['IS_PARENT']) {
                    $parent = &$currentItem;
                    $currentItem = &$currentItem['CHILD'][$i];
                }
            }
            $prev = $arItem['DEPTH_LEVEL'];
            $i++;
        }

        return $arResult;
    }

    public static function getCatalogMenu($arFilter = array())
    {
        $obCache = new \CPHPCache();
        if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/menu/catalog")) {
            $arSections = $obCache->GetVars();
        } elseif ($obCache->StartDataCache()) {
            $arSections = [];
            if (\CModule::IncludeModule("iblock")) {
                $arMenu = array_column(\Realweb\Site\Site::getUserFieldEnumValues(
                    array(
                        'XML_ID' => 'UF_TYPE_MENU',
                        'ENTITY_ID' => 'IBLOCK_' . IBLOCK_CATALOG_CATALOG . '_SECTION'
                    )
                ), null, 'XML_ID');
                unset($arMenu['top']);
                unset($arMenu['bottom']);
                $arSections['MENU'] = $arMenu;
                $rsSection = \CIBlockSection::GetList(
                    array('SORT' => 'ASC'),
                    array('IBLOCK_ID' => IBLOCK_CATALOG_CATALOG, 'ACTIVE' => 'Y', 'UF_TYPE_MENU' => array_column($arMenu, 'ID', null))
                    , false, array('*', 'UF_*')
                );
                while ($arSection = $rsSection->GetNext()) {
                    if ($arSection['UF_PICTURE_MENU']) {
                        $arSection['UF_PICTURE_MENU'] = \CFile::GetFileArray($arSection['UF_PICTURE_MENU']);
                    }
                    foreach ($arSection['UF_TYPE_MENU'] as $typeId) {
                        $arSections['SECTIONS'][$typeId][$arSection['ID']] = $arSection;
                    }
                }

                $arSections['SECTIONS'][$arMenu['style']['ID']] = array();
                $rsSection = \CIBlockSection::GetList(
                    array('SORT' => 'ASC'),
                    array('IBLOCK_ID' => IBLOCK_CATALOG_STYLE, 'ACTIVE' => 'Y')
                    , false, array('*', 'UF_*')
                );
                while ($arSection = $rsSection->GetNext()) {
                    if ($arSection['UF_PICTURE_MENU']) {
                        $arSection['UF_PICTURE_MENU'] = \CFile::GetFileArray($arSection['UF_PICTURE_MENU']);
                    }
                    $arSections['SECTIONS'][$arMenu['style']['ID']][$arSection['ID']] = $arSection;
                }

                if (defined("BX_COMP_MANAGED_CACHE")) {
                    global $CACHE_MANAGER;
                    $CACHE_MANAGER->StartTagCache("/iblock/menu/aks");
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . IBLOCK_CATALOG_CATALOG);
                    $CACHE_MANAGER->EndTagCache();
                }
            }
            $obCache->EndDataCache($arSections);
        }

        return $arSections;
    }

    public static function setPageStatus404()
    {
        \Bitrix\Iblock\Component\Tools::process404("", true, true, true, "");
    }

    public static function removeHtmlAttributes($text, $allowed = [])
    {
        $attributes = implode('|', $allowed);
        $reg = '/(<[\w]+)([^>]*)(>)/i';
        $text = preg_replace_callback(
            $reg,
            function ($matches) use ($attributes) {
                if (!$attributes) {
                    return $matches[1] . $matches[3];
                }

                $attr = $matches[2];
                $reg = '/(' . $attributes . ')="[^"]*"/i';
                preg_match_all($reg, $attr, $result);
                $attr = implode(' ', $result[0]);
                $attr = ($attr ? ' ' : '') . $attr;

                return $matches[1] . $attr . $matches[3];
            },
            $text
        );

        return $text;
    }
}
