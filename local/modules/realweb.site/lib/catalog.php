<?php

namespace Realweb\Site;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Web\Uri;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Data\Cache;

class Catalog
{
    private static $skipItems = array();

    public static function getCatalogSectionParams($params = array(), $isAvailable = false)
    {
        $result = array_merge(array(
            "ACTION_VARIABLE" => "action",
            "ADD_PICT_PROP" => "ADDITIONALPHOTOS",
            "BRAND_PROPERTY" => "BRAND",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "ADD_TO_BASKET_ACTION" => "ADD",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "BACKGROUND_IMAGE" => "-",
            "BASKET_URL" => "/basket/",
            "BROWSER_TITLE" => "-",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "COMPATIBLE_MODE" => "Y",
            "CUSTOM_FILTER" => "",
            "CURRENCY_ID" => "RUB",
            "CONVERT_CURRENCY" => "Y",
            "DETAIL_URL" => "",
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_COMPARE" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "ELEMENT_SORT_FIELD" => "sort",
            "ELEMENT_SORT_FIELD2" => "id",
            "ELEMENT_SORT_ORDER" => "asc",
            "ELEMENT_SORT_ORDER2" => "desc",
            "ENLARGE_PRODUCT" => "STRICT",
            "FILTER_NAME" => "arrFilter",
            "HIDE_NOT_AVAILABLE" => "Y",
            "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
            "IBLOCK_ID" => IBLOCK_CATALOG_CATALOG,
            "IBLOCK_TYPE" => "catalog",
            "INCLUDE_SUBSECTIONS" => "Y",
            "LABEL_PROP" => array(),
            "LAZY_LOAD" => "N",
            "LINE_ELEMENT_COUNT" => "3",
            "LOAD_ON_SCROLL" => "N",
            "MESSAGE_404" => "",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_BTN_LAZY_LOAD" => "Показать ещё",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "META_DESCRIPTION" => "-",
            "META_KEYWORDS" => "-",
            "OFFERS_CART_PROPERTIES" => array(),
            "OFFERS_FIELD_CODE" => array(0 => "NAME"),
            "OFFERS_LIMIT" => "0",
            "OFFERS_PROPERTY_CODE" => array(),
            "OFFERS_SORT_FIELD" => "sort",
            "OFFERS_SORT_FIELD2" => "id",
            "OFFERS_SORT_ORDER" => "asc",
            "OFFERS_SORT_ORDER2" => "desc",
            "OFFER_ADD_PICT_PROP" => "-",
            "OFFER_TREE_PROPS" => array(),
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "catalog",
            "PAGER_TITLE" => "Товары",
            "PAGE_ELEMENT_COUNT" => "12",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRICE_CODE" => array("BASE"),
            "PRICE_VAT_INCLUDE" => "Y",
            "PRODUCT_BLOCKS_ORDER" => "sku,price,props,buttons,compare",
            "PRODUCT_DISPLAY_MODE" => "Y",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPERTIES" => array(),
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "LIST_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
            "PRODUCT_SUBSCRIPTION" => "Y",
            "PROPERTY_CODE" => array("ARTICUL", ""),
            "PROPERTY_CODE_MOBILE" => array("ARTICUL", ""),
            "RCM_PROD_ID" => "",
            "RCM_TYPE" => "personal",
            "SECTION_CODE" => "",
            "SECTION_ID" => "",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "SECTION_URL" => "",
            "SECTION_USER_FIELDS" => array("", ""),
            "SEF_MODE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SHOW_ALL_WO_SECTION" => "Y",
            "SHOW_CLOSE_POPUP" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_FROM_SECTION" => "N",
            "SHOW_MAX_QUANTITY" => "N",
            "SHOW_OLD_PRICE" => "Y",
            "SHOW_PRICE_COUNT" => "1",
            "SHOW_SLIDER" => "Y",
            "SLIDER_INTERVAL" => "3000",
            "SLIDER_PROGRESS" => "N",
            "TEMPLATE_THEME" => "blue",
            "USE_ENHANCED_ECOMMERCE" => "N",
            "USE_MAIN_ELEMENT_SECTION" => "Y",
            "USE_PRICE_COUNT" => "N",
            "USE_PRODUCT_QUANTITY" => "N",
            "TYPE_ITEM_VIEW" => "CARD",
            'HIDE_SECTION_DESCRIPTION' => 'Y'
        ), $params);
        if ($isAvailable) {
            //$GLOBALS[$result['FILTER_NAME']][] = self::getCatalogFilter();
        }
        return $result;
    }

    public static function getIblockId()
    {
        return IBLOCK_CATALOG_CATALOG;
    }

    public static function getIblockOffersId()
    {
        return IBLOCK_OFFERS_OFFERS;
    }

    public static function getSortCatalog(&$arParams)
    {
        $result = [];
        $uri = new Uri(Application::getInstance()->getContext()->getRequest()->getRequestUri());
        $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        if (empty($sortValue)) {
            $sortValue = [
                "price" => array('name' => "цена", 'code' => 'PROPERTY_MIN_PRICE_DISCOUNT'),
                "rating" => array('name' => "популярность", 'code' => 'SORT'),
                "rating" => array('name' => "популярность", 'code' => 'SORT'),
                "rayting" => array('name' => "новизна", 'code' => 'TIMESTAMP_X'),
              // "skidka" => array('name' => "скидка", 'code' => 'SORT'),
            ];
        }
        $sort = $request->get("sort");
        $order = $request->get("order");

        if ($sort && $order) {
            $_SESSION['sort'] = $sort;
            $_SESSION['order'] = $order;
        } elseif ($_SESSION['sort'] && $_SESSION['order']) {
            $sort = $_SESSION['sort'];
            $order = $_SESSION['order'];
        } else {
            $sort = 'sort';
            $order = 'ASC,nulls';
            $arParams['ELEMENT_SORT_FIELD2'] = $sort;
            $arParams['ELEMENT_SORT_ORDER2'] = $order;
        }

        foreach ($sortValue as $key => $item) {

            $strUri = $uri
                ->deleteParams(["bxajaxid", 'PAGEN_1', 'PAGEN_2'])
                ->addParams(["sort" => $key, "order" => $order == 'ASC,nulls' ? 'DESC,nulls' : 'ASC,nulls'])
                ->getUri();

            $strUri = str_replace($uri->getPath(), '', $strUri);
            $sortItem = [
                "order" => $order == 'ASC,nulls' ? 'DESC,nulls' : 'ASC,nulls',
                "name" => $item['name'] . (($order == 'ASC,nulls') ? ' &darr;' : ' &uarr;'),
                "field" => $item['code'],
                "selected" => ($key == $sort),
                "url" => $strUri
            ];

            if ($sortItem['selected']) {
                $arParams['ELEMENT_SORT_FIELD2'] = $sortItem['field'];
                $arParams['ELEMENT_SORT_ORDER2'] = $order;
            }

            $result[] = $sortItem;
        }
        return $result;
    }

    public static function getViewedProducts()
    {
        $ids = unserialize(Application::getInstance()->getContext()->getRequest()->getCookie("VIEWED"));
        return $ids;
    }

    private static function getChildCond($childs, &$ids)
    {
        foreach ($childs as $child) {
            if ($child['CHILDREN']) {
                self::getChildCond($child['CHILDREN'], $ids);
            }
            if ($child['CLASS_ID'] == 'CondIBSection') {
                foreach ($child['DATA']['value'] as $id) {
                    $ids[$id] = $id;
                }
            }
        }
    }

    public static function getDiscount()
    {
        $arResult = array();
        $cacheTime = 3600;
        $cacheDir = __FUNCTION__;
        $cacheId = md5(__FUNCTION__);
        $cache = Cache::createInstance();
        if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
            $arResult = $cache->GetVars();
        } elseif ($cache->StartDataCache()) {
            Loader::includeModule('catalog');
            $dbProductDiscounts = \CCatalogDiscount::GetList(
                array("SORT" => "ASC"),
                array(
                    "COUPON" => false,
                    "ACTIVE" => 'Y'
                ),
                false,
                false,
                array('*')
            );
            while ($arProductDiscounts = $dbProductDiscounts->Fetch()) {
//                $ids = [];
//                if ($cond = unserialize($arProductDiscounts['CONDITIONS'])) {
//                    self::getChildCond($cond['CHILDREN'], $ids);
//                }
//                $arProductDiscounts['SECTION_ID'] = $ids;
                $arResult[] = $arProductDiscounts;
            }
            $cache->endDataCache($arResult);
        }
        return $arResult;
    }

    public static function updateCatalogAgent()
    {
        $iPage = 1;
        do {
            $obCatalogComponent = new Catalog\CatalogSectionComponent();
            $obCatalogComponent->arParams = self::getCatalogSectionParams(array(
                'CACHE_TYPE' => 'N',
                'CACHE_TIME' => '0',
                'PAGE_ELEMENT_COUNT' => '100',
                'FILTER_NAME' => 'arrFilterCatalog',
                "HIDE_NOT_AVAILABLE" => "N",
                "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                "CUSTOM_FILTER" => array()
            ));
            $obCatalogComponent->setGlobalFilter(array(
                'PROPERTY_QUERYPRICE' => false,
            ));
            $obCatalogComponent->setCacheUsage(false);
            $obCatalogComponent->setCurrentPage($iPage);
            $obCatalogComponent->executeComponent();
            foreach ($obCatalogComponent->arResult['ITEMS'] as $arItem) {
                $bDiscount = false;
                if ($arItem['OFFERS']) {
                    foreach ($arItem['OFFERS'] as $arOffer) {
                        if ($arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]['DISCOUNT'] > 0) {
                            $bDiscount = true;
                            break;
                        }
                    }
                } else {
                    if ($arItem['ITEM_PRICES'][$arItem['ITEM_PRICE_SELECTED']]['DISCOUNT'] > 0) {
                        $bDiscount = true;
                    }
                }
                if ($bDiscount) {
                    \CIBlockElement::SetPropertyValuesEx($arItem['ID'], $arItem['IBLOCK_ID'], array('IS_SALE' => 'Y'));
                } else {
                    \CIBlockElement::SetPropertyValuesEx($arItem['ID'], $arItem['IBLOCK_ID'], array('IS_SALE' => 'N'));
                }
                \Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex($arItem['IBLOCK_ID'], $arItem['ID']);
            }
            $iPage++;
        } while ($obCatalogComponent->arResult['NAV_RESULT']->NavPageCount >= $iPage);
        Application::getInstance()->getTaggedCache()->clearByTag('iblock_' . IBLOCK_CATALOG_CATALOG);
        Application::getInstance()->getManagedCache()->cleanAll();


        if(CModule::IncludeModule("iblock"))
        {
// меняем обязательно ваш номер ИБ
            $resElem = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y"), false, false, Array("ID","IBLOCK_ID"));
            while($obElem = $resElem->Fetch())
            {
                $arPrice = CCatalogProduct::GetOptimalPrice($obElem["ID"], 1, array(2), "N", array() , "s1");

                if($arPrice["DISCOUNT_PRICE"] > 0){
                    CIBlockElement::SetPropertyValues($obElem["ID"], $obElem["IBLOCK_ID"], $arPrice["DISCOUNT_PRICE"], "MIN_PRICE_DISCOUNT");
                }else{
                    CIBlockElement::SetPropertyValues($obElem["ID"], $obElem["IBLOCK_ID"], 1000000, "MIN_PRICE_DISCOUNT");
                }
            }
        }

        return '\Realweb\Site\Catalog::updateCatalogAgent();';
    }

    public static function checkAgentCatalogUpdate()
    {
        $rsAgent = \CAgent::GetList(array(), array('NAME' => '\Realweb\Site\Catalog::updateCatalogAgent();'));
        if ($arAgent = $rsAgent->GetNext()) {
            \CAgent::Update($arAgent['ID'], array(
                'ACTIVE' => 'Y',
                'RETRY_COUNT' => 0,
                'DATE_CHECK' => null,
                'NEXT_EXEC' => date('d.m.Y H:i:s')
            ));
        } else {
            \CAgent::AddAgent(
                "\Realweb\Site\Catalog::updateCatalogAgent();",
                "realweb.site",
                "N",
                3600,
                date('d.m.Y H:i:s'),
                "Y",
                date('d.m.Y H:i:s'),
                0);
        }
    }

    private static function _updateProductPrice(int $iProductId)
    {
        $fPrice = 0;
        $iIblockId = \CIBlockElement::GetIBlockByID($iProductId);
        if ($iIblockId == self::getIblockId()) {
            $iElementId = $iProductId;
        } else {
            $arOffer = Site::getIBlockElement(array('ID' => $iProductId, 'IBLOCK_ID' => self::getIblockOffersId()));
            if ($arOffer) {
                $iElementId = $arOffer['PROPS']['CML2_LINK'];
            }
        }
        if (!empty($iElementId)) {
            $arOffers = Site::getIBlockElements(array('PROPERTY_CML2_LINK' => $iElementId, 'IBLOCK_ID' => self::getIblockOffersId()));
            if ($arOffers) {
                foreach ($arOffers as $arOffer) {
                    if (!$fPrice) {
                        $arPrice = \CCatalogProduct::GetOptimalPrice($arOffer['ID']);
                        if (!empty($arPrice['PRICE']['PRICE'])) {
                            $fPrice = $arPrice['PRICE']['PRICE'];
                        }
                    }
                }
            } else {
                $arPropValue = Site::getPropValue('QUERYPRICE', self::getIblockId(), $iElementId);
                if ($arPropValue && current($arPropValue) && current($arPropValue)['VALUE']) {
                    $fPrice = 0;
                } else {
                    $arPrice = \CCatalogProduct::GetOptimalPrice($iElementId);
                    if (!empty($arPrice['PRICE']['PRICE'])) {
                        $fPrice = $arPrice['PRICE']['PRICE'];
                    }
                }
            }
            if (!doubleval($fPrice)) {
                \CIBlockElement::SetPropertyValuesEx($iElementId, self::getIblockId(), array('WITHOUT_PRICE' => '1'));
            } else {
                \CIBlockElement::SetPropertyValuesEx($iElementId, self::getIblockId(), array('WITHOUT_PRICE' => '0'));
            }
        }
    }

    function OnAfterPriceSave(\Bitrix\Main\Entity\Event $event)
    {
        $dataFields = $event->getParameter("fields");
        $iProductId = $dataFields['PRODUCT_ID'];
        if ($iProductId) {
            self::_updateProductPrice($iProductId);
        }
    }

    function OnAfterPriceDelete($productId, $arPricesId)
    {
        if (!$arPricesId && $productId) {
            self::_updateProductPrice($productId);
        }
    }

    public static function updateProductAgent()
    {
        Loader::includeModule('iblock');
        Loader::includeModule('catalog');
        $arElements = ElementTable::query()
            ->setSelect(array('ID'))
            ->where('IBLOCK_ID', '=', self::getIblockId())
            ->exec()
            ->fetchAll();
        foreach ($arElements as $arElement) {
            $iElementId = $arElement['ID'];
            $fPrice = 0;
            $arOffers = Site::getIBlockElements(array('PROPERTY_CML2_LINK' => $iElementId, 'IBLOCK_ID' => self::getIblockOffersId()));
            if ($arOffers) {
                foreach ($arOffers as $arOffer) {
                    if (!$fPrice) {
                        $arPrice = \CCatalogProduct::GetOptimalPrice($arOffer['ID']);
                        if (!empty($arPrice['PRICE']['PRICE'])) {
                            $fPrice = $arPrice['PRICE']['PRICE'];
                        }
                    }
                }
            } else {
                $arPropValue = Site::getPropValue('QUERYPRICE', self::getIblockId(), $iElementId);
                if ($arPropValue && current($arPropValue) && current($arPropValue)['VALUE']) {
                    $fPrice = 0;
                } else {
                    $arPrice = \CCatalogProduct::GetOptimalPrice($iElementId);
                    if (!empty($arPrice['PRICE']['PRICE'])) {
                        $fPrice = $arPrice['PRICE']['PRICE'];
                    }
                }
            }
            if (!doubleval($fPrice)) {
                \CIBlockElement::SetPropertyValuesEx($iElementId, self::getIblockId(), array('WITHOUT_PRICE' => '1'));
            } else {
                \CIBlockElement::SetPropertyValuesEx($iElementId, self::getIblockId(), array('WITHOUT_PRICE' => '0'));
            }
            \Bitrix\Catalog\ProductTable::update($iElementId, Array('AVAILABLE' => 'Y'));
        }

        return "\Realweb\Site\Catalog::updateProductAgent();";
    }
}