<?php namespace Realweb\Site;

use Bitrix\Main\Event;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

class Handler
{
    public function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
    {
        if ($arFields['ORDER_ID'] && \Bitrix\Main\Loader::includeModule('sale')) {
            //\Bitrix\Main\Diag\Debug::writeToFile($arFields);
            if ($order = \Bitrix\Sale\Order::load($arFields['ORDER_REAL_ID'])) {
                $propertyCollection = $order->getPropertyCollection();
                $arPropertyCollection = $propertyCollection->getArray();
                $arProps = [];
                foreach ($arPropertyCollection['properties'] as $prop) {
                    $code = $prop['CODE'] ? $prop['CODE'] : $prop['ID'];
                    $arProps[$code] = current($prop['VALUE']);
                    if ($arProps[$code]) {
                        $arFields['PROPERTY_' . $code] = '<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;"><b>' . $prop['NAME'] . '</b>: ' . current($prop['VALUE']) . '</p>';
                        $arFields['PROPERTY_RAW_' . $code] = current($prop['VALUE']);
                    }
                }
                //$arFields['LOCATION'] = Site::getCityName($arFields['PROPERTY_RAW_LOCATION']);
                $service = \Bitrix\Sale\Delivery\Services\Manager::getById($order->getField("DELIVERY_ID"));
                $arFields['DELIVERY_NAME'] = $service['NAME'];
                $service = \Bitrix\Sale\PaySystem\Manager::getById($order->getField("PAY_SYSTEM_ID"));
                $arFields['PAY_SYSTEM_NAME'] = $service['NAME'];
                $arFields['COMMENTS'] = $order->getField('COMMENTS');
                $arFields['USER_DESCRIPTION'] = $order->getField('USER_DESCRIPTION');
                $res = \CSaleStatus::GetList(array(), array('ID' => $order->getField('STATUS_ID')));
                if ($row = $res->Fetch()) {
                    $arFields['ORDER_STATUS'] = $row['NAME'];
                }
                $arFields['ORDER_PRICE'] = $order->getPrice();
                $basket = $order->getBasket();
                $basketItems = $basket->getBasketItems();
                $arFields['PRODUCT_LIST'] = '';
                foreach ($basketItems as $basketItem) {
                    $basketPropertyCollection = $basketItem->getPropertyCollection();
                    $props = $basketPropertyCollection->getPropertyValues();
                    $arFields['PRODUCT_LIST'] .= '
                    <tr>
                        <td style="padding: 10px;">' . $basketItem->getField('NAME') . '</td>
                        <td style="padding: 10px;">' . $basketItem->getQuantity() . '</td>
                        <td style="padding: 10px;">' . $basketItem->getPrice() . ' руб.</td>
                        <td style="padding: 10px;">' . $basketItem->getFinalPrice() . ' руб.</td>
                    </tr>';
                }
                $arFields['PRODUCT_PRICE'] = $basket->getPrice();
                $arFields['DELIVERY_PRICE'] = $order->getDeliveryPrice();
            }
        }
    }

    function reindex($arFields)
    {
        if ($arFields["PARAM2"] == IBLOCK_CATALOG_CATALOG && $arFields["URL"]) {
            if (substr($arFields["ITEM_ID"], 0, 1) == "S") {

            } else {
                \Bitrix\Main\Loader::includeModule('iblock');
                $title = $arFields["TITLE"];
                $db_props = \CIBlockElement::GetProperty(IBLOCK_CATALOG_CATALOG, $arFields["ITEM_ID"], array("sort" => "asc"), Array("CODE" => "ARTICUL", "EMPTY" => 'N'));
                if ($ar_props = $db_props->Fetch()) {
                    if (stripos($title, $ar_props["VALUE"]) === false)
                        $title .= ' ' . $ar_props["VALUE"];
                }
                $arFields["BODY"] = $arFields["TITLE"] = $title;
            }
        }
        return $arFields;
    }

//    function OnAfterUpdateDiscount(Event $event)
//    {
//
//    }

    function OnDiscountAdd($ID, $arFields)
    {
        Catalog::checkAgentCatalogUpdate();
    }

    function OnDiscountUpdate($ID, $arFields)
    {
        Catalog::checkAgentCatalogUpdate();
    }

    function OnDiscountDelete($ID, $arFields)
    {
        Catalog::checkAgentCatalogUpdate();
    }
}