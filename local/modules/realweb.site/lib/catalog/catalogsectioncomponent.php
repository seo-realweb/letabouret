<?php

namespace Realweb\Site\Catalog;

use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global \CUser $USER
 * @global \CMain $APPLICATION
 */

Loc::loadMessages(__FILE__);

if (!Loader::includeModule('iblock'))
{
    ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
    return;
}

\CBitrixComponent::includeComponentClass('bitrix:catalog.section');

class CatalogSectionComponent extends \CatalogSectionComponent
{
    private $_current_page = 1;

    public function setCacheUsage($state)
    {
        return parent::setCacheUsage($state);
    }

    public function setGlobalFilter($arFilter)
    {
        $this->globalFilter = $arFilter;

        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->_current_page;
    }

    /**
     * @param int $current_page
     */
    public function setCurrentPage(int $current_page): void
    {
        $this->_current_page = $current_page;
    }

    protected function getElementList($iblockId, $products)
    {
        $this->navParams['iNumPage'] = $this->getCurrentPage();

        return parent::getElementList($iblockId, $products);
    }
}