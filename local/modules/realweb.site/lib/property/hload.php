<?php

namespace Realweb\Site\Property;

use Bitrix\Main\Localization\Loc;
use Bitrix\Highloadblock as HL;

class Hload
{
    const TABLE_PREFIX = 'b_hlbd_';

    protected static $arFullCache = array();
    protected static $arItemCache = array();
    protected static $directoryMap = array();
    protected static $hlblockCache = array();
    protected static $hlblockClassNameCache = array();

    function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "hload",
            "DESCRIPTION" => "Привязка к HighLoad block",
            "GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
            "ConvertToDB" => array(__CLASS__, "ConvertToDB"),
            "ConvertFromDB" => array(__CLASS__, "ConvertFromDB"),
            "PrepareSettings" => Array(__CLASS__, "PrepareSettings"),
            "GetSettingsHTML" => Array(__CLASS__, "GetSettingsHTML"),
        );
    }

    function PrepareSettings($arFields)
    {
        return array(
            "TABLE_NAME" => $arFields['USER_TYPE_SETTINGS']['TABLE_NAME'],
        );
    }

    function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $html = '';
        $settings = self::PrepareSettings($arProperty);
        $rsData = HL\HighloadBlockTable::getList(array(
            'select' => array('*', 'NAME_LANG' => 'LANG.NAME'),
            'order' => array('NAME_LANG' => 'ASC', 'NAME' => 'ASC')
        ));
        $cellOption = '<option value=""'.('' == $settings["TABLE_NAME"] ? ' selected' : '').'></option>';
        while($arData = $rsData->fetch())
        {
            $arData['NAME_LANG'] = (string)$arData['NAME_LANG'];
            $hlblockTitle = ($arData['NAME_LANG'] != '' ? $arData['NAME_LANG'] : $arData['NAME']).' ('.$arData["TABLE_NAME"].')';
            $selected = ($settings["TABLE_NAME"] == $arData['TABLE_NAME']) ? ' selected' : '';
            $cellOption .= '<option '.$selected.' value="'.htmlspecialcharsbx($arData["TABLE_NAME"]).'">'.htmlspecialcharsbx($hlblockTitle).'</option>';
            unset($hlblockTitle);
        }
        $html .= '<tr>
        <td>HB:</td>
        <td>
        <select name="' . $strHTMLControlName["NAME"] . '[TABLE_NAME]">';
        $html .= $cellOption;
        $html .= '</select>
            </td>
        </tr>';
        return $html;
    }

    /**
     * Returns list values.
     *
     * @param array $arProperty			Property description.
     * @param array $values				Current value.
     * @return string
     */
    public static function GetOptionsHtml($arProperty, $values)
    {
        $selectedValue = false;
        $cellOption = '';
        $defaultOption = '';
        $highLoadIBTableName = (isset($arProperty["USER_TYPE_SETTINGS"]["TABLE_NAME"]) ? $arProperty["USER_TYPE_SETTINGS"]["TABLE_NAME"] : '');
        if($highLoadIBTableName != '')
        {
            if (empty(self::$arFullCache[$highLoadIBTableName]))
            {
                self::$arFullCache[$highLoadIBTableName] = self::getEntityFieldsByFilter(
                    $highLoadIBTableName,
                    array(
                        'select' => array('ID', 'UF_NAME', 'ID')
                    )
                );
            }
            foreach(self::$arFullCache[$highLoadIBTableName] as $data)
            {
                $options = '';
                if(in_array($data["ID"], $values))
                {
                    $options = ' selected';
                    $selectedValue = true;
                }
                if (current($values) == "" || ($values && $options))
                    $cellOption .= '<option '.$options.' value="'.htmlspecialcharsbx($data['ID']).'">'.htmlspecialcharsEx($data["UF_NAME"].' ['.$data["ID"]).']</option>';
            }
            $defaultOption = '<option value=""'.($selectedValue ? '' : ' selected').'>'.Loc::getMessage('HIBLOCK_PROP_DIRECTORY_EMPTY_VALUE').'</option>';
        }
        else
        {
            $cellOption = '<option value="" selected>'.Loc::getMessage('HIBLOCK_PROP_DIRECTORY_EMPTY_VALUE').'</option>';
        }
        return $defaultOption.$cellOption;
    }

    /**
     * Returns entity data.
     *
     * @param string $tableName				HL table name.
     * @param array $listDescr				Params for getList.
     * @return array
     */
    private static function getEntityFieldsByFilter($tableName, $listDescr = array())
    {
        $arResult = array();
        $tableName = (string)$tableName;
        if (!is_array($listDescr))
            $listDescr = array();
        if (!empty($tableName))
        {
            if (!isset(self::$hlblockCache[$tableName]))
            {
                self::$hlblockCache[$tableName] = HL\HighloadBlockTable::getList(
                    array(
                        'select' => array('TABLE_NAME', 'NAME', 'ID'),
                        'filter' => array('=TABLE_NAME' => $tableName)
                    )
                )->fetch();
            }
            if (!empty(self::$hlblockCache[$tableName]))
            {
                if (!isset(self::$directoryMap[$tableName]))
                {
                    $entity = HL\HighloadBlockTable::compileEntity(self::$hlblockCache[$tableName]);
                    self::$hlblockClassNameCache[$tableName] = $entity->getDataClass();
                    self::$directoryMap[$tableName] = $entity->getFields();
                    unset($entity);
                }
                if (!isset(self::$directoryMap[$tableName]['ID']))
                    return $arResult;
                $entityDataClass = self::$hlblockClassNameCache[$tableName];

                $listDescr['select'] = array('ID', 'UF_NAME');
                $listDescr['order']['UF_NAME'] = 'ASC';

                /** @var \Bitrix\Main\DB\Result $rsData */
                $rsData = $entityDataClass::getList($listDescr);
                while($arData = $rsData->fetch())
                {
                    $arResult[$arData['ID']] = $arData;
                }
                unset($arData, $rsData);
            }
        }
        return $arResult;
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $settings = self::PrepareSettings($arProperty);
        $options = self::GetOptionsHtml($arProperty, array($value["VALUE"]));
        $html = '<select name="'.$strHTMLControlName["VALUE"].'">';
        $html .= $options;
        $html .= '</select>';
        return  $html;
    }

    /**
     * Return html for edit multiple value.
     *
     * @param array $arProperty				Property description.
     * @param array $value					Current value.
     * @param array $strHTMLControlName		Control description.
     * @return string
     */
    public static function GetPropertyFieldHtmlMulty($arProperty, $value, $strHTMLControlName)
    {
        $max_n = 0;
        $values = array();
        if(is_array($value))
        {
            $match = array();
            foreach($value as $property_value_id => $arValue)
            {
                $values[$property_value_id] = $arValue["VALUE"];
                if(preg_match("/^n(\\d+)$/", $property_value_id, $match))
                {
                    if($match[1] > $max_n)
                        $max_n = intval($match[1]);
                }
            }
        }

        $settings = self::PrepareSettings($arProperty);

        if($settings["multiple"]=="Y")
        {
            $options = self::GetOptionsHtml($arProperty, $values);
            $html = '<select multiple name="'.$strHTMLControlName["VALUE"].'[]">';
            $html .= $options;
            $html .= '</select>';
        }
        else
        {
            if(end($values) != "" || substr(key($values), 0, 1) != "n")
                $values["n".($max_n+1)] = "";

            $name = $strHTMLControlName["VALUE"]."VALUE";

            $html = '<table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="tb'.md5($name).'">';
            foreach($values as $property_value_id=>$value)
            {
                $html .= '<tr><td>';

                $options = self::GetOptionsHtml($arProperty, array($value));

                $html .= '<select name="'.$strHTMLControlName["VALUE"].'['.$property_value_id.'][VALUE]">';
                $html .= $options;
                $html .= '</select>';

                $html .= '</td></tr>';
            }
            $html .= '</table>';

            $html .= '<input type="button" value="'.Loc::getMessage("HIBLOCK_PROP_DIRECTORY_MORE").'" onclick="if(window.addNewRow){addNewRow(\'tb'.md5($name).'\', -1)}else{addNewTableRow(\'tb'.md5($name).'\', 1, /\[(n)([0-9]*)\]/g, 2)}">';
        }
        return  $html;
    }

    function ConvertToDB($arProperty, $value)
    {
        return $value;
    }

    function ConvertFromDB($arProperty, $value)
    {
        return $value;
    }
}