<?php

namespace Realweb\Site\Property;

use Realweb\Site\Util;

class StringDesc {



    function GetUserTypeDescription() {

        return array(
            'USER_TYPE_ID' => 'uf_multi_string',
            'CLASS_NAME' => __CLASS__,
            'DESCRIPTION' => 'Строка с описанием',
            'BASE_TYPE' => 'string',
        );

    }



    function GetDBColumnType($arUserField) {

        switch (strtolower($GLOBALS['DB']->type)) {

            case 'mysql':

                return 'text';

                break;

        }

    }

    function GetEditFormHTML($arUserField, $arHtmlControl) {

        if ($arUserField["ENTITY_VALUE_ID"] < 1 && strlen($arUserField["SETTINGS"]["DEFAULT_VALUE"]) > 0)
            $arHtmlControl["VALUE"] = $arUserField["SETTINGS"]["DEFAULT_VALUE"];
        $value = unserialize(base64_decode($arHtmlControl["VALUE"]));
        ob_start();

        ?>
        <input type="text" name="<?= $arHtmlControl["NAME"] ?>[]" value="<?= $value[0] ?>">
        <input type="text" name="<?= $arHtmlControl["NAME"] ?>[]" value="<?= $value[1] ?>">
        <?

        $b = ob_get_clean();

        return $b;

    }


    function OnBeforeSave($arUserField, $value) {
        $res = '';
        if ($value[0]) {
            $res = base64_encode(serialize($value));
        } else {
            $res = '';
        }
        return $res;

    }


}

