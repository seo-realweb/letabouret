<?php

namespace Realweb\Site\Property;

class MultiString
{
    function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "multi_string",
            "DESCRIPTION" => "Мулти строка",
            "GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
            "ConvertToDB" => array(__CLASS__, "ConvertToDB"),
            "ConvertFromDB" => array(__CLASS__, "ConvertFromDB"),
        );
    }

    function getParams()
    {
        return array(
            0 => array('NAME' => 'X', 'SIZE' => 10),
            1 => array('NAME' => 'Y', 'SIZE' => 10),
            2 => array('NAME' => 'Ссылка', 'SIZE' => 20),
            3 => array('NAME' => 'Текст', 'SIZE' => 50),
        );
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $html = '<div style="margin: 0 0 5px;">';
        $varName = $strHTMLControlName["VALUE"];
        foreach (self::getParams() as $i => $input) {
            $html .= $input['NAME'] . ' <input type="text" size="' . $input['SIZE'] . '" name="' . $varName . '[' . $i . ']" value="' . $value['VALUE'][$i] . '" /> ';
        }
        $html .= '</div>';
        return $html;
    }

    function ConvertToDB($arProperty, $value)
    {
        $result = [];
        foreach ($value['VALUE'] as $val) {
            if ($val) {
                $result[] = $val;
            }
        }
        if ($result) {
            $value = serialize($result);
        }
        return $value;
    }

    function ConvertFromDB($arProperty, $value)
    {
        $value['VALUE'] = unserialize($value['VALUE']);
        return $value;
    }
}