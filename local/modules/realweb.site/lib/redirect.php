<?php

namespace Realweb\Site;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class RedirectTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_ACTIVE int optional
 * <li> UF_FROM string optional
 * <li> UF_TO string optional
 * <li> UF_ELEMENT_LINK int optional
 * <li> UF_SECTION_LINK int optional
 * </ul>
 *
 * @package Bitrix\Redirect
 **/

class RedirectTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'realweb_redirect';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('REDIRECT_ENTITY_ID_FIELD'),
            ),
            'UF_ACTIVE' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('REDIRECT_ENTITY_UF_ACTIVE_FIELD'),
            ),
            'UF_FROM' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('REDIRECT_ENTITY_UF_FROM_FIELD'),
            ),
            'UF_TO' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('REDIRECT_ENTITY_UF_TO_FIELD'),
            ),
            'UF_ELEMENT_LINK' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('REDIRECT_ENTITY_UF_ELEMENT_LINK_FIELD'),
            ),
            'UF_SECTION_LINK' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('REDIRECT_ENTITY_UF_SECTION_LINK_FIELD'),
            ),
        );
    }
}