<?

namespace Realweb\Site\Action;;

class Error extends \Bitrix\Main\Error
{


    private $line = '';
    private $file = '';
    private $vizibleData = '';

    public function __construct($message, $code = 0)
    {
        parent::__construct($message, $code = 0);
        $this->message = $message;
        $this->code = $code;
        $arError = current(debug_backtrace());
        $this->file = $arError['file'];
        $this->line = $arError['line'];


    }


    public function getVizibleData()
    {
        return $this->vizibleData;
    }

    public function setVizibleData(Array $vizibleData)
    {
        $this->vizibleData = $vizibleData;
    }

    public function addVizibleData($vizibleData, $add = false)
    {
        if (!is_array($vizibleData) || $add) {
            $this->vizibleData[] = $vizibleData;
        } else {
            $this->vizibleData = array_merge($vizibleData, $this->vizibleData);
        }
    }

    public function getInfo()
    {
        return empty($this->vizibleData) ? ($this->getMessage() . " " . $this->file . ":" . $this->line) : $this->getVizibleData();
    }

}
