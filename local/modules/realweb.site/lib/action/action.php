<?

namespace Realweb\Site\Action;

use \Bitrix\Iblock\ElementTable,
    \Bitrix\Main\Application,
    \Bitrix\Main\Context,
    \Bitrix\Main\Loader,
    \Realweb_Favorite_Class,
    \Realweb\Site\EventsTable,
    \Realweb\Site\Message,
    \Realweb\Site\Site,
    \Realweb\Site\User,
    \Realweb\Site\PatientsTable,
    \Bitrix\Main\Diag\Debug,
    \Bitrix\Main\UserTable,
    \Bitrix\Main\UserGroupTable,
    \Realweb\Site\ApplicationsPatientsTable,
    \Realweb\Site\MedicineOrgTable,
    \Realweb\Site\RegionTable,
    \Realweb\Site\UserMedicineOrgTable,
    \Realweb\Site\Util,
    \Bitrix\Main\Entity\ExpressionField,
    \Bitrix\Main\Entity\ReferenceField,
    \Realweb\Site\PropertyTable,
    \Bitrix\Main\DB,
    \Realweb\Site\FormEdcUserTable,
    \Realweb\Site\FormEdcTable,
    \Realweb\Site\UsersSponsorResearchTable,
    \Realweb\Site\ApplicationsDoctorsTable,
    \Realweb\Site\FormEdcBlankTable,
    \Realweb\Site\SpecialtiesTable;

class Action
{

    protected $request;
    protected $files;
    protected $server;

    private $defaultFilePathForAdd = "/upload/default-research/Форма информирования пациента об исследовании и согласие пациента на предоставление сведений и обработку ПД.docx";

    /**
     * Action constructor.
     * @throws \Bitrix\Main\SystemException
     */
    public function __construct()
    {
        $context = Application::getInstance()->getContext();
        $this->request = $context->getRequest();
        $this->server = $context->getServer();
        $this->files = $_FILES;
        if ($this->request['data_object']) {
            $this->request->set(array_merge($this->request->toArray(), \CUtil::JsObjectToPhp($this->request['data_object'])));
        }
    }

    public function __destruct()
    {
        if ($this->request['DO_FUNCTION']) {
            $function = $this->request['DO_FUNCTION'];
            $this->$function();
        }
    }

    /**
     * Установка cookie
     * @return Result
     */
    public function setCookie()
    {
        $res = array('result' => 0);
        if ($this->request->get("name") && $this->request->get("value")) {
            Site::setCookie($this->request->get("name"), $this->request->get("value"), time() + 60 * 60 * 24 * 360);
            $res = array('result' => 1);
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Добавление в избранное
     * @return Result
     * @throws \Bitrix\Main\LoaderException
     */
    public function toggleFavorite()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized()) {
            $ID = $this->request->get("ID");
            $LIST = $this->request->get('LIST');
            if (intval($ID) > 0 && $LIST) {
                Loader::includeModule('realweb.favorite');
                if (!Realweb_Favorite_Class::Add($ID, $LIST)) {
                    Realweb_Favorite_Class::Delete($ID, $LIST);
                    $res = array('result' => -1, 'count' => Realweb_Favorite_Class::GetCount($LIST));
                } else {
                    $res = array('result' => 1, 'count' => Realweb_Favorite_Class::GetCount($LIST));
                }
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Добавление заявки доктора для исследования
     * @return \Bitrix\Main\Entity\AddResult|Result
     * @throws \Exception
     */
    public function addApplicationDoctor()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::isActiveDoctor()) {
            $resultAppDoctor = ApplicationsDoctorsTable::add(array(
                'UF_ELEMENT_ID' => $this->request['ID'],
                'UF_COUNT' => intval($this->request['COUNT'])
            ));
            $resultEvent = EventsTable::add(array(
                'UF_CODE' => 'CONFIRM_RESEARCH_DOCTOR',
                'UF_ELEMENT_ID' => $this->request['ID'],
                'UF_PARAM' => serialize(array('COUNT' => intval($this->request['COUNT'])))
            ));
            if ($resultEvent->getId() > 0 && $resultAppDoctor->getId() > 0) {
                $res = array('result' => 1, 'message' => 'Ваша заявка на участие успешно отправлена');
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Подтвержение/отклонение доктора для участия в исследовании
     * @return Result
     * @throws \Bitrix\Main\LoaderException
     */
    public function addDoctorResearch()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('EDIT_RESEARCH') && $this->request['USER_ID'] && $this->request['ELEMENT_ID']) {
            if (Loader::includeModule("iblock")) {
                $arData = Site::getPropValue('DOCTOR', IBLOCK_CONTENT_RESEARCH, $this->request['ELEMENT_ID']);
                if ($this->request['STATUS']) {
                    $arData[] = $this->request['USER_ID'];
                    $status = 1;
                } else {
                    if (in_array($this->request['USER_ID'], $arData) || (isset($this->request['STATUS']) && $this->request['STATUS'] == 0)) {
                        $status = 0;
                        $arData = array_diff($arData, array($this->request['USER_ID']));
                    } else {
                        $arData[] = $this->request['USER_ID'];
                        $status = 1;
                    }
                }
                $arData = array_unique($arData);
                Site::setPropValue('DOCTOR', $arData, IBLOCK_CONTENT_RESEARCH, $this->request['ELEMENT_ID']);
                ApplicationsDoctorsTable::add(array(
                    'UF_USER' => $this->request['USER_ID'],
                    'UF_ELEMENT_ID' => $this->request['ELEMENT_ID'],
                    'UF_STATUS' => $status
                ), $this->request['DELETE'] === 'Y' ? true : false);
                $arElement = \CIBlockElement::GetByID($this->request['ELEMENT_ID'])->GetNext();
                Message::addMessage(array(
                    'USER_FROM' => $USER->GetID(),
                    'USER_TO' => $this->request['USER_ID'],
                    'THEME' => $arElement['NAME'],
                    'MESSAGE' => "Ваша заявка в исследование   " . ($status ? 'одобрена' : 'отклонена') . ' ' . Site::getDomain() . $arElement['DETAIL_PAGE_URL']
                ));
                if ($this->request['EVENT_ID']) {
                    EventsTable::update($this->request['EVENT_ID'], array('UF_STATUS' => $status, 'UF_VIEW' => 1));
                }
                $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_RESEARCH);
                $res = array('result' => 1, 'btn' => $status ? 'Одобрено' : 'Отклонено', 'status' => $status);
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Изменение события
     * @return Result
     * @throws \Exception
     */
    public function changeEvent()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::isActiveManager() && $this->request['EVENT_ID']) {
            $arFields = array();
            $row = EventsTable::getList(array('filter' => array('ID' => $this->request['EVENT_ID'])))->fetch();
            if ($row) {
                $status = 0;
                if ($this->request['UF_VIEW']) {
                    if ($row['UF_VIEW']) {
                        $status = 0;
                    } else {
                        $status = 1;
                    }
                    $arFields['UF_VIEW'] = $status;
                }
                if ($this->request['UF_STATUS']) {
                    $arFields['UF_STATUS'] = $this->request['UF_STATUS'];
                }
                EventsTable::update($this->request['EVENT_ID'], $arFields);
                $res = array('result' => 1, 'status' => $status);
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Изменение статуса пациента
     * @return Result
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function changeStatusPatient()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('CHANGE_STATUS_PATIENT') && intval($this->request['USER_ID']) > 0 && intval($this->request['ELEMENT_ID']) > 0 && intval($this->request['STATUS_ID']) > 0) {
            $arPatients = User::getPatients(false, $this->request['ELEMENT_ID'], $USER->GetId());
            if ($arPatients[$this->request['USER_ID']]) {
                $date = $this->request['UF_DATETIME'] ? $this->request['UF_DATETIME'] : date('d.m.Y H:i:s');
                $rows = ApplicationsPatientsTable::getList(array(
                    'select' => array("*"),
                    'filter' => array("UF_ELEMENT_ID" => $this->request['ELEMENT_ID'], "UF_USER" => $this->request['USER_ID']),
                    'order' => array("ID" => "DESC")
                ))->fetchAll();
                $arStatus = array_column($rows, null, 'UF_STATUS');
                if ($arStatus[$this->request['STATUS_ID']]) {
                    $res = array('result' => 0, 'message' => 'Ошибка изменения статуса: данный статус уже выставлен');
                } elseif (MakeTimeStamp(current($arStatus)['UF_DATETIME']) <= MakeTimeStamp($date)) {
                    ApplicationsPatientsTable::add(array(
                        "UF_STATUS" => $this->request['STATUS_ID'],
                        "UF_ELEMENT_ID" => $this->request['ELEMENT_ID'],
                        "UF_USER" => $this->request['USER_ID'],
                        "UF_DATETIME" => $date,
                        "UF_DESCRIPTION" => $this->request['UF_DESCRIPTION']
                    ));
                    ApplicationsDoctorsTable::calc(array(
                        "UF_ELEMENT_ID" => $this->request['ELEMENT_ID'],
                        "UF_USER" => $arPatients[$this->request['USER_ID']]['DOCTOR_ID']
                    ));
                    $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_RESEARCH);
                    $CACHE_MANAGER->ClearByTag("user_list");
                    $res = array('result' => 1, 'message' => 'Статус изменен');
                } else {
                    $res = array('result' => 0, 'message' => 'Ошибка изменения статуса: дата введена не корректно');
                }
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Удаление пациента
     * @return Result
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function deletePatient()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('DELETE_PATIENT') && intval($this->request['USER_ID']) > 0 && intval($this->request['ELEMENT_ID']) > 0) {
            if (User::isActiveManager()) {
                if (User::deletePatient(array(
                    'ELEMENT_ID' => $this->request['ELEMENT_ID'],
                    'DELETE_ID' => $this->request['USER_ID']
                ))) {
                    $res = array('result' => 1);
                }
            } else {
                $arPatient = array_intersect($this->request['USER_ID'], array_keys(User::getPatients($USER->GetId(), $this->request['ELEMENT_ID'])));
                if ($arPatient) {
                    if (User::deletePatient(array(
                        'ELEMENT_ID' => $this->request['ELEMENT_ID'],
                        'DELETE_ID' => $arPatient
                    ))) {
                        $res = array('result' => 1);
                    }
                }
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Подтвержение пользователя
     * @return Result
     * @throws \Exception
     */
    public function confirmUser()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::isActiveManager() && $this->request['USER_ID']) {
            $user = new \CUser;
            $user->Update($this->request['USER_ID'], array('UF_ACTIVE' => intval($this->request['STATUS'])));
            if ($this->request['EVENT_ID']) {
                EventsTable::update($this->request['EVENT_ID'], array(
                    'UF_STATUS' => intval($this->request['STATUS']),
                    'UF_VIEW' => 1
                ));
            }
            if ($this->request['STATUS']) {
                $arGroups = \CUser::GetUserGroup($this->request['USER_ID']);
                $message = array(
                    'MESS' => 'Поздравляю! Ваш аккаунт подтвержден. Предлагаем ознакомится со справочными материалами:'
                );
                if (in_array(USER_GROUP_DOCTOR, $arGroups)) {
                    $message['LINK'] = array(
                        Site::getDomain() . '/upload/tutorials/Как направить заявку на участие в исследовании.pdf',
                        Site::getDomain() . '/upload/tutorials/Как направить пациента в исследование.pdf',
                        Site::getDomain() . '/upload/tutorials/Как стать исследователем в наблюдательном исследовании.pdf'
                    );
                } elseif (in_array(USER_GROUP_SPONSOR, $arGroups)) {
                    $message['LINK'] = array(
                        Site::getDomain() . '/upload/tutorials/Как создать или отредактировать исследование.pdf',
                        Site::getDomain() . '/upload/tutorials/Как управлять набором пациентов в условиях аренды enrollme ru.pdf'
                    );
                }
                Message::addMessage(array(
                    'USER_FROM' => $USER->GetID(),
                    'USER_TO' => $this->request['USER_ID'],
                    'THEME' => 'Подтверждение',
                    'MESSAGE' => $message['MESS'],
                    'LINK' => $message['LINK']
                ));
            }
            $res = array('result' => 1, 'btn' => intval($this->request['STATUS']) ? 'Подтвержден' : 'Не подтвержден');
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Включение/отключение медицинского центра для исследования
     * @return Result
     */
    public function switchMedicineOrg()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('ADD_MEDICINE_ORG') && !empty($this->request['XML_ID']) && intval($this->request['ELEMENT_ID']) > 0) {
            $VALUES = Site::getPropValueDesc('MEDICINE_ORG', IBLOCK_CONTENT_RESEARCH, $this->request['ELEMENT_ID']);
            foreach ($VALUES as &$arProp) {
                if ($arProp['VALUE'] == $this->request['XML_ID']) {
                    $arProp['DESCRIPTION'] = $this->request['ACTIVE'];
                }
            };
            if ($VALUES) {
                \CIBlockElement::SetPropertyValuesEx($this->request['ELEMENT_ID'], false, array('MEDICINE_ORG' => $VALUES));
                $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_RESEARCH);
            }
            $res = array('result' => 1, 'message' => "");
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Добавление нового медицинского центра
     * @return Result
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function addMedicineOrg()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('ADD_MEDICINE_ORG') && intval($this->request['ELEMENT_ID']) > 0) {
            if (!empty($this->request['UF_NAME']) && !empty($this->request['UF_CITY']) && !empty($this->request['UF_ADDRESS']) && !empty($this->request['UF_FULL_DESCRIPTION'])) {
                $XML_ID = md5($this->request['UF_NAME']);
                $isExist = MedicineOrgTable::getList(array(
                    'filter' => array('UF_XML_ID' => $XML_ID)
                ))->getSelectedRowsCount();
                if (!$isExist) {
                    $res = MedicineOrgTable::add(array(
                        'UF_NAME' => $this->request['UF_NAME'],
                        'UF_CITY' => $this->request['UF_CITY'],
                        'UF_ADDRESS' => $this->request['UF_ADDRESS'],
                        'UF_FULL_DESCRIPTION' => $this->request['UF_FULL_DESCRIPTION'],
                        'UF_XML_ID' => $XML_ID,
                    ));
                    if ($res->getId()) {
                        $VALUES = Site::getPropValueDesc('MEDICINE_ORG', IBLOCK_CONTENT_RESEARCH, $this->request['ELEMENT_ID']);
                        $VALUES[] = array("VALUE" => $XML_ID, "DESCRIPTION" => "");
                        \CIBlockElement::SetPropertyValuesEx($this->request['ELEMENT_ID'], false, array('MEDICINE_ORG' => $VALUES));
                        $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_RESEARCH);
                        $CACHE_MANAGER->ClearByTag("medicine_org_list");
                        $res = array('result' => 1, 'message' => "Центр добавлен");
                    }
                } else {
                    $res = array('result' => 0, 'message' => "Центр с таким названием уже существует");
                }
            } else {
                $res = array('result' => 0, 'message' => "Заполните все обязательные поля");
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Выбор представителя спонсора для медицинского центра у исследования
     * @return Result
     * @throws \Exception
     */
    public function changeUserSponsorResearch()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('CHANGE_USER_SPONSOR_RESEARCH') && $this->request['ELEMENT_ID'] && $this->request['ID']) {
            if ($this->request['USER_ID']) {
                UsersSponsorResearchTable::add(array(
                    'ELEMENT_ID' => $this->request['ELEMENT_ID'],
                    'MEDICINE_ORG_ID' => $this->request['ID'],
                    'USER_ID' => $this->request['USER_ID'],
                ));
            } else {
                UsersSponsorResearchTable::delete(array(
                    'ELEMENT_ID' => $this->request['ELEMENT_ID'],
                    'MEDICINE_ORG_ID' => $this->request['ID'],
                ));
            }
            $res = array('result' => 1);
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Добавление нового исследователя
     * @return Result
     */
    public function addResearcher()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('ADD_RESEARCHER') && !empty($this->request['ID']) && intval($this->request['ELEMENT_ID']) > 0) {
            if (!empty($this->request['NAME']) && !empty($this->request['EMAIL']) && !empty($this->request['PERSONAL_PHONE'])) {
                $res = User::addResearcher(
                    array(
                        'NAME' => $this->request['NAME'],
                        'EMAIL' => $this->request['EMAIL'],
                        'PERSONAL_PHONE' => $this->request['PERSONAL_PHONE'],
                    ),
                    $this->request['ID'],
                    $this->request['ELEMENT_ID'],
                    array(
                        'MONITOR_NAME' => $this->request['MONITOR_NAME'],
                        'MONITOR_EMAIL' => $this->request['MONITOR_EMAIL'],
                        'MONITOR_PHONE' => $this->request['MONITOR_PHONE'],
                    )
                );
            } else {
                $res = array('result' => 0, 'message' => "Заполните все обязательные поля");
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Добавление форма EDC
     * @return Result
     * @throws \Exception
     */
    public function addEdcForm()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && intval($this->request['ELEMENT_ID']) > 0 && User::getPermission('EDIT_RESEARCH')) {
            if ($this->request['NAME']) {
                $res = FormEdcTable::add(array(
                    'NAME' => $this->request['NAME'],
                    'ELEMENT_ID' => $this->request['ELEMENT_ID'],
                    'PARAMS' => ''
                ));
                if ($id = $res->getId()) {
                    $res = array('result' => 1, 'url' => '/personal/formator/?ID=' . $id);
                }
            } else {
                $res['message'] = 'Введите название формы';
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Сохранение формы EDC
     * @return Result
     * @throws \Exception
     */
    public function saveEdcForm()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && intval($this->request['ID']) > 0 && User::getPermission('EDIT_RESEARCH')) {
            $form = FormEdcTable::getById($this->request['ID'])->fetch();
            if ($form) {
                $arFields = [];
                if ($this->request['SORT']) {
                    $arFields['SORT'] = $this->request['SORT'];
                }
                if ($this->request['PERIOD']) {
                    $arFields['PERIOD'] = $this->request['PERIOD'];
                }
                if (isset($this->request['COPY'])) {
                    $arFields['COPY'] = intval($this->request['COPY']);
                }
                if ($this->request['PARAMS']) {
                    $arFields['PARAMS'] = $this->request['PARAMS'];
                }
                FormEdcTable::update($this->request['ID'], $arFields);
                if ($arFields['PARAMS']) {
                    $rsItem = \CIBlockElement::GetByID($form["ELEMENT_ID"]);
                    if ($item = $rsItem->GetNext()) {
                        $res = array('result' => 1, 'message' => "Форма сохранена", 'url' => $item['DETAIL_PAGE_URL']);
                    }
                } else {
                    $res = array('result' => 1, 'message' => "Форма сохранена");
                }
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Сохранение в файл формы
     * @return Result
     */
    public function saveEdcFormToFile()
    {
        global $USER;
        $res = array('result' => 0);
        if ($this->request['PARAMS']) {
            $ob = json_decode($this->request['PARAMS']);
            if($ob !== null) {
                $file = "/upload/formator/" . rand(0, 100000) . time() . '.json';
                file_put_contents($_SERVER['DOCUMENT_ROOT'] . $file, $this->request['PARAMS']);
                $res = array('result' => 1, 'message' => "JSON сохранен", 'file' => $file);
            } else {
                $res['message'] = 'JSON не валиден';
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    public function openJson()
    {
        $res = array('result' => 0);
        if ($this->files['FILE']) {
            $data = file_get_contents($this->files['FILE']['tmp_name']);
            $ob = json_decode($data, true);
            if($ob !== null) {
                $res = array('result' => 1, 'ob' => $ob);
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Изменение исследования
     * @return Result
     * @throws \Bitrix\Main\LoaderException
     */
    public function changeResearch()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && intval($this->request['ID']) > 0 && User::getPermission('EDIT_RESEARCH')) {
            if (Loader::includeModule("iblock")) {
                $el = new \CIBlockElement();
                $arLoadProductArray = array(
                    "MODIFIED_BY" => $USER->GetID()
                );
                if ($this->request['PREVIEW_TEXT'])
                    $arLoadProductArray['PREVIEW_TEXT'] = $this->request['PREVIEW_TEXT'];
                if ($this->request['DETAIL_TEXT'])
                    $arLoadProductArray['DETAIL_TEXT'] = $this->request['DETAIL_TEXT'];
                if ($this->request['DATE_ACTIVE_FROM'])
                    $arLoadProductArray['DATE_ACTIVE_FROM'] = $this->request['DATE_ACTIVE_FROM'];
                if ($this->request['DATE_ACTIVE_TO'])
                    $arLoadProductArray['DATE_ACTIVE_TO'] = $this->request['DATE_ACTIVE_TO'];
                if ($this->request['DATE_CREATE'])
                    $arLoadProductArray['DATE_CREATE'] = $this->request['DATE_CREATE'];
                if ($el->Update($this->request['ID'], $arLoadProductArray)) {
                    if ($this->request['PROPS']) {
                        foreach ($this->request['PROPS'] as $key => $val) {
                            \CIBlockElement::SetPropertyValuesEx($this->request['ID'], false, array($key => $val ? $val : false));
                        }
                    }
                    if (!User::isActiveManager()) {
                        \CIBlockElement::SetPropertyValuesEx($this->request['ID'], false, array('CONFIRM' => false));
                        EventsTable::add(array(
                            'UF_CODE' => 'CHANGE_RESEARCH',
                            'UF_ELEMENT_ID' => $this->request['ID'],
                            'UF_PARAM' => ''
                        ));
                    }
                    $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_RESEARCH);
                    $res = array('result' => 1, 'message' => 'Данные успешно изменены');
                }
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Удаление формы EDC
     * @return Result
     * @throws \Exception
     */
    public function deleteEdcForm()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && intval($this->request['ID']) > 0 && User::getPermission('EDIT_RESEARCH')) {
            FormEdcTable::delete($this->request['ID']);
            $res = array('result' => 1, 'message' => "Форма удалена");
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Сохранение сессии
     * @return Result
     */
    public function saveSession()
    {
        $res = array('result' => 0);
        if ($this->request['SESSION_NAME'] && $this->request['SESSION_VALUE']) {
            $_SESSION[$this->request['SESSION_NAME']] = $this->request['SESSION_VALUE'];
            $res = array('result' => 1);
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Сохранение заготовки
     * @return Result
     * @throws \Exception
     */
    public function saveFormEdcBlank()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && $this->request['NAME'] && $this->request['PARAMS'] && User::getPermission('EDIT_RESEARCH')) {
            $elementId = null;
            if ($this->request['ID']) {
                if ($row = FormEdcTable::getList(array(
                    'filter' => array('ID' => $this->request['ID'])
                ))->fetch()) {
                    $elementId = $row['ELEMENT_ID'];
                }
            }
            FormEdcBlankTable::add(array(
                'NAME' => $this->request['NAME'],
                'PARAMS' => $this->request['PARAMS'],
                'ELEMENT_ID' => $elementId
            ));
            $res = array('result' => 1, 'message' => "Заготовка сохранена");
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Удаление заготовки
     * @return Result
     * @throws \Exception
     */
    public function deleteFormEdcBlank()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && intval($this->request['ID']) > 0 && User::getPermission('EDIT_RESEARCH')) {
            FormEdcBlankTable::delete($this->request['ID']);
            $res = array('result' => 1, 'message' => "Заготовка удалена");
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Изменение свойств исследования
     * @return Result
     * @throws \Bitrix\Main\LoaderException
     */
    public function changePropResearch()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && intval($this->request['ID']) > 0 && User::getPermission('EDIT_RESEARCH')) {
            if (Loader::includeModule("iblock")) {
                if ($this->request['PROPS']) {
                    foreach ($this->request['PROPS'] as $key => $val) {
                        \CIBlockElement::SetPropertyValuesEx($this->request['ID'], false, array($key => $val ? $val : false));
                    }
                    $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_RESEARCH);
                }
                $res = array('result' => 1, 'message' => 'Изменения сохранены');
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Отправка сообщения
     * @return \Bitrix\Main\Entity\AddResult|Result
     */
    public function sendMessage()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && intval($this->request['UF_USER_TO']) > 0) {
            if (is_array($this->request['UF_MESSAGE'])) {
                $message = implode("\n", $this->request['UF_MESSAGE']);
            } else {
                $message = $this->request['UF_MESSAGE'];
            }
            if (Message::addMessage(array(
                'USER_FROM' => $USER->GetID(),
                'USER_TO' => $this->request['UF_USER_TO'],
                'THEME' => $this->request['UF_THEME'],
                'MESSAGE' => strip_tags($message),
                'UF_PARENT' => $this->request['UF_PARENT']
            ))) {
                $res = array('result' => 1, 'message' => "Сообщение успешно отправлено");
            } else {
                $res = array('result' => 0, 'message' => "Сообщение не отправлено");
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Удаление исследования и всех данных
     * @return Result
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\Db\SqlQueryException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function deleteResearch()
    {
        global $USER, $DB;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && intval($this->request['ID']) > 0 && User::getPermission('DELETE_RESEARCH')) {
            if (Loader::includeModule("iblock")) {
                //if (\CIBlock::GetPermission(IBLOCK_CONTENT_RESEARCH) >= 'W') {
                    $DB->StartTransaction();
                    if (!\CIBlockElement::Delete($this->request['ID'])) {
                        $DB->Rollback();
                        $res['message'] = 'Ошибка удаления';
                    } else {
                        $DB->Commit();
                        $connection = Application::getConnection();
                        $connection->queryExecute("DELETE FROM " . ApplicationsPatientsTable::getTableName() . " WHERE UF_ELEMENT_ID=" . $this->request['ID']);
                        $connection->queryExecute("DELETE FROM " . ApplicationsDoctorsTable::getTableName() . " WHERE UF_ELEMENT_ID=" . $this->request['ID']);
                        $connection->queryExecute("DELETE FROM " . EventsTable::getTableName() . " WHERE UF_ELEMENT_ID=" . $this->request['ID']);
                        $connection->queryExecute("DELETE FROM " . UserMedicineOrgTable::getTableName() . " WHERE ELEMENT_ID=" . $this->request['ID']);
                        $connection->queryExecute("DELETE FROM " . UsersSponsorResearchTable::getTableName() . " WHERE ELEMENT_ID=" . $this->request['ID']);
                        $ids = array_column(FormEdcTable::getList(array('ELEMENT_ID' => $this->request['ID']))->fetchAll(), "ID", null);
                        $connection->queryExecute("DELETE FROM " . FormEdcTable::getTableName() . " WHERE ELEMENT_ID=" . $this->request['ID']);
                        $connection->queryExecute("DELETE FROM " . FormEdcUserTable::getTableName() . " WHERE FORM_ID=" . $ids);
                        $res = array('result' => 1, 'url' => '/personal/');
                    }
                //}
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Удаление пользователя
     * @return Result
     */
    public function deleteUser()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::isActiveManager() && intval($this->request['USER_ID']) > 0) {
            if (\CUser::Delete($this->request['USER_ID'])) {
                if (\CUser::Delete($this->request['ID'])) {
                    $res = array('result' => 1, 'message' => "Пользователь удален", "url" => '/personal/');
                } else {
                    $res['message'] = 'Ошибка удаления';
                }
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Добавление документов
     * @return Result
     */
    public function addDocs()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('ADD_DOCS') && !empty($this->files['FILES'])) {
            $el = new \CIBlockElement();
            $arElements = [];
            foreach ($this->files['FILES'] as $fileKey => $arFiles) {
                foreach ($arFiles as $key => $value) {
                    $arElements[$key][$fileKey] = $value;
                }
            }
            $props = [];
            if (!User::isManager()) {
                $props['ONLY_CURRENT_USER'] = Site::getPropsEnumValue("ONLY_CURRENT_USER", "Y", IBLOCK_CONTENT_DOCS);
            }
            foreach ($arElements as $element) {
                $props['FILE'] = $element;
                $arLoadProductArray = Array(
                    "MODIFIED_BY" => $USER->GetID(),
                    "IBLOCK_SECTION_ID" => $this->request['ELEMENT_ID'] ? $this->request['ELEMENT_ID'] : false,
                    "IBLOCK_ID" => IBLOCK_CONTENT_DOCS,
                    "PROPERTY_VALUES" => $props,
                    "NAME" => $element["name"],
                    "ACTIVE" => "Y",
                );
                if ($PRODUCT_ID = $el->Add($arLoadProductArray))
                    $res = array('result' => 1, 'message' => "Документы добавлены");
                else
                    $res = array('result' => 0, 'message' => $el->LAST_ERROR);
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }


    /**
     * Удаление документа
     * @return Result
     */
    public function deleteDocs()
    {
        global $USER, $DB;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('ADD_DOCS') && !empty($this->request['ELEMENT_ID'])) {
            $DB->StartTransaction();
            if (!\CIBlockElement::Delete($this->request['ELEMENT_ID'])) {
                $DB->Rollback();
                $res['message'] = 'Ошибка удаления';
            } else {
                $DB->Commit();
                $res = array('result' => 1, 'message' => "Документ удален");
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }


    /**
     * Загрузка и удаление документов для исследования
     * @return Result
     */
    public function downloadDocs()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('EDIT_RESEARCH') && intval($this->request['ELEMENT_ID']) > 0) {
            $FILES = array();
            $rs = \CIBlockElement::GetProperty(IBLOCK_CONTENT_RESEARCH, $this->request['ELEMENT_ID'], array(), ['CODE' => 'DOCS']);
            while ($arFileProp = $rs->Fetch()) {
                if (!empty($this->request['DEL_ID']) && $this->request['DEL_ID'] == $arFileProp['VALUE']) {
                    \CFile::Delete($arFileProp['VALUE']);
                    continue;
                }
                $FILES[] = [
                    'VALUE' => \CFile::MakeFileArray($arFileProp['VALUE']),
                    'DESCRIPTION' => $arFileProp['DESCRIPTION'],
                ];
            };
            $newFiles = [];
            if (!empty($this->files['FILES'])) {
                foreach ($this->files['FILES'] as $fileKey => $arFiles) {
                    foreach ($arFiles as $key => $value) {
                        $newFiles[$key][$fileKey] = $value;
                    }
                }
                foreach ($newFiles as $file) {
                    $FILES[] = array("VALUE" => \CFile::MakeFileArray(\CFile::SaveFile($file, '/iblock/')), "DESCRIPTION" => "");
                }
            }

            \CIBlockElement::SetPropertyValuesEx($this->request['ELEMENT_ID'], false, array('DOCS' => $FILES));
            $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_RESEARCH);
            $res = array('result' => 1, 'message' => "Документы добавлены");
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Отправка сообщения докторам
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function sendMessageToDoctor()
    {
        global $USER;
        $arUsers = [];
        $arFilter = array(
            'UF_ACTIVE' => 1,
            'ACTIVE' => 'Y'
        );
        if ($this->request['PROPS']['ACCESS_SPECIALS']) {
            $arFilter['UF_SPECIALITY'] = array_column(SpecialtiesTable::getList(array(
                'select' => array('ID'),
                'filter' => array('UF_XML_ID' => $this->request['PROPS']['ACCESS_SPECIALS'])
            ))->fetchAll(), 'ID', null);
        }
        if ($this->request['PROPS']['ACCESS_REGION']) {
            $arFilter['PERSONAL_CITY'] = array_column(RegionTable::getList(array(
                'select' => array('UF_NAME'),
                'filter' => array('UF_XML_ID' => $this->request['PROPS']['ACCESS_REGION'])
            ))->fetchAll(), 'UF_NAME', null);
            if ($arFilter['PERSONAL_CITY']) {
                $arFilter['PERSONAL_CITY'] = implode(" | ", $arFilter['PERSONAL_CITY']);
            }
        }
        if ($this->request['SEND_ONLY_USER_INCLUDE_RESEARCH']) {
            $ids = Site::getPropValue('DOCTOR', IBLOCK_CONTENT_RESEARCH, $this->request['ID']);
            if ($ids) {
                $arFilter['ID'] = implode(" | ", $ids);
            }
        }
        if ($arFilter) {
            $userBy = "id";
            $userOrder = "desc";
            $userFilter = $arFilter;
            $userParams = array(
                'SELECT' => array("ID"),
            );
            $rsUsers = \CUser::GetList($userBy, $userOrder, $userFilter, $userParams);
            while ($arUser = $rsUsers->Fetch()) {
                $arUsers[] = $arUser['ID'];
            }
            if ($arUsers) {
                $arElement = \CIBlockElement::GetByID($this->request['ID'])->GetNext();
                $message = $this->request['MESSAGE'] ? $this->request['MESSAGE'] : ("Запрос предложений по исследованию " . $arElement['NAME'] . ' открыт ');
                foreach ($arUsers as $id) {
                    Message::addMessage(array(
                        'USER_FROM' => $USER->GetID(),
                        'USER_TO' => $id,
                        'THEME' => $arElement['NAME'],
                        'MESSAGE' => $message . ' ' . Site::getDomain() . $arElement['DETAIL_PAGE_URL']
                    ));
                }
            }
        }
    }

    /**
     * Переключение на исследователя
     * @return Result
     */
    public function changeDoctorGroup()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && intval($this->request['GROUP_ID']) > 0) {
            if (in_array($this->request['GROUP_ID'], array(USER_GROUP_DOCTOR, USER_GROUP_RESEARCHER))) {
                $arGroups = array_diff($USER->GetUserGroupArray(), array(USER_GROUP_DOCTOR, USER_GROUP_RESEARCHER));
                $arGroups[] = $this->request['GROUP_ID'];
                $USER->SetUserGroupArray($arGroups);
                $res = array('result' => 1, 'url' => '/personal/');
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Удаление представителя
     * @return Result
     */
    public function deleteUserAgent()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::isSponsor() && intval($this->request['ID']) > 0) {
            $arAgent = User::getUsersSponsor($USER->GetID());
            if ($arAgent[$this->request['ID']]) {
                if (\CUser::Delete($this->request['ID'])) {
                    $res = array('result' => 1, 'message' => "Представитель удален");
                } else {
                    $res['message'] = 'Ошибка удаления представителя';
                }
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Изменение данных
     * @return Result
     */
    public function changeUserData()
    {
        global $USER;
        $res = array('result' => 0);
        if ((User::isActiveManager() || $USER->GetID() == $this->request['USER_ID']) && intval($this->request['USER_ID']) > 0) {
            $fields = array();
            if ($this->request['FIELDS']) {
                $fields = $this->request['FIELDS'];
            }
            if (isset($this->request['UF_SMS'])) {
                $fields['UF_SMS'] = intval($this->request['UF_SMS']);
            }
            $user = new \CUser();
            if ($user->Update($this->request['USER_ID'], $fields)) {
                $res = array('result' => 1, 'message' => 'Данные изменены');
            } else {
                $res['message'] = 'Ошибка изменения';
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    public function changePermissionResearch()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && intval($this->request['ID']) > 0 && User::getPermission('EDIT_RESEARCH')) {
            if (Loader::includeModule("iblock")) {
                $el = new \CIBlockElement;
                $arLoadProductArray = array(
                    "RIGHTS" => array()
                );
                if ($el->Update($this->request['ID'], $arLoadProductArray)) {
                    \CIBlockElement::SetPropertyValuesEx($this->request['ID'], false, array('CONFIRM' => Site::getPropsEnumValue("CONFIRM", "Y", IBLOCK_CONTENT_RESEARCH)));
                    $res = array('result' => 1);
                }
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    public function activeResearch()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && intval($this->request['ID']) > 0 && User::getPermission('EDIT_RESEARCH')) {
            if (Loader::includeModule("iblock")) {
                $sponsorId = current(Site::getPropValue('SPONSOR', IBLOCK_CONTENT_RESEARCH, $this->request['ID']));
                $message = [];
                switch ($this->request['TYPE']) {
                    case "CONFIRM":
                        \CIBlockElement::SetPropertyValuesEx($this->request['ID'], false, array('CONFIRM' => Site::getPropsEnumValue("CONFIRM", "Y", IBLOCK_CONTENT_RESEARCH)));
                        \CIBlockElement::SetPropertyValuesEx($this->request['ID'], false, array('MODERATE' => Site::getPropsEnumValue("MODERATE", "CONFIRM", IBLOCK_CONTENT_RESEARCH)));
                        $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_RESEARCH);
                        $message['THEME'] = 'Редактирование исследования';
                        $message['MESSAGE'] = 'Вам разрешено редактировать исследование';
                        break;
                    case "APPROVED":
                        \CIBlockElement::SetPropertyValuesEx($this->request['ID'], false, array('CONFIRM' => Site::getPropsEnumValue("CONFIRM", "Y", IBLOCK_CONTENT_RESEARCH)));
                        \CIBlockElement::SetPropertyValuesEx($this->request['ID'], false, array('MODERATE' => Site::getPropsEnumValue("MODERATE", "APPROVED", IBLOCK_CONTENT_RESEARCH)));
                        $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_RESEARCH);
                        $message['THEME'] = 'Исследование одобрено';
                        $message['MESSAGE'] = 'Ваше исследование одобрено, внесите дополнительные данные.';
                        break;
                    case "ACTIVE":
                        \CIBlockElement::SetPropertyValuesEx($this->request['ID'], false, array('CONFIRM' => false));
                        \CIBlockElement::SetPropertyValuesEx($this->request['ID'], false, array('MODERATE' => Site::getPropsEnumValue("MODERATE", "ACTIVE", IBLOCK_CONTENT_RESEARCH)));
                        $el = new \CIBlockElement;
                        $arLoadProductArray = array(
                            "ACTIVE" => "Y"
                        );
                        $el->Update($this->request['ID'], $arLoadProductArray);
                        $message['THEME'] = 'Подтверждение исследования';
                        $message['MESSAGE'] = 'Ваше исследование подтверждено';
                        break;
                }
                if ($message) {
                    $rsResearch = \CIBlockElement::GetByID($this->request['ID']);
                    if ($research = $rsResearch->GetNext()) {
                        Message::addMessage(array(
                            'USER_FROM' => $USER->GetID(),
                            'USER_TO' => $sponsorId,
                            'THEME' => $message['THEME'],
                            'MESSAGE' => $message['MESSAGE'] . ' ' . Site::getDomain() . $research['DETAIL_PAGE_URL']
                        ));
                        $res = array('result' => 1);
                    }
                }
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    public function deleteDoctor()
    {
        global $USER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('DELETE_DOCTOR') && intval($this->request['USER_ID']) > 0 && intval($this->request['ELEMENT_ID']) > 0) {
            if (User::deleteDoctor(array(
                'ELEMENT_ID' => $this->request['ELEMENT_ID'],
                'DELETE_ID' => $this->request['USER_ID']
            ))) {
                $res = array('result' => 1);
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /*
    public function addResearch() // функция не используется
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission('ADD_RESEARCH') && $this->request['PROPS']) {
            $ERROR = array();
            $PROPS = $this->request['PROPS'];
            $PROPS['SPONSOR'] = $USER->GetID();
            $medOrg = $this->request['MEDICINE_ORG'];
            $regions = RegionTable::getElements();
            $regions = array_column($regions, null, "UF_NAME");
            foreach ($medOrg as &$arItem) {
                if ($arItem['UF_NAME'] && $arItem['UF_CITY'] && $arItem['UF_ADDRESS'] && $arItem['UF_FULL_DESCRIPTION']) {
                    //$XML_ID = md5(serialize(array($arItem['UF_NAME'], $arItem['UF_CITY'], $arItem['UF_ADDRESS'], $arItem['UF_FULL_DESCRIPTION'])));
                    $XML_ID = md5($arItem['UF_NAME']);
                    $CITY = $regions[$arItem['UF_CITY']];
                    if (!$CITY) {
                        $CITY_XML_ID = md5($arItem['UF_CITY']);
                        $rsRegion = RegionTable::add(array(
                            "UF_NAME" => $arItem['UF_CITY'],
                            "UF_XML_ID" => $CITY_XML_ID
                        ));
                        $CACHE_MANAGER->ClearByTag("region_list");
                        $CITY_ID = $rsRegion->getId();
                    } else {
                        $CITY_XML_ID = $CITY['UF_XML_ID'];
                        $CITY_ID = $CITY['ID'];
                    }
                    //$PROPS['REGION'][] = $CITY_XML_ID;
                    $row = MedicineOrgTable::getList(array(
                        "select" => array("ID", "UF_XML_ID", "UF_CITY"),
                        "filter" => array("UF_XML_ID" => $XML_ID)
                    ))->fetch();
                    if ($row) {
                        $PROPS['MEDICINE_ORG'][] = $row['UF_XML_ID'];
                        $MEDICINE_ORG_ID = $row['ID'];
                    } else {
                        $resMedOrg = MedicineOrgTable::add(array(
                            'UF_NAME' => $arItem['UF_NAME'],
                            'UF_CITY' => $CITY_ID,
                            'UF_ADDRESS' => $arItem['UF_ADDRESS'],
                            'UF_FULL_DESCRIPTION' => $arItem['UF_FULL_DESCRIPTION'],
                            'UF_XML_ID' => $XML_ID,
                        ));
                        $CACHE_MANAGER->ClearByTag("medicine_org_list");
                        if ($MEDICINE_ORG_ID = $resMedOrg->getId()) {
                            $PROPS['MEDICINE_ORG'][] = $XML_ID;
                        }
                    }
                    if ($MEDICINE_ORG_ID) {
                        $arItem['MEDICINE_ORG_ID'] = $MEDICINE_ORG_ID;
                        if ($arItem['NAME'] && $arItem['EMAIL'] && $arItem['PERSONAL_PHONE']) {
                            $resUser = User::addResearcher(array(
                                'NAME' => $arItem['NAME'],
                                'EMAIL' => $arItem['EMAIL'],
                                'PERSONAL_PHONE' => $arItem['PERSONAL_PHONE'],
                            ));
                            if ($resUser['result'] == 1) {
                                $arItem['USER_ID'] = $resUser['USER_ID'];
                            } else {
                                $ERROR[] = "Ошибка добавления пользователя «" . $arItem['NAME'] . "» :" . $resUser['message'];
                            }
                        }
                    } else {
                        $ERROR[] = "Ошибка добавления медицинского центра «" . $arItem['UF_NAME'] . "»";
                    }
                }
            }
            if (!$ERROR) {
                $strFilesCode = 'DOCS';
                foreach ($this->files['FILES'] as $fileKey => $arData) {
                    foreach ($arData as $code => $arFiles) {
                        if (!$strFilesCode) $strFilesCode = $code;
                        foreach ($arFiles as $key => $value) {
                            $PROPS[$code][$key][$fileKey] = $value;
                        }
                    }
                }
                $isResearchExists = false;
                if (intval($this->request['ID']) > 0) {
                    $rsResult = \Bitrix\Iblock\ElementTable::getList([
                        'select' => ['ID'],
                        'filter' => [
                            'IBLOCK_ID' => IBLOCK_CONTENT_RESEARCH,
                            'ID' => $this->request['ID']
                        ]
                    ]);
                    if ($row = $rsResult->fetch()) {
                        $isResearchExists = true;
                    }
                }
                $name = $this->request['PROPS']['PROTOCOL_NUMBER'] . " " . $this->request['PROPS']['DRUG'];
                $el = new \CIBlockElement;
                $arLoadProductArray = Array(
                    "MODIFIED_BY" => $USER->GetID(),
                    "DATE_ACTIVE_FROM" => $this->request['DATE_FROM'],
                    "DATE_ACTIVE_TO" => $this->request['DATE_TO'],
                    "IBLOCK_SECTION_ID" => false,
                    "IBLOCK_ID" => IBLOCK_CONTENT_RESEARCH,
                    "PROPERTY_VALUES" => $PROPS,
                    "NAME" => $name,
                    "CODE" => Site::transliterate($name . "-" . date("d-m-Y")),
                    "ACTIVE" => "N",
                    "PREVIEW_TEXT" => $this->request['PREVIEW_TEXT'],
                    "DETAIL_TEXT" => $this->request['DETAIL_TEXT'],
//                    'RIGHTS' => array(
//                        "n0" => array("GROUP_CODE" => "G2", "DO_CLEAN" => "N", "TASK_ID" => 33),
//                        "n1" => array("GROUP_CODE" => "G" . USER_GROUP_MANAGER, "DO_CLEAN" => "N", "TASK_ID" => 40),
//                        "n2" => array("GROUP_CODE" => "G" . USER_GROUP_SPONSOR, "DO_CLEAN" => "N", "TASK_ID" => 40)
//                    ),
                );
                $ELEMENT_ID = 0;
                if ($isResearchExists) {
                    if ($el->Update($this->request['ID'], $arLoadProductArray)) {
                        $ELEMENT_ID = $this->request['ID'];
                        $res = array("result" => 1, "message" => "Исследование обновлено");
                    }
                } else {
                    //добавление файла по умолчанию
                    $defaultFilePath = $_SERVER["DOCUMENT_ROOT"] . $this->defaultFilePathForAdd;
                    if (file_exists($defaultFilePath)) {
                        $arLoadProductArray["PROPERTY_VALUES"]['DOCS'][] = \CFile::MakeFileArray($defaultFilePath);
                    }
                    //end
                    $ELEMENT_ID = $el->Add($arLoadProductArray);
                    $res = array("result" => 1, "message" => "Исследование добавлено");
                    if ($arElement = \CIBlockElement::GetByID($ELEMENT_ID)->GetNext()) {
                        $res["url"] = $arElement["DETAIL_PAGE_URL"];
                    }
                }
                if (intval($ELEMENT_ID) > 0) {
                    foreach ($medOrg as $arItem) {
                        if ($arItem['MEDICINE_ORG_ID'] && $arItem['USER_ID']) {
                            User::addResearcher(array("USER_ID" => $arItem['USER_ID']), $arItem['MEDICINE_ORG_ID'], $ELEMENT_ID);
                        }
                    }
                    EventsTable::add(array(
                        'UF_CODE' => 'NEW_RESEARCH',
                        'UF_ELEMENT_ID' => $ELEMENT_ID,
                        'UF_PARAM' => ""
                    ));
                } else {
                    $res = array("result" => 0, "message" => $el->LAST_ERROR);
                }

            } else {
                $res = array("result" => 0, "message" => implode("<br>", $ERROR));
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }
    */

    public function generateReportIrk()
    {
        global $USER;
        if ($USER->IsAuthorized() && User::isActiveManager()) {

            $out = fopen('php://output', 'w');

            $arItems = FormEdcUserTable::getList(array(
                'select' => array(
                    "FORM_ID",
                    "FIO_FULL",
                    "FORM_DATA",
                    "PERSONAL_PHONE" => "USER.PERSONAL_PHONE",
                    "EMAIL" => "USER.EMAIL",
                    "FORM_NAME" => "FORM.NAME",
                    "FORM_PARAMS" => "FORM.PARAMS",
                    "NAME" => "USER.NAME",
                    "SECOND_NAME" => "USER.SECOND_NAME",
                    "LAST_NAME" => "USER.LAST_NAME",
                    "PASSPORT" => "USER_FIELD.UF_PASSPORT",
                ),
                'runtime' => array(
                    new ReferenceField(
                        'FORM',
                        '\Realweb\Site\FormEdcTable',
                        array('=this.FORM_ID' => 'ref.ID'),
                        array('join_type' => 'INNER')
                    ),
                    new ReferenceField(
                        'USER',
                        '\Bitrix\Main\UserTable',
                        array('=this.USER_ID' => 'ref.ID'),
                        array('join_type' => 'INNER')
                    ),
                    new ReferenceField(
                        'USER_FIELD',
                        '\Realweb\Site\UserFieldTable',
                        array('=ref.VALUE_ID' => 'this.ID'),
                        array('join_type' => 'LEFT')
                    ),
                    new ExpressionField("FIO_FULL", "CONCAT(%s, ' ', %s, ' ', %s)", array('USER.LAST_NAME', 'USER.NAME', 'USER.SECOND_NAME'))
                ),
                'order' => array('FORM_ID' => 'ASC')
            ))->fetchAll();
            $arData = [];
            $curForm = 0;

            foreach ($arItems as $item) {
                if ($curForm != $item['FORM_ID']) {
                    $arData[] = array($item['FORM_NAME']);
                    $formData = FormEdcUserTable::parseFormData(json_decode($item['FORM_PARAMS'], tue), unserialize($item['FORM_DATA']));
                    $formHead = [];
                    foreach ($formData as $data) {
                        if ($data['NAME']) {
                            $formHead[] = $data['NAME'];
                        } else {
                            foreach ($data as $val) {
                                $formHead[] = $val['NAME'];
                            }
                        }
                    }
                    $arData[] = array_merge(array(
                        'ФИО',
                        'Телефон',
                        'Email',
                        'Паспортные данные'
                    ), $formHead);
                } else {
                    $formData = FormEdcUserTable::parseFormData(json_decode($item['FORM_PARAMS'], tue), unserialize($item['FORM_DATA']));
                }
                $formValues = [];
                foreach ($formData as $data) {
                    if ($data['NAME']) {
                        $formValues[] = $data['VALUE'];
                    } else {
                        foreach ($data as $val) {
                            $formValues[] = $val['VALUE'];
                        }
                    }
                }
                $arData[] = array_merge(array(
                    $item['FIO_FULL'],
                    $item['PERSONAL_PHONE'],
                    $item['EMAIL'],
                    $item['PASSPORT']
                ), $formValues);
            }
            foreach ($arData as $item) {
                foreach ($item as &$val) $val = iconv("UTF-8", "Windows-1251", $val);
                fputcsv($out, $item, ";");
            }

            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=irk.csv");
            header("Pragma: no-cache");
            header("Expires: 0");

            fclose($out);

            exit;
        } else {
            $result = new Result();
            $result->addVizibleData(array("result" => 0));
            return $result;
        }
    }

    /**
     * Отчет по пользователям
     * @return Result
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function generateReport()
    {
        global $USER;
        if ($USER->IsAuthorized() && User::isActiveManager()) {
            $arPatient = [];
            $arFilter = [];
            if ($this->request['q']) {
                $arFilter['ELEMENT_NAME'] = "%" . $this->request['q'] . "%";
            }
            if ($this->request['PERSONAL_CITY']) {
                $arFilter['CITY_NAME'] = $this->request['PERSONAL_CITY'];
            }
            if (!$this->request['GROUP_ID'] || ($this->request['GROUP_ID'] && $this->request['GROUP_ID'] == USER_GROUP_PATIENT)) {
                $arPatient = array(
                    array("Пациенты"),
                    array("Исследование", "Город", "Центр", "Статус", "Количество")
                );
                $arPatient = array_merge($arPatient, PatientsTable::getList(array(
                    "select" => array("ELEMENT_NAME" => "ELEMENT.NAME", "CITY_NAME" => "REGION.UF_NAME", "MEDICINE_ORG_NAME" => "MEDICINE_ORG.UF_NAME", "STATUS_VALUE" => "STATUS.VALUE", "COUNT_USER"),
                    "filter" => $arFilter,
                    "group" => array("ELEMENT_NAME", "MEDICINE_ORG_NAME", "CITY_NAME", "STATUS_VALUE"),
                    "runtime" => array(
                        new ReferenceField(
                            'ELEMENT',
                            '\Bitrix\Iblock\ElementTable',
                            array('=ref.ID' => 'this.ELEMENT_ID'),
                            array('join_type' => 'INNER')
                        ),
                        new ReferenceField(
                            'MEDICINE_ORG',
                            '\Realweb\Site\MedicineOrgTable',
                            array('=ref.ID' => 'this.MEDICINE_ORG_ID'),
                            array('join_type' => 'INNER')
                        ),
                        new ReferenceField(
                            'REGION',
                            '\Realweb\Site\RegionTable',
                            array('=ref.ID' => 'this.MEDICINE_ORG.UF_CITY'),
                            array('join_type' => 'INNER')
                        ),
                        new ReferenceField(
                            'APPLICATION',
                            '\Realweb\Site\ApplicationsPatientsTable',
                            array('=ref.UF_USER' => 'this.PATIENT_ID'),
                            array('join_type' => 'LEFT')
                        ),
                        new ReferenceField(
                            'STATUS',
                            '\Realweb\Site\UserFieldEnumTable',
                            array('=ref.ID' => 'this.APPLICATION.UF_STATUS'),
                            array('join_type' => 'LEFT')
                        ),
                        new ExpressionField('COUNT_USER', 'COUNT(PATIENT_ID)'),
                    )
                ))->fetchAll());
            }
            $arDoctor = [];
            if (!$this->request['GROUP_ID'] || ($this->request['GROUP_ID'] && $this->request['GROUP_ID'] == USER_GROUP_DOCTOR)) {
                $arDoctor = array(
                    array(""),
                    array("Докторы"),
                    array("Исследование", "Город", "Количество")
                );
                $arDoctor = array_merge($arDoctor, PatientsTable::getList(array(
                    "select" => array("ELEMENT_NAME" => "ELEMENT.NAME", "CITY_NAME" => "REGION.UF_NAME", "COUNT_USER"),
                    "filter" => $arFilter,
                    "group" => array("ELEMENT_NAME", "CITY_NAME"),
                    "runtime" => array(
                        new ReferenceField(
                            'ELEMENT',
                            '\Bitrix\Iblock\ElementTable',
                            array('=ref.ID' => 'this.ELEMENT_ID'),
                            array('join_type' => 'INNER')
                        ),
                        new ReferenceField(
                            'MEDICINE_ORG',
                            '\Realweb\Site\MedicineOrgTable',
                            array('=ref.ID' => 'this.MEDICINE_ORG_ID'),
                            array('join_type' => 'INNER')
                        ),
                        new ReferenceField(
                            'REGION',
                            '\Realweb\Site\RegionTable',
                            array('=ref.ID' => 'this.MEDICINE_ORG.UF_CITY'),
                            array('join_type' => 'INNER')
                        ),
                        new ExpressionField('COUNT_USER', 'COUNT(DISTINCT DOCTOR_ID)'),
                    )
                ))->fetchAll());
            }
            $arSponsor = [];
            if (!$this->request['GROUP_ID'] || ($this->request['GROUP_ID'] && $this->request['GROUP_ID'] == USER_GROUP_SPONSOR)) {
                $arSponsor = array(
                    array(""),
                    array("Спонсоры"),
                    array("Город", "Количество")
                );
                $arRegions = RegionTable::getElements();
                $arRegions = array_column($arRegions, null, "UF_XML_ID");
                $arFilter = array("IBLOCK_ID" => IBLOCK_CONTENT_RESEARCH, "!PROPERTY_SPONSOR" => false);
                if ($this->request['q']) {
                    $arFilter['NAME'] = "%" . $this->request['q'] . "%";
                }
                if ($this->request['PERSONAL_CITY']) {
                    $region = RegionTable::getList(array('filter' => array('UF_NAME' => $this->request['PERSONAL_CITY'])))->fetchAll();
                    $region = array_column($region, "UF_XML_ID", null);
                    $arFilter['PROPERTY_REGION'] = $region;
                }
                $rsResult = \CIBlockElement::GetList(array(), $arFilter, array("PROPERTY_SPONSOR", "PROPERTY_REGION"));
                while ($arFields = $rsResult->GetNext()) {
                    $arSponsor[] = array(
                        'CITY' => $arRegions[$arFields['PROPERTY_REGION_VALUE']]['UF_NAME'],
                        'COUNT' => $arFields['CNT']
                    );
                }
            }

            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=report.csv");
            header("Pragma: no-cache");
            header("Expires: 0");

            $out = fopen('php://output', 'w');

            foreach ($arPatient as $item) {
                foreach ($item as &$val) $val = iconv("UTF-8", "Windows-1251", $val);
                fputcsv($out, $item, ";");
            }
            foreach ($arDoctor as $item) {
                foreach ($item as &$val) $val = iconv("UTF-8", "Windows-1251", $val);
                fputcsv($out, $item, ";");
            }
            foreach ($arSponsor as $item) {
                foreach ($item as &$val) $val = iconv("UTF-8", "Windows-1251", $val);
                fputcsv($out, $item, ";");
            }
            fclose($out);

            exit;
        } else {
            $result = new Result();
            $result->addVizibleData(array("result" => 0));
            return $result;
        }
    }

    public function addThemeChat()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission("ADD_THEME_CHAT")) {
            if ($this->request['NAME'] && $this->request['DESCRIPTION']) {
                $bs = new \CIBlockSection;
                $arFields = Array(
                    "ACTIVE" => "Y",
                    "IBLOCK_SECTION_ID" => false,
                    "IBLOCK_ID" => IBLOCK_CONTENT_CHAT,
                    "NAME" => $this->request['NAME'],
                    "SORT" => "100",
                    "DESCRIPTION" => $this->request['DESCRIPTION'],
                    "DESCRIPTION_TYPE" => "text"
                );
                if ($ID = $bs->Add($arFields)) {
                    $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_CHAT);
                    $res = array('result' => 1, 'message' => "Тема добавлена", 'id' => $ID, 'url' => '/personal/chat/' . $ID . '/');
                } else {
                    $res = array('result' => 0, 'message' => $bs->LAST_ERROR);
                }
            } else {
                $res = array('result' => 0, 'message' => "Заполните все поля");
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    public function addMessageChat()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::getPermission("ADD_MESSAGE_CHAT") && $this->request['SECTION_ID']) {
            if ($this->request['PREVIEW_TEXT']) {
                $el = new \CIBlockElement;
                $arFields = Array(
                    "ACTIVE" => "Y",
                    "IBLOCK_SECTION_ID" => $this->request['SECTION_ID'],
                    "IBLOCK_ID" => IBLOCK_CONTENT_CHAT,
                    "NAME" => "Сообщение от " . $USER->GetFullName() . " " . date("d.m.Y H:i:s"),
                    "SORT" => "100",
                    "PREVIEW_TEXT" => $this->request['PREVIEW_TEXT'],
                    "PREVIEW_TEXT_TYPE" => "text"
                );
                if ($ID = $el->Add($arFields)) {
                    $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_CHAT);
                    $res = array('result' => 1, 'message' => "Сообщение отправлено");
                } else {
                    $res = array('result' => 0, 'message' => $el->LAST_ERROR);
                }
            } else {
                $res = array('result' => 0, 'message' => "Заполните все поля");
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /**
     * Добавление события
     * @return Result
     * @throws \Exception
     */
    public function sendEvent()
    {
        global $USER;
        $res = array('result' => 0, 'message' => "Сообщение не отправлено");
        if ($USER->IsAuthorized() && $this->request['MESSAGE']) {
            $arFiles = array();
            $FILES = array();
            $elementId = false;
            foreach ($this->files['FILES'] as $fileKey => $arData) {
                foreach ($arData as $key => $value) {
                    $arFiles[$key][$fileKey] = $value;
                }
            }
            foreach ($arFiles as $file) {
                $FILES[] = \CFile::SaveFile($file, "site");
            }
            if ($this->request['ELEMENT_ID']) {
                $elementId = $this->request['ELEMENT_ID'];
            }
            EventsTable::add(array(
                'UF_CODE' => 'USER_MESSAGE',
                'UF_ELEMENT_ID' => $elementId,
                'UF_PARAM' => serialize(array('MESSAGE' => $this->request['MESSAGE'], 'FILES' => $FILES))
            ));
            $res = array("result" => 1, "message" => "Запрос отправлен");
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }

    /*public function changeResearchDoctor()
    {
        global $USER, $CACHE_MANAGER;
        $res = array('result' => 0);
        if ($USER->IsAuthorized() && User::isActiveManager() && User::getPermission('EDIT_RESEARCH') && $this->request['USER'] && $this->request['ELEMENT_ID']) {
            if (Loader::includeModule("iblock")) {
                $arData = Site::getPropValue('DOCTOR', IBLOCK_CONTENT_RESEARCH, $this->request['ELEMENT_ID']);
                if ($this->request['USER']['ACTIVE']) {
                    $arData[] = $this->request['USER']['ID'];
                } else {
                    $key = array_search($this->request['USER']['ID'], $arData);
                    unset($arData[$key]);
                }
                Site::setPropValue('DOCTOR', $arData, IBLOCK_CONTENT_RESEARCH, $this->request['ELEMENT_ID']);
                $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_CONTENT_RESEARCH);
                $res = array('result' => 1);
            }
        }
        $result = new Result();
        $result->addVizibleData($res);
        return $result;
    }*/

    public function searchElementResearch()
    {
        global $APPLICATION;
        $result = new Result();
        $result->setHtmlType();
        $res = array('html' => '');
        if ($this->request['FIELDS'] && Loader::includeModule("iblock")) {
            $rsElement = \CIBlockElement::GetList(
                array(),
                array_merge(array('IBLOCK_ID' => IBLOCK_CONTENT_RESEARCH), $this->request['FIELDS']),
                false,
                false,
                array("*", "PROPERTY_*")
            );
            if ($obElement = $rsElement->GetNextElement()) {
                $arFields = $obElement->GetFields();
                $arProps = $obElement->GetProperties();
                $arElement = array(
                    'ID' => $arFields['ID'],
                    'NAME' => $arFields['NAME'],
                    'DATE_FROM' => $arFields['ACTIVE_FROM'],
                    'DATE_TO' => $arFields['ACTIVE_TO'],
                );
                foreach ($arProps as $prop) {
                    $arElement['PROPS'][$prop['CODE']] = $prop['VALUE'];
                }

                if ($arElement['PROPS']['MEDICINE_ORG']) {
                    $regions = RegionTable::getElements();
                    $arElement['MEDICINE_ORG'] = MedicineOrgTable::getList(array("filter" => array('UF_XML_ID' => $arElement['PROPS']['MEDICINE_ORG'])))->fetchAll();
                    foreach ($arElement['MEDICINE_ORG'] as &$item) {
                        $item['UF_CITY'] = $regions[$item['UF_CITY']]['UF_NAME'];
                        $row = UserMedicineOrgTable::getList(array(
                            'select' => array('USER_NAME' => 'USER.NAME', 'USER_EMAIL' => 'USER.EMAIL', 'USER_PERSONAL_PHONE' => 'USER.PERSONAL_PHONE'),
                            'filter' => array(
                                'MEDICINE_ORG_ID' => $item['ID'],
                                'ELEMENT_ID' => $arElement['ID']
                            )
                        ))->fetch();
                        if ($row) {
                            $item['NAME'] = $row['USER_NAME'];
                            $item['EMAIL'] = $row['USER_EMAIL'];
                            $item['PERSONAL_PHONE'] = $row['USER_PERSONAL_PHONE'];
                        }
                    }
                }

                ob_start();
                $APPLICATION->ShowAjaxHead();
                $APPLICATION->IncludeComponent('realweb:blank', 'add_research', array('RESULT' => $arElement));
                $html = ob_get_contents();
                ob_end_clean();
                $res = array("html" => $html);
            }
        }
        $result->addVizibleData($res);
        return $result;
    }
}

/*
TRUNCATE TABLE realweb_applications_doctors;
TRUNCATE TABLE realweb_applications_patients;
TRUNCATE TABLE realweb_events;
TRUNCATE TABLE realweb_favorite;
TRUNCATE TABLE realweb_form_edc;
TRUNCATE TABLE realweb_form_edc_user;
TRUNCATE TABLE realweb_message;
TRUNCATE TABLE realweb_patients;
TRUNCATE TABLE realweb_users_sponsor_research;
TRUNCATE TABLE realweb_user_medicine_org;
TRUNCATE TABLE b_hlbd_medicinearea;
TRUNCATE TABLE b_hlbd_medicineorg;
 */