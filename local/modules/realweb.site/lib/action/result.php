<?

namespace Realweb\Site\Action;

class Result extends \Bitrix\Main\Result
{

    private $vizibleData = Array();
    private $isHtmlType = false;

    public function isHtmlType()
    {
        return $this->isHtmlType;
    }

    public function setHtmlType($htmlType = true)
    {
        $this->isHtmlType = $htmlType;
    }


    public function getVizibleData()
    {
        return $this->vizibleData;
    }

    public function setVizibleData(Array $vizibleData)
    {
        $this->vizibleData = $vizibleData;
    }

    public function addVizibleData($vizibleData, $add = false)
    {
        if (!is_array($vizibleData) || $add) {
            $this->vizibleData[] = $vizibleData;
        } else {
            $this->vizibleData = array_merge($vizibleData, $this->vizibleData);
        }
    }

    public function __toString()
    {
        $html = '';
        if (!$this->isHtmlType()) {

            $arResult = Array();
            $arResult["success"] = ($this->isSuccess()) ? "Y" : "N";
            $arResult["error"] = Array();
            if (!$this->isSuccess()) {
                $errors = $this->getErrors();
                $digitCode = 0;
                foreach ($errors as $error) {
                    $code = $error->getCode() === 0 ? $digitCode++ : $error->getCode();
                    var_dump($error->getMessage());
                    if ($error instanceof Error) {
                        $arResult["error"][$code] = $error->getInfo();
                    } else {
                        $arResult["error"][$code] = $error->getMessage();
                    }

                }


            }
            $arResult["data"] = $this->getVizibleData();
            $html = \Bitrix\Main\Web\Json::encode($arResult);
        } else {
            $arData = $this->getVizibleData();

            $html = $arData['html'];

        }
        return $html;
    }

}
