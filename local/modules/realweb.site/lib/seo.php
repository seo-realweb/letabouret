<?php


namespace Realweb\Site;

use Bitrix\Main\Application;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\Page\Asset;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/iblock/lib/template/functions/fabric.php');

class Seo
{
    /**
     * @var HttpRequest
     */
    private $_request = null;

    /**
     * @var string
     */
    private $_canonical = null;

    /**
     * @var string
     */
    private $_meta_title = null;

    /**
     * @var string
     */
    private $_meta_description = null;

    /**
     * @var string
     */
    private $_meta_keywords = null;

    /**
     * @var string
     */
    private $_title = null;

    /**
     * @var array
     */
    private $_breadcrumbs = array();

    /**
     * @var bool
     */
    private $_no_index = false;

    private $_params = array();

    /**
     * @var Seo
     */
    protected static $instance;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getRequest()
    {
        if ($this->_request === null) {
            $this->_request = Application::getInstance()->getContext()->getRequest();
        }

        return $this->_request;
    }

    public function getCurPage()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPage(false);
    }

    public function processOgMeta()
    {
        global $APPLICATION;
        $APPLICATION->SetPageProperty('title', str_replace(array('"', '&nbsp;'), array('', ' '), htmlspecialchars_decode($APPLICATION->GetPageProperty('title'))));
        $APPLICATION->SetPageProperty('description', str_replace(array('"', '&nbsp;'), array('', ' '), htmlspecialchars_decode($APPLICATION->GetPageProperty('description'))));
        $APPLICATION->SetPageProperty('og:locale', 'ru_RU');
        $APPLICATION->SetPageProperty('og:type', 'website');
        $APPLICATION->SetPageProperty('og:title', $APPLICATION->GetPageProperty('title'));
        $APPLICATION->SetPageProperty('og:description', $APPLICATION->GetPageProperty('description'));
        $APPLICATION->SetPageProperty('og:url', Site::getDomain() . \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getRequestUri());
        $APPLICATION->SetPageProperty('og:site_name', 'Bonafiderussia- официальный интернет-магазин спортивной одежды Bona Fide');
        $APPLICATION->SetPageProperty('og:image', Site::getDomain() . ($APPLICATION->GetPageProperty('IMAGE') ?: (SITE_TEMPLATE_PATH . "/local/templates/main/images/logo.png")));
    }

    public function processRobots()
    {
        global $APPLICATION;

        if (strpos($_SERVER['REQUEST_URI'], 'filter') != false) {

            $arResult = \Realweb\Site\Site::getIBlockElement(array(
                'IBLOCK_ID' => IBLOCK_SERVICE_SEO,
                'CODE' => \Realweb\Site\Site::getCurrentPage(),
                'ACTIVE' => 'Y'
            ), true);
            if (!empty($arResult)) {

                if ($arResult['PROPS']['NOINDEX'] == 'Да') {

                    $APPLICATION->SetPageProperty('robots', 'noindex, nofollow');
                } else {


                    if (strpos($_SERVER['REQUEST_URI'], 'mebel/krovati-meb/filter/collection-is-') != false) {

                        $APPLICATION->SetPageProperty('robots', 'noindex, nofollow');
                    } else {
                        $APPLICATION->SetPageProperty('robots', 'index, follow');

                    }


                }

                $arh = [];

                if (strpos($_SERVER['REQUEST_URI'], 'krovati-meb/dvuspalnye') != false) {
                    $arh = ['krovat_color-is', 'style-is', 'material_krovat-is'];
                }
                if (strpos($_SERVER['REQUEST_URI'], 'krovati-meb/odnospalnye') != false) {
                    $arh = ['krovat_color-is', 'style-is', 'material_krovat-is'];
                }
                if (strpos($_SERVER['REQUEST_URI'], 'krovati-meb/polutoraspalnye') != false) {
                    $arh = ['krovat_color-is', 'material_krovat-is'];
                }
                if (strpos($_SERVER['REQUEST_URI'], 'krovati-meb/derevyannye') != false||strpos($_SERVER['REQUEST_URI'], 'krovati-meb/s-myagkim-izgolovem') != false||strpos($_SERVER['REQUEST_URI'], 'krovati-meb/s-podemnym-mekhanizmom') != false) {
                    $arh = ['krovat_size-is', 'style-is', 'krovat_color-is', 'material_krovat-is'];
                }


                foreach ($arh as $hide) {
                    if (strpos($_SERVER['REQUEST_URI'], $hide) != false) {

                        $APPLICATION->SetPageProperty('robots', 'noindex, follow');
                    }

                }


            } else {


                $APPLICATION->SetPageProperty('robots', 'noindex, nofollow');

            }


        } else {
            $arResult = \Realweb\Site\Site::getIBlockElement(array(
                'IBLOCK_ID' => IBLOCK_SERVICE_SEO,
                'CODE' => \Realweb\Site\Site::getCurrentPage(),
                'ACTIVE' => 'Y'
            ), true);
            if (!empty($arResult)) {

                if ($arResult['PROPS']['NOINDEX'] == 'Да') {

                    $APPLICATION->SetPageProperty('robots', 'noindex, nofollow');
                } else {


                    if (strpos($_SERVER['REQUEST_URI'], 'mebel/krovati-meb/filter/collection-is-') != false) {

                        $APPLICATION->SetPageProperty('robots', 'noindex, nofollow');
                    } else {
                        $APPLICATION->SetPageProperty('robots', 'index, follow');

                    }


                }


            }
        }


    }

    public function processCanonical()
    {
        if ($this->getCanonical()) {
            Asset::getInstance()->addString('<link rel="canonical" href="' . Site::getDomain() . $this->getCanonical() . '"/>', true);
        } else {
            foreach ($this->getRequest()->toArray() as $key => $value) {
                if (stripos($key, 'PAGEN_') !== false) {
                    Asset::getInstance()->addString('<link rel="canonical" href="' . Site::getDomain() . $this->getCurPage() . '"/>', true);
                }
            }
        }
    }

    public function processPagenMeta()
    {
        /*Мета для пагинации*/
        global $APPLICATION;
        $h1 = $APPLICATION->getTitle();

        $APPLICATION->SetPageProperty('title', $h1 . ' - каталог страница №' . $_REQUEST['PAGEN_1'] . ' | Le Tabouret');

        $APPLICATION->SetPageProperty('description', $h1 . ' - страница №' . $_REQUEST['PAGEN_1'] . ' каталога интернет-магазина Le Tabouret');


    }

    public function processMeta()
    {
        global $APPLICATION;
        if ($this->getMetaTitle()) {
            $APPLICATION->SetPageProperty('title', $this->getMetaTitle());
        }
        if ($this->getMetaDescription()) {
            $APPLICATION->SetPageProperty('description', $this->getMetaDescription());
        }
        if ($this->getMetaKeywords()) {
            $APPLICATION->SetPageProperty('keywords', $this->getMetaKeywords());
        }
        if ($this->getTitle()) {
            $APPLICATION->SetTitle($this->getTitle());
        }
    }

    public function processBreadcrumbs()
    {
        global $APPLICATION;
        if ($this->getBreadcrumbs()) {
            foreach ($this->getBreadcrumbs() as $arItem) {
                $APPLICATION->AddChainItem($arItem['name'], $arItem['url']);
            }
        }
    }

    /**
     * @return string
     */
    public function getCanonical(): ?string
    {
        return $this->_canonical;
    }

    /**
     * @param string $strCanonical
     * @return $this
     */
    public function setCanonical(string $strCanonical)
    {
        $this->_canonical = $strCanonical;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTitle(): ?string
    {
        return $this->_meta_title;
    }

    /**
     * @param string $strMetaTitle
     * @return $this
     */
    public function setMetaTitle(string $strMetaTitle)
    {
        $this->_meta_title = $strMetaTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): ?string
    {
        return $this->_meta_description;
    }

    /**
     * @param string $strMetaDescription
     * @return $this
     */
    public function setMetaDescription(string $strMetaDescription)
    {
        $this->_meta_description = $strMetaDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeywords(): ?string
    {
        return $this->_meta_keywords;
    }

    /**
     * @param string $strMetaKeywords
     * @return $this
     */
    public function setMetaKeywords(string $strMetaKeywords)
    {
        $this->_meta_keywords = $strMetaKeywords;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->_title;
    }

    /**
     * @param string $strTitle
     * @return $this
     */
    public function setTitle(string $strTitle)
    {
        $this->_title = $strTitle;
        return $this;
    }

    /**
     * @return array
     */
    public function getBreadcrumbs(): array
    {
        return $this->_breadcrumbs;
    }

    /**
     * @param array $arBreadcrumbs
     * @return $this
     */
    public function setBreadcrumbs(array $arBreadcrumbs)
    {
        $this->_breadcrumbs = $arBreadcrumbs;

        return $this;
    }

    public function addBreadcrumbs(string $strName, string $strUrl = '')
    {
        $this->_breadcrumbs[] = array(
            'name' => $strName,
            'url' => $strUrl
        );
    }

    public function isPagen()
    {
        foreach ($this->getRequest()->toArray() as $key => $value) {
            if (stripos($key, 'PAGEN_') !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isNoIndex()
    {
        return $this->_no_index;
    }

    /**
     * @param bool $bNoIndex
     * @return $this
     */
    public function setNoIndex(bool $bNoIndex = true)
    {
        $this->_no_index = $bNoIndex;

        return $this;
    }

    public function setParam(string $strField, $value)
    {
        $this->_params[$strField] = $value;

        return $this;
    }

    public function getParam(string $strField)
    {
        return ArrayHelper::getValue($this->_params, $strField);
    }

    public function removeKeywords()
    {
        global $APPLICATION;
        $APPLICATION->SetPageProperty('keywords', '');
    }

    function myOnTemplateGetFunctionClass(\Bitrix\Main\Event $event)
    {
        $arParam = $event->getParameters();
        $functionClass = '\\Realweb\\Site\\Seo\\' . $arParam[0];
        if (is_string($functionClass) && class_exists($functionClass) && $arParam[0] == 'iffilled') {
            $result = new \Bitrix\Main\EventResult(1, $functionClass);
            return $result;
        }
    }

    function OnEpilog()
    {


        $arResult = \Realweb\Site\Site::getIBlockElement(array(
            'IBLOCK_ID' => IBLOCK_SERVICE_SEO,
            'CODE' => \Realweb\Site\Site::getCurrentPage(),
            'ACTIVE' => 'Y'
        ), true);
        if (!empty($arResult)) {

            if (!empty($arResult['PROPS']['CANONICAL'])) {
                \Realweb\Site\Seo::getInstance()->setCanonical($arResult['PROPS']['CANONICAL']);
            }
            if (!empty($arResult['META'])) {
                if (!empty($arResult['META']['ELEMENT_META_TITLE'])) {
                    \Realweb\Site\Seo::getInstance()->setMetaTitle($arResult['META']['ELEMENT_META_TITLE']);
                }
                if (!empty($arResult['META']['ELEMENT_META_KEYWORDS'])) {
                    \Realweb\Site\Seo::getInstance()->setMetaKeywords($arResult['META']['ELEMENT_META_KEYWORDS']);
                }
                if (!empty($arResult['META']['ELEMENT_META_DESCRIPTION'])) {
                    \Realweb\Site\Seo::getInstance()->setMetaDescription($arResult['META']['ELEMENT_META_DESCRIPTION']);
                }
                if (!empty($arResult['META']['ELEMENT_PAGE_TITLE'])) {
                    \Realweb\Site\Seo::getInstance()->setTitle($arResult['META']['ELEMENT_PAGE_TITLE']);
                }
            }
        }

        self::getInstance()->removeKeywords();
        self::getInstance()->processCanonical();
        self::getInstance()->processMeta();
        self::getInstance()->processRobots();

        if ($_REQUEST['PAGEN_1'] && $_REQUEST['PAGEN_1'] > 1) {

            self::getInstance()->processPagenMeta();

        }

    }
}