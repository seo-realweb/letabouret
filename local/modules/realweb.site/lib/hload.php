<?php

namespace Realweb\Site;

class Hload
{

    public $hlblock_id;

    public function __construct($NAME = "", $ID = 0)
    {
        \CModule::IncludeModule('iblock');
        \CModule::IncludeModule('highloadblock');
        $this->hlblock_id = 0;
        if (intval($ID) > 0) {
            $filter = array(
                'select' => array('ID', 'NAME', 'TABLE_NAME', 'FIELDS_COUNT'),
                'filter' => array('=ID' => $ID)
            );
            $hlblock_db = \Bitrix\Highloadblock\HighloadBlockTable::getList($filter);
            if ($hlblock = $hlblock_db->fetch()) {
                $this->hlblock_id = $hlblock["ID"];
            }
        }
        if (intval($this->hlblock_id) == 0) {
            if (strlen($NAME) > 0) {
                $filter = array(
                    'select' => array('ID', 'NAME', 'TABLE_NAME', 'FIELDS_COUNT'),
                    'filter' => array('=NAME' => $NAME)
                );
                $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList($filter)->fetch();
                $this->hlblock_id = $hlblock["ID"];
            }
        }
    }

    public function SetId($ID)
    {
        $this->hlblock_id = $ID;
    }

    public function GetFields($list = false, $FIELDS = array(), $UNFIELDS = array())
    {
        global $USER_FIELD_MANAGER;
        $hlblock_id = $this->hlblock_id;

        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
        $_userfields = $USER_FIELD_MANAGER->GetUserFields('HLBLOCK_' . $hlblock['ID'], 0, LANGUAGE_ID);
        foreach ($_userfields as $_userfield) {
            if (($list && $_userfield['SHOW_IN_LIST'] == 'Y') || !$list) {
                if (!empty($FIELDS)) {
                    if (in_array($_userfield['FIELD_NAME'], $FIELDS)) {
                        $userfields[$_userfield['FIELD_NAME']] = $_userfield;
                    }
                } else {
                    if (!empty($UNFIELDS)) {
                        if (!in_array($_userfield['FIELD_NAME'], $UNFIELDS)) {
                            $userfields[$_userfield['FIELD_NAME']] = $_userfield;
                        }
                    } else {
                        $userfields[$_userfield['FIELD_NAME']] = $_userfield;
                    }
                }
            }
        }
        return $userfields;
    }

    public function GetData($params = array("order" => array('ID'), "filter" => array()))
    {
        global $USER_FIELD_MANAGER;

        $rows = array();

        $hlblock_id = $this->hlblock_id;
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
        if (!empty($hlblock)) {
            $userfields = array("ID");
            $_userfields = $USER_FIELD_MANAGER->GetUserFields('HLBLOCK_' . $hlblock['ID'], 0, LANGUAGE_ID);
            foreach ($_userfields as $_userfield) {
                $userfields[] = $_userfield['FIELD_NAME'];
            }
            if ($userfields) {
                // validated successfully. get data
                $hlDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
                $rows = $hlDataClass::getList(array(
                    'select' => $userfields,
                    'order' => $params['order'],
                    'filter' => $params['filter']
                ))->fetchAll();
            }
        }

        return $rows;
    }

    public function GetList($params = array("select" => array("*"), "order" => array('ID'), "filter" => array(), "group" => array(), "runtime" => array(), "limit" => ""))
    {
        $rows = array();
        $hlblock_id = $this->hlblock_id;
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
        if (!empty($hlblock)) {
            $hlDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
            $rows = $hlDataClass::getList(array(
                'select' => $params['select'],
                'order' => $params['order'],
                'filter' => $params['filter'] ? $params['filter'] : array(),
                'group' => $params['group'] ? $params['group'] : array(),
                'runtime' => $params['runtime'],
                'limit' => $params['limit'],
            ))->fetchAll();
        }

        return $rows;
    }

    public function GetAll($params = array("order" => array('ID'), "filter" => array()))
    {
        return $this->GetData($params);
    }

    public function GetRow($params = array("order" => array('ID'), "filter" => array()))
    {
        global $USER_FIELD_MANAGER;

        $rows = array();

        $hlblock_id = $this->hlblock_id;
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
        if (!empty($hlblock)) {
            $userfields = array("ID");
            $_userfields = $USER_FIELD_MANAGER->GetUserFields('HLBLOCK_' . $hlblock['ID'], 0, LANGUAGE_ID);
            foreach ($_userfields as $_userfield) {
                $userfields[] = $_userfield['FIELD_NAME'];
            }
            if ($userfields) {
                // validated successfully. get data
                $hlDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
                $rows = $hlDataClass::getList(array(
                    'select' => $userfields,
                    'order' => $params['order'],
                    'filter' => $params['filter']
                ))->fetch();
            }
        }

        return $rows;
    }

    public function GetIds($params = array("order" => array('ID'), "filter" => array()))
    {
        global $USER_FIELD_MANAGER;

        $rows = array();
        $hlblock_id = $this->hlblock_id;
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
        if (!empty($hlblock)) {
            $userfields = array("ID", "UF_FILTER_ID");
            if ($userfields) {
                // validated successfully. get data
                $hlDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
                $rows = $hlDataClass::getList(array(
                    'select' => $userfields,
                    'order' => $params['order'],
                    'filter' => $params['filter']
                ))->fetchAll();
            }
        }
        $IDS = array();
        foreach ($rows as $key => $row) {
            $IDS[] = $row['UF_FILTER_ID'];
        }

        return $IDS;
    }

    public function Add($arFields)
    {
        $hlblock_id = $this->hlblock_id;
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
        if (!empty($hlblock)) {
            $hlDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
            $result = $hlDataClass::add($arFields);
            return $result;
        }
        return false;
    }

    public function Update($ID, $arFields)
    {
        $hlblock_id = $this->hlblock_id;
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
        if (!empty($hlblock)) {
            $hlDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
            $result = $hlDataClass::update($ID, $arFields);
            return $result;
        }
        return false;
    }

    public function Delete($ID)
    {
        $hlblock_id = $this->hlblock_id;
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
        if (!empty($hlblock)) {
            $hlDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
            $result = $hlDataClass::delete($ID);
            return $result;
        }
        return false;
    }

    public function Truncate()
    {
        global $DB;
        $hlblock_id = $this->hlblock_id;
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
        if (!empty($hlblock)) {
            $result = $DB->Query("TRUNCATE TABLE ".$hlblock['TABLE_NAME']);
            return $result;
        }
        return false;
    }

}
