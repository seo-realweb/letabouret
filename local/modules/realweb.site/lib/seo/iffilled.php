<?php

namespace Realweb\Site\Seo;

use Realweb\Site\Util;

class iffilled extends \Bitrix\Iblock\Template\Functions\FunctionBase
{
    public function onPrepareParameters(\Bitrix\Iblock\Template\Entity\Base $entity, $parameters)
    {
        $arguments = array();
        /** @var \Bitrix\Iblock\Template\NodeBase $parameter */
        foreach ($parameters as $parameter) {
            $arguments[] = $parameter->process($entity);
        }
        return $arguments;
    }

    public function calculate(array $parameters)
    {
        if (!empty($parameters[0]) && is_array($parameters[0])) {
            sort($parameters[0]);
            $parameters[0] = implode(" / ", $parameters[0]);
        }
        if (isset($parameters[0]) && $parameters[0] && isset($parameters[1]) && isset($parameters[2])) {
            if (substr($parameters[2], 0, 1) == "!") {
                $parameters[2] = substr($parameters[2], 1, 1000);
                if ($parameters[2] == $parameters[0]) return "";
            } else {
                if ($parameters[2] != $parameters[0]) return "";
            }

            return sprintf($parameters[1], $parameters[0]);
        } elseif (isset($parameters[0]) && $parameters[0] && isset($parameters[1])) {
            return sprintf($parameters[1], $parameters[0]);
        }
        return "";
    }
}