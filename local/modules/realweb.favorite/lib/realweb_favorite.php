<?php

namespace Realweb;

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

Class RealwebFavoriteTable extends Main\Entity\DataManager {

    public static function getTableName() {
        return 'realweb_favorite';
    }

    public static function getAll() {
        return static::getList();
    }

    public static function getMap() {
        return array(
            'USER_ID' => new Main\Entity\IntegerField('USER_ID', array(
                'primary' => true,
                'required' => true,
                    )),
            'FUSER_ID' => new Main\Entity\IntegerField('FUSER_ID', array(
                'primary' => true,
                'required' => true,
                    )),
            'ELEMENT_ID' => new Main\Entity\IntegerField('ELEMENT_ID', array(
                'primary' => true,
                'required' => true,
                    )),
            'LIST_CODE' => new Main\Entity\StringField('LIST_CODE', array(
                'primary' => true,
                    )),
            'TIMESTAMP_X' => new Main\Entity\DatetimeField('TIMESTAMP_X', array(
                'default_value' => new Main\Type\DateTime(),
                    )),
            'USER' => new Main\Entity\ReferenceField(
                    'USER', '\Bitrix\Main\User', array('=this.USER_ID' => 'ref.ID'), array('join_type' => 'LEFT')
            ),
            'FUSER' => new Main\Entity\ReferenceField(
                    'FUSER', '\Bitrix\Sale\Internals\Fuser', array('=this.FUSER_ID' => 'ref.ID'), array('join_type' => 'LEFT')
            ),
            'ELEMENT' => new Main\Entity\ReferenceField(
                    'ELEMENT', '\Bitrix\Iblock\Element', array('=this.ELEMENT_ID' => 'ref.ID'), array('join_type' => 'LEFT')
            ),
        );
    }

    /**
     * @return ScalarField[]
     */
    public function getScalarFields() {
        $scalarFields = array();
        foreach (self::getMap() as $field) {
            if ($field instanceof \Bitrix\Main\Entity\ScalarField) {
                $scalarFields[$field->getName()] = $field;
            }
        }

        return $scalarFields;
    }

    public static function GetPrimaryKey() {
        $primary = array();
        $map = self::getMap();
        foreach ($map as $field) {
            if ($field instanceof \Bitrix\Main\Entity\ScalarField && $field->isPrimary()) {
                $primary[] = $field->getColumnName();
            }
        }
        return $primary;
    }

    public static function GetRowPrimaryKey($row) {
        $primary = self::GetPrimaryKey();
        $RowPrimary = array();
        foreach ($primary as $field) {
            $RowPrimary[$field] = $row[$field];
        }
        return $RowPrimary;
    }

    public static function GetByPrimary($PRIMARY) {
        $primary_key = self::GetPrimaryKey();

        $filter = array();
        foreach ($PRIMARY as $FIELD => $VALUE) {
            if (in_array($FIELD, $primary_key)) {
                $filter["=" . $FIELD] = $VALUE;
            }
        }
        return static::getList(array(
                    "filter" => $filter
        ));
    }

}
