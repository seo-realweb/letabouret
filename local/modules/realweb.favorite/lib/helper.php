<?php

namespace Realweb\Favorite;

class Helper {

    public $strField = "";
    public $arFilter;
    public $sSelect;
    public $sFrom;
    public $sWhere;

    public static function SubQuery($strField = "ID",$LIST_CODE="") {
        if ($strField == "ID") {
            $ob = new Self();
            $ob->arFilter = \Realweb_Favorite_Class::GetPrimaryByElementId(false,$LIST_CODE);
            return $ob;
        }
        return null;
    }

    function prepareSql($arSelectFields = array(), $arFilter = array(), $arGroupBy = false, $arOrder = array("SORT" => "ASC")) {

        $this->sSelect = "ELEMENT_ID";
        $this->sFrom = \Realweb\RealwebFavoriteTable::getTableName();
        $this->sWhere = " AND USER_ID=".$arFilter['USER_ID'].' AND FUSER_ID='.$arFilter['FUSER_ID'].' AND LIST_CODE="'.$arFilter['LIST_CODE'].'"';
        
        
    }

}
