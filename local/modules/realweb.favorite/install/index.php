<?

use Bitrix\Main\Application;
use Realweb\RealwebFavoriteTable;

$localPath = getLocalPath("");
$bxRoot = strlen($localPath) > 0 ? rtrim($localPath, "/\\") : BX_ROOT;

require_once($_SERVER['DOCUMENT_ROOT'] . $bxRoot . '/modules/realweb.favorite/prolog.php'); // пролог модуля

Class realweb_favorite extends CModule {

    // Обязательные свойства.
    /**
     * Имя партнера - автора модуля.
     * @var string
     */
    var $PARTNER_NAME;

    /**
     * URL партнера - автора модуля.
     * @var string
     */
    var $PARTNER_URI;

    /**
     * Версия модуля.
     * @var string
     */
    var $MODULE_VERSION;

    /**
     * Дата и время создания модуля.
     * @var string
     */
    var $MODULE_VERSION_DATE;

    /**
     * Имя модуля.
     * @var string
     */
    var $MODULE_NAME;

    /**
     * Описание модуля.
     * @var string
     */
    var $MODULE_DESCRIPTION;

    /**
     * ID модуля.
     * @var string
     */
    var $MODULE_ID = 'realweb.favorite';
    private $bxRoot = BX_ROOT;

    /**
     * Конструктор класса. Задаёт начальные значения свойствам.
     */
    function realweb_favorite() {
        $this->PARTNER_NAME = 'Realweb';
        $this->PARTNER_URI = 'http://www.realweb.ru';
        $this->errors = array();
        $this->result = array();
        $arModuleVersion = array();

        $path = str_replace('\\', '/', __FILE__);
        $path = substr($path, 0, strlen($path) - strlen('/index.php'));
        include($path . '/version.php');

        $localPath = getLocalPath("");
        $this->bxRoot = strlen($localPath) > 0 ? rtrim($localPath, "/\\") : BX_ROOT;

        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = GetMessage('REALWEB.FAVORITE.NAME');
        if (CModule::IncludeModule($this->MODULE_ID)) {
            $this->MODULE_DESCRIPTION = GetMessage('REALWEB.FAVORITE.INSTALL_DESCRIPTION');
        } else {
            $this->MODULE_DESCRIPTION = GetMessage('REALWEB.FAVORITE.PREINSTALL_DESCRIPTION');
        }
    }

    function DoInstall() {
        $connection = Application::getInstance()->getConnection();

        $included = false;
        if (!CModule::IncludeModule($this->MODULE_ID)) {
            RegisterModule($this->MODULE_ID);
            $included = CModule::IncludeModule($this->MODULE_ID);
        }

        if ($included) {
            $tableName = RealwebFavoriteTable::getTableName();
            if (!$connection->isTableExists($tableName)) {
                $connection->createTable($tableName, RealwebFavoriteTable::getScalarFields(), array("USER_ID", "FUSER_ID", "ELEMENT_ID","LIST_CODE"), array());
            }

            RegisterModuleDependences("main", "OnAfterUserAuthorize", "realweb.favorite", "Realweb_Favorite_Class", "OnAfterUserAuthorize");
        }
    }

    function DoUninstall() {
        if (CModule::IncludeModule($this->MODULE_ID)) {


            $connection = Application::getInstance()->getConnection();
            $tableName = RealwebFavoriteTable::getTableName();
            if ($connection->isTableExists($tableName)) {
                $connection->dropTable($tableName);
            }

            UnRegisterModuleDependences("main", "OnAfterUserAuthorize", "realweb.favorite", "Realweb_Favorite_Class", "OnAfterUserAuthorize");
            UnRegisterModule($this->MODULE_ID);
        }
    }

}

?>
