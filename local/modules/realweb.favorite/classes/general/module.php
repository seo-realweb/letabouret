<?php

IncludeModuleLangFile(__FILE__);

Class Realweb_Favorite_Class {

    function OnAfterUserAuthorize($arUser) {
        \Bitrix\Main\Loader::includeModule('sale');

        $filter = self::GetPrimaryByElementId(array(), "", "=");

        $FUSER_ID = intval(CSaleBasket::GetBasketUserID());
        $res_fuser_id = \Bitrix\Sale\Internals\FuserTable::getList(array(
                    "filter" => array('=USER_ID' => $arUser['user_fields']['ID']),
                    "limit" => 1
        ));
        if ($row_fuser_id = $res_fuser_id->fetch()) {
            $FUSER_ID = $row_fuser_id['ID'];
        }

        unset($filter['=USER_ID']);
        $res = \Realweb\RealwebFavoriteTable::getList(array(
                    "filter" => $filter
        ));
        while ($row = $res->fetch()) {
            //всем этим элементам проставим пользователя
            $data = array(
                'USER_ID' => $arUser['user_fields']['ID']
            );
            if (intval($FUSER_ID) > 0) {
                $data['FUSER_ID'] = $FUSER_ID;
            }

            $primary_row = $row;
            unset($primary_row['TIMESTAMP_X']);
            try {
                \Realweb\RealwebFavoriteTable::update($primary_row, $data);
            } catch (Exception $ex) {
                \Realweb\RealwebFavoriteTable::delete($primary_row);
            }
        }

        //теперь всем вообще проставим актуальный $FUSER_ID

        $res = \Realweb\RealwebFavoriteTable::getList(array(
                    "filter" => array('=USER_ID' => $arUser['user_fields']['ID'], '!=FUSER_ID' => $FUSER_ID),
        ));

        while ($row = $res->fetch()) {
            $data = array(
                'FUSER_ID' => $FUSER_ID
            );
            $primary_row = $row;
            unset($primary_row['TIMESTAMP_X']);
            try {
                \Realweb\RealwebFavoriteTable::update($primary_row, $data);
            } catch (Exception $ex) {
                \Realweb\RealwebFavoriteTable::delete($primary_row);
            }
        }
    }

    public static function GetCount($LIST_CODE = "") {
        $filter = self::GetPrimaryByElementId(array(), $LIST_CODE, "=");
        //нужно чтобы элементы тоже существовали и были активными
        \Bitrix\Main\Loader::includeModule('iblock');
        $runtime = array(new \Bitrix\Main\Entity\ExpressionField('CNT', 'COUNT(*)'));

        $runtime['ELEMENT'] = array(
            "data_type" => "\\Bitrix\\Iblock\\ElementTable",
            'reference' => array(
                '=ref.ID' => 'this.ELEMENT_ID',
                '=ref.ACTIVE' => new Bitrix\Main\DB\SqlExpression('?s', 'Y'),
            ),
            'join_type' => "INNER"
        );

        $res = \Realweb\RealwebFavoriteTable::getList(array(
                    'select' => array('CNT'),
                    "filter" => $filter,
                    "runtime" => $runtime,
        ));
        if($row = $res->fetch()) {
            return intval($row['CNT']);
        }
    }

    public static function GetByElementIds($arElements, $LIST_CODE = "") {
        $arResult = array();
        $filter = self::GetPrimaryByElementId($arElements, $LIST_CODE, "=");
        $res = \Realweb\RealwebFavoriteTable::getList(array(
                    "filter" => $filter
        ));
        while ($row = $res->fetch()) {
            $arResult[$row['ELEMENT_ID']] = $row;
        }
        return $arResult;
    }

    public static function Add($ELEMENT_ID, $LIST_CODE = "") {
        $data = self::GetPrimaryByElementId($ELEMENT_ID, $LIST_CODE);
        try {
            $result = \Realweb\RealwebFavoriteTable::add($data);
            if ($result->isSuccess()) {
                return $data;
            }
        } catch (Exception $exc) {
            
        }
        return false;
    }

    public static function Delete($ELEMENT_ID, $LIST_CODE = "") {
        $PRIMARY = self::GetPrimaryByElementId($ELEMENT_ID, $LIST_CODE);
        try {
            $res = \Realweb\RealwebFavoriteTable::GetByPrimary($PRIMARY);
            if ($row = $res->fetch()) {
                \Realweb\RealwebFavoriteTable::delete($PRIMARY);
                return true;
            }
        } catch (Exception $exc) {
            
        }
        return false;
    }

    public static function GetPrimaryByElementId($ELEMENT_ID, $LIST_CODE = "", $PREFIX = "") {
        $primaryKey = \Realweb\RealwebFavoriteTable::GetPrimaryKey();
        $primary = array();
        foreach ($primaryKey as $KEY) {
            if ($KEY == "USER_ID") {
                global $USER;
                $primary[$PREFIX . $KEY] = intval($USER->GetId());
            } elseif ($KEY == "FUSER_ID") {
                \Bitrix\Main\Loader::includeModule('sale');
                $primary[$PREFIX . $KEY] = intval(CSaleBasket::GetBasketUserID());
            } elseif ($KEY == "ELEMENT_ID" && !empty($ELEMENT_ID)) {
                $primary[$PREFIX . $KEY] = $ELEMENT_ID;
            } elseif ($KEY == "LIST_CODE" && !empty($LIST_CODE)) {
                $primary[$PREFIX . $KEY] = $LIST_CODE;
            }
        }
        return $primary;
    }

}
