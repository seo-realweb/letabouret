<?php
$strPath2Lang = str_replace('\\', '/', __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang) - strlen('/include.php'));

CModule::AddAutoloadClasses('realweb.favorite', array(
        'Realweb_Favorite_Class' => 'classes/general/module.php',
        '\\Realweb\\Favorite\\Helper' => 'lib/helper.php',
        '\\Realweb\\RealwebFavoriteTable' => 'lib/realweb_favorite.php',
    )
);
