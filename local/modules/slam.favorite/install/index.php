<?
 
 
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . "/local/modules/slam.favorite/include.php");

 
if (class_exists(str_replace(".", "_", "slam.favorite"))) {
    return;
} 
Class slam_favorite extends CModule {

    var $MODULE_ID = "slam.favorite";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = "Y";

    function slam_favorite() {
        $arModuleVersion = array();
 
        include(__DIR__ . "/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = Loc::GetMessage("SLAM_INSTALL_NAME");
        $this->MODULE_DESCRIPTION = Loc::GetMessage("SLAM_INSTALL_DESCRIPTION");

        $this->PARTNER_NAME = Loc::GetMessage("SLAM_EASYFORM_PARTNER_NAME");
        $this->PARTNER_URI = Loc::GetMessage("SLAM_EASYFORM_PARTNER_URI");
        $this->MODULE_SORT = 1;
    }

    function DoInstall() {
        global $APPLICATION;
        $this->InstallFiles();
        $this->InstallDB();
        $GLOBALS["errors"] = $this->errors;


        $APPLICATION->IncludeAdminFile(Loc::GetMessage("SLAM_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/local/modules/slam.favorite/install/step1.php");
    }

    function DoUninstall() {
        global $APPLICATION, $step;
        $step = IntVal($step);
        if ($step < 2) {
            $APPLICATION->IncludeAdminFile(Loc::GetMessage("SLAM_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/local/modules/slam.favorite/install/unstep1.php");
        }
        elseif ($step == 2) {

            $this->UnInstallDB(array(
             "savedata" => $_REQUEST["savedata"],
            ));
            $this->UnInstallFiles();

            $GLOBALS["errors"] = $this->errors;
            $APPLICATION->IncludeAdminFile(Loc::GetMessage("SLAM_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/local/modules/slam.favorite/install/unstep2.php");
        }
    }

    function InstallDB() {
        global $DB, $APPLICATION;

        $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/local/modules/slam.favorite/install/db/mysql/install.sql");

        if ($this->errors !== false) {
            $APPLICATION->ThrowException(implode("", $this->errors));
            return false;
        }

        RegisterModule("slam.favorite");
        return true;
    }

    function UnInstallDB($arParams = array()) {
        CModule::IncludeModule($this->MODULE_ID);

        global $DB, $APPLICATION;

        $this->errors = false;
        if (array_key_exists("savedata", $arParams) && $arParams["savedata"] != "Y") {
            $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/local/modules/slam.favorite/install/db/mysql/uninstall.sql");
            if ($this->errors !== false) {
                $APPLICATION->ThrowException(implode("", $this->errors));
                return false;
            }
        }

        COption::RemoveOption("slam.favorite");
        UnRegisterModule("slam.favorite");
        return true;
    }

    function InstallFiles() {
        
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/slam.favorite/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/slam.favorite/install/components", $_SERVER["DOCUMENT_ROOT"] . "/local/components", true, true);

        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] .  "/local/modules/slam.favorite/install/themes", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/themes", true, true);
        return true;
    }

    function UnInstallFiles() {
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/slam.favorite/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/slam.favorite/install/components/slam", $_SERVER["DOCUMENT_ROOT"] . "/local/components/slam");
        
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] .  "/local/modules/slam.favorite/install/themes", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/themes");
        return true;
    }

}
