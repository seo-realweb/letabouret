<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();


$arComponentParameters = array(
    "PARAMETERS" => array(
        "PATH_TO_FAV_LIST" => array(
            "NAME" => GetMessage("SLAM_PATH_TO_FAV"),
            "TYPE" => "STRING",
            "DEFAULT" => '={SITE_DIR."personal/favorites/"}',
            "PARENT" => "BASE",
        ),
        "HIDE_ON_FAV_PAGES" => array(
            "NAME" => GetMessage("SLAM_HIDE_ON_FAV_PAGES"),
            "TYPE" => "CHECKBOX",
            "PARENT" => "BASE",
            "DEFAULT" => "Y"
        )
    )
);

