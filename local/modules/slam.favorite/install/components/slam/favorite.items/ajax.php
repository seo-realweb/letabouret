<?
/*
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$arResult = array();

if(\Bitrix\Main\Loader::includeModule('sh.favorite')) {

    if (($_REQUEST["FAVORITE_ADD"] || $_REQUEST["FAVORITE_DEL"]) && $_REQUEST["MODULE"] == 'favorite') {

        $oFavorite = new Sh\Favorite\UserFavorite();
        $dbl = $oFavorite
            ->getFavoriteObj()
            ->query()
            ->setSelect(array("ITEM_ID"))
            ->setFilter(array(
                    "=FAVORITE_USER_ID_LINK.HASH" => $oFavorite->getCurrentUserHash(),
                    "!FAVORITE_USER_ID_LINK.HASH" => false
                )
            )
            ->exec();

        while ($res = $dbl->fetch()) {
            $arResult[$res["ITEM_ID"]] = $res["ITEM_ID"];
        }
    }

}
$GLOBALS['APPLICATION']->RestartBuffer();
echo \Bitrix\Main\Web\Json::encode($arResult);
*/
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);


if(strlen($_POST['arParams']['templateLang'])==2)
    define('LANGUAGE_ID', $_POST['arParams']['templateLang']);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');




$_POST['arParams']['AJAX'] = 'Y';

$APPLICATION->RestartBuffer();
header('Content-Type: text/html; charset='.LANG_CHARSET);
$APPLICATION->IncludeComponent('slam:favorite.items', $_POST['arParams']['templateName'], $_POST['arParams']);

?>