<?
class CFavoriteCatalog  extends CBitrixComponent {

    protected $favoriteUser;

    function __construct($component = null) {

        if(!CModule::IncludeModule("slam.favorite")){
            return false;
        }

        $this->favoriteUser = new slam\Favorite\UserFavorite();
        parent::__construct($component);
    }

    function onPrepareComponentParams($arParams) {
        if(!CModule::IncludeModule("slam.favorite")){
            return false;
        }

        global $APPLICATION;
        $arParams['PATH_TO_FAV_LIST'] = trim($arParams['PATH_TO_FAV_LIST']);
        if ($arParams['PATH_TO_FAV_LIST'] == '')
            $arParams['PATH_TO_FAV_LIST'] = SITE_DIR.'personal/favorites/';


        $arParams["UPDATE_TIME"] = $APPLICATION->get_cookie("SH_FAVORITE_USER_UPDATE")? : time();
        $arParams["USER_HASH"] = $this->favoriteUser->getCurrentUserHash();

        $arParams['TEMPLATE_FOLDER'] = $this->GetPath().'/ajax.php';
        $arParams['TEMPLATE_NAME'] = $this->GetTemplateName();//$templateName;


        if ($arParams['AJAX'] != 'Y')
            $arParams['AJAX'] = 'N';

        return parent::onPrepareComponentParams($arParams);
    }


    public function executeComponent()
    {
        if(!CModule::IncludeModule("slam.favorite")){
            return false;
        }

        if ($_REQUEST["FAVORITE_ADD"] || $_REQUEST["FAVORITE_DEL"] && $_REQUEST["MODULE"] == 'favorite') {
            $oFavorite = new slam\Favorite\UserFavorite();
        }

        if ($this->arParams['HIDE_ON_FAV_PAGES'] == 'Y')
        {
            $currentPage = strtolower(\Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPage());
            $favPage = strtolower($this->arParams['PATH_TO_FAV_LIST']);
            if (strncmp($currentPage, $favPage, strlen($favPage)) == 0){
                return;
            }
        }


        $this->idFavorite = array();
        $dbl = $this->favoriteUser
            ->getFavoriteObj()
            ->query()
            ->setSelect(array("ITEM_ID"))
            ->setFilter(array(
                    "=FAVORITE_USER_ID_LINK.HASH" => $this->favoriteUser->getCurrentUserHash(),
                    "!FAVORITE_USER_ID_LINK.HASH" => false
                )
            )
            ->exec();

        while ($res = $dbl->fetch()) {
            $this->idFavorite[] = $res["ITEM_ID"];
        }


        $this->arParams['FAVORITE_ITEMS_ID'] = $this->idFavorite;
        $this->arResult['ITEMS'] = $this->idFavorite;
        $this->arResult['COUNT'] = count($this->idFavorite);

        // output
        if ($this->arParams['AJAX'] == 'Y')
            $this->includeComponentTemplate('ajax_template');
        else
            $this->includeComponentTemplate();

        return $this->idFavorite;
    }
}
