<?

global $APPLICATION;

use Bitrix\Main\Localization\Loc;

ClearVars("str_");

$ID = $_REQUEST["ID"]? : 0;

if (!empty($ID)) {

    $currentValues = $queryObj
            ->setSelect(array("*"))
            ->setFilter(array("=ID" => $ID))
            ->exec()
            ->fetch();

    $ID = 0;

    if ($currentValues) {
        $ID = $currentValues["ID"];
    }
}


$aTabs = array();
$aTabs[] = array(
 "DIV"   => "edit10",
 "TAB"   => Loc::GetMessage("SHFAVORITES_MAIN_OPTION"),
 "ICON"  => "",
 "TITLE" => Loc::GetMessage("SHFAVORITES_MAIN_OPTION"),
);

$tabControl = new CAdminTabControl(("tabControl" . time()), $aTabs);


if (
        $_SERVER["REQUEST_METHOD"] == "POST" // проверка метода вызова страницы
        &&
        ($save != "" || $apply != "") // проверка нажатия кнопок "Сохранить" и "Применить"
        &&
        $MODULE_RIGHT == "W"          // проверка наличия прав на запись для модуля
        &&
        check_bitrix_sessid()     // проверка идентификатора сессии
) {


    $arFields = array();

    /* @var $field  Bitrix\Main\Entity\Field() */
    foreach ($map as $field) {
        if ($field->getName() == "ID") {
            continue;
        }

        if ($field instanceof Bitrix\Main\Entity\ReferenceField) {
            continue;
        }

        if (isset($_POST[$field->getName()])) {
            $arFields[$field->getName()] = $_POST[$field->getName()];
        }
    }

    $res = false;

    // сохранение данных 
    if ($ID > 0) {
        $resultSave = $tblObj->Update($ID, $arFields);
        $res = $ID = $resultSave->isSuccess() ? $resultSave->getId() : 0;
    }
    else {
        $resultSave = $tblObj->Add($arFields);
        $res = $ID = $resultSave->isSuccess() ? $resultSave->getId() : 0;
    }

    if (!$res) {

        // если в процессе сохранения возникли ошибки - полу
        $errors .= "Не получилось сохранить";
        $errors .= implode(",\n ", $resultSave->getErrors());
        $bVarsFromForm = true;
    }
    else {
        if ($apply != "") { // если была нажата кнопка "Применить" - отправляем обратно на форму.
            LocalRedirect("{$url}?ID=" . $ID . "&lang=" . LANG);
        }
        else { // если была нажата кнопка "Сохранить" - отправляем к списку элементов.
            LocalRedirect("{$back_url}?lang=" . LANG);
        }
    }
}


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/iblock/admin_tools.php");


if ($bVarsFromForm) {
    $DB->InitTableVarsForEdit("b_sh_banner", "", "str_");
}
elseif (!$ID) {
    foreach ($map as $field) {
        if ($field instanceof Bitrix\Main\Entity\ReferenceField) {
            continue;
        }

        if ($field->getName() == "ID") {
            continue;
        }

        $currentValues[$field->getName()] = $field->getDefaultValue();
    }
}



$aMenu = array(
 array(
  "TEXT"  => $APPLICATION->getTitle(),
  "TITLE" => $APPLICATION->getTitle(),
  "LINK"  => "{$back_url}?lang=" . LANG,
  "ICON"  => "btn_list"
 )
);

if (intval($ID) > 0) {

    $aMenu[] = array(
     "TEXT"  => Loc::GetMessage("SHFAVORITES_ADD"),
     "TITLE" => Loc::GetMessage("SHFAVORITES_ADD"),
     "LINK"  => "sh_product_subscribes_edit.php?lang=" . LANG,
     "ICON"  => "btn_new"
    );

    $aMenu[] = array("SEPARATOR" => "Y");

    $aMenu[] = array(
     "TEXT"  => Loc::GetMessage("SHFAVORITES_DELETE"),
     "TITLE" => Loc::GetMessage("SHFAVORITES_DELETE"),
     "LINK"  => "javascript:if(confirm('" . Loc::GetMessage("SHFAVORITES_ERR_DEL") . "')) window.location='{$back_url}?ID=" . $ID . "&action=delete&lang=" . LANG . "&" . bitrix_sessid_get() . "';",
     "ICON"  => "btn_delete"
    );
}


$context = new CAdminContextMenu($aMenu);
$context->Show();

if (strlen($errors))
    CAdminMessage::ShowMessage(array(
     "MESSAGE" => Loc::GetMessage("SELLERS_ERROR_DETECTED"),
     "DETAILS" => $errors,
     "HTML"    => true,
     "TYPE"    => $type,
    ));



/* @var $APPLICATION CMain\ */

$APPLICATION->SetTitle(Loc::GetMessage("SHFAVORITES_USER_LIST") . " - " . Loc::GetMessage("SHFAVORITES_EDIT"));
?>

<form method="POST" id="form" name="form" enctype="multipart/form-data"   action="<?= $APPLICATION->GetCurPageParam() ?>" >
    <?= bitrix_sessid_post(); ?>


    <input type="hidden" name="ID" value="<?= intval($ID) ?>">

    <?
    $tabControl->Begin();
    $tabControl->BeginNextTab();
    ?>
    <? if (intval($ID) > 0): ?>
        <tr>
            <td class="adm-detail-content-cell-l">ID</td>
            <td class="adm-detail-content-cell-r"><?= $ID ?></td>
        </tr>
    <? endif; ?>

    <? foreach ($map as $field) : ?>    
        <?
        if ($field instanceof Bitrix\Main\Entity\ReferenceField || $field->getName() == "TIMESTAMP_X") {
            continue;
        }
        ?>
        <? if ($field->getName() == "ID") continue; ?>
        <tr>
            <td width="40%" class="adm-detail-content-cell-l" valign="top"><?= (loc::GetMessage("SHFAVORITES_FIELD_{$field->getName()}")? : $field->getTitle() ) ?></td>
            <td width="60%" class="adm-detail-content-cell-r">

                <?
                switch (get_class($field)):
                    case "Bitrix\Main\Entity\DateField":
                        ?>
                        <input type="text" class="typeinput" name="<?= $field->getName() ?>" value="<?= strlen($currentValues[$field->getName()]) ? $currentValues[$field->getName()] : ""; ?>" >
                        <?= Calendar($field->getName(), "curform") ?>
                        <?
                        break;
                    case "Bitrix\Main\Entity\EnumField":
                        $values = $field->getValues();
                        ?>

                        <select name="<?= $field->getName() ?>">
                            <? foreach ($values as $valueID => $valueName): ?>
                                <option value="<?= $valueID ?>" <?= $currentValues[$field->getName()] == $valueID ? "selected" : "" ?> ><?= $valueName ?></option>
                            <? endforeach; ?>
                        </select>
                        <?
                        break;
                    case "Bitrix\Main\Entity\TextField":
                        ?>
                        <textarea  type="number" rows="10" cols="50" name="<?= $field->getName() ?>"><?= strlen($currentValues[$field->getName()]) ? htmlspecialchars($currentValues[$field->getName()]) : ""; ?></textarea>

                        <?
                        break;
                    case "Bitrix\Main\Entity\BooleanField":
                        ?>
                        <input  type="hidden" name="<?= $field->getName() ?>" value="0"/>
                        <input  type="checkbox" name="<?= $field->getName() ?>" value="1" <?= ($currentValues[$field->getName()] ? "checked" : ""); ?>/>
                        <?
                        break;
                    case "Bitrix\Main\Entity\IntegerField":
                        ?>
                        <input  type="number" name="<?= $field->getName() ?>" value="<?= strlen($currentValues[$field->getName()]) ? intval($currentValues[$field->getName()]) : ""; ?>"/>
                        <?
                        break;
                    case "Bitrix\Main\Entity\FloatField":
                        ?>

                        <input  type="text" name="<?= $field->getName() ?>" value="<?= floatval($currentValues[$field->getName()]) ? floatval($currentValues[$field->getName()]) : 0; ?>"/>
                        <?
                        break;
                    default :
                        ?>
                        <input  type="text" name="<?= $field->getName() ?>" value="<?= strlen($currentValues[$field->getName()]) ? trim($currentValues[$field->getName()]) : ""; ?>"/>
                <? endswitch; ?>
            </td>
        </tr>
    <? endforeach; ?>



    <?
    $tabControl->EndTab();
    $tabControl->Buttons(array("disabled" => false, "back_url" => $back_url));
    $tabControl->End();
    ?>
</form>

<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

