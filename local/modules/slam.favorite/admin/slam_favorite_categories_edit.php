<?

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use slam\Favorite;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . "/local/modules/slam.favorite/include.php");

$APPLICATION->SetTitle(Loc::GetMessage("SHFAVORITES_CATEGORY_LIST"));



\Bitrix\Main\Loader::IncludeModule("slam.favorite");



$MODULE_RIGHT = $APPLICATION->GetGroupRight("slam.favorite");


if ($MODULE_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::GetMessage("ACCESS_DENIED"));


$tblObj = new Favorite\CategoriesTable();

$queryObj = $tblObj->query();

$map = $tblObj->getMap();

$sTableID = $tblObj->getTableName();



$back_url = '/bitrix/admin/' . basename(__FILE__, "_edit.php") . ".php";
$url = "/bitrix/admin/" . basename(__FILE__);


require "editBulder.php";
