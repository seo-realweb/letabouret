<?
$arHeaders = array();
global $APPLICATION;

use Bitrix\Main\Localization\Loc;

/* @var $field  Bitrix\Main\Entity\Field() */
foreach ($map as $field) {

    if ($field instanceof Bitrix\Main\Entity\ReferenceField) {
        continue;
    }

    $arHeaders[] = array("id" => $field->getName(), "content" => (loc::GetMessage("SHFAVORITES_FIELD_{$field->getName()}")? : $field->getTitle() ), "sort" => $field->getName(), "default" => (count($arHeaders) < 6));
}


$arFilter = array();

if (!empty($_REQUEST["FINDBY"])) {
    $arFilter = array_filter($_REQUEST["FINDBY"], function(&$a)
        {
        if (is_array($a)) {

            $a = array_filter($a, function($b)
                {
                $b = (!is_array($b)) ? trim($b) : $b;
                return !empty($b);
                });
        }


        return !empty($a);
        });
}


$aContext[] = array(
 "TEXT" => Loc::GetMessage("SHFAVORITES_ADD"),
 "ICON" => "btn_new",
 "LINK" => $url,
);

$lAdmin->AddAdminContextMenu($aContext, false);


if (($arID = $lAdmin->GroupAction())) {

    if ($_REQUEST['action_target'] == 'selected') {
        $rsData = $queryObj
                ->setFilter($arFilter)
                ->setOrder(array($by => $order))
                ->exec();

        while ($arRes = $rsData->Fetch()) {
            $arID[] = $arRes['ID'];
        }
    }

    foreach ($arID as $propID) {
        if (intval($propID) <= 0) {
            continue;
        }
        switch ($_REQUEST['action']) {
            case "delete":
                $DB->StartTransaction();

                if (!$tblObj->Delete($propID)) {
                    $DB->Rollback();
                    $error = is_object($APPLICATION->GetException()) ? $APPLICATION->GetException()->GetString() : "";
                    $error = (!strlen($error)) ? Loc::GetMessage("SHFAVORITES_ERR_DEL") . " (&quot;" . htmlspecialchars($propID) . "&quot;)" : $error;
                    $lAdmin->AddGroupError($error, $propID);
                }

                $DB->Commit();
                break;
        }
    }
}

$lAdmin->AddHeaders($arHeaders);

$rsData = $queryObj
        ->setSelect(array("*"))
        ->setFilter($arFilter)
        ->setOrder(array($by => $order))
        ->exec();

$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();


$lAdmin->NavText($rsData->GetNavPrint('', true, '', false, false));


$events = array();
while ($arRes = $rsData->Fetch()) {

    foreach ($map as $field) {

        if ($field instanceof Bitrix\Main\Entity\ReferenceField) {
            continue;
        }
        if (method_exists($field, "getValues") && count($field->getValues())) {
            $valuesList = $field->getValues();
            $arRes[$field->getName()] = isset($valuesList[$arRes[$field->getName()]]) ? $valuesList[$arRes[$field->getName()]] : $arRes[$field->getName()];
        }
    }

    $contentMenu = array(
     array("ICON" => "edit", "TEXT" => Loc::GetMessage("SHFAVORITES_EDIT"), "ACTION" => $url . "?ID={$arRes["ID"]}"),
     array("ICON" => "delete", "TEXT" => Loc::GetMessage("SHFAVORITES_DEL"), "ACTION" => "if(confirm('" . Loc::GetMessage("SHFAVORITES_ERR_DEL") . "')) " . $lAdmin->ActionDoGroup($arRes["ID"], "delete"))
    );


    $row = & $lAdmin->AddRow($arRes["ID"], $arRes, $url . "?ID={$arRes["ID"]}", Loc::GetMessage("MAIN_ADMIN_MENU_EDIT"));

    $row->AddActions($contentMenu);
}

$lAdmin->AddFooter(array(
 array("title" => Loc::GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()),
 array("counter" => true, "title" => Loc::GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"))
);

// Add form with actions
$lAdmin->AddGroupActionTable(Array(
 "delete" => Loc::GetMessage("MAIN_ADMIN_LIST_DELETE")
));


$lAdmin->CheckListMode();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
<form name="form1" method="GET" action="<?= $APPLICATION->GetCurPage() ?>">
    <?
//тут должно быть в обратном порядке  поля

    $propsArray = array();
    foreach ($map as $field) {

        if ($field instanceof Bitrix\Main\Entity\ReferenceField) {
            continue;
        }

        $propsArray["FINDBY[{$field->getName()}]"] = (loc::GetMessage("SHFAVORITES_FIELD_{$field->getName()}")? : $field->getTitle() );
    }

    $oFilter = new CAdminFilter($sTableID . "_filter", $propsArray);

    $oFilter->Begin();
    ?>

    <? foreach ($map as $field) : ?>
        <?
        if ($field instanceof Bitrix\Main\Entity\ReferenceField) {
            continue;
        }
        ?>
        <tr>
            <td><?= (loc::GetMessage("SHFAVORITES_FIELD_{$field->getName()}")? : $field->getTitle() ) ?>:</td>
            <td>

                <?
                switch (get_class($field)):
                    case "Bitrix\Main\Entity\DateField":
                        ?>
                        <input type="text" class="typeinput" name="<?= "FINDBY[{$field->getName()}]"; ?>"  value="<?= strlen($currentValues[$field->getName()]) ? $currentValues[$field->getName()] : ""; ?>">
                        <?= Calendar($field->getName(), "curform") ?>
                        <?
                        break;
                    case "Bitrix\Main\Entity\EnumField":
                        $values = $field->getValues();

                        $values[0] = "--//--";
                        ?>

                        <select name="<?= "FINDBY[{$field->getName()}]"; ?>">
                            <? foreach ($values as $valueID => $valueName): ?>
                                <option value="<?= $valueID ?>"  <?= $currentValues[$field->getName()] == $valueID ? "selected" : "" ?> ><?= $valueName ?></option>
                            <? endforeach; ?>
                        </select>
                        <?
                        break;
                    case "Bitrix\Main\Entity\BooleanField":
                        ?>
                        <input  type="hidden" name="<?= "FINDBY[{$field->getName()}]"; ?>" value="0"/>
                        <input  type="checkbox" name="<?= "FINDBY[{$field->getName()}]"; ?>" value="1" <?= ($currentValues[$field->getName()] ? "checked" : ""); ?>/>
                        <?
                        break;
                    case "Bitrix\Main\Entity\IntegerField":
                        ?>
                        <input  type="number" name="<?= "FINDBY[{$field->getName()}]"; ?> " value="<?= strlen($currentValues[$field->getName()]) ? $currentValues[$field->getName()] : ""; ?>"/>
                        <?
                        break;
                    case "Bitrix\Main\Entity\FloatField":
                    default :
                        ?>
                        <input  type="text" name="<?= "FINDBY[{$field->getName()}]"; ?> " value="<?= htmlspecialchars(strlen($currentValues[$field->getName()]) ? $currentValues[$field->getName()] : ""); ?>"/>
                <? endswitch; ?>

        </tr>


    <? endforeach; ?>

    <?
    $oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage()));
    $oFilter->End();
    ?>
</form>

<?
$lAdmin->DisplayList();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
