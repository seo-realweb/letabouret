<?

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

if ($APPLICATION->GetGroupRight("slam.favorite") != "D") {
    $aMenu = array(
     "parent_menu" => "global_menu_services",
     "section"     => "slam.favorites",
     "sort"        => 1,
     "text"        => GetMessage("SHFAVORITES_MODULE"),
     "title"       => GetMessage("SHFAVORITES_MODULE"),
     "icon"        => "favorite_menu_icon",
     "page_icon"   => "favorite_page_icon",
     "items_id"    => "menu_favorite",
     "items"       => array(
      array(
       "text"     => GetMessage("SHFAVORITES_USER_LIST"),
       "title"    => GetMessage("SHFAVORITES_USER_LIST"),
       "url"      => "/bitrix/admin/slam_favorite_user.php?lang=" . LANGUAGE_ID,
       "more_url" => array(
        "/bitrix/admin/slam_favorite_user_edit.php",
        "/bitrix/admin/slam_favorite_user.php"
       ),
      ),
      array(
       "text"     => GetMessage("SHFAVORITES_CATEGORY_LIST"),
       "title"    => GetMessage("SHFAVORITES_CATEGORY_LIST"),
       "url"      => "/bitrix/admin/slam_favorite_categories.php?lang=" . LANGUAGE_ID,
       "more_url" => array(
        "/bitrix/admin/slam_favorite_categories_edit.php",
        "/bitrix/admin/slam_favorite_categories.php"
       ),
      ),
      array(
       "text"     => GetMessage("SHFAVORITES_ITEM_LIST"),
       "title"    => GetMessage("SHFAVORITES_ITEM_LIST"),
       "url"      => "/bitrix/admin/slam_favorite.php?lang=" . LANGUAGE_ID,
       "more_url" => array(
        "/bitrix/admin/slam_favorite_edit.php",
        "/bitrix/admin/slam_favorite.php"
       ),
      )
     )
    );
    return $aMenu;
}
return false;
