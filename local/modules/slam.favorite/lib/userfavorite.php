<?


namespace slam\Favorite;

class UserFavorite {

    /**
     * Проверка на бота
     * @return boolean
     */
    private function isSearchEngine() {
        $engines = array(
         'Aport'           => 'Aport',
         'Google'          => 'Google',
         'msnbot'          => 'MSN',
         'Rambler'         => 'Rambler',
         'Yahoo'           => 'Yahoo',
         'Yandex'          => 'Yandex',
         'Aport'           => 'Aport robot',
         'Google'          => 'Google',
         'msnbot'          => 'MSN',
         'Rambler'         => 'Rambler',
         'Yahoo'           => 'Yahoo',
         'AbachoBOT'       => 'AbachoBOT',
         'accoona'         => 'Accoona',
         'AcoiRobot'       => 'AcoiRobot',
         'ASPSeek'         => 'ASPSeek',
         'CrocCrawler'     => 'CrocCrawler',
         'Dumbot'          => 'Dumbot',
         'FAST-WebCrawler' => 'FAST-WebCrawler',
         'GeonaBot'        => 'GeonaBot',
         'Gigabot'         => 'Gigabot',
         'Lycos'           => 'Lycos spider',
         'MSRBOT'          => 'MSRBOT',
         'Scooter'         => 'Altavista robot',
         'AltaVista'       => 'Altavista robot',
         'WebAlta'         => 'WebAlta',
         'IDBot'           => 'ID-Search Bot',
         'eStyle'          => 'eStyle Bot',
         'Mail.Ru'         => 'Mail.Ru Bot',
         'Scrubby'         => 'Scrubby robot',
         'Yandex'          => 'Yandex',
         'bot'             => 'Any Other Bot'
        );

        foreach ($engines as $engine => $engineName) {
            if (stristr($_SERVER['HTTP_USER_AGENT'], $engine)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Получение текущего кода пользователя
     * @return type
     */
    protected function GetSessionUserHash() {
        return md5(\CUser::IsAuthorized() ? \CUser::GetID() : session_id());
    }

    /**
     * Получение текущего кода пользователя
     * @return type
     */
    protected function GetCookieUserHash() {
        global $APPLICATION;

        if (static::$isSendCookies) {
            return static::$isSendCookies;
        }


        $cookieName = "SH_FAVORITE_USER_HASH";
        return $APPLICATION->get_cookie($cookieName);
    }

    public function getCurrentUserHash() {
        return $this->GenerateUserHash();
    }

    protected function GenerateUserHash() {

        if ($this->isSearchEngine()) {
            return "";
        }
        /* @var $USER CUser */
        /* @var $APPLICATION CMain */
        global $USER, $APPLICATION;

        $currentHash = $this->GetCookieUserHash();

        $userHashID = $this->GetSessionUserHash();


        //если хэш из куки не равено текущему то обноляю страрые записи
        if ($currentHash != $userHashID && \CUser::IsAuthorized() && $currentHash) {
            $userTable = new UsersTable();

            $userTable->updateFromGetlist(
                    array(
             "HASH"    => $userHashID,
             "USER_ID" => \CUser::GetID()
                    ), array(
             array(
              "=USER_ID" => false,
              "=HASH"    => $currentHash,
              "!HASH"    => false
             )
                    )
            );
        }

        $this->SetCookie($userHashID);

        return $userHashID;
    }

    protected static $isSendCookies = "";

    private function SetCookie($value, $cookieName = "", $lifeTime = 0) {
        if (!empty(static::$isSendCookies) && empty($cookieName)) {
            return;
        }

        global $USER, $APPLICATION;

        if (empty($cookieName)) {
            static::$isSendCookies = $value;
        }

        $lifeTime = empty($lifeTime) ? (time() + 60 * 60 * 365 * 5) : $lifeTime;
        $cookieName = $cookieName ?: "SH_FAVORITE_USER_HASH";



        $APPLICATION->set_cookie($cookieName, $value, $lifeTime, "/", false, false, \Bitrix\Main\Config\Option::get("main", "auth_multisite", "N") == "Y", false, true);
    }

    protected $oCategories, $oFavorites, $oUser;

    public function __construct() {

        $this->oCategories = new CategoriesTable();
        $this->oFavorites = new FavoritesTable();
        $this->oUser = new UsersTable();
        $this->checkRequest();
    }

    /**
     * 
     * @return UsersTable
     */
    public function getFavoriteObj() {
        return $this->oFavorites;
    }

    /**
     * @return \Sh\BannerRules\Rules\GroupTable
     */
    public function getCategoriesObj() {
        return $this->oCategories;
    }

    protected static $eventDone = false;

    protected function checkRequest() {

        if (!isset($_REQUEST["MODULE"]) || $_REQUEST["MODULE"] != "favorite") {
            return;
        }

        $params = array();

        $params["FAVORITE_ADD"] = $_REQUEST["FAVORITE_ADD"] ? (intval($_REQUEST["FAVORITE_ADD"]) ?: "") : "";
        $params["FAVORITE_DEL"] = $_REQUEST["FAVORITE_DEL"] ? (intval($_REQUEST["FAVORITE_DEL"]) ?: "") : "";
        $params["CATEGORY_FAVORITE_ID"] = $_REQUEST["CATEGORY_FAVORITE_ID"] ? (intval($_REQUEST["CATEGORY_FAVORITE_ID"]) ?: "") : "";
        $params["CATEGORY_FAVORITE_DEL"] = $_REQUEST["CATEGORY_FAVORITE_DEL"] ? (intval($_REQUEST["CATEGORY_FAVORITE_DEL"]) ?: "") : "";
        $params["CATEGORY_FAVORITE_NAME"] = $_REQUEST["CATEGORY_FAVORITE_NAME"] ? trim($_REQUEST["CATEGORY_FAVORITE_NAME"]) : "";

        $params = array_filter($params, "strlen");

        if (!UserFavorite::$eventDone && !empty($params)) {

            if ($params["CATEGORY_FAVORITE_DEL"]) {
                $this->removeCategory($params["CATEGORY_FAVORITE_DEL"]);
            }

            if ($params["CATEGORY_FAVORITE_ID"] && $params["CATEGORY_FAVORITE_NAME"]) {
                $this->updateCategoryName($params["CATEGORY_FAVORITE_ID"], $params["CATEGORY_FAVORITE_NAME"]);
            }
            elseif ($params["CATEGORY_FAVORITE_NAME"]) {
                $params["CATEGORY_FAVORITE_ID"] = $this->addCategory($params["CATEGORY_FAVORITE_NAME"]);
            }

            if ($params["FAVORITE_ADD"]) {
                $this->addCategoryItem($params["FAVORITE_ADD"], $params["CATEGORY_FAVORITE_ID"]);
            }

            if ($params["FAVORITE_DEL"]) {
                $this->removeCategoryItem($params["FAVORITE_DEL"], $params["CATEGORY_FAVORITE_ID"]);
            }


            $this->SetCookie(time(), "SH_FAVORITE_USER_UPDATE");

            UserFavorite::$eventDone = true;
        }
    }

    public static function eventIsProccessed() {
        return UserFavorite::$eventDone;
    }

    protected static $categoryList = array(), $userFavoriteID = 0;

    protected function getUserID($reset = false) {
        if (!empty(static::$userFavoriteID) && empty($reset)) {
            return static::$userFavoriteID;
        }
        static::$categoryList = array();


        $query = $this->oUser->query();

        $dbl = $query->setSelect(array("ID"))
                ->setFilter(array(
                 "!HASH" => false,
                 "HASH"  => $this->GenerateUserHash(),
                        )
                )
                ->setOrder(array("ID" => "ASC"))
                ->exec();

        $userList = array();

        while ($res = $dbl->fetch()) {
            $userList[] = $res["ID"];
        }

        $userList = array_unique($userList);

        if ($userList) {
            static::$userFavoriteID = intval(array_shift($userList));
        }

        if (!empty($userList)) {
            $this->oFavorites->updateFromGetlist(array("FAVORITE_USER_ID" => static::$userFavoriteID), array("=FAVORITE_USER_ID" => $userList));
            $this->oCategories->updateFromGetlist(array("FAVORITE_USER_ID" => static::$userFavoriteID), array("=FAVORITE_USER_ID" => $userList));
            $this->oUser->deleteFromGetList(array("ID" => $userList));
        }

        return static::$userFavoriteID;
    }

    public function getCategoryList($reset = false) {
        if (!empty(static::$categoryList) && empty($reset)) {
            return static::$categoryList;
        }

        static::$categoryList = array();

        $userID = $this->getUserID($reset);

        if (empty($userID)) {
            return static::$categoryList;
        }

        $query = $this->oCategories->query();

        $dbl = $query->setSelect(
                        array(
                         "ID",
                         "NAME",
                         "DESCRIPTION"
                ))
                ->setFilter(
                        array(
                         "=FAVORITE_USER_ID" => $userID
                        )
                )->setOrder(array("TIMESTAMP_X" => "DESC"))
                ->exec();

        while ($res = $dbl->fetch()) {
            static::$categoryList[$res["ID"]] = $res;
        }

        return static::$categoryList;
    }

    protected function addCategoryItem($itemID, $categoryID, $type = "E") {
        
        if (empty($itemID) || (!empty($categoryID) && !isset(static::getCategoryList()[$categoryID]))) {
            return;
        }

        $userID = $this->checkUser();

        if (empty($userID)) {
            return;
        }

        $type = trim($type) ?: "E";
        $userID = $this->getUserID();
        $arrayAdd = array("FAVORITE_USER_ID" => $userID, "CATEGORY_ID" => ($categoryID ?: 0), 'TYPE' => $type, "ITEM_ID" => $itemID);

        $resutl = $this->oFavorites->query()->setFilter($arrayAdd)->setSelect(array("ID"))->exec()->fetch();

        if ($resutl) {
            return $resutl["ID"];
        }

        $result = $this->oFavorites->add($arrayAdd);
        return $result->isSuccess() ? $result->getId() : 0;
    }

    protected function removeCategoryItem($itemID, $categoryID, $type = "E") {
        if (empty($itemID) || (!empty($categoryID) && !isset(static::getCategoryList()[$categoryID]))) {
            return;
        }
        $type = trim($type) ?: "E";
        $userID = $this->getUserID();
        return $this->oFavorites->deleteFromGetList(array("=FAVORITE_USER_ID" => $userID, "=CATEGORY_ID" => $categoryID, '=TYPE' => $type, "ITEM_ID" => $itemID));
    }

    protected function removeCategory($categoryID) {
        if (empty($categoryID) || !isset(static::getCategoryList()[$categoryID])) {
            return;
        }
        return $this->oCategories->delete($categoryID);
    }

    protected function updateCategoryName($categoryID, $categoryName) {
        if (empty($categoryID) || !isset(static::getCategoryList()[$categoryID]) || !strlen(trim($categoryName))) {
            return;
        }

        $userID = $this->checkUser();

        if (empty($userID)) {
            return;
        }

        $result = $this->oCategories->update($categoryID, array("FAVORITE_USER_ID" => $userID, "NAME" => trim($categoryName)));

        if ($result->isSuccess()) {
            $this->getCategoryList(true);
        }

        return $result->isSuccess() ? $result->getId() : 0;
    }

    protected function addCategory($categoryName) {
        if (!strlen(trim($categoryName))) {
            return;
        }

        $userID = $this->checkUser();

        if (empty($userID)) {
            return;
        }

        $result = $this->oCategories->add(array("FAVORITE_USER_ID" => $userID, "NAME" => trim($categoryName)));

        if ($result->isSuccess()) {
            $this->getCategoryList(true);
        }

        return $result->isSuccess() ? $result->getId() : 0;
    }

    protected function checkUser() {

        if (!$this->getUserID()) {

            $result = $this->oUser->add(array(
             "USER_ID" => \CUser::IsAuthorized() ? \CUser::GetID() : 0,
             "HASH"    => $this->GenerateUserHash(),
            ));

            static::$userFavoriteID = $result->isSuccess() ? $result->getId() : 0;
        }

        return $this->getUserID();
    }

}
