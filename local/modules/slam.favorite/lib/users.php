<?


namespace slam\Favorite;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class UsersTable extends iTables {

    public static function getTableName() {
        return "b_sh_favorite_users";
    }

    public static function delete($primary) {
        //после удаления категории удаляем  записи связанные с категорией
        if (parent::delete($primary)->isSuccess()) {
            $oFavorites = new FavoritesTable();
            $oFavorites->deleteFromGetList(array("FAVORITE_USER_ID" => $primary));
            
            $oCategories = new CategoriesTable();
            $oCategories->deleteFromGetList(array("FAVORITE_USER_ID" => $primary));
        }
        
        return parent::delete($primary)->isSuccess();
    }

}
