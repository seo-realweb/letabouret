<?


namespace slam\Favorite;

use \Bitrix\Main,
    \Bitrix\Main\Entity,
    \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

abstract class iTables extends Main\Entity\DataManager {

    function updateFromGetlist($updateArr, $arFilter = array()) {


        $connection = Application::getConnection();

        $where = "";



        if (!empty($arFilter)) {
            $query = $this->query();
            $where = stristr($query->setFilter($arFilter)->getQuery(), "where ");
        }


        $helper = $connection->getSqlHelper();
        $dataReplacedColumn = static::replaceFieldName($updateArr);
        $update = $helper->prepareUpdate($this->getTableName(), $dataReplacedColumn);
        $sql = "UPDATE {$this->getTableName()} as  {$this->query()->getInitAlias()}  SET {$update[0]}  {$where}";

                
        $connection->query($sql);

        $result = new Entity\UpdateResult();
        $result->setAffectedRowsCount($connection);
        $result->setData($updateArr);

        return $result;
    }

    function deleteFromGetList($arFilter = array()) {

        $connection = Application::getConnection();

        if (empty($arFilter)) {
            return $connection->truncateTable($this->getTableName());
        }

        $where = stristr($this->query()->setFilter($arFilter)->getQuery(), "where ");

        return $connection->query("DELETE {$this->query()->getInitAlias()} FROM {$this->getTableName()} as {$this->query()->getInitAlias()} " . $where);
    }

    protected function mapByDbParams() {


        global $DB;

        $connection = Application::getConnection();

        $query = "SELECT colum.COLUMN_NAME as CODE , colum.DATA_TYPE as TYPE, colum.COLUMN_KEY
                    FROM information_schema.COLUMNS as colum
                    WHERE TABLE_SCHEMA = DATABASE()
                      AND TABLE_NAME = '{$this->getTableName()}'
                    ORDER BY ORDINAL_POSITION";


        $dbl = $connection->query($query);

        $resultMap = array();

        $typesMapper = array(
         "N"  => array("int"),
         "NF" => array("float", "double", "decimal"),
         "S"  => array("varchar"),
         "T"  => array("longtext", "text"),
         "DT" => array("datetime", "timestamp"),
         "D"  => array("date"),
        );

        while ($res = $dbl->fetch()) {
            $res["TYPE"] = strtolower($res["TYPE"]);

            foreach ($typesMapper as $key => $types) {
                if (in_array($res["TYPE"], $types)) {
                    $resultMap[$res["CODE"]] = array("TYPE" => $key, "PRIMARY" => ($res["COLUMN_KEY"] == "PRI"));
                    break;
                }
            }

            if (empty($resultMap[$res["CODE"]])) {
                $resultMap[$res["CODE"]] = array("TYPE" => "S", "PRIMARY" => ($res["COLUMN_KEY"] == "PRI"));
            }
        }



        return $this->mapByParams($resultMap);
    }

    public static function getInstance() {
        return new static;
    }

    public static function getMap() {

        return static::getInstance()->mapByDbParams();
    }

    protected function mapByParams($paramsFields) {

        $map = array();
        foreach ($paramsFields as $paramKey => $params) {
            $setParams = array("primary" => ($paramKey == "ID" || $params["PRIMARY"] ));

            if ($params["ALIAS"]) {
                $setParams["column_name"] = $paramKey;
                $paramKey = $params["ALIAS"];
            }


            if ($paramKey == "USER_ID") {
                $map[$paramKey] = new \Bitrix\Main\Entity\IntegerField($paramKey, $setParams);
                $map[$paramKey . "_LINK"] = new \Bitrix\Main\Entity\ReferenceField(
                        $paramKey . "_LINK", Main\UserTable::getEntity(), array("=this.{$paramKey}" => 'ref.ID'), array('join_type' => 'LEFT')
                );
                continue;
            }

            if ($paramKey == "CATEGORY_ID") {
                $map[$paramKey] = new \Bitrix\Main\Entity\IntegerField($paramKey, $setParams);
                $map[$paramKey . "_LINK"] = new \Bitrix\Main\Entity\ReferenceField(
                        $paramKey . "_LINK", CategoriesTable::getEntity(), array("=this.{$paramKey}" => 'ref.ID'), array('join_type' => 'LEFT')
                );
                continue;
            }

            if ($paramKey == "FAVORITE_USER_ID") {
                $map[$paramKey] = new \Bitrix\Main\Entity\IntegerField($paramKey, $setParams);
                $map[$paramKey . "_LINK"] = new \Bitrix\Main\Entity\ReferenceField(
                        $paramKey . "_LINK", UsersTable::getEntity(), array("=this.{$paramKey}" => 'ref.ID'), array('join_type' => 'LEFT')
                );
                continue;
            }


            switch ($params["TYPE"]) {

                case "G":

                    $map[$paramKey] = new \Bitrix\Main\Entity\IntegerField($paramKey, $setParams);
                    $map[$paramKey . "_LINK"] = new \Bitrix\Main\Entity\ReferenceField(
                            $paramKey . "_LINK", "Bitrix\Iblock\Section", array("=this.{$paramKey}" => 'ref.ID'), array('join_type' => 'INNER')
                    );
                    break;
                case "NF":
                    $map[$paramKey] = new \Bitrix\Main\Entity\FloatField($paramKey, $setParams);

                    break;

                case "T":
                    $map[$paramKey] = new \Bitrix\Main\Entity\TextField($paramKey, $setParams);

                    break;
                case "N":
                case "F":
                    $map[$paramKey] = new \Bitrix\Main\Entity\IntegerField($paramKey, $setParams);

                    break;
                case "E":
                    $map[$paramKey] = new \Bitrix\Main\Entity\IntegerField($paramKey, $setParams);
                    $map[$paramKey . "_LINK"] = new \Bitrix\Main\Entity\ReferenceField(
                            $paramKey . "_LINK", "Bitrix\Iblock\Element", array("=this.{$paramKey}" => 'ref.ID'), array('join_type' => 'INNER')
                    );
                    break;

                case "D":
                    $map[$paramKey] = new \Bitrix\Main\Entity\DateField($paramKey, $setParams);
                    break;
                case "DT":
                    $map[$paramKey] = new \Bitrix\Main\Entity\DateField($paramKey, $setParams);
                    break;
                case "S":

                default :
                    $map[$paramKey] = new \Bitrix\Main\Entity\StringField($paramKey, $setParams);
            }
        }

        return $map;
    }

}
