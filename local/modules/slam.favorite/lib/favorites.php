<?

/**
 * @author Dmitry Sharyk
 * email: d.sharyk@gmail.com
 */
namespace slam\Favorite;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class FavoritesTable extends iTables {

    public static function getTableName() {
        return "b_sh_favorite_items";
    }

}
