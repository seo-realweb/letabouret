<?


namespace slam\Favorite;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class CategoriesTable extends iTables {

    public static function getTableName() {
        return "b_sh_favorite_categories";
    }

    public static function delete($primary) {
        //после удаления категории удаляем  записи связанные с категорией
        if (parent::delete($primary)->isSuccess()) {
            $oFavorites = new FavoritesTable();
            $oFavorites->deleteFromGetList(array("CATEGORY_ID" => $primary));
        }
    }

}
