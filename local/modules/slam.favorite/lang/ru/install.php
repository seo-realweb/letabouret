<?

$MESS["SLAM_EASYFORM_PARTNER_NAME"] = "S.L.A.M.";
$MESS["SLAM_EASYFORM_PARTNER_URI"] = "https://slam.by";

$MESS["SLAM_INSTALL_NAME"] = "SLAM: Избранные товары пользователей";

$MESS["SLAM_INSTALL_DESCRIPTION"] = "Модуль для работы с избранными элементами пользователей";
$MESS["SLAM_INSTALL_TITLE"] = "Установка модуля '{$MESS["SLAM_INSTALL_NAME"] }'";
$MESS["SLAM_UNINSTALL_ERROR"] = "Обнаружены ошибки при удалении модуля";
