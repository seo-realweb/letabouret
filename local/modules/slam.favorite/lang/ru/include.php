<?

$MESS["SHFAVORITES_MAIN_OPTION"] = "Общие параметры";
$MESS["SHFAVORITES_ADD"] = "Добавить";
$MESS["SHFAVORITES_DELETE"] = "Удалить";
$MESS["ERROR_DETECTED"] = "Обнаружены ошибки";

$MESS["SHFAVORITES_EDIT"] = "Редактирование";
$MESS["SHFAVORITES_DEL"] = "Удаление";
$MESS["SHFAVORITES_FIELD_NAME"] = "Название";
$MESS["SHFAVORITES_FIELD_USER_ID"] = "Принадлежность к пользовтелям";
$MESS["SHFAVORITES_FIELD_ID"] = "ID";

$MESS["SHFAVORITES_FIELD_ITEM_ID"] = "ID элемента";
$MESS["SHFAVORITES_FIELD_CATEGORY_ID"] = "Категория";
$MESS["SHFAVORITES_FIELD_TYPE"] = "Тип";
$MESS["SHFAVORITES_FIELD_SORT"] = "Сортировка";


$MESS["SHFAVORITES_FIELD_FAVORITE_USER_ID"] = "Пользователь избранного";
$MESS["SHFAVORITES_FIELD_DESCRIPTION"] = "Описание";
$MESS["SHFAVORITES_FIELD_TIMESTAMP_X"] = "Последнее изменение";



$MESS["SHFAVORITES_FIELD_HASH"] = "Хэш пользователя";
 


include 'admin/menu.php';

include 'install.php';