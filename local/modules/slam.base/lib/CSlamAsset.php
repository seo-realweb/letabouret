<?
namespace Slam\Base;

class CSlamAsset extends \CBitrixComponent{
    private static $instance;
    private static $arCss = array();
    const MODULE_ID = 'slam.base';

    public static function getInstance()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new CSlamAsset();
        }
        return self::$instance;
    }

    public function addCss($path, $additional = false)
    {
        $cssMode = \Bitrix\Main\Config\Option::get(self::MODULE_ID, "ADD_CSS_MODE", '', SITE_ID);

        // Если включен режим вставки css inline
        if($cssMode == 'inline'){
            $cacheTime = CUSTOM_CACHE_TIME;
            $cacheId = md5($path);
            $cacheDir = 'slam_header_css/'.$cacheId;

            if(in_array($cacheId, self::$arCss)){
                return false;
            }else{
                self::$arCss[] = $cacheId;
            }

            $fileContent = '';
            $cache = \Bitrix\Main\Data\Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir))
            {
                $fileContent = $cache->getVars();
            }
            elseif ($cache->startDataCache())
            {
                // Пробуем найти min.css версию
                $minPath = substr($path, 0, -3).'min.css';

                if(file_exists($_SERVER['DOCUMENT_ROOT'].$minPath)){
                    $path = $minPath;
                }

                $filePath = $_SERVER['DOCUMENT_ROOT'].$path;

                if(file_exists($_SERVER['DOCUMENT_ROOT'].$path)){
                    $fileContent = file_get_contents($filePath);
//                $fileContent = preg_replace(array('/(\s+)/i', '/\/\*(.*?)\*\//'), array(''), $fileContent);
                }

                if(strlen(trim($fileContent)) > 0){
                    $copyright = '/* Created by slam.base module - '.$path.' */'.PHP_EOL;
                    $fileContent = $copyright . $fileContent.PHP_EOL;
                }

                $cache->endDataCache( $fileContent);
            }

            if(strlen($fileContent) > 0){
                global $APPLICATION;
                $APPLICATION->AddViewContent('slam_head_css', $fileContent);
            }

         // Или вставляем css как обычно
        }else{
            \Bitrix\Main\Page\Asset::getInstance()->addCss($path, $additional);
        }
    }

    public function showHead(){
        global $APPLICATION;
        echo '<style>'.PHP_EOL;
        $APPLICATION->ShowViewContent('slam_head_css');
        echo '</style>';
    }
}