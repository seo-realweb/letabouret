<?
$MODULE_ID = 'slam.share';
define("ADMIN_MODULE_NAME", $MODULE_ID);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
CJSCore::Init(array("jquery2"));

$GLOBALS['APPLICATION']->AddHeadScript("/local/modules/slam.base/include/clipboard.min.js");
$APPLICATION->SetTitle(GetMessage("SLAM_INCLUDE_AREA_MODULE"));

//======================================================================================================================
//FORM:
$aTabs = array(
    array("DIV" => "edit1", "TAB" => GetMessage("INCLUDE_AREA_TAB"), "ICON" => "smile", "TITLE" => GetMessage("INCLUDE_AREA_TAB_TITLE"))
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
$tabControl->BeginNextTab();

//$catalogOffersId = COption::GetOptionString($MODULE_ID, "catalog_offers_id", 4);
//$catalogOffersProp = json_decode(COption::GetOptionString($MODULE_ID, "offers_properties", ''));

?>
    <tr>
        <td>
            <form class = 'js-form' method="POST" action="<?= $APPLICATION->GetCurPageParam() ?>" name="copy_database"
                  enctype="multipart/form-data">

                <input type="hidden" name="Update" value="Y"/>
                <input type="hidden" name="lang" value="<?= LANG ?>"/>
                <input type="hidden" name="ID" value="<?= $ID ?>"/>


                <button class="adm-btn js-copy" data-sended="false">Копировать бд и файлы с stage на dev</button>
            </form>
        </td>
        <td style="width:70%; padding-left: 40px;">
            <div class = 'js-result'></div>
        </td>
    </tr>



<?


$tabControl->EndTab();


$tabControl->End();
//$tabControl->ShowWarnings("smile_import", $message);


//======================================================================================================================
//BUSINESS-LOGIC:
?>

    <script>
        let resultBlock = $('.js-result');

        var sendAdminAjax = function (step) {

            let stepTitles = {1: 'Создание дампа stage бд', 2: 'stage бд заливается на dev сайт', 3: 'Копирование папки upload'};

            resultBlock.append(step + ') ' + stepTitles[step]  + '<br>');

            $.ajax({
                type: 'post',//тип запроса: get,post либо head
                url: '/local/modules/slam.base/include/ajax_copy_database.php',//url адрес файла обработчика
                data: {'step': step},//параметры запроса
                response: 'text',//тип возвращаемого ответа text либо xml

                success: function (data) {//возвращаемый результат от сервера
                    try {
                        let result = JSON.parse(data);
                        resultBlock.append(result['message'] + ' ' + result['sub_message']  + '<br><br>');

                        if(step < 3){
                            sendAdminAjax(++step);
                        }else{
                            resultBlock.append('Копирование завершено!');
                        }
                    }
                    catch (e) {
                        console.log(e);
                    }
                }
            });

        };


        $(".js-copy").on("click", function() {

            if($(this).data('sended') === false){
                $(this).data('sended', true);
                resultBlock.html('');
                let step = 1;
                sendAdminAjax(step);
            }

            return false;
        });

        $('form.js-form').on('submit', function(e){
            e.preventDefault();
        })

    </script>

<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");