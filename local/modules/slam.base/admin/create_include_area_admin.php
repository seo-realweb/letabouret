<?
$MODULE_ID = 'slam.share';
define("ADMIN_MODULE_NAME", $MODULE_ID);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
CJSCore::Init(array("jquery2"));

$GLOBALS['APPLICATION']->AddHeadScript("/local/modules/slam.base/include/clipboard.min.js");
$APPLICATION->SetTitle(GetMessage("SLAM_INCLUDE_AREA_MODULE"));

//======================================================================================================================
//FORM:
$aTabs = array(
    array("DIV" => "edit1", "TAB" => GetMessage("INCLUDE_AREA_TAB"), "ICON" => "smile", "TITLE" => GetMessage("INCLUDE_AREA_TAB_TITLE"))
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
$tabControl->BeginNextTab();

//$catalogOffersId = COption::GetOptionString($MODULE_ID, "catalog_offers_id", 4);
//$catalogOffersProp = json_decode(COption::GetOptionString($MODULE_ID, "offers_properties", ''));

?>
    <form method="POST" action="<?= $APPLICATION->GetCurPageParam() ?>" name="smile_import"
          enctype="multipart/form-data">

        <input type="hidden" name="Update" value="Y"/>
        <input type="hidden" name="lang" value="<?= LANG ?>"/>
        <input type="hidden" name="ID" value="<?= $ID ?>"/>

        <tr>
            <td width="40%" class="adm-detail-content-cell-l">
                <label for="include_file_folder"><?= GetMessage("SAVE_DIR") ?>:</label>
            </td>
            <td width="60%" class="adm-detail-content-cell-r">
                <input type="text" size="" maxlength="255" value="/include/" name="include_file_folder"
                       id='include_file_folder'>
            </td>
        </tr>
        <tr class="adm-detail-required-field">
            <td class="adm-detail-content-cell-l">
                <label for='include_file_name'><?= GetMessage("FILE_NAME") ?>:</label>
            </td>
            <td class="adm-detail-content-cell-r">
                <input type="phone.php" size="" maxlength="255" value="file_name.php" name="include_file_name"
                       id="include_file_name"></td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l">
                <label for='rewrite_for_exists'><?= GetMessage("REWRITE_FILE_IF_EXISTS") ?>:</label
            </td>
            <td class="adm-detail-content-cell-r">
                <input type="checkbox" size="" maxlength="255" name="rewrite_for_exists"
                       id="rewrite_for_exists">
            </td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l">
                <label for='type_file'><?= GetMessage("INCLUDE_FILE_TYPE") ?>:</label>
            </td>
            <td class="adm-detail-content-cell-r">
                <select name='type_file' id='type_file'>
                    <option value='text'>text</option>
                    <option value='html'>html</option>
                    <option value='php'>php</option>
                </select>
            </td>
        </tr>

        <tr>
            <td class="adm-detail-content-cell-l">
                <label for='rewrite_for_exists'><?= GetMessage("T_SHOW_BORDER") ?>:</label
            </td>
            <td class="adm-detail-content-cell-r">
                <input type="checkbox" size="" maxlength="255" name="show_border" checked
                       id="show_border">
            </td>
        </tr>
        <tr class="adm-detail-required-field">
            <td class="adm-detail-content-cell-l">
                <label for='include_area_text'><?= GetMessage("FILE_CONTENT") ?>:</label>
            </td>
            <td class="adm-detail-content-cell-r">
                <textarea cols='90' rows='9' id='include_area_text'></textarea>
            </td>
        </tr>

        <tr>
            <td>

            </td>
            <td>
                <button class='adm-btn' onclick='return false' id='btn-create-area'>Отправить</button>
            </td>
        </tr>

        <tr>
            <td class="adm-detail-content-cell-l">
                <label for='output_content'></label>
            </td>
            <td class="adm-detail-content-cell-r">
                <div class='result-message'>
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
            <td style='font-weight: 700'>
                <div id='result-data'></div>
            </td>
        </tr>

    </form>

    <tr>
        <td></td>
        <td>

            <button class="adm-btn btn-copy-clipboard" data-clipboard-action="copy" data-clipboard-target="#result-data"
                    style='display:none;'>Копировать в буфер
            </button>
        </td>
    </tr>
<?


$tabControl->EndTab();


$tabControl->End();
//$tabControl->ShowWarnings("smile_import", $message);


//======================================================================================================================
//BUSINESS-LOGIC:
?>

    <script>

        var sendAdminAjax = function (sendSec) {

            var resultMessageBlock = $('.result-message');
            var resultDataBlock = $('#result-data');
            var clipboardCopyBtn = $('.btn-copy-clipboard');
            resultMessageBlock.html('');
            resultDataBlock.html('');

            var saveDir = $('#include_file_folder').val();
            var fileName = $('#include_file_name').val();
            var rewriteForExists = 0;
            if ($('#rewrite_for_exists').is(':checked')) {
                rewriteForExists = 1;
            }
            var text = $('#include_area_text').val();
            var type = $('#type_file').val();
            var showBorder = 0;
            if ($('#show_border').is(':checked')) {
                showBorder = 1;
            }

            var bx_sessied = '<?=$_SESSION['fixed_session_id']?>';

            var params = {
                'saveDir': saveDir,
                'fileName': fileName,
                'rewriteForExists': rewriteForExists,
                'text': text,
                'showBorder': showBorder,
                'type': type,
                <?//Эти параметры нужны, что бы проактивный фильтр bitrix не ругался на то что мы пробуем вставить в включаемую область?>
                'sessid': bx_sessied,
                '____SECFILTER_ACCEPT_JS': 'Все равно принять'
            };

            $.ajax({
                type: 'post',//тип запроса: get,post либо head
                url: '/local/modules/slam.base/include/ajax_incl_create.php',//url адрес файла обработчика
                data: params,//параметры запроса
                response: 'text',//тип возвращаемого ответа text либо xml

                success: function (data) {//возвращаемый результат от сервера
                    try {
                        var result = JSON.parse(data);
                        resultMessageBlock.html('<pre>' + result['MESSAGE'] + '</pre>');
                        resultDataBlock.html('<pre>' + result['DATA'] + '</pre>');
                        if (result['SUCCESS'] == true) {
                            var clipboard = new Clipboard('.btn-copy-clipboard');

                            clipboardCopyBtn.show();

                        } else {
                            clipboardCopyBtn.hide();
                        }
                    }
                    catch (e) {
                        clipboardCopyBtn.hide();
                        resultDataBlock.html(data);

                        $('input[name="____SECFILTER_CONVERT_JS"]').hide();
                        $('input[name="____SECFILTER_ACCEPT_JS"]').on("click", function () {
                            return false
                        });

                        $('input[name="____SECFILTER_ACCEPT_JS"]').on("click", function () {
                            sendAdminAjax(true);
                        });

                    }
                }
            });

        };


        $("#btn-create-area").on("click", function() {
            sendAdminAjax();

            var copyTextarea = $('#result-data');
            copyTextarea.select();

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
            } catch (err) {
                console.log('Oops, unable to copy');
            }

        });



    </script>

<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");