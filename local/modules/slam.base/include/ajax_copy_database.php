<?
$MODULE_ID = 'slam.share';
define("ADMIN_MODULE_NAME", $MODULE_ID);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

function copyStage($step)
{
    $result = array();

    switch($step) {
        case 1:
            // Дампм бд со стейджа
            shell_exec('cd '.STAGE_SITE_DIR.' && mysqldump -u'.STAGE_LOGIN.' -p\''.STAGE_PASSWORD.'\' '.STAGE_DATABASE_NAME.' > '.STAGE_DATABASE_NAME.'.sql');
            $result = array('success' => 'Y', 'message' => 'Дамп stage бд создан',  'sub_message' => '');
            break;
        case 2:
            // Заливаем на dev
            shell_exec('mysql -u'.DEV_LOGIN.' -p\''.DEV_PASSWORD.'\' '.DEV_DATABASE_NAME.' < '.STAGE_SITE_DIR.'/'.STAGE_DATABASE_NAME.'.sql');
            shell_exec('rm -f '.STAGE_SITE_DIR.'/'.STAGE_DATABASE_NAME.'.sql');

            $result = array('success' => 'Y', 'message' => 'Stage бд залита на dev сайт',  'sub_message' => '');
            break;
        case 3:
            shell_exec('rsync -avzh '.STAGE_SITE_DIR.'/upload/* '.DEV_SITE_DIR.'/upload/');
            $result = array('success' => 'Y', 'message' => 'Папка upload скопирована', 'sub_message' => '');
            break;
    }

    return $result;
}


if($_REQUEST['step'] > 0){
    print json_encode(copyStage($step));
}