<?
$MODULE_ID = 'slam.share';
define("ADMIN_MODULE_NAME", $MODULE_ID);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");


function readIncludeAreaTemplate($fullPathNameFile, $type, $showBorder = 'false', $name = 'include area'){
    $filename = 'include_area.txt';
    $fp = fopen($filename, "r");
    $content = fread($fp, filesize($filename));
    fclose($fp);

    if (strpos($fullPathNameFile, '/') === 0) {
        $fullPathNameFile = substr($fullPathNameFile, 1);
    }

    $filenameStart = strrpos($name, '/');
    $filenameEnd = strrpos($name, '.');

    if($filenameEnd == 0){
        $filenameEnd = strlen($name);
    }

    $fileName = substr($name, $filenameStart + 1, $filenameEnd - $filenameStart - 1);
    $fileName = str_replace(array('-', '_', '.'), ' ', $fileName);

    $content = preg_replace('/#FULL_PATH_NAME_FILE#/i', $fullPathNameFile, $content);
    $content = preg_replace('/#TYPE#/i', $type, $content);
    $content = preg_replace('/#SHOW_BORDER#/i', $showBorder, $content);
    $content = preg_replace('/#NAME#/i', $fileName, $content);

    return $content;
}

//TODO ПРОВЕРИТЬ И ЕСЛИ НАДО УБРАТЬ СЛЕШИ В НАЗВАНИИ ДИРЕКТОРИИ И ФАЙЛОВ


$dir = '';
$fileNamePath = '';
$fileNameFullPath = '';
$success = false;
$message = '';
$data = '';

// проверяем и есле надо создаем директорию
if (strlen($_REQUEST['saveDir']) > 0) {
    $subDir = $_REQUEST['saveDir'];

    if (strpos($subDir, '/') !== 0) {
        $subDir = '/' . $subDir;
    }

    $dir = $_SERVER['DOCUMENT_ROOT'] . $subDir;


    if (!is_dir($dir)) {
        $createDir = mkdir($dir, 0755, true);
    }

} else {
    $success = false;
    $message =  'Не указана директория!';
}

// Генерируем файл для включения, если надо перезатираем уже существующий

if (strlen($dir) > 0 && strlen($_REQUEST['fileName']) > 0) {

    $text = $_REQUEST['text'];
    if (strlen($text) > 0) {

        if (strrpos($dir, '/') === strlen($dir) - 1) {
            $fileNameFullPath = $dir . $_REQUEST['fileName'];
            $fileNamePath = $subDir.$_REQUEST['fileName'];
        } else {
            $fileNameFullPath = $dir . '/' . $_REQUEST['fileName'];
            $fileNamePath = $subDir.'/'.$_REQUEST['fileName'];
        }

//    if(file_exists($fileName) && )
        $mayRewriteFile = $_REQUEST['rewriteForExists'];
        $type = $_REQUEST['type'];
        $showBorder = 'false';
        if($_REQUEST['showBorder'] == true){
            $showBorder = 'true';
        }

        if (!file_exists($fileNameFullPath)) {
            $fp = fopen($fileNameFullPath, "w");
            fwrite($fp, $text);
            fclose($fp);

            $message = 'создали файл';
            $success = true;
            $data = htmlspecialcharsEx(readIncludeAreaTemplate($fileNamePath, $type, $showBorder, $fileNamePath));


        }elseif(file_exists($fileNameFullPath) && $mayRewriteFile == true){


            $fp = fopen($fileNameFullPath, "w");
            fwrite($fp, $text);
            fclose($fp);

            $message = 'Файл '.$fileNamePath.' перезаписан.';
            $success = true;
            $data = htmlspecialcharsEx(readIncludeAreaTemplate($fileNamePath, $type, $showBorder, $fileNamePath));

        }elseif(file_exists($fileNameFullPath) && ($mayRewriteFile == false)){
            $message =  'Файл существеует и включена опция запрета на перезатирание';
        }

    }else{
        $success = false;
        $message =  'Не указан текст!';
    }
} else {
    $success = false;

    if(strlen($dir) == 0){
        $message =  'Не указана директория!';
    }else{
        $message =  'Не указан файл!';
    }

}



$result = array('SUCCESS' => $success, 'MESSAGE' => $message, 'DATA' => $data );


echo json_encode($result);


