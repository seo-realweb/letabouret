<?
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\EventManager;
use \Bitrix\Main\ModuleManager;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\IO\File;
use \Bitrix\Main\IO\Directory;
use \Bitrix\Main\Entity\Base;
use \Bitrix\Main\Application;
use \Bitrix\Main\Config as Conf;

Loc::loadMessages(__FILE__);

if (!class_exists("slam_base")) {

    class slam_base extends CModule
    {
        const MODULE_ID = 'slam.base';
        var $MODULE_ID = "slam.base";
        var $MODULE_VERSION;
        var $MODULE_VERSION_DATE;
        var $MODULE_NAME;
        var $PARTNER_NAME;
        var $MODULE_DESCRIPTION;

        function __construct()
        {
            $arModuleVersion = array();
            include($this->GetPath() . "/install/version.php");
            if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
                $this->MODULE_VERSION = $arModuleVersion["VERSION"];
                $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            }

            $this->MODULE_NAME = Loc::GetMessage("SLAM_BASE_MODULE_NAME");
            $this->MODULE_DESCRIPTION = Loc::GetMessage("SLAM_BASE_MODULE_DESCRIPTION");
            $this->PARTNER_NAME = Loc::GetMessage("SLAM_BASE_PARTNER_NAME");
            $this->PARTNER_URI = Loc::GetMessage("SLAM_BASE_PARTNER_URI");
            $this->MODULE_SORT = 1;
        }

        /**
         * @param bool $notDocumentRoot
         * @return mixed|string
         */
        public function GetPath($notDocumentRoot = false) // метод возвращает путь до корня модуля
        {
            if ($notDocumentRoot)
                return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__)); // исключаем документ роот
            else
                return dirname(__DIR__); // текущая папка с полным путем можем узнать с помощью константы __DIR__ а путь то родительского каталога узнаем ф-ей dirname()
        }

        /**
         * @return bool
         * Проверяем что система поддерживает D7
         */
        public function isVersionD7()
        {
            return CheckVersion(ModuleManager::getVersion('main'), '14.00.00');
        }


        function InstallEvents()
        {
            $eventManager = \Bitrix\Main\EventManager::getInstance();
//
//            $eventManager->registerEventHandler ("main","OnBeforeProlog",self::MODULE_ID, "Slam\\Base\\Event","OnBeforePrologHandler");
//            $eventManager->registerEventHandler ("main","OnBeforeEndBufferContent",self::MODULE_ID, "Slam\\Base\\Event","OnBeforeEndBufferContentHandler");
//            $eventManager->registerEventHandler ("main","OnEpilog",self::MODULE_ID, "Slam\\Base\\Event", "OnEpilogHandler");
//            $eventManager->registerEventHandler ("main","OnEndBufferContent",self::MODULE_ID, "Slam\\Base\\Event", "OnEndBufferContentHandler");

            $eventManager->registerEventHandler ("main","OnEndBufferContent",self::MODULE_ID, "Slam\\Base\\Event", "ChangeCssLoad");

            return true;
        }

        function UnInstallEvents()
        {
            $eventManager = \Bitrix\Main\EventManager::getInstance();
//
//            $eventManager->unRegisterEventHandler ("main","OnBeforeProlog",self::MODULE_ID, "Slam\\Base\\Event","OnBeforePrologHandler");
//            $eventManager->unRegisterEventHandler ("main","OnBeforeEndBufferContent",self::MODULE_ID, "Slam\\Base\\Event","OnBeforeEndBufferContentHandler");
//            $eventManager->unRegisterEventHandler ("main","OnEpilog",self::MODULE_ID, "Slam\\Base\\Event", "OnEpilogHandler");
//            $eventManager->unRegisterEventHandler ("main","OnEndBufferContent",self::MODULE_ID, "Slam\\Base\\Event", "OnEndBufferContentHandler");

            $eventManager->unRegisterEventHandler ("main","OnEndBufferContent",self::MODULE_ID, "Slam\\Base\\Event", "ChangeCssLoad");

            return true;
        }

        function InstallFiles($arParams = array())
        {
            CopyDirFiles(
                $this->GetPath()."/install/js/",
                $_SERVER['DOCUMENT_ROOT']."/bitrix/js/".self::MODULE_ID."/",
                true, true
            );
            CopyDirFiles(
                $this->GetPath()."/install/images/",
                $_SERVER["DOCUMENT_ROOT"]."/bitrix/images/".self::MODULE_ID."/",
                true
            );

            // Копирование файлов модуля для админки (например страницы генерации включаемых областей в админке)
            CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/".$this->MODULE_ID."/install/admin",
                $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true, true);

            return true;
        }

        function UnInstallFiles()
        {
            DeleteDirFilesEx("/bitrix/images/".self::MODULE_ID."/");
            DeleteDirFilesEx("/bitrix/js/".self::MODULE_ID."/");

            // удаление файлов модуля для админки (например страницы генерации включаемых областей в админке)
            DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/".$this->MODULE_ID."/install/admin",
                $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");

            return true;
        }


        function InstallLocal()
        {

            CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/'.self::MODULE_ID.'/', $_SERVER["DOCUMENT_ROOT"] . '/local/modules/'.self::MODULE_ID.'/', true, true, true); //если есть файлы для копирования

        }

        function DoInstall()
        {

            global $APPLICATION;
            $context = Application::getInstance()->getContext();
            $request = $context->getRequest();
            $step = intval($request["step"]);

            if (!Directory::isDirectoryExists($_SERVER["DOCUMENT_ROOT"] . '/local')) {
                $step = 2;
            }


            if(strpos($this->GetPath(), '/local/') !== false){
                $step = 2;
            }

            if ($step < 2) {
                $APPLICATION->IncludeAdminFile(Loc::getMessage("SLAM_BASE_INSTALL_TITLE"), $this->GetPath() . "/install/step1.php");
            } elseif ($step == 2) {

                if ($this->isVersionD7()) {

                    ModuleManager::registerModule($this->MODULE_ID);

                    $this->InstallEvents();
                    $this->InstallFiles();
           //         \Bitrix\Main\Config\Option::set($this->MODULE_ID, "SHOW_MESSAGE", 'Y');


                    if ($request['savedata'] == 'Y') {
                        $this->InstallLocal();
                    }

                    $APPLICATION->IncludeAdminFile(Loc::getMessage("SLAM_BASE_INSTALL_TITLE"), $this->GetPath() . "/install/step2.php");

                } else {

                    $APPLICATION->ThrowException(Loc::getMessage("SLAM_BASE_INSTALL_ERROR_VERSION"));
                    $APPLICATION->IncludeAdminFile(Loc::getMessage("SLAM_BASE_INSTALL_TITLE"), $this->GetPath() . "/install/step1.php");

                }

            }
        }

        function DoUninstall()
        {
            global $APPLICATION;
            $this->UnInstallFiles();
            $this->UnInstallEvents();
            ModuleManager::unRegisterModule($this->MODULE_ID);
            $APPLICATION->IncludeAdminFile(Loc::getMessage("ADELSHIN_PERSONE_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep.php");
        }

    }

}
?>