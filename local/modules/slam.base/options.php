<?
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

$module_id = 'slam.base';

if($_REQUEST['autosave_id']){
    $insertManually = $_REQUEST['USE_MANUALLY'] ? $_REQUEST['USE_MANUALLY'] : 'N';

}else{
    $insertManually = \Bitrix\Main\Config\Option::get($module_id, "USE_MANUALLY", '');
}

$showRightsTab = true;

$arTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::GetMessage('MSG_TAB_NAME'),
        'ICON' => '',
        'TITLE' => Loc::GetMessage('MSG_TAB_TITLE')
    )
);

$arGroups = array(
    'ADD_CSS' => array('TITLE' => Loc::GetMessage('MSG_GROUP_NAME_ADD_CSS'), 'TAB' => 0),
    'MAIN' => array('TITLE' => Loc::GetMessage('MSG_GROUP_NAME_MAIN'), 'TAB' => 0),
    'META_STRING' => array('TITLE' => Loc::GetMessage('MSG_META_STRING_TITLE'), 'TAB' => 0),
    'BODY_TOP' => array('TITLE' => Loc::GetMessage('MSG_BODY_TOP_TITLE'), 'TAB' => 0),
    'BODY_FOTER' => array('TITLE' => Loc::GetMessage('MSG_BODY_FOTER_TITLE'), 'TAB' => 0),
);


$arOptions = array(
    'ADD_CSS_MODE' => array(
        'GROUP' => 'ADD_CSS',
        'TITLE' => Loc::GetMessage('MSG_ADD_CSS_MODE'),
        'TYPE' => 'SELECT',
        "VALUES" => array('REFERENCE_ID' => array('default', 'inline'), 'REFERENCE' => array('Обычный', '«inline» режим')),
        'REFRESH' => 'Y',
        'SORT' => '4',
    ),
    'USE_ADD_CSS' => array(
        'GROUP' => 'ADD_CSS',
        'TITLE' => Loc::GetMessage('MSG_ADD_CSS_ATTR_ON_LOAD'),
        'TYPE' => 'CHECKBOX',
        'SORT' => '4',
        'DEFAULT' => 'Y',
    ),
//    'USE_MANUALLY' => array(
//        'GROUP' => 'MAIN',
//        'TITLE' => Loc::GetMessage('MSG_USE_MANUALLY_TITLE'),
//        'TYPE' => 'CHECKBOX',
//        'REFRESH' => 'Y',
//        'SORT' => '5',
//    ),
//    'NOTE' => array(
//        'GROUP' => 'META_STRING',
//        'TITLE' => Loc::GetMessage('MSG_META_STRING_NOTES_1'),
//        'TYPE' => 'NOTE',
//        'SORT' => '8'
//    ),
//    'META_STRING' => array(
//        'GROUP' => 'META_STRING',
//        'TITLE' => "",
//        'TYPE' => 'TEXT',
//        'SORT' => '10',
//        'COLS' => '67',
//        'ROWS' => '10',
//        'NOTES' => $insertManually == 'Y' ? Loc::GetMessage('MSG_META_STRING_NOTES') : '',
//    ),
//    'BODY_TOP_STRING' => array(
//        'GROUP' => 'BODY_TOP',
//        'TITLE' => "",
//        'TYPE' => 'TEXT',
//        'SORT' => '20',
//        'COLS' => '67',
//        'ROWS' => '10',
//        'NOTES' => $insertManually == 'Y' ? Loc::GetMessage('MSG_BODY_TOP_STRING_NOTES') : ''
//    ),
//    'BODY_FOTER_STRING' => array(
//        'GROUP' => 'BODY_FOTER',
//        'TITLE' => "",
//        'TYPE' => 'TEXT',
//        'SORT' => '30',
//        'COLS' => '67',
//        'ROWS' => '10',
//        'NOTES' => $insertManually == 'Y' ? Loc::GetMessage('MSG_BODY_FOTER_STRING_NOTES') : '',
//    ),
);

$dbSites = \Bitrix\Main\SiteTable::getList(array(
    'filter' => array('ACTIVE' => 'Y')
));
$aSitesTabs = $arOptionsSite = array();
while ($site = $dbSites->fetch()) {
    $arOptionsSite[$site['LID']] = $arOptions;
    $aSitesTabs[] = array('DIV' => 'opt_site_'.$site['LID'], "TAB" => '('.$site['LID'].') '.$site['NAME'], 'TITLE' => '('.$site['LID'].') '.$site['NAME'], 'LID' => $site['LID']);
}

//pr($arOptionsSite);
//pr($aSitesTabs);

$arOptions = $arOptionsSite;
if(Loader::IncludeModule($module_id)){
    $opt = new \Slam\Base\Options($module_id, $aSitesTabs, $arTabs, $arGroups,  $arOptions, $showRightsTab);
    $opt->ShowHTML();
}
?>