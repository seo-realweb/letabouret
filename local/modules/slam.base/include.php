<?
use Bitrix\Main\Loader;
$module_id = 'slam.base';

function SlamShowTitleOrHeader($template = '<h1 class="heading #CLASS#" >#TITLE#</h1>', $trigerTitleCnt = 15, $extClass = '')
{
    global $APPLICATION;
    $flgShowTitle = $APPLICATION->GetProperty("HideTitle", 'N') == 'N' ? true : false;

    if($flgShowTitle){
        if ($APPLICATION->GetPageProperty("ADDITIONAL_TITLE")) {
            return $APPLICATION->GetPageProperty("ADDITIONAL_TITLE");
        }else {
            $strTitle = htmlspecialchars_decode($APPLICATION->GetTitle(false));


            $class = $extClass;

            if(strlen($strTitle) > $trigerTitleCnt){
                $class .= ' small';
            }else{
                $class .= '';
            }
            return str_replace(array('#TITLE#', '#CLASS#'), array($strTitle , $class), $template);
        }
    }
}

function SlamShowBrowserTitle($isInner, $getMsg, $template = '', $delimiter = ' — ')
{
    $arTitle = strip_tags(htmlspecialchars_decode($GLOBALS['APPLICATION']->GetPageProperty('title')));
    if(strlen($arTitle)<=0){
        $arTitle = $GLOBALS['APPLICATION']->GetTitle(false);
    }

    if($isInner && strlen($getMsg) > 0){
        $arTitle .= $delimiter.$getMsg;
    }

    foreach($_GET as $key => $val) {
        if (preg_match('/^PAGEN/', $key)) {
            if($val && intval($val)>0){
                $arTitle .= $delimiter.'страница '.$val;
                break;
            }
        }
    }
    return $arTitle;
}



unset(Loader::$semiloadedModules[$module_id]);


if (!Loader::includeModule($module_id))
    return false;



\Bitrix\Main\Loader::registerAutoLoadClasses(
    $module_id,
    array(
        '\Slam\Base\CSlamAsset' => 'lib/CSlamAsset.php',
    )
);




//$arClasses = array( "\Slam\Base\CSlamMenuIcons" => "lib/icons.php" );
//\Bitrix\Main\Loader::registerAutoLoadClasses("slam.base", $arClasses);
?>