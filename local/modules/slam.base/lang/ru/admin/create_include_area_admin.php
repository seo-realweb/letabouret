<?
$MESS["SLAM_INCLUDE_AREA_MODULE"] = "SLAM: генерация включаемых областей";
$MESS["INCLUDE_AREA_TAB"] = "Главная";
$MESS["INCLUDE_AREA_TAB_TITLE"] = "Создание включаемых файлов";
$MESS["SAVE_DIR"] = "Папка, куда сохранять";
$MESS["FILE_NAME"] = "Название файла";
$MESS["REWRITE_FILE_IF_EXISTS"] = "Перезатирать при совпадении";
$MESS["INCLUDE_FILE_TYPE"] = "Тип включаемого файла";
$MESS["FILE_CONTENT"] = "Содержания файла";
$MESS["OUTPUT_CONTENT"] = "Результат";
$MESS["T_SHOW_BORDER"] = "Показывать ли рамку (SHOW_BORDER)";