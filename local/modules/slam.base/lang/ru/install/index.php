<?
$MESS["SLAM_BASE_MODULE_NAME"] = "SLAM: базовый модуль";
$MESS["SLAM_BASE_MODULE_DESCRIPTION"] = "В модуле собраны нарботки для удобной разработки проекта: инструменты для контент-менеджера и разработчика, вспомогательные настройки, продуманные компоненты";
$MESS["SLAM_BASE_INSTALL_TITLE"] = "Установка модуля '{$MESS["SLAM_SHAREWATERMARK_INSTALL_NAME"] }'";
$MESS["SLAM_BASE_UNINSTALL_ERROR"] = "Обнаружены ошибки при удалении модуля";
$MESS["SLAM_BASE_PARTNER_NAME"] = "S.L.A.M.";
$MESS["SLAM_BASE_PARTNER_URI"] = "https://slam.by";

$MESS["SLAM_INSTAL_SAVE_LOCAL"] = 'Установить модуль в папку /local/';
$MESS["SLAM_INSTAL_SAVE"] = 'Сохранить';
