<?
$MESS ["SLAM_IMAGE_OPTIONS"] = "настройки";
$MESS ["SLAM_IMAGE_MAIN"] = "Общие параметры";
$MESS ["MAIN_OPTIONS_TAB"] = "Настройки";
$MESS ["PARAM_OPTIONS"] = "Настройка параметров модуля";
$MESS ["SLAM_IMAGE_METHOD_TITLE"] = "Режим работы модуля";
$MESS ["SLAM_IMAGE_TITLE_JPEG_COMPRESS"] = "Настройки обработки изображений jpeg/jpg";
$MESS ["SLAM_IMAGE_TITLE_PNG_COMPRESS"] = "Настройки обработки изображений png";
$MESS ["SLAM_IMAGE_TITLE_WEBP_COMPRESS"] = "Настройки обработки изображений webp";
$MESS ["SLAM_IMAGE_TITLE_ADDITIONAL_COMPRESS"] = "Дополнительные настройки";
$MESS ["SLAM_IMAGE_JPEG_COMPRESS"] = "Качество изображений jpeg/jpg:";
$MESS ["SLAM_IMAGE_PNG_COMPRESS"] = "Качество изображений png:";
$MESS ["SLAM_IMAGE_WEBP_COMPRESS"] = "Качество изображений webp:";
$MESS ["SLAM_IMAGE_WEBP_USE_COMPRESS"] = "Использовать формат изображений webp";
$MESS ["SLAM_IMAGE_METOD_COMPRESS"] = "Режим сжатия изображений";
$MESS ["SLAM_IMAGE_CLEAR_CACHE_COMPRESS"] = "Очистка кеша изображений:";
$MESS ["SLAM_IMAGE_CLEAR_CACHE_TEXT_COMPRESS"] = "Очистить";
$MESS ["SLAM_IMAGE_ERROR_CLEAR_CACHE"] = "Ошибка удаления кеша.";
$MESS ["SLAM_IMAGE_CLEAR_CACHE_YES"] = "Готово!";

$MESS ["SLAM_IMAGE_METHOD_NONE"] = "Без сжатия (отключить сжатие)";
$MESS ["SLAM_IMAGE_METHOD_DEFAULT"] = "Обычный (png и jpeg)";
$MESS ["SLAM_IMAGE_METHOD_WEBP"] = "Google (webp)";
$MESS ["SLAM_IMAGE_METHOD_MULTI"] = "Совмещенный (png и jpeg + webp)";

$MESS ["SLAM_IMAGE_LIB_WEBP_YES"] = "Библиотека <b style='color:green;'>cwebp</b> установлена на сервере. Обработка изображений формата <b>.webp</b> будет работать корректно.";
$MESS ["SLAM_IMAGE_LIB_JEPG_YES"] = "Библиотека <b style='color:green;'>mozjpeg</b> установлена на сервере. Обработка изображений формата <b>.jpeg/jpg</b> будет работать корректно.";
$MESS ["SLAM_IMAGE_LIB_PNG_YES"] = "Библиотека <b style='color:green;'>pngquant</b> установлена на сервере. Обработка изображений формата <b>.png</b> будет работать корректно.";

$MESS ["SLAM_IMAGE_LIB_WEBP_USE_DEFAULT"] = "<p id='text_default'>При обработке картинок будут использованы алгоритмы сжатия, рекомендованные сервисом <a href='https://developers.google.com/speed/pagespeed/insights/' target='_blank'>PageSpeed Insights</a>.<br>
Оригиналы картинок не будут затерты - сжатые копии картинок хранятся в отдельной папке <a href='/bitrix/admin/fileman_admin.php?PAGEN_1=1&SIZEN_1=20&lang=ru&site=s1&path=%2Fupload%2Fresize_cache%2Fslam.image&show_perms_for=0' target='_blank'>/upload/resize_cache/slam.image/</a>  </p>";

$MESS ["SLAM_IMAGE_LIB_WEBP_USE_WEBP"] = "<p id='text_webp'>Все картинки будут сконвертированны в формат <a href='https://habr.com/ru/post/275735/' target='_blank'>.webp</a>. Данный формат разработан и рекомендован компанией Google. <br>
Для корректного отображения картинок .webp во всех браузерах будет использоваться JS-библиотека(<a href='http://webpjs.appspot.com' target='_blank'>webpjs</a>).</p>";

$MESS ["SLAM_IMAGE_LIB_WEBP_USE_SERVER"] = "<p id='text_multi'>При обработке картинок будет создаваться 2 картинки: сжатая копия картинки оригинального формата (.png или .jpeg) и версия <a href='https://habr.com/ru/post/275735/' target='_blank'>.webp</a>. <br>
Для корректного отображения картинок .webp во всех браузерах <b>необходимо внести правки в конфигурацию NGNIX</b> после которых браузерам, <br>
поддерживающим технологию  WebP будет отдаваться картинки .webp, а остальным оптимизированные копии оригинальных форматов (.jpeg и .png).   <br>
<br>
<a href='/local/modules/slam.image/instruction/instruction.txt' download='Инструкция по настройке webp  NGNIX для BitrixWM'><b>Инструкция по настройке webp  NGNIX для BitrixWM (пример настройки нашего сервера)</b></a>.<br>
<a target='_blank' href='https://habr.com/ru/post/281124/'><b>Сама статья на habr</b></a><br>
<a target='_blank' href='https://github.com/igrigorik/webp-detect/'><b>Полные примеры config-файлов для различных конфигураций </b></a>
</p>";

$MESS ["SLAM_IMAGE_LIB_WEBP_NON"] = "Библиотека <b style='color:red;'>cwebp</b> НЕ установлена на сервере. Обработка изображений формата <b>webp</b> не будет работать.
<br><br>Ниже показана инструкция установки библиотеки через консоль на Centos OS:<br><br>yum install libwebp-tools<br>";

$MESS ["SLAM_IMAGE_LIB_JEPG_NON"] = "Библиотека <b style='color:red;'>mozjpeg</b> НЕ установлена на сервере. Обработка изображений формата <b>jpeg/jpg</b> не будет работать.
<br><br>
Ниже показана инструкция установки библиотеки через консоль на Centos OS:<br><br>
wget https://github.com/mozilla/mozjpeg/archive/v3.3.1.tar.gz<br>
tar -zxvf v3.3.1.tar.gz <br>
cd mozjpeg-3.3.1/<br>
autoreconf -fiv<br>
mkdir build && cd build<br>
sh ../configure<br>
sudo make install";

$MESS ["SLAM_IMAGE_LIB_PNG_NON"] = "Библиотека <b style='color:red;'>pngquant</b> НЕ установлена на сервере. Обработка изображений формата <b>png</b> не будет работать.
<br><br>Ниже показана инструкция установки библиотеки через консоль на Centos OS:<br><br>yum install pngquant<br>";
$MESS['SLAM_IMAGE_LIB_BITRIX_VM_VERSION'] = "Модуль <b style='color:red;'>SLAM.IMAGE</b>  работает только с виртуальной машиной <b style='color:red;'>Bitrix 7й</b> версии и выше";
$MESS['SLAM_WEBP_REPLACE_NON'] = "Замена изображения на WEBP <b style='color:red;'> не удалась </b>";
$MESS['SLAM_WEBP_REPLACE_YES'] = "Замена изображения на WEBP <b style='color:green;'>прошла успешна</b>";
$MESS['SLAM_IMAGE_BITRIX_CLEAR_CACHE_INFO'] = 'После удаления файлов кеша выводимые данные будут обновлены до актуального состояния. Новые файлы кеша будут создаваться постепенно по мере обращений к страницам с закешированными областями.';
$MESS['SLAM_IMAGE_BITRIX_CLEAR_CACHE'] = 'Очистка файлов кеша системы';

