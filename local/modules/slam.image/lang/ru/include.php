<?
$MESS["SLAM_IMAGE_ACCESS_DENIED"] = "Доступ запрещен";
$MESS["SLAM_IMAGE_MAIN_OPTION"] = "Общие параметры";
$MESS["ERROR_DETECTED"] = "Обнаружены ошибки";
$MESS["SLAM_IMAGE_DEL"] = "Удалить";

$MESS["SLAM_IMAGE_FIELD_ID"] = "ID";
$MESS["SLAM_IMAGE_FIELD_IMAGE_NAME"] = "Имя файла";
$MESS["SLAM_IMAGE_FIELD_IMAGE_SRC"] = "Изображение";
$MESS["SLAM_IMAGE_FIELD_IMAGE_SIZE"] = "Размер до сжатия";
$MESS["SLAM_IMAGE_FIELD_IMAGE_NEW_SIZE"] = "Размер после сжатия";

$MESS["SLAM_IMAGE_COMPRESS_ALL"] = "Сжать все картинки";


$MESS["SLAM_IMAGE_COMPRESS"] = "Сжать";
$MESS["SLAM_IMAGE_ERROR_IMAGE_SRC"] = "Файл на диске не найден";


$MESS["SLAM_IMAGE_GET_NAV_PRINT"] = "Страница";
$MESS["SLAM_IMAGE_INDEX_BTN"] = "Запусить";
$MESS["SLAM_IMAGE_INDEX"] = "Запусить переиндексацию";
$MESS["SLAM_IMAGE_INDEX_SL"] = "1. Сначала нажмите на эту кнопку";


include 'admin/menu.php';
include 'install.php';
?>