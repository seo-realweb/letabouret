<?
$MESS["SLAM_IMAGE_IN_PROGRESS"] = '<b>Идёт поиск...</b><br> Текущий файл:';
$MESS["SLAM_IMAGE_FILE_NOT_FOUND"] = 'Файл не найден:';
$MESS["SLAM_IMAGE_COMPLETED"] = "Поиск завершен";
$MESS["SLAM_IMAGE_INDEX_BTN"] = "Запусить";
$MESS["SLAM_IMAGE_INDEX"] = "Идексация каталогов";
$MESS["SLAM_IMAGE_COMPRESS"] = "Запусить переиндексацию";
$MESS["SLAM_IMAGE_COMPRESS_ALL"] = "Сжать все картинки";
$MESS["SLAM_IMAGE_DEL"] = "Удалить";
$MESS["SLAM_IMAGE_GET_NAV_PRINT"] = "Страница";
$MESS["SLAM_IMAGE_FIELD_MIME_TYPE"] = "Тип файла";
$MESS["SLAM_IMAGE_RESIZE_DATE"] = "Дата сжатия файла";

$MESS["SLAM_IMAGE_FIELD_ID"] = "ID";
$MESS["SLAM_IMAGE_FIELD_IMAGE_NAME"] = "Имя файла";
$MESS["SLAM_IMAGE_FIELD_IMAGE_SRC"] = "Оригинал изображения";
$MESS["SLAM_IMAGE_FIELD_IMAGE_SIZE"] = "Размер файла оригинал / сжатый (.webp)";
$MESS["SLAM_IMAGE_FIELD_IMAGE_RESIZE_SIZE"] = "Размер после сжатия";
$MESS["ORIGINAL_FILE_SIZE"] = "Размер оригинального изображения";
$MESS["COMPRESS_FILE_SIZE"] = "Размер обжатого изображения";
$MESS["WEBP_FILE_SIZE"] = "Размер обжатого webp изображения";


$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Выбрать";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Отметить";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "Удалить";

$MESS["SLAM_IMAGE_FIELD_URL"] = "Страница сайта";
$MESS["SLAM_IMAGE_FIELD_PATH_NAME"] = "Путь к файлу";
$MESS["SLAM_IMAGE_FIELD_IMAGE_RESIZE_SRC"] = "Сжатое изображение";
$MESS["SLAM_IMAGE_FIELD_IMAGE_WEBP_SRC"] = "webp изображение";

$MESS["SLAM_IMAGE_ERROR_IMAGE_SRC"] = "Файл на диске не найден";

?>