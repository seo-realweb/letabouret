<?
$MESS["SLAM_IMAGE_PARTNER_NAME"] = "S.L.A.M.";
$MESS["SLAM_IMAGE_PARTNER_URI"] = "https://slam.by";
$MESS["SLAM_IMAGE_MODULE_NAME"] = "SLAM: Оптимизации изображений";
$MESS["SLAM_IMAGE_MODULE_DESCRIPTION"] = "Модуль оптимизации изображений под Google PageSpeed";
$MESS["SLAM_IMAGE_INSTALL_TITLE"] = "Установка модуля \"{$MESS["SLAM_IMAGE_MODULE_NAME"] }\"";
$MESS["SLAM_IMAGE_UNINSTALL_TITLE"] = "Удаление модуля \"{$MESS["SLAM_IMAGE_MODULE_NAME"] }\"";
$MESS["SLAM_IMAGE_UNINSTALL_ERROR"] = "Обнаружены ошибки при удалении модуля";
$MESS["SLAM_IMAGE_INSTALL_ERROR_VERSION"] = "Ядро D7 1С-Битрикса на вашем сайте не поддерживается ";
$MESS["SLAM_IMAGE_INSTALL_GO_TO_SETTINGS"] = 'Перейти к настройкам модуля';
$MESS["SLAM_INSTAL_SAVE"] = 'Сохранить';
