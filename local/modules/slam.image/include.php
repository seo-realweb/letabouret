<?

use Slam\Image\ImageCompress;

function GetCompressImage($file, $quality=false){
    return CSlamImage::GetCompressImage($file, $quality);
}

function createWebp($file, $quality){
    return ImageCompress::getInstance()->compressToWebp($file, $quality);
}

class CSlamImage
{
    public static function ResizeImageGet($file, $arSize, $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL, $bInitSizes = false, $arFilters = false, $bImmediate = false, $jpgQuality = false){
        $arFileTmp = \CFile::ResizeImageGet(
            $file,
            $arSize,
            $resizeType,
            $bInitSizes,
            $arFilters,
            $bImmediate,
            $jpgQuality
        );

        if(strlen($arFileTmp['src']) > 0){
            $arFileTmp['src'] = Slam\Image\ImageCompress::getInstance()->compressImg($arFileTmp['src'], false);
        }

        return $arFileTmp;
    }

    public static function GetCompressImage($file, $quality=false){

        return Slam\Image\ImageCompress::getInstance()->compressImg($file, $quality);
    }

}
?>