<?
namespace Slam\Image;
use Bitrix\Main\Entity,
    Bitrix\Main\Application,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Localization\Loc;
    ImageCompress;

Loc::loadMessages(__FILE__);

class ImageTable extends Entity\DataManager
{
    public static $MAX_TIME = 10;
    public static $IMAGE_EXTENSIONS = array("png", "jpg", "jpeg");
    public $MODULE_ID = 'slam.image';
    public $LAST_ERROR;
    private
        $jpegoptim = false,
        $pngoptim = false,
        $jpegOptimCompress,
        $pngOptimCompress,
        $jpegProgress = false;

    public static function getTableName()
    {
        return 'slam_image';
    }

    public static function getMap()
    {
        return array(
            'ID' => new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('SLAM_IMAGE_FIELD_ID')
            )),
            'URL' => new Entity\StringField('URL', array(
                'default_value' => null,
                'title' => Loc::getMessage('SLAM_IMAGE_URL')
            )),
            'PATH_NAME' => new Entity\StringField('PATH_NAME', array(
                'default_value' => null,
                'title' => Loc::getMessage('SLAM_IMAGE_FIELD_PATH_NAME')
            )),
            'IMAGE_MIME_TYPE' => new Entity\StringField('IMAGE_MIME_TYPE', array(
                'default_value' => null,
                'title' => Loc::getMessage('SLAM_IMAGE_FIELD_MIME_TYPE')
            )),
            'IMAGE_SRC' => new Entity\StringField('IMAGE_SRC', array(
                'title' => Loc::getMessage('SLAM_IMAGE_FIELD_IMAGE_SRC'),
            )),
            'IMAGE_RESIZE_SRC' => new Entity\StringField('IMAGE_RESIZE_SRC', array(
                'title' => Loc::getMessage('SLAM_IMAGE_FIELD_IMAGE_RESIZE_SRC')
            )),
            'IMAGE_WEBP_SRC' => new Entity\StringField('IMAGE_WEBP_SRC', array(
                'title' => Loc::getMessage('SLAM_IMAGE_FIELD_IMAGE_WEBP_SRC')
            )),
            'IMAGE_RESIZE_DATE' => new Entity\DateField('IMAGE_RESIZE_DATE', array(
                'title' => Loc::getMessage('SLAM_IMAGE_RESIZE_DATE')
            )),
            'ORIGINAL_FILE_SIZE' => new Entity\IntegerField('ORIGINAL_FILE_SIZE', array(
                'title' => Loc::getMessage('ORIGINAL_FILE_SIZE')
            )),
            'COMPRESS_FILE_SIZE' => new Entity\IntegerField('COMPRESS_FILE_SIZE', array(
                'title' => Loc::getMessage('COMPRESS_FILE_SIZE')
            )),
            'WEBP_FILE_SIZE' => new Entity\IntegerField('WEBP_FILE_SIZE', array(
                'title' => Loc::getMessage('WEBP_FILE_SIZE')
            )),
            'ORIGINAL_FILE_SUM' => new Entity\ExpressionField('ORIGINAL_FILE_SUM',
                '(SUM(%s)', ['ORIGINAL_FILE_SIZE']
            ),
            'COMPRESS_FILE_SUM' => new Entity\ExpressionField('COMPRESS_FILE_SUM',
                '(SUM(%s)', ['COMPRESS_FILE_SIZE']
            ),
            'WEBP_FILE_SUM' => new Entity\ExpressionField('WEBP_FILE_SUM',
                '(SUM(%s)', ['WEBP_FILE_SIZE']
            )
        );
    }

    function truncateTable($arFilter = array())
    {
        $connection = Application::getConnection();
        return $connection->query("TRUNCATE TABLE {$this->getTableName()}");
    }

    public static function replaceDoubleSflesh(&$path)
    {
        $path = str_replace('\\', '/', $path);
        do {
            $path = str_replace('//', '/', $path, $cnt);
        } while ($cnt);
    }

    public static function fileSize($s)
    {
        $i = 0;
        $ar = array('b', 'kb', 'M', 'G');
        while ($s > 1024) {
            $s /= 1024;
            $i++;
        }
        return round($s, 1) . ' ' . $ar[$i];
    }

    public static function strposa($haystack, $needle, $offset = 0)
    {
        if (!is_array($needle)) $needle = array($needle);
        foreach ($needle as $query) {
            if (self::binStrpos($haystack, $query, $offset) !== false) return true; // stop on first true result
        }
        return false;
    }

    public static function binStrpos($s, $a)
    {
        if (function_exists('mb_orig_strpos'))
            return mb_orig_strpos($s, $a);

        if ($s && $a)
            return strpos($s, $a);
    }

    public static function ShowMsg($str, $color = 'green')
    {
        \CAdminMessage::ShowMessage(array(
            "MESSAGE" => '',
            "DETAILS" => $str,
            "TYPE" => $color == 'green' ? "OK" : 'ERROR',
            "HTML" => true));
    }

    public static function SearchImage($path, $arDir = array())
    {

        self::replaceDoubleSflesh($path);

        if (time() - START_TIME > self::$MAX_TIME) {
            if (!defined('BREAK_POINT'))
                define('BREAK_POINT', $path);

            return;
        }

        if (defined('SKIP_PATH') && !defined('FOUND')) {
            if (0 !== self::binStrpos(SKIP_PATH, dirname($path)))
                return;

            if (SKIP_PATH == $path)
                define('FOUND', true);
        }

        if (is_dir($path)) // dir
        {
            $p = realpath($path);

            if ($path != $_SERVER['DOCUMENT_ROOT'] && self::strposa($path, $arDir, 1) === false) {
                return;
            }


            if (is_link($path)) {
                $d = dirname($path);
                if (strpos($p, $d) !== false || strpos($d, $p) !== false)
                    return true;
            }


            $dir = opendir($path);
            while ($item = readdir($dir)) {
                if ($item == '.' || $item == '..') {
                    continue;
                }

                $file = $path . DIRECTORY_SEPARATOR . $item;
                self::SearchImage($file, $arDir);

            }
            closedir($dir);
        } else // file
        {
            if (!defined('SKIP_PATH') || defined('FOUND'))
                $arInfo = pathinfo($path);

            if (!empty($path) && !empty($arDir) && self::strposa($path, $arDir, 1) === false) {
                return;
            }

            if (!empty($arInfo) && in_array(strtolower($arInfo["extension"]), self::$IMAGE_EXTENSIONS)) {

                $result = self::Add(
                    array(
                        "IMAGE_NAME" => $arInfo['basename'],
                        'IMAGE_SRC' => str_replace($_SERVER['DOCUMENT_ROOT'], '', $arInfo['dirname'] . '/' . $arInfo['basename']),
                        'IMAGE_SIZE' => filesize($arInfo['dirname'] . '/' . $arInfo['basename']),
                    )
                );
            }
        }
    }

    public static function getSearchSection($path = "")
    {
        self::replaceDoubleSflesh($path);

        if ($handle = opendir($path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $file = $path . DIRECTORY_SEPARATOR . $entry;
                    if (is_dir($file)) {
                        $arResult[$file] = $entry;
                    }
                }
            }
            closedir($handle);
        }

        if (!empty($arResult)) {
            asort($arResult);
        }

        return $arResult;
    }

    public static function getTreeSectionUl($arSiteSections, $checked = true)
    {
        if ($arSiteSections) {
            echo "<ul style=\"list-style:none\">";
            foreach ($arSiteSections as $path => $entry) {
                self::getTreeSectionLi($path, $entry, $checked);
            }
            echo "</ul>";
        }
    }

    public static function getTreeSectionLi($path, $entry, $checked = true)
    {

        echo "<li style=\"list-style:none\" id=\"list-" . md5($path) . "\">";
        echo "<input type=\"checkbox\" value=\"" . $path . "\" onclick=\"setActive(this, '" . md5($path) . "')\"  " . ($checked ? "checked='checked'" : '') . ">";
        echo "<a href=\"javascript:void(0)\" onclick=\"getDirectory('" . md5($path) . "', '" . $path . "')\">" . $entry . "</a>";
        echo "</li>";
    }

    public static function getTreeSection($title, $path)
    {
        self::replaceDoubleSflesh($path);

        echo "<ul style=\"padding:0;list-style:none\">";
        echo "<li style=\"list-style:none\" id=\"list-" . md5($path) . "\">";
        echo "<input type=\"checkbox\" value=\"" . $path . "\" onclick=\"setActive(this, '" . md5($path) . "')\" checked=\"checked\">";
        echo "<a href=\"javascript:void(0)\" onclick=\"getDirectory('" . md5($path) . "', '" . $path . "')\">" . $title . "</a>";
        $arSiteSections = self::getSearchSection($path);
        self::getTreeSectionUl($arSiteSections);
        echo "</li>";
        echo "</ul>";

    }

    public function isPNGOptim()
    {
        if (!$this->pngoptim) {
            exec(Option::get(self::$MODULE_ID, 'path_to_optipng', '/usr/bin') . '/optipng -v', $s);
            if ($s) $this->pngoptim = true;
        }
        return $this->pngoptim;
    }

    public function isJPEGOptim()
    {
        if (!$this->jpegoptim) {
            exec(Option::get(self::$MODULE_ID, 'path_to_jpegoptim', '/usr/bin') . '/jpegoptim --version', $s);
            if ($s) $this->jpegoptim = true;
        }
        return $this->jpegoptim;
    }

    public function compressJPG($strFilePath)
    {
        $res = false;
        if (!$this->isJPEGOptim()) {
            $this->LAST_ERROR = 'error';
            return $res;
        }
        if (file_exists($strFilePath)) {
            $strFilePath = strtr(
                $strFilePath,
                array(
                    ' ' => '\ ',
                    '(' => '\(',
                    ')' => '\)',
                    ']' => '\]',
                    '[' => '\[',
                )
            );
            $strCommand = '';
            if ($this->jpegProgress) {
                $strCommand .= '--all-progressive';
            }
            $strCommand .= ' --strip-all -t';
            if ($this->jpegOptimCompress) {
                $strCommand .= " -m{$this->jpegOptimCompress}";
            }
            exec(Option::get(self::$MODULE_ID, 'path_to_jpegoptim', '/usr/bin') . "/jpegoptim $strCommand $strFilePath 2>&1", $res);
            chmod($strFilePath, BX_FILE_PERMISSIONS);
        }
        return $res;
    }

    public function compressPNG($strFilePath)
    {
        $res = false;
        if (!$this->isPNGOptim()) {
            $this->LAST_ERROR = 'error';
            return $res;
        }
        if (file_exists($strFilePath)) {
            $strFilePath = strtr(
                $strFilePath,
                array(
                    ' ' => '\ ',
                    '(' => '\(',
                    ')' => '\)',
                    ']' => '\]',
                    '[' => '\[',
                )
            );
            exec(Option::get(self::$MODULE_ID, 'path_to_optipng', '/usr/bin') . "/optipng -strip all -o{$this->pngOptimCompress} $strFilePath 2>&1", $res);
            chmod($strFilePath, BX_FILE_PERMISSIONS);
        }
        return $res;
    }

    public function compressImageByIdResult($intFileID){
        $res = false;
        if(intval($intFileID) <= 0) return null;


        $strFile = false;
        $rsData =  self::getById($intFileID)->exec();
        if ($arRes = $rsData->Fetch()) {
            $strFile = $_SERVER["DOCUMENT_ROOT"] . $arRes['IMAGE_SRC'];
        }

        if(file_exists($strFile)){

            $arInfo = pathinfo($strFile);

            switch (strtolower($arInfo["extension"])) {
                case 'jpg' :
                    $isCompress = $this->compressJPG($strFile);
                    break;
                case 'jpeg' :
                    $isCompress = $this->compressJPG($strFile);
                    break;
                case 'png' :
                    $isCompress = $this->compressPNG($strFile);
                    break;
                default :
                    $this->LAST_ERROR = 'error';
                    return null;
            }

            if($isCompress) {
                clearstatcache(true,$strFile);
                $res = self::update($intFileID, array('IMAGE_NEW_SIZE' => filesize($strFile)));
            }
        } else {
            return null;
        }
        return $res;
    }

    public static function compressImageByUrlResult($HOST, $PATH, $FILE_NAME)
    {

        $PATH = str_replace($_SERVER["DOCUMENT_ROOT"], "", $PATH);

        $arResult = array("STATUS" => "progress", "HOST" => $HOST, "PATH" => $PATH, "FILE_NAME" => $FILE_NAME);
        if (function_exists('curl_init')) {
            if ($curl = curl_init()) {
                curl_setopt($curl, CURLOPT_URL, 'http://188.225.34.116/test_optimization/result_' . md5($HOST));

                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_TIMEOUT, 1);
                $out = curl_exec($curl);

                curl_close($curl);
            }
        } else {
            $sURL = 'http://188.225.34.116/test_optimization/result_' . md5($HOST);
            $out = file_get_contents($sURL);
        }

        $arr = json_decode($out, true);

        $PATH = trim($PATH, "/");
        $arr["FILE_PATH"] = trim($arr["FILE_PATH"], "/");

        if ($arr["FILE_PATH"] == $PATH && $arr["FILE_NAME"] == $FILE_NAME) {
            return $out;
        } elseif (isset($arr["FILE_PATH"]) && isset($arr["FILE_NAME"]) && ($arr["FILE_PATH"] != $PATH || $arr["FILE_NAME"] != $FILE_NAME)) {
            $arResult = array("STATUS" => "error", "HOST" => $HOST, "PATH" => $PATH, "FILE_NAME" => $FILE_NAME);
        }
        return json_encode($arResult);
    }


}