<?
namespace Slam\Image;
use Bitrix\Main\Entity,
    Bitrix\Main\Config\Option;

define('LOG_FILENAME',  $_SERVER["DOCUMENT_ROOT"]."/log.txt");

class ImageCompress extends Entity\DataManager
{

    public static $IMAGE_EXTENSIONS = array("png", "jpg", "jpeg");
    CONST MODULE_ID = 'slam.image';
    public $DOCUMENT_ROOT;
    public $LAST_ERROR;
    private
        $jpegoptim = false,
        $webpoptim = false,
        $pngoptim = false,
        $useWebp = false,
        $jpegOptimCompress,
        $pngOptimCompress,
        $webpOptimCompress,
        $Metod,
        $prepareData = [];

    private static $instance;

    public function __construct(){
        $this->jpegOptimCompress = Option::get(self::MODULE_ID,'jpegoptim_compress', 70);
        $this->pngOptimCompress = Option::get(self::MODULE_ID,'pngoptim_compress', 85);
        $this->webpOptimCompress = Option::get(self::MODULE_ID,'webpoptim_compress', 70);
        $this->Metod = Option::get(self::MODULE_ID,'webpmetodoptim_compress', 'none');
        if($this->Metod == 'multi' || $this->Metod == 'webp' ){
            $this->useWebp = true;
        }

        $this->DOCUMENT_ROOT = \Bitrix\Main\Application::getDocumentRoot();
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }

    /**
     * Check Bitrix VM version
     */
    public function isBitrixSeven(){
        exec('cd / && cat /etc/issue', $serverInfo);
        preg_match_all("/(Bitrix VM)|(7.\d{1}.\d{1})/", $serverInfo[0], $matches);

        if(count($matches[0]) >= 2){
           return true;
        }

        return false;
    }

    /**
     * Check pngquant is install
     * @return bool
     */
    public function isPNGOptim()
    {
        if (!$this->pngoptim) {
            exec('/usr/bin/pngquant --version', $s);
            if ($s) $this->pngoptim = true;
        }

        return $this->pngoptim;
    }

    /**
     * Check cwebp is install
     * @return bool
     */
    public function isWEBPOptim()
    {
        if (!$this->webpoptim) {
            exec('cwebp -version', $s);
            if ($s) $this->webpoptim = true;
        }

        return $this->webpoptim;
    }

    /**
     * Check cjpeg is install
     * @return bool
     */
    public function isJPEGOptim()
    {
        if (!$this->jpegoptim) {
            /*$str = 'cjpeg -version 2>&1';
            exec($str, $s); */

            if (file_exists('/usr/bin/cjpeg') || file_exists('/opt/mozjpeg/bin/cjpeg')) {
                $this->jpegoptim = true;
            } else {
                $this->jpegoptim = false;
            }

            return $this->jpegoptim;

        }

        return $this->jpegoptim;
    }

    public function compressJPG($serverPath, $incomingPath, $quality_ex){
        $res = $incomingPath;
        if (!$this->isJPEGOptim()) {
            $this->LAST_ERROR = 'error';
            return $res;
        }


        if (file_exists($serverPath)) {
            $strFilePath = strtr(
                $serverPath,
                array(
                    ' ' => '\ ',
                    '(' => '\(',
                    ')' => '\)',
                    ']' => '\]',
                    '[' => '\[',
                )
            );

            if($quality_ex && $quality_ex > 0){
                $quality = $quality_ex;
            }else{
                $quality = $this->jpegOptimCompress;
            }

            $newFileName = $this->getNewFilePath($strFilePath, true, false, $quality_ex);
            $newServerFileName = $this->DOCUMENT_ROOT.$newFileName;

            shell_exec("cjpeg -optimize -progressive -quality ".$quality." ".$serverPath." > ".$newServerFileName." ");

            if(file_exists($newServerFileName)) {
                $res = $newFileName;
                $this->prepareData['new_file'] = $newFileName;
            }
        }

        return $res;
    }

    /**
     * Create WEBP file
     * @param $serverPath
     * @param $incomingPath
     * @param $quality_ex
     * @param bool $change_path - ставим в true если файл сохраняем в той же директории
     * @return string
     */
    public function convertToWebp($serverPath, $incomingPath, $quality_ex, $change_path = false){
        $res = $incomingPath;
        if (!$this->isWEBPOptim()) {
            $this->LAST_ERROR = 'error';
            return $res;
        }

        if (file_exists($serverPath)) {
            $path = strtr(
                $serverPath,
                array(
                    ' ' => '\ ',
                    '(' => '\(',
                    ')' => '\)',
                    ']' => '\]',
                    '[' => '\[',
                )
            );

            if($quality_ex && $quality_ex > 0){
                $quality = $quality_ex;
            }else{
                $quality = $this->webpOptimCompress;
            }

            if(!$change_path){
                $newFileName = $this->getNewFilePath($path, true, true, $quality_ex);
            }else{
                $newFileName = $incomingPath.'.webp';
            }

            $newServerFileName = $this->DOCUMENT_ROOT.$newFileName;

            shell_exec("cwebp -q $quality ".$path." -o ".$newServerFileName . " ");

            if(file_exists($newServerFileName)) {
                $res = $newFileName;
                $this->prepareData['webp_file'] = $newFileName;
            }
        }

        return $res;
    }

    public function compressPNG($serverPath, $incomingPath, $quality_ex){
        $res = $incomingPath;
        if (!$this->isPNGOptim()) {
            $this->LAST_ERROR = 'error';
            return $res;
        }

        if (file_exists($serverPath)) {

            $strFilePath = strtr(
                $serverPath,
                array(
                    ' ' => '\ ',
                    '(' => '\(',
                    ')' => '\)',
                    ']' => '\]',
                    '[' => '\[',
                )
            );

            if($quality_ex && $quality_ex > 0){
                $quality = $quality_ex;
            }else{
                $quality = $this->pngOptimCompress;
            }

            //exec("/usr/bin/pngquant  --ext _ex.png --quality=$quality-$quality < ".escapeshellarg($strFilePath)." 2>&1", $res);
            $compressed_png_content = shell_exec("pngquant --quality=$quality-$quality - < ".escapeshellarg(    $strFilePath));

            if($compressed_png_content){

                $newFileName = $this->getNewFilePath($strFilePath, true, false, $quality_ex);
                $newServerFileName = $this->DOCUMENT_ROOT.$newFileName;
                file_put_contents($newServerFileName, $compressed_png_content);
                chmod($newServerFileName, BX_FILE_PERMISSIONS);

                if(file_exists($newServerFileName)){
                    $res = $newFileName;
                    $this->prepareData['new_file'] = $newFileName;
                }
            }
        }
        return $res;
    }

    protected function checkPath($path){
        if (strpos($path, $_SERVER["SERVER_NAME"]) <= 0){
            $newPath = $this->DOCUMENT_ROOT.$path;
        }else{
            $path = explode($_SERVER["SERVER_NAME"], $path);
            $newPath = $path[1];
        }

        return $newPath;
    }

    protected function getNewFilePath($strFilePath, $createDir = true, $isWebp = false, $param = false){
        $arInfo = pathinfo($strFilePath);
        $arServerPath = explode('/', $this->DOCUMENT_ROOT);
        $arFilePath = explode('/', $arInfo['dirname']);
        $arPathDiff = array_diff($arFilePath, $arServerPath);


        if($param && $param != ''){
            $arInfo['filename'] = $arInfo['filename']."-".$param;
        }

        $i = 1;

        foreach($arPathDiff as $key => &$arDir){
            if($i == 1){
                if($arDir == 'upload'){//если в корне папки upload
                    $arDir = 'upload/'.self::MODULE_ID;
                }else{
                    $arDir = 'upload/'.self::MODULE_ID.'/'.$arDir;
                }
            }

            if($i == 2){
                if($arDir == 'resize_cache'){
                    unset($arPathDiff[$key]);
                }
                break;
            }
            $i++;
        }

        $fileDir ='/'.implode('/',$arPathDiff).'/';

        if($createDir){
            \Bitrix\Main\IO\Directory::createDirectory($this->DOCUMENT_ROOT.$fileDir);
        }

        if($isWebp){
            $newFileName = $fileDir.$arInfo['filename'].'.'.$arInfo['extension'].'.webp';
        }else{
            $newFileName = $fileDir.$arInfo['filename'].'.'.$arInfo['extension'];
        }

        return $newFileName;
    }

    protected function issetCompressFile($path){
        $error = true;
        $returnFile = '';

        if($this->useWebp){
            $webpFile = $this->getNewFilePath($path, false, true);
            $serverWebpFile = $this->DOCUMENT_ROOT.$webpFile;;
            if(file_exists($serverWebpFile)){
                $error = false;
            }else{
                $error = true;
            }
        }

        if($this->Metod == 'default' || $this->Metod == 'multi'){
            $newFileName = $this->getNewFilePath($path, false);
            $newServerFileName = $this->DOCUMENT_ROOT.$newFileName;
            if(file_exists($newServerFileName)){

                $error = false;
            }else{

                $error = true;
            }

            $returnFile = $newFileName;

        }elseif($this->Metod == 'webp'){
            $returnFile = $webpFile;
        }

        if($error){
            return false;
        }else{
            return $returnFile;
        }
    }

    /**
     * Compress Image
     * @param $incomingPath
     * @param $quality
     * @return bool|string
     */
    public function compressImg($incomingPath, $quality){
        if($this->Metod == 'none'){
            return $incomingPath;
        }

		if($quality == ''){
            $quality = $this->jpegOptimCompress;
        }

        $modPath = $this->pathModified($incomingPath, $quality);

        if($pathCompressFile = $this->issetCompressFile($modPath)){
            return $pathCompressFile;
        }

        $serverPath = self::checkPath($incomingPath);

        if(!file_exists($serverPath)) return $incomingPath;

        $this->prepareData['url'] = $_SERVER['SCRIPT_URL'];

        if($this->useWebp){
            $webpFile = $this->convertToWebp($serverPath, $incomingPath, $quality);
        }

        if($this->Metod == 'default' || $this->Metod == 'multi'){
            $arInfo = pathinfo($serverPath);
            switch (strtolower($arInfo["extension"])) {
                case 'jpg' :
                    $newFile = $this->compressJPG($serverPath, $incomingPath, $quality);
                    break;
                case 'jpeg' :
                    $newFile = $this->compressJPG($serverPath, $incomingPath, $quality);
                    break;
                case 'png' :
                    $newFile = $this->compressPNG($serverPath, $incomingPath, $quality);
                    break;
                default :
                    $this->LAST_ERROR = 'error';
                    return $incomingPath;
            }

            $this->addToDb($incomingPath);

            if($newFile){
                return $newFile;
            }
        }elseif($this->Metod == 'webp'){
            if($webpFile){
                return $webpFile;
            }
        }



        return $incomingPath;
    }

    private function pathModified($path, $param){
        $strPosition = strripos ( $path, ".");
        return substr($path,0 , $strPosition)."-".$param. substr($path,$strPosition);
    }

    /*
     * Check replacement JPEG/PNG to WEBP
     */
    public function checkWebpReplacement($url = false){
        if(!$url){
            $url = $_SERVER['SERVER_NAME'].'/upload/testWEBP/test.jpg';
        }else{
            $url = $_SERVER['SERVER_NAME'].$url;
        }



        $request = "wget --header='accept: image/webp,image/apng,image/*,*/*;q=0.8' --spider -S $url"." 2>&1 ";


        exec($request, $s);


        $match = false;
        foreach ($s as $s_item){
            if(preg_match('/webp/', $s_item))
                $match = true;
        }
        return $match;
    }

    /**
     * @param $incomingPath
     * @param $quality
     * @return bool
     */
    public function compressToWebp($incomingPath, $quality){
        $serverPath = self::checkPath($incomingPath);

		if($quality == ''){
            $quality = $this->webpOptimCompress;
        }

        if($this->useWebp){
            $webpFile = $this->convertToWebp($serverPath, $incomingPath, $quality, true);
        }

        $result = $this->checkWebpReplacement($webpFile)? true : false;

        return $result;
    }

    private function addToDb($path){
        $arFields = array(
                'PATH_NAME' => $this->prepareData['new_file'],
                'URL' => $this->prepareData['url'],
                'IMAGE_MIME_TYPE' => $this->getFileType(),
                'IMAGE_SRC' => $path,
                'IMAGE_RESIZE_SRC' => $this->prepareData['new_file'],
                'IMAGE_WEBP_SRC' => $this->prepareData['webp_file'],
                'IMAGE_RESIZE_DATE' => date('d.m.Y h:i'),
                'ORIGINAL_FILE_SIZE' => filesize($_SERVER["DOCUMENT_ROOT"].$path),
                'COMPRESS_FILE_SIZE' => filesize($_SERVER["DOCUMENT_ROOT"].$this->prepareData['new_file']),
                'WEBP_FILE_SIZE' => filesize($_SERVER["DOCUMENT_ROOT"].$this->prepareData['webp_file'])
            );

        global $DB;
        $arInsert = $DB->PrepareInsert("slam_image", $arFields);

        $strSql = "INSERT INTO slam_image (".$arInsert[0].") VALUES (".$arInsert[1].")";
        $DB->Query($strSql, false);
    }

    public function getFileSize($s)
    {
        $i = 0;
        $ar = array('b', 'kb', 'M', 'G');
        while ($s > 1024) {
            $s /= 1024;
            $i++;
        }
        return round($s, 1) . ' ' . $ar[$i];
    }

    private function getFileType(){
        $mime = mime_content_type($_SERVER["DOCUMENT_ROOT"].$this->prepareData['new_file']);
        preg_match('/jpg|jpeg|png/', $mime, $result);
        return $result[0];
    }

}