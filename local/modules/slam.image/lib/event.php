<?
namespace Slam\Image;
use Bitrix\Main\Config\Option,
    Bitrix\Main\Loader;

/** 
 * Class with basic functions
 */ 
class Event
{
    const MODULE_ID = 'slam.image';
    var $MODULE_ID = 'slam.image';


    private function setWebpJS()
    {
        global $APPLICATION;

        $method = Option::get(self::MODULE_ID,'webpmetodoptim_compress', 'default');

        if ( $method == 'webp' )
        {
            $APPLICATION->AddHeadScript('/bitrix/js/'.self::MODULE_ID.'/webpjs-0.0.2.min.js');
        }
    }

    private function includeModuleSlamImage()
    {
        Loader::includeModule(self::MODULE_ID);
    }


    public static function OnBeforePrologHandler()
    {
        self::includeModuleSlamImage();
    }

    public static function OnEpilogHandler(){
        self::setWebpJS();
    }
}
?>