<?
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bitrix\Main\EventManager;
use \Bitrix\Main\ModuleManager;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\IO\Directory;
use \Bitrix\Main\Entity\Base;

global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install.php"));


if (!class_exists("slam_image")) {
    class slam_image extends CModule
    {
        const MODULE_ID = 'slam.image';
        var $MODULE_ID = "slam.image";
        var $MODULE_VERSION;
        var $MODULE_VERSION_DATE;
        var $MODULE_NAME;
        var $PARTNER_NAME;
        var $PARTNER_URI;
        var $MODULE_DESCRIPTION;

        function __construct()
        {
            $arModuleVersion = array();
            include($this->GetPath() . "/install/version.php");
            if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
                $this->MODULE_VERSION = $arModuleVersion["VERSION"];
                $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            }

            $this->MODULE_NAME = GetMessage("SLAM_IMAGE_MODULE_NAME");
            $this->MODULE_DESCRIPTION = GetMessage("SLAM_IMAGE_MODULE_DESCRIPTION");
            $this->PARTNER_NAME = GetMessage("SLAM_IMAGE_PARTNER_NAME");
            $this->PARTNER_URI = GetMessage("SLAM_IMAGE_PARTNER_URI");
            $this->MODULE_SORT = 1;
        }

        public function GetPath($notDocumentRoot = false)
        {
            if ($notDocumentRoot)
                return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
            else
                return dirname(__DIR__);
        }

        public function isVersionD7()
        {
            return CheckVersion(ModuleManager::getVersion('main'), '14.00.00');
        }


        function InstallDB()
        {
            Loader::includeModule($this->MODULE_ID);
            if (!Application::getConnection(\Slam\Image\ImageTable::getConnectionName())->isTableExists(Base::getInstance('\Slam\Image\ImageTable')->getDBTableName())) {
                Base::getInstance('\Slam\Image\ImageTable')->createDbTable();
            }
            return true;
        }

        function UnInstallDB()
        {
            Loader::includeModule($this->MODULE_ID);
            Application::getConnection(\Slam\Image\ImageTable::getConnectionName())->queryExecute('drop table if exists ' . Base::getInstance('\Slam\Image\ImageTable')->getDBTableName());
            Option::delete($this->MODULE_ID);
        }

        function InstallEvents()
        {
            $eventManager = \Bitrix\Main\EventManager::getInstance();

            $eventManager->registerEventHandler ("main","OnBeforeProlog",self::MODULE_ID, "Slam\\Image\\Event","OnBeforePrologHandler");
            $eventManager->registerEventHandler ("main","OnEpilog",self::MODULE_ID, "Slam\\Image\\Event", "OnEpilogHandler");

            return true;
        }

        function UnInstallEvents()
        {
            $eventManager = \Bitrix\Main\EventManager::getInstance();

            $eventManager->unRegisterEventHandler ("main","OnBeforeProlog",self::MODULE_ID, "Slam\\Image\\Event","OnBeforePrologHandler");
            $eventManager->unRegisterEventHandler ("main","OnEpilog",self::MODULE_ID, "Slam\\Image\\Event", "OnEpilogHandler");

            return true;
        }


        function InstallFiles($arParams = array())
        {
            if (Directory::isDirectoryExists($path = $this->GetPath() . '/install/admin/')) {
                CopyDirFiles($this->GetPath() . "/install/admin/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
            }
            if (Directory::isDirectoryExists($path = $this->GetPath() . '/install/themes/')) {
                CopyDirFiles($this->GetPath() . "/install/themes/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/themes",true,true);
            }
            if (Directory::isDirectoryExists($path = $this->GetPath() . '/install/tools/')) {
                CopyDirFiles($this->GetPath() . "/install/tools/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/tools");
            }
            if (Directory::isDirectoryExists($path_js = $this->GetPath() . '/install/files/bitrix/js/'. $this->MODULE_ID)) {
                CopyDirFiles($path_js, $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/". $this->MODULE_ID);
            }

            if (Directory::isDirectoryExists($rep_path = $this->GetPath() . '/install/replacement/')){
                CopyDirFiles($rep_path, $_SERVER["DOCUMENT_ROOT"] . "/upload/testWEBP/");
            }


            return true;
        }

        function UnInstallFiles()
        {
            if (Directory::isDirectoryExists($path = $this->GetPath() . '/install/admin')) {
                DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . $this->GetPath() . '/install/admin/', $_SERVER["DOCUMENT_ROOT"] . '/bitrix/admin');
            }
            if (Directory::isDirectoryExists($path = $this->GetPath() . '/install/themes')) {
                DeleteDirFilesEx("/bitrix/images/".self::MODULE_ID."/");
                DeleteDirFilesEx("/bitrix/themes/.default/icons/".self::MODULE_ID);
                unlink($_SERVER["DOCUMENT_ROOT"].'/bitrix/themes/.default/slam.image.css');
//                DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . $this->GetPath() . '/install/themes/', $_SERVER["DOCUMENT_ROOT"] . '/bitrix/themes');
            }
            if (Directory::isDirectoryExists($path = $this->GetPath() . '/install/tools')) {
                DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . $this->GetPath() . '/install/tools/', $_SERVER["DOCUMENT_ROOT"] . '/bitrix/tools');
            }
            if (Directory::isDirectoryExists($path_js = $this->GetPath() . '/install/files/bitrix/js/'. $this->MODULE_ID)) {
                DeleteDirFiles($path_js, $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/". $this->MODULE_ID);
            }
            if (Directory::isDirectoryExists($rep_path = $this->GetPath() . '/install/replacement/')){
                DeleteDirFiles($rep_path, $_SERVER["DOCUMENT_ROOT"] . "/upload/testWEBP/");
            }
            return true;
        }

        function DoInstall()
        {
            global $APPLICATION;

            if ($this->isVersionD7()) {

                ModuleManager::registerModule($this->MODULE_ID);
                $this->InstallDB();
                $this->InstallEvents();
                $this->InstallFiles();

            } else {
                $APPLICATION->ThrowException(GetMessage("SLAM_IMAGE_INSTALL_ERROR_VERSION"));
            }
            $APPLICATION->IncludeAdminFile(GetMessage("SLAM_IMAGE_INSTALL_TITLE"), $this->GetPath() . "/install/step1.php");
        }

        function DoUninstall()
        {
            global $APPLICATION;
            $context = Application::getInstance()->getContext();
            $request = $context->getRequest();
            if ($request["step"] < 2) {
                $APPLICATION->IncludeAdminFile(GetMessage("SLAM_IMAGE_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep1.php");
            } elseif ($request["step"] == 2) {
                $this->UnInstallFiles();
                $this->UnInstallEvents();

                if ($request['savedata'] != 'Y')
                    $this->UnInstallDB();

                ModuleManager::unRegisterModule($this->MODULE_ID);

                $APPLICATION->IncludeAdminFile(GetMessage("SLAM_IMAGE_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep2.php");
            }
        }

    }
}