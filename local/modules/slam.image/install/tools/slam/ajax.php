<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Application,
    \Bitrix\Main\Loader;

$request = Application::getInstance()->getContext()->getRequest();

if(Loader::includeModule('slam.image')) {
    if (isset($request["SECTION"]) && $request["SECTION"] == "Y") {
        $arSubSections = \Slam\Image\ImageTable::getSearchSection($request["PATH"]);
        \Slam\Image\ImageTable::getTreeSectionUl($arSubSections);
        die();
    }
}