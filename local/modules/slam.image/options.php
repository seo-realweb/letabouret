<?php
global $APPLICATION;
if (!defined("SLAM_IMAGE_MODULE_ID")) {
    define("SLAM_IMAGE_MODULE_ID", "slam.image");
}
$module_id = SLAM_IMAGE_MODULE_ID;
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/options.php");
IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Slam\Image\ImageCompress;


CJSCore::Init(array("jquery"));

CModule::IncludeModule(SLAM_IMAGE_MODULE_ID);
$RIGHT = $APPLICATION->GetGroupRight(SLAM_IMAGE_MODULE_ID);

$arIBlocks = array();
$arPropertyFields = array();
$mid = $_REQUEST["mid"];


if ($RIGHT >= "R"):
    $checkWebp = ImageCompress::getInstance()->checkWebpReplacement();
    $libWebp = ImageCompress::getInstance()->isWEBPOptim();
    $libJepg = ImageCompress::getInstance()->isJPEGOptim();
    $libPng =  ImageCompress::getInstance()->isPNGOptim();
    $birixVM = ImageCompress::getInstance()->isBitrixSeven();

    $arWebpReplace = array();
    $arMainOptionsBitrixVM = array();
    $arMainOptionsLibsWebp = array();
    $arMainOptionsLibsJepg = array();
    $arMainOptionsLibPng = array();

    if(!$checkWebp){
        $arWebpReplace = array (
            'note' => Loc::getMessage("SLAM_WEBP_REPLACE_NON")
        );
    }else{
        $arWebpReplace = array (
            'note' => Loc::getMessage("SLAM_WEBP_REPLACE_YES")
        );
    }

    if(!$birixVM){
        $arMainOptionsBitrixVM = array(
          'note' => Loc::getMessage("SLAM_IMAGE_LIB_BITRIX_VM_VERSION")
        );
    }

    if(!$libWebp){
        $arMainOptionsLibsWebp =
            array(
                "note" => Loc::getMessage("SLAM_IMAGE_LIB_WEBP_NON")
            );
    }else{
        $arMainOptionsLibsWebp =
            array(
                "note" => Loc::getMessage("SLAM_IMAGE_LIB_WEBP_YES")
            );
    }

    if(!$libJepg){
        $arMainOptionsLibsJepg =
            array(
                "note" => Loc::getMessage("SLAM_IMAGE_LIB_JEPG_NON")
            );
    }else{
        $arMainOptionsLibsJepg =
            array(
                "note" => Loc::getMessage("SLAM_IMAGE_LIB_JEPG_YES")
            );
    }

    if(!$libPng){
        $arMainOptionsLibPng = array(
                "note" => Loc::getMessage("SLAM_IMAGE_LIB_PNG_NON")
            );
    }else{
        $arMainOptionsLibPng =
            array(
                "note" => Loc::getMessage("SLAM_IMAGE_LIB_PNG_YES")
            );
    }

    $arMainOptions = array(
        Loc::getMessage("SLAM_IMAGE_METHOD_TITLE"),
        array("webpmetodoptim_compress",
            Loc::getMessage("SLAM_IMAGE_METOD_COMPRESS"),
            "JS",
            array(
                "selectbox",
                array(
                    "none" => Loc::getMessage("SLAM_IMAGE_METHOD_NONE"),
                    "default" => Loc::getMessage("SLAM_IMAGE_METHOD_DEFAULT"),
                    "webp" => Loc::getMessage("SLAM_IMAGE_METHOD_WEBP"),
                    "multi" => Loc::getMessage("SLAM_IMAGE_METHOD_MULTI")
                )
            ),
        ),

        array(
            "note" => Loc::getMessage("SLAM_IMAGE_LIB_WEBP_USE_DEFAULT")
        ),
        array(
            "note" => Loc::getMessage("SLAM_IMAGE_LIB_WEBP_USE_WEBP")
        ),
        array(
            "note" => Loc::getMessage("SLAM_IMAGE_LIB_WEBP_USE_SERVER")
        ),
        $arWebpReplace,
        $arMainOptionsBitrixVM,

        Loc::getMessage("SLAM_IMAGE_TITLE_JPEG_COMPRESS"),
        $arMainOptionsLibsJepg,
        array("jpegoptim_compress",
            Loc::getMessage("SLAM_IMAGE_JPEG_COMPRESS"),
            "70",
            Array("text", 3),
        ),

        Loc::getMessage("SLAM_IMAGE_TITLE_PNG_COMPRESS"),
        $arMainOptionsLibPng,
        array("pngoptim_compress",
            Loc::getMessage("SLAM_IMAGE_PNG_COMPRESS"),
            "85",
            Array("text", 3),
        ),

        Loc::getMessage("SLAM_IMAGE_TITLE_WEBP_COMPRESS"),
        $arMainOptionsLibsWebp,
        array("webpoptim_compress",
            Loc::getMessage("SLAM_IMAGE_WEBP_COMPRESS"),
            "85",
            Array("text", 3),
        ),

        Loc::getMessage("SLAM_IMAGE_TITLE_ADDITIONAL_COMPRESS"),
        array( "OPTION_NAME_7",
            Loc::getMessage("SLAM_IMAGE_CLEAR_CACHE_COMPRESS"),
            "<input type='button' class='adm-btn' onclick='delete_img_cache();return false;' value='".Loc::getMessage("SLAM_IMAGE_CLEAR_CACHE_TEXT_COMPRESS")."' ></input> 
            <span class='clear_cache_img_slam' style='display:none;color:green;'>".Loc::getMessage("SLAM_IMAGE_CLEAR_CACHE_YES")."</span>",
            array(
                "statichtml"
            )
        ),
        array('space', "<br>", "<br>",array("statichtml")),
        array( "OPTION_NAME_8",
            Loc::getMessage('SLAM_IMAGE_BITRIX_CLEAR_CACHE')." 
            <br> 
            <div class='adm-info-message-wrap'>
                <div class='adm-info-message'>" . Loc::getMessage('SLAM_IMAGE_BITRIX_CLEAR_CACHE_INFO')." </div>
            </div>
            <br>
            <div id='clear_result_div'>

            </div>",

            "<div class=\"adm-detail-content-item-block\" style=\"height: auto; overflow-y: visible;\">
            <table class=\"adm-detail-content-table edit-table\" id=\"fedit2_edit_table\" style=\"opacity: 1;\">
                <tbody>
                <form method=\"POST\" action=\"/bitrix/admin/cache.php?lang=ru\"><?=bitrix_sessid_post()?></form>
                <input type=\"hidden\" name=\"sessid\" id=\"sessid_2\" value=\"bf27f7e296296932b7ebcacb8884ee55\"><tr>
                    <td colspan=\"2\" valign=\"top\" align=\"left\">
                        
                        <input type=\"hidden\" name=\"clearcache\" value=\"Y\"> 
                        <!--<input type=\"radio\" class=\"cache-types\" name=\"cachetype\" id=\"cachetype1\" value=\"expired\" checked=\"\"> <label for=\"cachetype1\">Только устаревшие</label><br>-->
                        
                        <input type=\"radio\" class=\"cache-types\" name=\"cachetype\" id=\"cachetype2\" value=\"all\"> <label for=\"cachetype2\">Все</label><br>
                        
                        <!--<input type=\"radio\" class=\"cache-types\" name=\"cachetype\" id=\"cachetype3\" value=\"menu\"> <label for=\"cachetype3\">Меню</label><br>
                        <input type=\"radio\" class=\"cache-types\" name=\"cachetype\" id=\"cachetype4\" value=\"managed\"> <label for=\"cachetype4\">Весь управляемый</label><br>
                        <input type=\"radio\" class=\"cache-types\" name=\"cachetype\" id=\"cachetype5\" value=\"html\"> <label for=\"cachetype5\">Все страницы HTML кеша</label><br>-->
                        
                        <br>
                        <script type=\"text/javascript\">
                            cache_types_cnt = document.getElementsByClassName('cache-types').length;
                        </script>
                    </td>
                </tr>
                <tr>
                    <td valign=\"top\" colspan=\"2\" align=\"left\">
                        <input type=\"button\" id=\"start_button\" value=\"Начать\" onclick=\"StartClearCache();\" class=\"adm-btn-save\">
                        <input type=\"button\" id=\"stop_button\" value=\"Остановить\" onclick=\"StopClearCache();\" disabled=\"\">
                        <input type=\"button\" id=\"continue_button\" value=\"Продолжить\" onclick=\"ContinueClearCache();\" disabled=\"\">
                    </td>
                </tr>
                <!--
                <tr>
                    <td colspan=\"2\">
                        <div class=\"adm-info-message-wrap\"><div class=\"adm-info-message\">       После удаления файлов кеша выводимые данные будут обновлены до актуального состояния.
                                Новые файлы кеша будут создаваться постепенно по мере обращений к страницам с закешированными областями.
                            </div>
                        </div>
                    </td>
                </tr>
                -->


                </tbody>
            </table>


            <!--
            <div id=\"clear_result_div\">

            </div>
            -->
        ",
            array(
                "statichtml"
            )
        ),

    );


    if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && $RIGHT >= "W" && check_bitrix_sessid()) {
        if (strlen($RestoreDefaults) > 0) {
            COption::RemoveOption(SLAM_IMAGE_MODULE_ID);
            $z = CGroup::GetList($v1 = "id", $v2 = "asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
            while ($zr = $z->Fetch())
                $APPLICATION->DelGroupRight(SLAM_IMAGE_MODULE_ID, array($zr["ID"]));
        } else {
            COption::RemoveOption(SLAM_IMAGE_MODULE_ID, "sid");
            $arAllOptions = Array();
            $arAllOptions = $arMainOptions;
            foreach ($arAllOptions as $arOption) {
                __AdmSettingsSaveOption(SLAM_IMAGE_MODULE_ID, $arOption);
            }
        }
    }

    $aTabs = array(
        array("DIV" => "options",
            "TAB" => Loc::getMessage("MAIN_OPTIONS_TAB"),
            "ICON" => "settings",
            "TITLE" => Loc::getMessage("PARAM_OPTIONS")
        ),
        array("DIV" => "rights",
            "TAB" => GetMessage("MAIN_TAB_RIGHTS"),
            "ICON" => "rights",
            "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS")
        ),
    );

    $tabControl = new CAdminTabControl("tabControl", $aTabs);
    ?>
    <?php $tabControl->Begin(); ?>


    <form method="post"
          action="<?php echo $APPLICATION->GetCurPage() ?>?mid=<?php echo urlencode($mid) ?>&amp;lang=<?php echo LANGUAGE_ID ?>"
          enctype="multipart/form-data" id="slam_partisan_form">
        <?php $tabControl->BeginNextTab(); ?>
        <?php foreach ($arMainOptions as $arOption) {
            __AdmSettingsDrawRow(SLAM_IMAGE_MODULE_ID, $arOption);
        } ?>
        <? $tabControl->BeginNextTab(); ?>
        <? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php"); ?>
        <?php $tabControl->Buttons(); ?>
        <?php
        if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && check_bitrix_sessid()) {
            if (strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0)
                LocalRedirect($_REQUEST["back_url_settings"]);
            else
                LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
        }
        ?>
        <input <?php if ($RIGHT < "W") echo "disabled" ?> type="submit" name="Apply" class="adm-btn-save"
                                                          value="<?php echo Loc::getMessage("MAIN_OPT_APPLY") ?>"
                                                          title="<?php echo Loc::getMessage("MAIN_OPT_APPLY_TITLE") ?>">

        <?php if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
            <input type="button" name="Cancel" value="<?php echo Loc::getMessage("MAIN_OPT_CANCEL") ?>"
                   title="<?php echo Loc::getMessage("MAIN_OPT_CANCEL_TITLE") ?>"
                   onclick="window.location='<?php echo htmlspecialchars(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
            <input type="hidden" name="back_url_settings"
                   value="<?php echo htmlspecialchars($_REQUEST["back_url_settings"]) ?>">
        <?php endif; ?>

        <input type="hidden" name="Update" value="Y">
        <?php echo bitrix_sessid_post(); ?>
        <?php $tabControl->End(); ?>
    </form>

    <!--<div class="adm-detail-content" id="fedit2" style="display: block;"><div class="adm-detail-title">Очистка файлов кеша</div>
        <div class="adm-detail-content-item-block" style="height: auto; overflow-y: visible;">
            <table class="adm-detail-content-table edit-table" id="fedit2_edit_table" style="opacity: 1;">
                <tbody>
                <form method="POST" action="/bitrix/admin/cache.php?lang=ru"><?/*=bitrix_sessid_post()*/?></form>
                <input type="hidden" name="sessid" id="sessid_2" value="bf27f7e296296932b7ebcacb8884ee55"><tr>
                    <td colspan="2" valign="top" align="left">
                        <input type="hidden" name="clearcache" value="Y">
                        <input type="radio" class="cache-types" name="cachetype" id="cachetype1" value="expired" checked=""> <label for="cachetype1">Только устаревшие</label><br>
                        <input type="radio" class="cache-types" name="cachetype" id="cachetype2" value="all"> <label for="cachetype2">Все</label><br>
                        <input type="radio" class="cache-types" name="cachetype" id="cachetype3" value="menu"> <label for="cachetype3">Меню</label><br>
                        <input type="radio" class="cache-types" name="cachetype" id="cachetype4" value="managed"> <label for="cachetype4">Весь управляемый</label><br>
                        <input type="radio" class="cache-types" name="cachetype" id="cachetype5" value="html"> <label for="cachetype5">Все страницы HTML кеша</label><br>
                        <br>
                        <script type="text/javascript">
                            cache_types_cnt = document.getElementsByClassName('cache-types').length;
                        </script>
                    </td>
                </tr>
                <tr>
                    <td valign="top" colspan="2" align="left">
                        <input type="button" id="start_button" value="Начать" onclick="StartClearCache();" class="adm-btn-save">
                        <input type="button" id="stop_button" value="Остановить" onclick="StopClearCache();" disabled="">
                        <input type="button" id="continue_button" value="Продолжить" onclick="ContinueClearCache();" disabled="">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="adm-info-message-wrap"><div class="adm-info-message">       После удаления файлов кеша выводимые данные будут обновлены до актуального состояния.
                                Новые файлы кеша будут создаваться постепенно по мере обращений к страницам с закешированными областями.
                            </div>
                        </div>
                    </td>
                </tr>


                </tbody>
            </table>


            <div id="clear_result_div">

            </div>
        </div>
    </div>-->

    <script>
        //очистка кеша изображений
        function delete_img_cache(){
            var btn = $('.adm-btn');
            btn.attr("disabled", true);

			var data = {
				ID: 'slam.image',
				action_button: 'delete',
				lang: 'ru',
				sessid: BX.bitrix_sessid(),
				site: '',
				path: '/upload/',
				show_perms_for: '0',
				mode: 'list',
				table_id: 'tbl_fileman_admin'
			};

			$.ajax({
				url: "/bitrix/admin/fileman_admin.php",
				type: 'GET',
				data: data,
				success: function () {
                    // удаление всех данных БД в таблице slam_image
                    $.ajax({
                        url:"/local/modules/slam.image/include/ajax_request.php",
                        type:'POST',
                        data: {truncate: true},
                        success: function () {
                            $('.clear_cache_img_slam').show();
                        }
                    });

                    setTimeout(function(){
						$('.clear_cache_img_slam').hide();
                        btn.attr("disabled", false);
					}, 1000);
				},
				error: function(){
					alert('<?=Loc::getMessage("SLAM_IMAGE_ERROR_CLEAR_CACHE") ?>');
				}
			});
        }

        function showDefault(){
            $('#text_multi').closest("tr").hide();
            $('#text_webp').closest("tr").hide();
            $('#text_default').closest("tr").show();
        }

        function showWebp(){
            $('#text_multi').closest("tr").hide();
            $('#text_default').closest("tr").hide();
            $('#text_webp').closest("tr").show();
        }

        function showMulti(){
            $('#text_webp').closest("tr").hide();
            $('#text_default').closest("tr").hide();
            $('#text_multi').closest("tr").show();
        }

        function hideAll(){
            $('#text_webp').closest("tr").hide();
            $('#text_default').closest("tr").hide();
            $('#text_multi').closest("tr").hide();
        }

        //в зависимости от выбраного select в режиме отображения изображений webp, показ. разную инфу
        var metod =  $('select[name="webpmetodoptim_compress"] option:selected').val();
        if(metod == 'none'){
            hideAll();
        }else if(metod == 'multi' ){
            showMulti();
        }else if(metod == 'webp' ){
            showWebp();
        }else if(metod == 'default'){
            showDefault();
        }

        $('select[name="webpmetodoptim_compress"]').on('change', function() {
            var metod =  $('select[name="webpmetodoptim_compress"] option:selected').val();
            if(metod == 'none'){
                hideAll();
            }else if(metod == 'multi'){
                showMulti();
            }else if(metod == 'webp'){
                showWebp();
            }else if(metod == 'default'){
                showDefault();
            }
        });


        /**
         * Clear cache
         * Copy from main module \bitrix\modules\main\admin\cache.php
         */
        var stop;
        var last_path;

        function StartClearCache()
        {
            stop=false;
            document.getElementById('clear_result_div').innerHTML='';
            document.getElementById('stop_button').disabled=false;
            document.getElementById('start_button').disabled=true;
            document.getElementById('continue_button').disabled=true;
            for(var i=1;i<=cache_types_cnt;i++)
                // document.getElementById('cachetype'+i).disabled=true;
            DoNext('');
        }
        function DoNext(path)
        {
            var queryString = 'ajax=y'
                + '&clearcache=Y'
                + '&lang=<?echo htmlspecialcharsbx(LANG)?>'
                + '&<?echo bitrix_sessid_get()?>'
            ;

            var cachetype = '';
            /*for(var i=1;i<=cache_types_cnt;i++)
            {
                var radio = document.getElementById('cachetype'+i);
                if(radio.checked)
                    cachetype = radio.value;
            }*/
            cachetype = $('#cachetype2').val();

            last_path = path;

            if(!stop)
            {
                ShowWaitWindow();
                BX.ajax.post(
                    'cache.php?'+queryString,
                    {'path': path, 'cachetype': cachetype},
                    function(result){
                        document.getElementById('clear_result_div').innerHTML = result;
                        CloseWaitWindow();
                    }
                );
            }

            return false;
        }
        function StopClearCache()
        {
            stop=true;
            document.getElementById('stop_button').disabled=true;
            document.getElementById('start_button').disabled=false;
            document.getElementById('continue_button').disabled=false;
            /*for(var i=1;i<=cache_types_cnt;i++)
                document.getElementById('cachetype'+i).disabled=false;*/
        }
        function ContinueClearCache()
        {
            stop=false;
            document.getElementById('stop_button').disabled=false;
            document.getElementById('start_button').disabled=true;
            document.getElementById('continue_button').disabled=true;
            /*for(var i=1;i<=cache_types_cnt;i++)
                 document.getElementById('cachetype'+i).disabled=true;*/
            DoNext(last_path);
        }
        function EndClearCache()
        {
            stop=true;
            document.getElementById('stop_button').disabled=true;
            document.getElementById('start_button').disabled=false;
            document.getElementById('continue_button').disabled=true;
            // for(var i=1;i<=cache_types_cnt;i++)
                // document.getElementById('cachetype'+i).disabled=false;
        }
    </script>

<?php else: //if ($RIGHT >="R"):?>
    <?php echo CAdminMessage::ShowMessage(Loc::getMessage('NO_RIGHTS_FOR_VIEWING')); ?>
<?php endif; ?>
