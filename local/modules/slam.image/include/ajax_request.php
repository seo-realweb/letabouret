<?php
use CSlamImage;

$MODULE_ID = 'slam.image';
define("ADMIN_MODULE_NAME", $MODULE_ID);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // clear slam_image table
    if($_POST['truncate'] == 'true'){
        $connection = \Bitrix\Main\Application::getConnection();
        $connection->truncateTable('slam_image');
    }

    $path = $_POST['path'];
    $quality = $_POST['quality'];

    if( !file_exists($_SERVER['DOCUMENT_ROOT'].$path) ){
        $result = array('SUCCESS'=> false, 'MESSAGE' => '<div style="color: red">'. GetMessage('FILE_NOT_FOUND').'</div>');
    }else{
        $convertResult = CSlamImage::GetCompressImage($path, $quality);;
        if(!$convertResult){
            $result = array('SUCCESS'=> false, 'MESSAGE' => '<div style="color: red">'. GetMessage('ERROR_MESSAGE').'</div>');
        }
        $result = array('SUCCESS' => true, 'MESSAGE' => '<div style="color: #00db54">'. GetMessage('SUCCESS_MESSAGE').'</div>' );
    }
}

echo json_encode($result);