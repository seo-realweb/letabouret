<?php
use
    Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc,
    Slam\Image;


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
$GLOBALS['APPLICATION']->AddHeadScript("/local/modules/slam.image/include/jquery-3.3.1.min.js");
global $APPLICATION;

$module_id = 'slam.image';

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

if (!Loader::includeModule($module_id))
    return false;

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($MODULE_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::GetMessage("SLAM_IMAGE_ACCESS_DENIED"));
?>

<form action="" method="post"  >
    <div class="input-group">
        <label for="path"><?= GetMessage('PATH_LABEL')?> </label>
        <input type="text" name="path" id="path">
    </div>

    <div class="input-group">
        <label for="quality"> <?= GetMessage('QUALITY_LABEL')?> </label>
        <input type="number" name="quality" max="100" min="1" value="80">
    </div>

    <input id="submit" type="submit" disabled  value="<?= GetMessage('BTN_TEXT')?>">
</form>
<div class="info-message">  </div>

<script type="text/javascript">
    $(document).ready(function () {
       $('#path').on('change', function () {
            var path = this.value;

            if(path.length > 0){
                $('#submit').attr('disabled' , false);
            }else {
                $('#submit').attr('disabled' , true);
            }
        });

       // form submit
       $("form").submit(function (event) {
           event.preventDefault();
           $('.info-message').html();
           var dataValue = {
               path: $(this).find('input[name="path"]').val(),
               quality: $(this).find('input[name="quality"]').val()
           };

           $.ajax({
               type: 'post',
               url: '/local/modules/slam.image/include/ajax_request.php',
               data: dataValue,
               response: 'json',

               success: function (data) {
                   var result = JSON.parse(data);
                   $('.info-message').html(result.MESSAGE);
               }
           });
       });
    });
</script>

<style>
    .input-group{
        width: 45%;
        margin: 10px;
    }

    #submit{
        margin-top: 10px;
    }
</style>
