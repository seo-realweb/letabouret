<?

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;

$prefix = 'slam';
$module = "image";
$module_id = $prefix.'.'.$module;

$tabs = array(
  array("text" => 'Таблица','url' =>"/bitrix/admin/".$prefix."_".$module.".php?lang=" . LANGUAGE_ID ),
  array('text' => 'Сжать изображение', 'url' =>"/bitrix/admin/new_".$prefix."_".$module.".php?lang=" . LANGUAGE_ID , )
);

if ($USER->IsAdmin()) {
    $data =  array(
        array(
            "parent_menu" => "global_menu_services",
            "section" => $module_id,
            "sort" => 1,
            "text" => GetMessage("SLAM_IMAGE_MODULE"),
            "title" => GetMessage("SLAM_IMAGE_MODULE"),
            "icon" => "slam-compress",
            "page_icon" => "slam-compress",
            "items_id" => "menu_". $module_id,
            "url" => "/bitrix/admin/".$prefix."_".$module.".php",
            "more_url" => array(
            "/bitrix/admin/".$prefix."_".$module.".php"
            ),
            'items' => $tabs
        )
    );

    return $data;
} else{
    return false;
}
