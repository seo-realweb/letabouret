<?
use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc,
    Slam\Image;



require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

global $APPLICATION;

$module_id = 'slam.image';

if (!Loader::includeModule($module_id))
    return false;

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/".$module_id."/include.php");

$APPLICATION->SetTitle(Loc::GetMessage("SLAM_IMAGE_LIST"));

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);

if ($MODULE_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::GetMessage("SLAM_IMAGE_ACCESS_DENIED"));



$strError = '';
define('START_TIME', time());


$dbTable = new Image\ImageTable();
$queryTable = $dbTable->query();
$mapFields = $dbTable->getMap();

$sTableID = $dbTable->getTableName();
$oSort = new CAdminSorting($sTableID, "ID", "ASC");
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilter = array();
if (!empty($_REQUEST["FINDBY"]) && !isset($_REQUEST['del_filter'])) {
    $arFilter = array_filter($_REQUEST["FINDBY"], function (&$a) {
        if (is_array($a)) {
            $a = array_filter($a, function ($b) {
                $b = (!is_array($b)) ? trim($b) : $b;
                return !empty($b);
            });
        }
        return !empty($a);
    });
}

/*$aContext = array(array(
    "TEXT"=> Loc::GetMessage("SLAM_IMAGE_INDEX"),
    "LINK"=>"#",
    "TITLE"=> Loc::GetMessage("SLAM_IMAGE_INDEX"),
    'ONCLICK' => 'popupFormIndex(this)',
    "ICON"=> "btn_new",
), array(
    "TEXT"=> Loc::GetMessage("SLAM_IMAGE_COMPRESS_ALL"),
    "LINK"=>"?COMPRESS_ALL=Y",
    "TITLE"=> Loc::GetMessage("SLAM_IMAGE_COMPRESS_ALL"),
));

$lAdmin->AddAdminContextMenu($aContext, false);*/

if (($arID = $lAdmin->GroupAction())) {
    if ($_REQUEST['action_target'] == 'selected') {
        $rsData = $queryTable->setFilter($arFilter)->setOrder(array($by => $order))->exec();
        while ($arRes = $rsData->Fetch()) {
            $arID[] = $arRes['ID'];
        }
    }

    foreach ($arID as $propID) {
        if (intval($propID) <= 0) {
            continue;
        }
        switch ($_REQUEST['action']) {
            case "delete":
                delCompressImg($propID);
                $DB->StartTransaction();
                if (!$dbTable->Delete($propID)) {
                    $DB->Rollback();
                }
                $DB->Commit();
                break;

        }
    }
}

/**
 * Delete compress images
 * @param $id
 */
function delCompressImg($id){
    global $DB;
    $q = 'SELECT IMAGE_RESIZE_SRC, IMAGE_WEBP_SRC from slam_image where ID='.$id;
    $result = $DB->Query($q);

    $arFiles = $result->GetNext();

    foreach ($arFiles as $i){
        if ( file_exists($_SERVER['DOCUMENT_ROOT'].$i)){
            unlink($_SERVER['DOCUMENT_ROOT'].$i);
        }
    }
}

$START_PATH = $_REQUEST['START_PATH'];
if (!$START_PATH)
    $START_PATH = $_SERVER['DOCUMENT_ROOT'];

if (!is_dir($START_PATH))
    $strError = "BITRIX_XSCAN_NACALQNYY_PUTQ_NE_NA";

$request = Application::getInstance()->getContext()->getRequest();


/*$indexFinish = false;*/


/*if ($request['INDEX'] && !$strError) {
    if ($request['break_point']){
        define('SKIP_PATH', htmlspecialcharsbx($request['break_point']));
    } else {
        $dbTable->truncateTable();
    }

    $arDir = explode(', ', $request['PATH']);
    $dbTable->SearchImage($START_PATH, $arDir);


    if (defined('BREAK_POINT'))
    {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    */?><!--


        <form method=post id="postform" action="?">
            <input type="hidden" name="START_PATH" value="<?/*=htmlspecialcharsbx($START_PATH)*/?>">
            <input type="hidden" name="INDEX" value="Y">
            <input type="hidden" name="break_point" value="<?/*=htmlspecialcharsbx(BREAK_POINT)*/?>">
            <textarea name="PATH" style="display: none"><?/*=$request['path']*/?></textarea>
        </form>
        <?/*
        $dbTable->ShowMsg(Loc::GetMessage("SLAM_IMAGE_IN_PROGRESS").'<i>'.htmlspecialcharsbx(BREAK_POINT).'</i>');
        */?>
        <script>window.setTimeout("document.getElementById('postform').submit()",500);</script>--><?/*

        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
        return;

    }
    else {
        $indexFinish = true;
    }

}*/

$queryTable->
$rsData = $queryTable->setSelect(array("*"))->setFilter($arFilter)->setOrder(array($by => $order))->exec();

$arHeaders = array();
foreach ($mapFields as $field) {
    if ($field instanceof Bitrix\Main\Entity\ReferenceField) {
        continue;
    }

    $arHeaders[] = array("id" => $field->getName(),
                   "content" => (Loc::GetMessage("SLAM_IMAGE_FIELD_{$field->getName()}") ?: $field->getTitle()),
                   "sort" => $field->getName(),
                   "default" => (count($arHeaders) < 8));
}
$lAdmin->AddHeaders($arHeaders);


$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();


$lAdmin->NavText($rsData->GetNavPrint(Loc::GetMessage("SLAM_IMAGE_GET_NAV_PRINT")));


$events = array();
while ($arRes = $rsData->Fetch()) {

    foreach ($mapFields as $field) {
        if ($field instanceof Bitrix\Main\Entity\ReferenceField) {
            continue;
        }
        if (method_exists($field, "getValues") && count($field->getValues())) {
            $valuesList = $field->getValues();
            $arRes[$field->getName()] = isset($valuesList[$arRes[$field->getName()]]) ? $valuesList[$arRes[$field->getName()]] : $arRes[$field->getName()];
        }
    }

    $contentMenu = array(
        // array("TEXT" => Loc::GetMessage("SLAM_IMAGE_COMPRESS"), "ACTION" => $lAdmin->ActionDoGroup($arRes["ID"], "compress")),
        array("ICON" => "delete", "TEXT" => Loc::GetMessage("SLAM_IMAGE_DEL"), "ACTION" => "if(confirm('" . Loc::GetMessage("SLAM_IMAGE_DEL") . "')) " . $lAdmin->ActionDoGroup($arRes["ID"], "delete")),
    );


    $row = &$lAdmin->AddRow($arRes["ID"], $arRes);

    foreach ($arRes as $key => $val) {
        if($key == 'IMAGE_SRC' || $key == 'IMAGE_RESIZE_SRC' || $key == 'IMAGE_WEBP_SRC') {
            $siz = '';
            if($key == 'IMAGE_SRC'){ $siz = $arRes['ORIGINAL_FILE_SIZE'];}
            if($key == 'IMAGE_RESIZE_SRC'){ $siz = $arRes['COMPRESS_FILE_SIZE'];}
            if($key == 'IMAGE_WEBP_SRC'){ $siz = $arRes['WEBP_FILE_SIZE'];}

            if (file_exists($_SERVER['DOCUMENT_ROOT'] . $val)) {
            $imgLink = $val;
            $fileSize =  getimagesize($_SERVER['DOCUMENT_ROOT'].$val);
            $imgWidth = $fileSize[0];
            $imgHeigth = $fileSize[1];
            $imgSize = Image\ImageTable::fileSize($siz) ;
                $val =  "<a title='Увеличить' onclick=\"ImgShw('".$imgLink."',$imgWidth,$imgHeigth,'')\" >
                        <img style='max-width: 150px; max-height: 150px;' src='" . $val. "'>".
                        "<span class='img_info' >
                            <span class='adm-input-file-hint-row'>".$imgSize."</span>
                            <span class='adm-input-file-hint-row'>".$imgWidth."x".$imgHeigth."</span>
                        </span> </a>";
            } else {
                $val = "<span class='text-error'>" .  Loc::GetMessage("SLAM_IMAGE_ERROR_IMAGE_SRC") . "</span>";
            }
        }

        if($key == 'ORIGINAL_FILE_SUM'){
            AddMessage2Log([$key => $val]);
        }

        $row->AddViewField($key, $val);

    }



    $row->AddActions($contentMenu);

    $lAdmin->BeginEpilogContent();
    $lAdmin->EndEpilogContent();
}

AddMessage2log($statistics);

$lAdmin->AddFooter(array(
        array("title" => Loc::GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()),
        array("counter" => true, "title" => Loc::GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"))
);
$contentMenu = array("delete" => Loc::GetMessage("MAIN_ADMIN_LIST_DELETE"));

$lAdmin->AddGroupActionTable($contentMenu);

$lAdmin->CheckListMode();

$arResult = array();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
<table>
    <tr>
        <td>
            <form name="form1" method="GET" action="<?= $APPLICATION->GetCurPage() ?>">
                <?
                $arValues = [];
                $propsArray = array();
                foreach ($mapFields as $field) {
                    if ($field instanceof Bitrix\Main\Entity\ReferenceField) {
                        continue;
                    }

                    $propsArray["FINDBY[{$field->getName()}]"] = (Loc::GetMessage("SLAM_IMAGE_FIELD_{$field->getName()}") ?: $field->getTitle());
                }
                $oFilter = new CAdminFilter($sTableID . "_filter", $propsArray);
                $oFilter->Begin();
                ?>

                <? foreach ($mapFields as $field) : ?>
                <?
                if ($field instanceof Bitrix\Main\Entity\ReferenceField) {
                    continue;
                }

                ?>
    <tr>
        <td><?= (Loc::GetMessage("SLAM_IMAGE_FIELD_{$field->getName()}") ?: $field->getTitle())?>:</td>
        <td>

            <?
            switch (get_class($field)):
                case "Bitrix\Main\Entity\DateField":
                    ?>
                    <input type="text" class="typeinput" name="<?= "FINDBY[{$field->getName()}]"; ?>"
                           value="<?= strlen($arValues[$field->getName()]) ? $arValues[$field->getName()] : ""; ?>">
                    <?= Calendar($field->getName(), "curform") ?>
                    <?
                    break;
                case "Bitrix\Main\Entity\EnumField":
                    $values = $field->getValues();
                    $values[0] = "--//--";
                    ?>
                    <select name="<?= "FINDBY[{$field->getName()}]"; ?>">
                        <? foreach ($values as $valueID => $valueName): ?>
                            <option value="<?= $valueID ?>"<?= $arValues[$field->getName()] == $valueID ? " selected" : "" ?>><?= $valueName ?></option>
                        <? endforeach; ?>
                    </select>
                    <?
                    break;
                case "Bitrix\Main\Entity\BooleanField":
                    ?>
                    <input type="hidden" name="<?= "FINDBY[{$field->getName()}]"; ?>" value="0"/>
                    <input type="checkbox" name="<?= "FINDBY[{$field->getName()}]"; ?>" value="1" <?= ($arValues[$field->getName()] ? "checked" : ""); ?>/>
                    <?
                    break;
                case "Bitrix\Main\Entity\IntegerField":
                    ?>
                    <input type="text" name="<?= "FINDBY[{$field->getName()}]"; ?>" value="<?= strlen($arValues[$field->getName()]) ? $arValues[$field->getName()] : ""; ?>"/>
                    <?
                    break;
                case "Bitrix\Main\Entity\FloatField":
                default :
                    ?>
                    <input type="text" name="<?= "FINDBY[{$field->getName()}]"; ?>" value="<?= htmlspecialchars(strlen($arValues[$field->getName()]) ? $arValues[$field->getName()] : ""); ?>"/>
                <? endswitch; ?>

    </tr>
    <? endforeach; ?>

    <?
    $oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage()));
    $oFilter->End();
    ?>
    </form>
        </td>
        <td width="20">

        </td>

    <td>
        <h3> Статистика:</h3>
        <table>
            <tr>
                <td>Всего файлов</td>
                <td width="10"></td>
                <td>3500</td>
            </tr>
            <tr>
                <td>Объем необжатых картинок</td>
                <td width="10"></td>
                <td> 25Gb</td>
            </tr>
            <tr>
                <td>Объем обжатых картинок</td>
                <td width="10"></td>
                <td> 5Gb</td>
            </tr>
        </table>
    </td>
    </tr>
</table>



<?

if($indexFinish) {
    $dbTable->ShowMsg(Loc::GetMessage("SLAM_IMAGE_COMPLETED"));
}

\CJSCore::Init(['popup']);
\CJSCore::Init(array("jquery"));
?>

<script>
    function popupFormIndex(obj) {
        var popup = BX.PopupWindowManager.create("popupIndex", obj, {
            content: BX("treeSection"),
            width: 400,
            height: 300,
            zIndex: 100,
            closeIcon: {opacity: 1},
            titleBar: '<?=Loc::GetMessage("SLAM_IMAGE_INDEX")?>',
            closeByEsc: true,
            darkMode: false,
            autoHide: false,
            draggable: true,
            resizable: true,
            min_height: 100,
            min_width: 100,
            lightShadow: true,
            angle: true,
            overlay: {
                backgroundColor: 'black',
                opacity: 500
            },
            buttons: [
                new BX.PopupWindowButton({
                    text: '<?=Loc::GetMessage("SLAM_IMAGE_INDEX_BTN")?>',
                    id: 'save-btn',
                    className: 'ui-btn ui-btn-success',
                    events: {
                        click: function () {

                            this.disabled = true;

                            var treeSection = document.getElementById("treeSection"),
                                inputs = treeSection.querySelectorAll("input[type=checkbox]:checked"),
                                arList = [];

                            for (var i in inputs){
                                if (inputs[i].checked && typeof inputs[i].value != "undefined"){
                                    arList.push(inputs[i].value);
                                }
                            }

                            this.disabled = false;

                            if(arList.length > 0) {

                                document.getElementById('postformPath').value = arList.join(", ");
                                document.getElementById('postform').submit();

                            } else {
                                alert('none selected');
                            }

                        }
                    }
                })
            ]
        });
        popup.show();
    }

    function getDirectory(id, path){
        var li = document.getElementById('list-' + id);
        var ul = li.getElementsByTagName("ul");
        if (ul.length <= 0){
            var checkbox = li.getElementsByTagName("input");
            var checked = checkbox[0].checked;
            BX.ajax({
                url: '/bitrix/tools/slam/ajax.php',
                data: {"PATH": path, "SECTION" : "Y"},
                method: 'POST',
                dataType: 'html',
                async: true,
                onsuccess: function(data){
                    li.innerHTML = document.getElementById('list-' + id).innerHTML + data;
                    var inputs = li.getElementsByTagName("input");
                    for (var i in inputs){
                        inputs[i].checked = checked;
                    }
                },
            });
        } else {
            ul = li.querySelector("ul");
            ul.style.display = (ul.style.display == 'none') ? '' : 'none'
        }
    }

    function setActive(obj, id){
        var li = document.getElementById('list-' + id);

        var inputs = li.querySelectorAll("input");
        for (var i in inputs){
            inputs[i].checked = obj.checked;
        }

        if(li.parentNode.parentNode.nodeName != 'DIV') {
            li.parentNode.parentNode.querySelector("input").checked = false;
        }
    }

</script>

<div id="treeSection" style="display: none">
    <?
    $dbTable->getTreeSection("root", $START_PATH);
    ?>
</div>

<form method="post" id="postform" action="javascript:void(0);">
    <input type="hidden" name="INDEX" value="Y">
    <textarea name="PATH" id="postformPath" style="display: none"></textarea>
</form>


<?
if (Loader::includeModule('ui')):
$spotlight = new \Bitrix\Main\UI\Spotlight("SLAM_IMAGE_INDEX");?>
<?if(!$spotlight->isViewed($USER->GetID())):?>
    <?\CJSCore::Init(["spotlight"]);?>
    <script type="text/javascript">
        BX.ready(
            function() {
                var elem = document.getElementsByClassName('adm-list-table-top');

                if(!elem[0] || !elem[0].nodeName || elem[0].nodeName !== 'DIV')
                    return;

                var target = null;

                for (var i = 0; i < elem[0].childNodes.length; i++)
                {
                    if(elem[0].childNodes[i].innerHTML === "<?=Loc::getMessage("SLAM_IMAGE_INDEX")?>")
                    {
                        target = elem[0].childNodes[i];
                        break;
                    }
                }

                if(target)
                {
                    var buttonRequestSpotlight = new BX.SpotLight({
                        targetElement: target,
                        lightMode: false,
                        targetVertex: "middle-center",
                        content:  "<?=Loc::getMessage("SLAM_IMAGE_INDEX_SL")?>",
                        id: "SLAM_IMAGE_INDEX",
                        autoSave: true
                    });

                    buttonRequestSpotlight.show();
                }
            });


    </script>
<?endif;?>
<?endif;?>


<?
$lAdmin->DisplayList();
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");