<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetPageProperty('NOT_SHOW_NAV_CHAIN', 'Y');
$APPLICATION->SetPageProperty('HIDE_H1', 'Y');
$APPLICATION->SetPageProperty('title', 'Страница не найдена – Not found | Le Tabouret');
$APPLICATION->SetPageProperty('robots', '');
\Realweb\Site\Seo::getInstance()->setMetaTitle('Страница не найдена – Not found | Le Tabouret');
\Realweb\Site\Seo::getInstance()->setMetaDescription('Страница не найдена – Not found | Le Tabouret')
?>

<section class="section-bg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
             <h1>Страница не найдена</h1>
                <div class="text-404">Извините, страница, которую вы искали, не найдена.</div>
                <div class="text-404">Вы можете перейти на <a href="/">главную страницу</a>  <br> или воспользоваться поиском:</div>
                <form action="/search/">
                <div class="input-group form search-404">
                    <input type="text" name="q" class="form-control" placeholder="Поиск по сайту" aria-label="Поиск по сайту" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-12 col-md-6">
                <img class="img-404" src="/local/templates/letabouret/images/404.png">
            </div>
        </div>
    </div>
</section>


<style>
    .text-404{
        font-size: 18px;
        font-weight: 600;
        margin-bottom: 30px;
    }
    .text-404 a{
        color: #8facbd;
    }
    .input-group.form.search-404 {
        max-width: 435px;
    }
    .img-404{
        width: 100%;
    }


    @media (max-width: 768px){
        .img-404{
            margin-top: 40px;
        }
    }
</style>
<?require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');?>
