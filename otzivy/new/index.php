<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("DESCRIPTION", "Вы можете оставить свой отзыв и поделиться впечатлением от сотрудничества с интерьерными салонами Le Tabouret. Мы будем рады и благодарны получить обратную связь от вас.");
$APPLICATION->SetPageProperty("TITLE", "Салон элитной мебели «Le Tabouret» - Оставить отзыв");
$APPLICATION->SetTitle("Оставить отзыв");
$APPLICATION->SetPageProperty('CONTENT_PAGE', 'Y');
$APPLICATION->SetPageProperty("robots", "noindex, nofollow");
if (isset($_POST["v_vopr"])) {
    $rows = file("iwix_faq.csv");
    //print_r($rows);
    foreach ($rows as $row) {
        $row = str_replace(array('&lt;', '&gt;', '&nbsp;', '&amp;', '&quot;'), array('<', '>', ' ', '&', '"'), $row);
        //print_r($row);
        $el = new CIBlockElement;
        $rowd = explode("~", $row);
        print_r($rowd);

        $PROP = array();
        $PROP[25] = $rowd[1];
        $PROP[26] = $rowd[7];
        $PROP[27] = $rowd[8];
        $PROP[28] = date("d.m.Y", strtotime($rowd[4]));
        $PROP[29] = $rowd[2];
        $PROP[30] = $rowd[3];

        $arLoadProductArray = array(
            "MODIFIED_BY" => $USER->GetID(),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => 4,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $rowd[1] . " " . date("d.m.Y", strtotime($rowd[4])),
            "ACTIVE" => "Y",
            "PREVIEW_TEXT" => "",
            "DETAIL_TEXT" => ""
        );

        $el->Add($arLoadProductArray);

    }
    $el = new CIBlockElement;

    $PROP = array();
    $PROP[25] = $_POST["v_name"];
    $PROP[26] = $_POST["v_email"];
    $PROP[27] = $_POST["v_tell"];
    $PROP[28] = date("d.m.Y H:i:s");
    $PROP[29] = $_POST["v_vopr"];
    $PROP[30] = "";

    $arLoadProductArray = array(
        "MODIFIED_BY" => $USER->GetID(),
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID" => 4,
        "PROPERTY_VALUES" => $PROP,
        "NAME" => $_POST["v_name"] . " " . date("d.m.Y H:i:s"),
        "ACTIVE" => "N",
        "PREVIEW_TEXT" => "",
        "DETAIL_TEXT" => ""
    );

    $iIdReview = $el->Add($arLoadProductArray);
}

?>
    <div class="row">
        <div class="col-lg-6">
            <form action="/otzivy/new/" method="post" name="GoForm">
                <div class="form-group">
                    <input type="email" placeholder="E-mail" class="form-control" maxLength="150" name="v_email"
                           value=""/>
                </div>
                <div class="form-group">
                    <input type="text" placeholder="Имя" class="form-control" maxLength="100" name="v_name" value=""/>
                </div>
                <div class="form-group">
                    <input type="tel" placeholder="Телефон" class="form-control" maxLength="250" name="v_tell"
                           value=""/>
                </div>
                <div class="form-group">
                    <textarea placeholder="Отзыв" name="v_vopr" rows="4" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Опубликовать" name="submit" class="basket__button"/>
                </div>
                <?php if (!empty($iIdReview)): ?>
                    <div class="alert alert-success">Отзыв успешно добавлен</div>
                <?php endif; ?>
                <div class="form-group">
                    <div class="small">Укажите E-mail и телефон, чтобы мы могли связаться с Вами</div>
                </div>
            </form>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>