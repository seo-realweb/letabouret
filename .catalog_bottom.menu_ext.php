<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Realweb\Site\Site;

global $APPLICATION;
$aMenuLinksExt = Site::getMenu(IBLOCK_CATALOG_CATALOG ,1, 'bottom');

$aMenuLinks = array(
    array(
        "Гостиная",
        "/pomeshcheniya/gostinaya-pom/",
        array(),
        array(),
        ""
    ),
    array(
        "Спальня",
        "/pomeshcheniya/spalnya/",
        array(),
        array(),
        ""
    ),
    array(
        "Детская",
        "/pomeshcheniya/detskie/",
        array(),
        array(),
        ""
    ),
    array(
        "Столовая и кухня",
        "/pomeshcheniya/stolovaya-i-kukhnya/",
        array(),
        array(),
        ""
    ),
    array(
        "Домашний кабинет",
        "/pomeshcheniya/domashshchniy-kabinet/",
        array(),
        array(),
        ""
    ),
    array(
        "Прихожая",
        "/pomeshcheniya/prikhozhaya/",
        array(),
        array(),
        ""
    ),
    array(
        "Ванная комната",
        "/pomeshcheniya/vannaya/",
        array(),
        array(),
        ""
    )
);

$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);
