var Accordeon = {
setting: {
    selector:{
        accordeon_item: '.accordeon-item',
        accordeon_header: '.accordeon-item-top',
        accordeon_content: '.accordeon-item-content',
        open_accordeon_class: 'accordeon-open'
      }
},
init: function(Openfirst, JustOneOpen){
    var obj = this;
    var accordeonItem = obj.setting.selector.accordeon_item;
    var accordeonItemHeader = obj.setting.selector.accordeon_header;
    var accordeonItemContent = obj.setting.selector.accordeon_content;
    var openClass = obj.setting.selector.open_accordeon_class;
    
    $(accordeonItemHeader).addClass('accordeon-item-add-style');
    $(accordeonItemContent).append('<div class="accordeon-item-content-end"><div class="accordeon-item-top arrow-contol" ></div></div>');
    $(accordeonItemContent).wrapInner('<div class="acc-cont-inner" style="overflow:hidden;text-align:justify"></div>');
    
  if (Openfirst) {
        $(accordeonItemContent).not(':first').hide();
        $(accordeonItemHeader+':first').addClass(openClass);
        $(accordeonItemHeader).not(':first').removeClass(openClass);
    } else {
      $(accordeonItemContent).hide();
      $(accordeonItemHeader).removeClass(openClass);
    }
    $(accordeonItemHeader).click(function(){
        if (JustOneOpen) {
           current = $(this).closest(accordeonItem).find(accordeonItemContent);
           $(accordeonItemContent).not(current).slideUp('slow');
           $(accordeonItemHeader).not(this).removeClass(openClass);
           obj.accordeonToogle(this);
    } else {
        obj.accordeonToogle(this);
    }
    });
},
      
accordeonToogle: function (el){
  var obj = this;
  var accordeonItem = obj.setting.selector.accordeon_item;
  var accordeonItemContent = obj.setting.selector.accordeon_content;
  var openClass = obj.setting.selector.open_accordeon_class;
  var closestAccordeonItemContent = $(el).closest(accordeonItem).find(accordeonItemContent);
  var itemContent = closestAccordeonItemContent.text();
  if ($.trim(itemContent) !== '') {
        closestAccordeonItemContent.slideToggle('fast');
        $(el).toggleClass(openClass);
    }
  }
}; 