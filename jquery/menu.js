/* 
Simple JQuery menu.
HTML structure to use:

Notes: 

Each menu MUST have a class 'menu' set. If the menu doesn't have this, the JS won't make it dynamic
If you want a panel to be expanded at page load, give the containing LI element the classname 'expand'.
Use this to set the right state in your page (generation) code.

Optional extra classnames for the UL element that holds an accordion:

noaccordion : no accordion functionality
collapsible : menu works like an accordion but can be fully collapsed

<ul class="menu [optional class] [optional class]">
<li><a href="#">Sub menu heading</a>
<ul>
<li><a href="http://site.com/">Link</a></li>
<li><a href="http://site.com/">Link</a></li>
<li><a href="http://site.com/">Link</a></li>
...
...
</ul>
// This item is open at page load time
<li class="expand"><a href="#">Sub menu heading</a>
<ul>
<li><a href="http://site.com/">Link</a></li>
<li><a href="http://site.com/">Link</a></li>
<li><a href="http://site.com/">Link</a></li>
...
...
</ul>
...
...
</ul>

Copyright 2007-2010 by Marco van Hylckama Vlieg

web: http://www.i-marco.nl/weblog/
email: marco@i-marco.nl

Free to use any way you like.
*/

var s = 0;
var b = 0;
var r = 0;
var z = 0;
jQuery.fn.initMenu = function () {

	return this.each(function () {
		var theMenu = $(this).get(0);
		$('.acitem', this).css('display', 'none');
		//$('.acitem', this).css('height','0px');
		$('.acitem', this).css('overflow', 'hidden');
		$('.kor_pad > ul > li > ul').hide();
		//$('li.expand > .acitem', this).show();
		//$('li.expand > .acitem', this).prev().addClass('active');
		$('.kor_pad > ul > li > .cur').next().css('height', '0px');

		$('.kor_pad > ul > li > .cur').next().addClass('act_bbb');

		$('.act_bbb > li').each(function (i, ee) {
			s = s + 26;
			console.log(s);
		});

		if (b == 0) {
			b = 1;
			$(".act_bbb").animate({
				height: s
			}, 700, function () {
				// Animation complete.
			});
		}
		$(window).load(function (eee) {


			eee.stopImmediatePropagation();
		});



		$('.kor_pad > ul > li > a').each(function (i, ee) {
			if ($(this).hasClass("cur")) {
				if (s == 1) {
					s = 1;
					//e.stopImmediatePropagation();
					var theElement = $(this).next();
					var parent = this.parentNode.parentNode;
					if ($(parent).hasClass('noaccordion')) {
						if (theElement[0] === undefined) {
							window.location.href = this.href;
						}
						$(theElement).slideToggle('normal', function () {
							if ($(this).is(':visible')) {
								$(this).prev().addClass('active');
							} else {
								$(this).prev().removeClass('active');
							}
						});
						return false;
					} else {
						if (theElement.hasClass('acitem') && theElement.is(':visible')) {
							if ($(parent).hasClass('collapsible')) {
								$('.acitem:visible', parent).first().slideUp('normal',
									function () {
										$(this).prev().removeClass('active');
									}
								);
								return false;
							}
							return false;
						}
						if (theElement.hasClass('acitem') && !theElement.is(':visible')) {
							$('.acitem:visible', parent).first().slideUp('normal', function () {
								$(this).prev().removeClass('active');
							});
							theElement.slideDown('normal', function () {
								$(this).prev().addClass('active');
							});
							return false;
						}
					}
				}
			}

		});


		/*$('li a', this).click(
			function (e) {

				if (r == 0) {
					r = 1;
					$(".act_bbb").animate({
						height: 0
					}, 700, function () {
						document.location.href = $(this).attr('href');
					});
				}


				//			window.onload = function(){

				//   }
			}

		);*/




	});
};

$(document).ready(function () {
	$('.lmenu').initMenu();



	/*
	$('.kor_pad ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace("#", "http://www.letabouret.ru/");
		$(this).attr('href', str);
	});



	$('.kor_pad ul .spalni ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".spalni.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/spalni/");
		$(this).attr('href', str);
	});

	$('.kor_pad ul .gostinaya ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".gostinaya.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/gostinaya/");
		$(this).attr('href', str);
	});

	$('.kor_pad ul .morbidi ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".morbidi.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/morbidi/");
		$(this).attr('href', str);
	});

	$('.kor_pad ul .prichozhie ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".prichozhie.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/prichozhie/");
		$(this).attr('href', str);
	});

	$('.kor_pad ul .stolovaya ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".stolovaya.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/stolovaya/");
		$(this).attr('href', str);
	});

	$('.kor_pad ul .biblioteki ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".biblioteki.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/biblioteki/");
		$(this).attr('href', str);
	});

	$('.kor_pad ul .detskaya ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".detskaya.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/detskaya/");
		$(this).attr('href', str);
	});

	$('.kor_pad ul .vanay ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".vanay.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/vanay/");
		$(this).attr('href', str);
	});
	$('.kor_pad ul .pletenayamebel ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".pletenayamebel.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/pletenayamebel/");
		$(this).attr('href', str);
	});
	$('.kor_pad ul .Luce ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".Luce.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/Luce/");
		$(this).attr('href', str);
	});
	$('.kor_pad ul .aksessuaryi ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".aksessuaryi.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/aksessuaryi/");
		$(this).attr('href', str);
	});
	$('.kor_pad ul .matrasyi ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".matrasyi.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/matrasyi/");
		$(this).attr('href', str);
	});

	$('.kor_pad ul .other ul li a').each(function (i, elem) {
		str = $(this).attr('href');
		str = str.replace(".other.", ".");
		$(this).attr('href', str);

		str = $(this).attr('href');
		str = str.replace(".ru/", ".ru/other/");
		$(this).attr('href', str);
	});


	$('.kor_pad ul li a').each(function (i, elem) {

		str = $(this).attr('href');
		str = str.replace(".ru/z", ".ru/producers/");
		$(this).attr('href', str);
	});
	* */


	//	$('link[rel="canonical"]').each(function(i,elem) {
	//	
	//		console.log($(this).attr('href'));
	//		str = $(this).attr('href');
	//		str = str.replace("http://www.letabouret.ru/letabouret.ru/producers/", "http://www.letabouret.ru/producers/artishok/");
	//		console.log(str);
	//		$(this).attr('href',str);

	//	});                                 









});


//Онлайн консультант
var stek = 0,
	smax = 0,
	b_type = 0;
//получить отступ слева
function get_left_pozition_block(){
	return parseInt($('body').css('width'),0)-parseInt($('#online_c').css('width'),0);
}

//получить отступ сверху
function get_top_pozition_block(){
	return parseInt($('body').css('height'),0)-parseInt($('#online_c').css('height'),0);
}

//установка блока в зависимости от скрола
function set_position_other_scroll(){
	stek = $(window).scrollTop();
	smax = $(document).height()-$(window).height();
	perc = stek*96/smax;
	
	//alert($("#online_c").css('bottom'));
	if(perc<80 && (b_type == 0)){
		$("#online_c").css("top",perc+'%'); 
	}
	if(perc<40 && (b_type == 1)){
		$("#online_c").css("top",perc+'%'); 
	}
}



function animation_open_step_1(){
	b_type = 1;
	$("#online_c").animate({
		width: 300,
		left: get_left_pozition_block()-265
	}, 600, function() {
		animation_open_step_2();
	});

	$("#o_txt").animate({
		opacity: 0
	}, 600, function() {
	});
	$("#oc_icon").animate({
		opacity: 0
	}, 600, function() {
	});
}

function animation_open_step_2(){
	$("#oc_icon").css('display','none');
	$("#o_txt").css('z-index','-100');
	$("#close_o").css('display','block');
	$("#sendm").css('display','block');
	//set_position_other_scroll()
	$("#online_c").animate({
		height: 400,
		top: 170
		//bottom: 395+smax-(smax-stek)
	}, 600, function() {
	});
	$("#mess").animate({
		opacity: 1
	}, 600, function() {
	});
	$("#sendm").animate({
		opacity: 1
	}, 600, function() {
	});
	$("#close_o").animate({
		opacity: 1
	}, 600, function() {
	});
	$("#tthx").animate({
		opacity: 1
	}, 600, function() {
	});
	$(".u_email").animate({
		opacity: 1
	}, 600, function() {
	});
}



function animation_close_step_1(){
	$("#oc_icon").css('display','block');
	$("#o_txt").css('z-index','10000000');
	$("#close_o").css('display','none');
	$("#sendm").css('display','none');
	//set_position_other_scroll()
	$("#online_c").animate({
		height: 250,
		top:300
	}, 600, function() {
		animation_close_step_2();
	});
	$("#mess").animate({
		opacity: 0
	}, 600, function() {
	});
	$("#sendm").animate({
		opacity: 0
	}, 600, function() {
	});
	$("#close_o").animate({
		opacity: 0
	}, 600, function() {
	});	
	$(".u_email").animate({
		opacity: 0
	}, 600, function() {
	});
	$("#tthx").animate({
		opacity: 0
	}, 600, function() {
	});
}

function animation_close_step_2(){
	b_type = 0;
	$("#online_c").animate({
		width: 40,
		left: get_left_pozition_block()+265
	}, 600, function() {
	});

	$("#o_txt").animate({
		opacity: 1
	}, 600, function() {
	});
	$("#oc_icon").animate({
		opacity: 1
	}, 600, function() {
	});
}
 
$(window).resize(function(){

});

$(document).ready(function() {

	$("#online_c").css("left",(parseInt($('body').css('width'),0)-parseInt($('#online_c').css('width'),0))+"px");
	$("#online_c").css("top",'45%');
	//установка блока в зависимости от скрола
	//$(document).scroll(function(){ 
	//	set_position_other_scroll();
	//}).scroll();
	
	
	$("#o_txt").click(function(){
		animation_open_step_1();
	});

	$("#close_o").click(function() {
		animation_close_step_1();
	});
	
	
	$("#online_c").css('left',get_left_pozition_block()+40+'px');

	function showc(){
		$("#online_c").animate({
			left: get_left_pozition_block()
		}, 600, function() {
		});
	}
setTimeout(showc,3000);

	
	
});




